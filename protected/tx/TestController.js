/**
 * Controller for testing input orders
 */

Ext.define('Crm.tx.TestController', {
    extend: "Crm.REST.RestBaseController"
    
    ,constructor: function() {
        this.callParent(arguments);
        this.params = this.req.body.reqData;
        this.txModel = Ext.create('Crm.tx.TxDataModel', {scope: this});
        //this.txModel = Ext.create('Crm.modules.orderTransaction.model.OrderTransactionModel', {scope: this});
    }
    /*
    ,order: function() {
        var me = this;
        
        //console.log(this.user)
        //console.log(this.params)
        me.txModel.write(this.params, function(d) {
            me.sendData(d);
        }, {add: true}) 
       
    }
    */
    ,order: function() {
        var me = this;
        
        this.txModel.processOrder(this.params, function(e,d) {
            me.sendData(d);
        }) 
        
    }
    
    ,i: 0
    ,cnt: 10
    
    ,$makeOrders: function() {
        var me = this;
        setTimeout(function() {
            me.sendOneOrder(function(e, d) {
                if(e) {
                    me.sendData({error: e});
                    return;
                }
                me.i++;
                console.log(me.i);
                if(me.i == me.cnt)
                    me.sendData({fin: me.i});
                else
                    me.$makeOrders();
            })
        }, 500)
    }
    
    ,sendOneOrder(cb) {
        var me = this;
        
        var userId = "9651657821";
        
        var partners = [
            "a9a476a7-5a5c-4653-bed5-5ed970e9b463",
            "434c7a31-ea7c-4f6e-9b30-bc536cb34e72"            
        ]
        
        var articules = [
            "4601728012380",
            "3259352243000",
            "111111",
            "222222"
        ]
        
        var opt = {
            url: 'http://localhost:8005/test/order',
            method: "POST", 
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic NG9UQXRlY09icE83N1g4ZTo3YjgxbG9PWGlpeG5vT1RlcmlITDJJeG1hWUpvZmc='},            
            json: {
                "header":{
                    "version":"0.1",
                    "txName":"login",
                    "lang":"EN"
                },
                "reqData":{
                  "partner_id": partners[this.getRandomInt(0, partners.length)],
                  "user_id": userId,
                  "shop_receipt_id": new Date().getTime(),
                  "order_create_timestamp": new Date().getTime(),
                  "items": [ ] 
                }
            }
        }
        
        for(var i = 0;i < 2;i++) {
            opt.json.reqData.items.push({
               "price": this.getRandomInt(10, 100),
               "quantity": this.getRandomInt(1,10),
               "item_name":  articules[this.getRandomInt(0, articules.length)]
            })
        }
        require('request')(
            opt
            ,function(e,res) {            
                cb(e,res)                
            }
        )
    }
    
    ,getRandomInt: function(min, max) {
      return Math.floor(Math.random() * (max - min)) + min;
    }
        
    ,$replaceDates: function() {
        var me = this;
        [
            function(next) {
                this.src.db.query("select tablename from pg_tables where (tablename !~'^pg' or tablename ~ '^pgd_') and schemaname='public'", [], function(e,res) {
                    next(res)
                })
            }
            ,function(tables, next) {               
                tables.prepEach(
                    function(table, nxt) {
                        me.prenOneTable(table.tablename, function() {
                            nxt()
                        })
                    },
                    function() {
                        console.log('fin')
                    }
                )
            }
        ].runEach(this)
        

        
    }
    
    ,prenOneTable: function(table,cb) {

        [
            function(next) {
                
                var sql = [
                    "SELECT a.attname,pg_catalog.format_type(a.atttypid, a.atttypmod) ",
                    "FROM pg_catalog.pg_attribute a ",
                    "WHERE a.attrelid = ( ",
                    "SELECT c.oid FROM pg_catalog.pg_class c ",
                    "LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace ",
                    "WHERE pg_catalog.pg_table_is_visible(c.oid) AND c.relname ~ '^"+table+"$' ",
                    ")", 
                    "AND a.attnum > 0 AND NOT a.attisdropped ",
                    "ORDER BY a.attnum;"
                ].join('\n');
                
                this.src.db.query(sql, [], function(e, fields) {                 
                    next(fields)
                })
            }
            ,function(fields, next) {               
                var me = this;
                fields.prepEach(
                    function(field, nxt) {
                        if(field.format_type == 'date')
                            me.prepOneField(table,field.attname, function() {
                                nxt()
                            })
                        else
                            nxt()
                    },
                    function() {                      
                        cb()
                    }
                )
            }
            
        ].runEach(this)
    }
    
    ,prepOneField: function(table, field, cb) {
        [
            function(next) {
                this.src.db.query("ALTER TABLE " +table+ " ADD _delme timestamp", [], function(e){
                    if(e) return console.log('e1:', e)
                    next()})
            },
            function(next) {
                this.src.db.query("UPDATE " +table+ " SET _delme = " + field, [], function(e){
                    if(e) return console.log('e2:', e)
                    next()
                })
            },
            function(next) {
                this.src.db.query("ALTER TABLE " +table+ " DROP " + field, [], function(e){
                    if(e) return console.log('e3:', e)
                    next()})
            },
            function(next) {
                this.src.db.query("ALTER TABLE " +table+ " ADD " + field + " timestamp", [], function(e){
                    if(e) return console.log('e4:', e)
                    next()})
            },
            function(next) {
                this.src.db.query("UPDATE " +table+ " SET " + field + " = _delme", [], function(e){
                    if(e) return console.log('e5:', e)
                    next()
                })
            },
            function(next) {
                this.src.db.query("ALTER TABLE " +table+ " DROP _delme", [], function(e){
                    if(e) return console.log('e6:', e);
                    
                    console.log(table,'.',field)
                    cb()
                })
            }
        ].runEach(this)
    }
    
})