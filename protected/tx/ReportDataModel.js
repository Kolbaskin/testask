const Cursor = require('pg-cursor');

Ext.define('Crm.tx.ReportDataModel',  {
    extend: "Core.AbstractModel"
    
    ,mixins: [
        'Crm.tx.reports.Brand',
        'Crm.tx.reports.Partner'
    ]
    
    ,$test: function() {
        this.makeReports()
    }
    
    ,makeReports: function(cb) {
        var me = this;
        this.currentReportName = Ext.Date.format(new Date(), 'Y-m-d H:i');        
        [
            function(next) {
                this.src.db.collection(_TABPREFIX + 'foundations')
                .find({active: 1}, {_id: 1, orgtype: 1, name: 1})
                .toArray(function(e,o) {
                    next(o)
                })
            },
            function(organizations, next) {
                organizations.prepEach(
                    function(org, nxt) {
                        me.generateReportsForOrg(org, nxt)
                    },
                    function() {
                        next()
                    }
                )
            },
            function() {
                console.log('FIN REPORT GENERATOR')
                if(!!cb) cb()
            }
        ].runEach(this)
        
    }
    
    ,generateReportsForOrg: function(org, cb) {
        switch(org.orgtype) {
            case 1: this.reportsForCharity(org, cb);break;
            case 2: this.reportsForBrand(org, cb);break; //reportsForPartner(org, cb);break;
            case 3: this.reportsForBrand(org, cb);break;
            default: cb();
        }
    }
    
    ,reportsForCharity: function(org, cb) {
        cb();
    }
    
    ,getRecords: function(sql, prepareRecord, cb) {
        var me = this;
        var cursor;// = this.src.db.conn.query(new Cursor(sql));

        this.src.db.query(sql,[],function(e,rows) {
            rows.prepEach(

                prepareRecord,

                function() {cb(true)}

            ) 
        })
        
        /*var f = function(cb) {            
            cursor.read(10, function (err, rows) {
                if(rows.length) {
                    rows.prepEach(
                        prepareRecord,
                        function() {f(cb)}
                    )                    
                    //me.prepareRows(rows, f)
                } else {  
                   // me.finalize(function() {
                        cursor.close(function() {
                            cb(true)
                            //client.release()
                        })
                    //})
                }
            })
        }   
        
        this.src.dbx.getConnection(function(db) {            
            cursor = db.conn.query(new Cursor(sql));
            f(function() {
                me.src.dbx.free(db)
                cb(true)
            });
        })
        */
            
    }
    
});
