Ext.define('Crm.tx.TxDataModel',  {
    extend: "Core.AbstractModel"

    
    ,processOrder: function(data, cb) {     
        [
            function(next) {               
                this.getPartner(data.partner_id, next)
            }            
            ,
            function(partner, next) {
               

               
                
                if(!partner) {
                    cb({code: 1, message: 'Partner not found'}, null);
                    return;
                }
                this.getUser(data.user_id, function(user) {
                    next(partner, user)
                })
            }
            ,function(partner, user, next) {
                if(!user) {
                    cb({code: 2, message: 'User not valid'}, null);
                    return;
                }
                data.user = user;
                this.calculatePoints(partner, user, data.items, next)
                
            }
            ,function(points, next) {  
                var me = this;  
      
                if(points)
                    me.saveOrderAndUserPoints(data, points, next);
                else
                    cb({code: 3, message: 'There isn\t points for this order'}, null)
            }
            
            ,function() {
                cb(null, data)
            }
        ].runEach(this)       
    }
    
    ,getPartner: function(partnerId, cb) {
        this.src.db.collection(_TABPREFIX + 'foundations')
        .findOne({_id: partnerId, active: 1}, {_id: 1, name: 1, orgtype: 1}, function(e,d) {
            if(d)
                cb(d)
            else
                cb(null)
        });
    }
    
    ,getUser: function(userId, cb) {
        var model = Ext.create('Crm.modules.accountHolders.model.AccountHoldersModel', {scope: this});
        
        [
            function(next) {
                this.src.db.collection('users').findOne({
                    $or: [
                        {mobile: userId}, 
                        {username: userId},
                        {cards___: '{"_arr":[{"card": "' + userId + '"}]}'}
                    ]
                },{_id:1, mobile:1, username:1, full_name:1, email:1}, function(e,d) {               
                    if(d) {                        
                        cb(d)
                    } else
                        next()
                })
            },
            function() {               
                model.write({
                    mobile: userId,
                    username: userId,
                    sendpassword: 1,
                    wallet_balance: 0,
                    user_type: 'U'
                }, function(res) {                    
                    if(res && res.success) {
                        cb(res.record)
                    } else {
                        cb(null)
                    }                    
                }, {add: true})
            }
        ].runEach(this);
    }
    
    ,calculatePoints: function(partner, user, items, cb) {
        [
            function(next) {
                this.getRules(partner, items, next)
            }
            ,function(rules, next) {
                if(!rules) {
                    cb(null);
                    return;
                }
                cb(this.calculatePointsByRules(items, rules))
            }
        ].runEach(this);
    }
    
    ,getRules: function(partner, items, cb) {

        var rulesTab =  _TABPREFIX + 'rules'
            ,skuTab = _TABPREFIX + 'rulessku'
            ,orgTab = _TABPREFIX + 'foundations'
            ,skus = []
            ,sql
            ,vals;
   
        items.forEach(function(i) {if(/^[a-z0-9\-]{1,20}$/.test(i.item_name)) skus.push(i.item_name)})
        
        sql = 'SELECT ts.*, tr.pid as partnerId FROM ' + rulesTab + ' tr, ' + skuTab + ' ts WHERE tr.active=1 and tr.removed != 1 and tr._id = ts.pid and ((ts.sku in (\''+skus.join("','")+'\') and (tr.pid in (SELECT _id FROM ' + orgTab + ' WHERE orgtype = 3)) or (tr.pid = $1 and ts.ruletype = \'A\')))';
        
        vals = [  partner._id];

        this.src.db.query(sql, vals, function(e, r) {

            cb(r)
        })        
    }
    
    ,calculatePointsByRules: function(items, rules) {
        var me = this, points = [], amount = 0; 

        items.forEach(function(item) {
            item.price = parseFloat(item.price);
            item.quantity = parseFloat(item.quantity);
            if(isNaN(item.price) || isNaN(item.quantity)) return;
            amount += item.price * item.quantity;

            rules.forEach(function(rule) {
            
                if(
                    rule.ruletype == 'P' &&
                    rule.sku == item.item_name &&
                    (!rule.rangeto || item.quantity <= rule.rangeto) &&
                    (!rule.rangefrom || item.quantity >= rule.rangefrom)
                ) {                                  
                    points.push({
                        partnerid: rule.partnerid,
                        sku: rule.sku,
                        points: me.calculatePointsByRule(item.price, item.quantity, rule)
                    })
                }
            })            
        });    


        rules.forEach(function(rule) {
            if(
                rule.ruletype == 'A' &&                    
                (!rule.rangeto || amount <= rule.rangeto) &&
                (!rule.rangefrom || amount >= rule.rangefrom)
            ) {     
            
                points.push({
                    partnerid: rule.partnerid,
                    sku: '',
                    points: me.calculatePointsByRule(amount, 1, rule)
                })
            }
        }) 

        return points;
    }
    
    ,calculatePointsByRule: function(price, quantity, rule) {
        if(rule.pointype == 'A') return rule.amount;
        if(rule.pointype == 'E') return rule.amount * quantity;
        return price * quantity * rule.amount / 100;
    }
    
    ,saveOrderAndUserPoints: function(order, points, cb) {       
        order.donation_amount = 0;

        points.forEach(function(p) {order.donation_amount += p.points});
        var me = this, err = function(e, rollb, n) {
            if(rollb)
                me.src.db.rollback(function() {
                    cb(null);
                });
            else
                cb(null)
        };
        [
            function(next) {
                this.src.db.begin(next)
            }
            ,function(e,d, next) {
                if(e) {
                    err(e, false, 1);
                    return;
                }                
                this.src.db.query("UPDATE users SET wallet_balance = wallet_balance + " + order.donation_amount + ", full_balance = full_balance + " + order.donation_amount + " WHERE _id = $1", [order.user._id], next);
            }
            ,function(e,d, next) {
                var me = this;
                if(e) {
                    err(e, true, 2);                    
                    return;
                } 

                points.prepEach(
                    function(item, nxt) {
                        me.savePoints(order, item, function(e,d) {
                            if(e) {
                                err(e, true, 3);                    
                                return;
                            }
                            nxt(item);
                        })                        
                    },
                    function() {
                        next();
                    }
                )
            }
            ,function(next) {                               
                this.src.db.commit(next);
            }
            ,function(e,d) {
                if(e) {
                    err(e, true, 4);                    
                    return;
                }
                cb(order);
            }
        ].runEach(this);
    }
    
    ,savePoints: function(order, item, cb) {
        [
            function(next) {
                this.src.db.collection("user_points").insert({
                    orderid: order.shop_receipt_id, 
                    userid: order.user._id,
                    shopid: order.partner_id,
                    partnerid: item.partnerid,
                    points: item.points,
                    ctime: new Date(),
                    sku: item.sku
                },function(e,d) {
                    if(e) 
                        cb(e);                
                        
                    else
                        next();
                })
            },
            function() {
                this.src.db.query("UPDATE " + _TABPREFIX + "foundations SET total2 = total2 + " + item.points + " WHERE _id = $1", [item.partnerid], function(e, d) {
                    cb(e,d)
                })
            }
        ].runEach(this)
    }
})
