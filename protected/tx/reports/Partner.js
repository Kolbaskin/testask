Ext.define('Crm.tx.reports.Partner',  {

    reportsForPartner: function(org, cb) {
        var me = this
            ,reports = {}
            ,reportsModel = Ext.create('Crm.modules.reports.model.ReportsModel', {scope: this});
        
        [
            function(next) {
                reportsModel.write({
                    name: this.currentReportName,
                    report_type: 'brand_donates',
                    pid: org._id
                }, function(res) {
                    if(res.success) {
                        reports.brand_donates = res.record._id;
                        next()
                    } else {
                        cb()
                    }
                }, {add: true})
            },

            function() {
                this.getRecords(
                    "SELECT * FROM charity_points WHERE partnerid = '"+org._id+"' and reported_partner is NULL", 
                    function(point, next) {
                        me.pointToPartnerReports(point, org, reports, next);
                    },
                    cb
                )
            }
        ].runEach(this);
    }
    
    ,pointToPartnerReports: function(point, org, reports, cb) {
        var me = this, insData = [], err = function(err, rollback) {            
            
            console.log('err:', err)
            
            if(rollback)
                me.src.db.rollback(function() {
                    cb(err)
                })
            else
                cb(err)
        };
        [
            function(next) {
            
                this.src.db.begin(function(e,d) {
                    if(e)
                        err(e, false)
                    else
                        next()
                })
            },
            function(next) {
                this.savePaymentData(point, reports.brand_donates, function(e) {
                    if(e)
                        err(e, true)
                    else
                        next();
                })
            },
            
            function(next) {
                this.src.db.collection('charity_points').update({_id:point._id}, {$set:{reported_partner:'1'}}, function(e,d) {
                    if(e)
                        err(e, true)
                    else
                        next();
                })
            },
            function() {
                this.src.db.commit(function(e) {
                    if(e)
                        err(e, true)
                    else
                        cb()
                })
            }
        ].runEach(this)
    }
    

})
