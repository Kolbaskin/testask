CREATE TABLE report_payment_data
(
  _id varchar(36) NOT NULL DEFAULT uuid_generate_v4(),
  pid varchar(36),
  catalogueid varchar(36),
  planid varchar(36),
  ngo_name varchar(100),
  amount real,
  tx integer,
  PRIMARY KEY (_id),
  CONSTRAINT unique_record UNIQUE (pid, catalogueid, planid)
);

CREATE TABLE report_partner_data
(
  _id varchar(36) NOT NULL DEFAULT uuid_generate_v4(),
  pid varchar(36),
  partnerid varchar(36),  
  amount real,
  tx integer,
  PRIMARY KEY (_id),
  CONSTRAINT unique_record_partner_data UNIQUE (pid, partnerid)
);

ALTER TABLE charity_points ADD reported_brand char;
ALTER TABLE charity_points ADD reported_partner char;
ALTER TABLE charity_points ADD reported_charity char;

ALTER TABLE user_points ADD shopid varchar(36);
ALTER TABLE charity_points ADD shopid varchar(36);

ALTER TABLE charity_plan ADD parent_plan varchar(36);


CREATE TABLE report_payments_cache
(
    _id varchar(36) NOT NULL DEFAULT uuid_generate_v4(),
    pid varchar(36),
    ctime date,
    ngo_id varchar(36),
    amount real,
    tx integer,
    emergency_donate real, 
    active_donate real, 
    user_donate real,
    status integer
    PRIMARY KEY (_id)
  
);