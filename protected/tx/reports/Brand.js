Ext.define('Crm.tx.reports.Brand',  {

    reportsForBrand: function(org, cb) {
        var me = this
            ,reports = {}
            ,reportsModel = Ext.create('Crm.modules.reports.model.ReportsModel', {scope: this});
            
        var err = function(err, rollback) {            
            
            console.log('err:', err)
            
            if(rollback)
                me.src.db.rollback(function() {
                    cb(err)
                })
            else
                cb(err)
        };
        
        [
            function(next) {
            
            
                this.src.db.begin(function(e,d) {
                    if(e)
                        err(e, false)
                    else
                        next()
                })
            },
            function(next) {
                reportsModel.write({
                    name: this.currentReportName,
                    report_type: 'brand_donates',
                    pid: org._id
                }, function(res) {
                    if(res.success) {
                        reports.brand_donates = res.record._id;
                        next()
                    } else {
                        cb()
                    }
                }, {add: true})
            },
            
            function(next) {
                reportsModel.write({
                    name: this.currentReportName,
                    report_type: 'brand_partners',
                    pid: org._id
                }, function(res) {
                    if(res.success) {
                        reports.brand_partners = res.record._id;
                        next()
                    } else {
                        cb()
                    }
                }, {add: true})
            },

            function(next) {
       
                this.getRecords(
                    "SELECT * FROM charity_points WHERE partnerid = '"+org._id+"' and "+(org.orgtype+'' == '2'? 'reported_partner':'reported_brand')+" is NULL", 
                    function(point, next) {
   

                        me.pointToBranReports(point, org, reports, function(e) {
                        
                    
                        
                            if(e)
                                err(e, true)
                            else
                                next()
                        });
                    },
                    next
                )
            },
            
            function(res, next) {
           
                this.reportPayment({
                    _id: reports.brand_donates,
                    ctime: new Date(),
                }, org, next)
            },
            
            function(data, next) {
                this.src.db.collection('report_payments_cache').insert(data,  function(e) {
                    if(e)
                        err(e, true)
                    else
                        next()
                })
            },
            function() {
                this.src.db.commit(function(e) {
                    if(e)
                        err(e, true)
                    else
                        cb()
                })
            }
            
        ].runEach(this);
    }
    
    ,reportPayment: function(report, org, cb) {
        [
            function(next) {
                this.src.db.query([
                    'SELECT f._id, d.planid, sum(d.tx) as tx, sum(d.amount) as amount FROM report_payment_data d, pgd_catalogue c, pgd_foundations f',
                    'WHERE d.pid = $1 and d.catalogueid = c._id and c.pid = f._id',
                    'GROUP BY f._id,d.planid'                    
                ].join(' '),[report._id], function(e,r) {
                    next(r)
                })
            },
            function(data, next) {
                if(!data) {
                    cb([])
                    return;
                }
                this.src.db.collection('charity_plan').find({partner_user_id: org._id},{_id:1,plan_type:1})
                .toArray(function(e,d) {  
                    var out = {}
                    d.forEach(function(d) {out[d._id] = d.plan_type})
                    next(data, out)
                })
            },
            function(data, myPlansIds, next) {
                var out = {};
               
                data.forEach(function(d) {
                    if(!out[d._id]) {
                        out[d._id] = {
                            pid: report._id,
                            ctime: report.ctime,
                            ngo_id: d._id,
                            amount: 0,
                            tx: 0,
                            emergency_donate: 0, 
                            active_donate: 0, 
                            user_donate: 0
                        };                        
                    }
                    out[d._id].amount += d.amount;                 
                    out[d._id].tx += parseInt(d.tx);
                   
                    if(d.planid) {                                         
                        if(myPlansIds[d.planid] && d.amount) {                        
                            out[d._id][myPlansIds[d.planid] == 'E'? 'emergency_donate':'active_donate'] += d.amount;
                        }                        
                    } else
                    if(d.amount) {
                        out[d._id].user_donate += d.amount;
                    }                    
                })
                next(out)
            },
            function(resObject) {
                var out = [];
                for(var i in resObject) out.push(resObject[i]);
                cb(out);
            }            
        ].runEach(this);     
    }
    
    
    ,pointToBranReports: function(point, org, reports, cb) {
        var me = this, insData = [], err = function(err, rollback) {            
             cb(err)
        };
        [
            
            function(next) {
                this.savePaymentData(point, reports.brand_donates, function(e) {
                    if(e)
                        err(e, true)
                    else
                        next();
                })
            },
            function(next) {

                this.savePartnersData(point, reports.brand_partners, function(e) {
                    if(e)
                        err(e, true)
                    else
                        next();
                })
            },

            function(next) {
                var set = {}
                set[org.orgtype+'' == '2'? 'reported_partner':'reported_brand'] = 1;
                this.src.db.collection('charity_points').update({_id:point._id}, {$set:set}, function(e,d) {
                    if(e)
                        err(e, true)
                    else
                        cb();
                })
            }
        ].runEach(this)
    }
    
    ,savePaymentData: function(point, reportId, cb) {
        this.src.db.query([
            "INSERT INTO report_payment_data (pid, catalogueid, planid, amount, tx)",
            "VALUES ($1, $2, $3, $4, 1)",
            "ON CONFLICT ON CONSTRAINT unique_record DO UPDATE SET",
            "amount = report_payment_data.amount + $4, tx = report_payment_data.tx + 1"].join(' '),
            [reportId, point.catalogueid, point.planid, point.points],
            cb
        )
    }
    
    ,savePartnersData: function(point, reportId, cb) {
        this.src.db.query([
            "INSERT INTO report_partner_data (pid, partnerid, amount, tx)",
            "VALUES ($1, $2, $3, 1)",
            "ON CONFLICT ON CONSTRAINT unique_record_partner_data DO UPDATE SET",
            "amount = report_partner_data.amount + $3, tx = report_partner_data.tx + 1"].join(' '),
            [reportId, point.shopid, point.points],
            cb
        )
    }
    
})
