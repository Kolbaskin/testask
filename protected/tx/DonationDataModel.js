const Cursor = require('pg-cursor');

Ext.define('Crm.tx.DonationDataModel',  {
    extend: "Core.AbstractModel"
    
    ,$test: function() {

        
        /*this.distributeDonations(function(res) {
            console.log(res)
        })
        */
    }   
    
    ,$testUserDonation: function() {
        
    }
    
    ,distributeDonations: function(cb) {
        [
            function(next) {
                this.initMessageModel(next)
            }
            ,function(next) {
                this.readPartnersPlans(next)
            }
            ,function() {
                this.processPoints(cb)
            }
        ].runEach(this)
    }
    
    ,initMessageModel: function(cb) {
        this.messModel = Ext.create('Crm.modules.messages.model.MessagesModel', {scope: this});
        this.blogModel = Ext.create('Crm.modules.catalogueBlogs.model.CatalogueBlogsModel', {scope: this});
        
        this.blogModel.fields.push({
            name: 'active',
            type: 'boolean',
            filterable: true,
            editable: true,
            visible: true
        });
        
        [
            function(next) {
                this.src.db.collection("admin_users").findOne({
                    superuser: true
                },{}, function(e,d) {
                    if(d)
                        next(d)
                    else
                        cb()
                })
            },
            function(sysUser) {
                this.messModel.user = {
                    id: sysUser._id,
                    profile: sysUser
                }
                this.blogModel.user = {
                    id: sysUser._id,
                    profile: sysUser
                }
                cb()
            }
        ].runEach(this);  
        
    }
    
    ,processPoints: function(cb) {
        var me = this;
        
        this.charityAmounts = {};
        
        this.messToUserTpl = Ext.create('Ext.XTemplate', this.config.messages.postToUserOnDonationsText).compile();
        
        this.messTpl = Ext.create('Ext.XTemplate', this.config.messages.postInBlogOnDonationsText).compile();
        
        
        var sql = "SELECT * FROM user_points WHERE distributed is NULL or distributed = 0 ORDER BY userid";       
        
        
        
        var cursor;
        
        this.bufferedRows = [];
        this.bufferedUser = '';
        
        
        var f = function(cb) {            
            cursor.read(10, function (err, rows) {
                if(rows.length) {
                    me.prepareRows(rows, function() {
                        f(cb)
                    })
                } else {  
                    me.finalize(function() {
                        cursor.close(function() {
                            cb(true)
                            //client.release()
                        })
                    })
                }
            })
        }

        this.src.dbx.getConnection(function(db) {            
            cursor = db.conn.query(new Cursor(sql));
            f(function() {
                me.src.dbx.free(db)
                cb(true)
            });
        })        
     
    }
    
    ,prepareRows: function(rows, cb) {
        var me = this;

        rows.prepEach(
            function(row, next) {
                if(row.userid != me.bufferedUser) {
                    me.calculateDonationsForUser(function() {
                        me.bufferedUser = row.userid;
                        me.bufferedRows = [row];
                        next()
                    })
                } else {
                    me.bufferedRows.push(row);
                    next();
                }
            },
            function() {
                cb();
            }
        )
    }
    
    ,finalize: function(cb) {
        [
            function(next) {
                this.calculateDonationsForUser(function() {
                    next();
                })
            }
            ,function() {
                this.sendBlogPosts(function() {
                    cb();
                })
            }
        ].runEach(this)
    }
    
    ,readPartnersPlans: function(cb) {
        var me = this;
        
        this.partnersPlans = {};       
        
        var sql = "SELECT pp.plan_name as name, pp.partner_user_id as pid, pp.plan_type as type, pp._id, pd.pgd_catalogue_id as cid, pd.percentage FROM charity_plan pp, charity_plan_details pd,  " + _TABPREFIX + "catalogue c WHERE pp.active = 1 and c.active = 1 and pp.partner_user_id in (SELECT _id FROM " +_TABPREFIX + "foundations WHERE active=1) and pp._id = pd.charity_plan_id and pd.pgd_catalogue_id = c._id ORDER BY pp.plan_type";
        
        this.src.db.query(sql, [], function(e,data) {

            if(data) {
                data.forEach(function(plan) {
                    if(!me.partnersPlans[plan.pid]) {
                        me.partnersPlans[plan.pid] = {}
                    }                    
                    if(!me.partnersPlans[plan.pid][plan.type])
                        me.partnersPlans[plan.pid][plan.type] = [];
                    
                    var p = parseFloat(plan.percentage);
                    if(!isNaN(p)) {
                        me.partnersPlans[plan.pid][plan.type].push({
                            _id: plan._id,
                            name: plan.name,
                            cid: plan.cid,
                            pid: plan.pid,
                            percentage: p
                        });
                    }
                })
            }    
         
            cb();
        })        
    }
    
    ,calculateDonationsForUser: function(cb) {
               
        if(!this.bufferedRows.length) {
            cb();
            return;
        };
        [
            function(next) {

                this.readUserPlan(this.bufferedUser, next);
            }
            ,function(plans, next) {            
           
                this.calculateDonationsForUserByPlan(plans, next)
            }
            ,function(resultAmounts, next) {           
                this.sendMessageToUser(resultAmounts, this.bufferedUser, next);
            }
            ,function() {
                cb()
            }
        ].runEach(this)
    }
    
    ,readUserPlan: function(userId, cb) {
        var plan = [];
        var sql = "SELECT pp.partner_user_id as pid, pp.plan_type as type, pp._id, pd.pgd_catalogue_id as cid, pd.percentage FROM charity_plan pp, charity_plan_details pd, " + _TABPREFIX + "catalogue c WHERE pp.partner_user_id = $1 and pp._id = pd.charity_plan_id and c.active = 1 and pd.pgd_catalogue_id = c._id";
        this.src.db.query(sql, [userId], function(e,data) {

            if(data) {

                data.forEach(function(d) {
                    var p = parseFloat(d.percentage);
                    if(!isNaN(p)) {
                        plan.push({
                            _id: d._id,
                            cid: d.cid,
                            percentage: p
                        });
                    }
                })
            }
            cb(plan);
        })
    }
    
    ,calculateDonationsForUserByPlan: function(userPlan, cb) {
        var me = this;
        var resultAmounts = {};
        this.bufferedRows.prepEach(
            function(point, next) {
         
            
                me.calculateDonationsByPoints(me.getPlan(userPlan, point.partnerid), point, function(res) {
                    if(res) {
                        for(var i in res) {
                            if(!resultAmounts[i])
                                resultAmounts[i] = 0;
                            resultAmounts[i] += res[i];
                        }
                    }
                    next();
                })
            },
            function() {
                cb(resultAmounts)
            }
        );
    }
    
    ,getPlan: function(userPlan, partnerId) {  

        if(userPlan && userPlan.length) return Ext.clone(userPlan);
        if(!this.partnersPlans[partnerId]) {          
            return false;
            
        }
        if(this.partnersPlans[partnerId]['A']) return Ext.clone(this.partnersPlans[partnerId]['A']);
        if(this.partnersPlans[partnerId]['E']) return Ext.clone(this.partnersPlans[partnerId]['E']);
        return false;
    }
    
    ,calculateDonationsByPoints: function(plan, point, cb) {
        if(!plan) {cb(false);return;}
        var nPlan = this.normalizePlan(plan);
        if(!nPlan) {cb();return;}

        var result = {};
        
        var me = this, err = function(e, rollback) {
            console.log('err:', e);
            if(rollback) {
                me.src.db.rollback(function() {cb(null)})
            } else
                cb(null)
        };
        
        [
            function(next) {
                this.src.db.begin(function(err, res) {
                    if(res) next();

                    else err(err, false);
                })
            },
            function(next) {
                this.src.db.query("LOCK TABLE pgd_catalogue", [], function(err, res) {
                    if(res) next();
                    else err(err, false);
                })
            },
            function(next) {

                nPlan.prepEach(
                    function(pItem, nxt) {                                     
                        me.savePointsToCatalogueItem(pItem, point, function(e, res) {
                            if(res) {
                                result[res.cid] = res.points;
                                nxt();
                            }
                            else
                                err(e, true)
                        })
                    },
                    function() {
                        next()
                    }
                );
            },
            function(next) {
                this.src.db.commit(function(err, res) {
                    if(res) cb(result);
                    else err(err, result);
                })
            }
        ].runEach(this);
    }
    
    ,savePointsToCatalogueItem: function(planItem, point, cb) {
        var me = this,   
            pointNewRes,
            p = point.points * planItem.percentage / 100,
            result = {cid: planItem.cid, points: p};

        if(p <= 0) {
            cb(null, true);
            return;
        }
        ;[
            
            function(next) {
                this.src.db.query('UPDATE  ' + _TABPREFIX + 'catalogue_publ SET donation_amount_collected = donation_amount_collected + $1 WHERE _id = $2', [p, planItem.cid], function(e, d) { 
                
                    if(e)
                        cb({n:1,e:e})
                    else                        
                        next()
                })
            }
            ,function(next) { 
                this.src.db.query('UPDATE  ' + _TABPREFIX + 'catalogue SET donation_amount_collected = donation_amount_collected + $1 WHERE _id = $2', [p, planItem.cid], function(e, d) {   
                
                    if(e)
                        cb({n:1,e:e})
                    else                        
                        next()
                })
            }
            ,function(next) {

                this.checkCatalogueItemLimit(planItem.cid, function(e,surrender) {
                    if(e)
                        cb({n:2,e:e})
                    else
                        next(surrender);
                })
            }
            ,function(surrender, next) {
                if(surrender) {
                    this.getSurrender(point, surrender, function(e, newPoint) {
                        if(e) {
                            //if(e.code == 'LMT0001')
                        
                            cb({n:2.1,e:e})
                        } else {
                            p -= surrender;
                            result.points -= surrender;
                            next(newPoint)
                        }
                    })
                } else {
                    next(null)
                }
            }
            
            ,function(newPoint, next) { 
           
                if(newPoint && this.bufferedRows) {
                    this.bufferedRows.push(newPoint);
                    
                }
                this.src.db.query([
                    'UPDATE users',
                    'SET wallet_balance = CASE', 
                    'WHEN wallet_balance > '+p+' THEN wallet_balance - '+p,
                    'ELSE 0',
                    'END',
                    'WHERE _id = $1'
                ].join('\n')                
                //'UPDATE users SET wallet_balance = wallet_balance - '+p+' WHERE _id = $1'
                , [point.userid], function(e, d) {                
                    if(e)
                        cb({n:0,e:e})
                    else
                        next()          
                        
                })
            }
            
            
            ,function(next) { 
                this.src.db.collection('charity_points').insert({
                    ctime: new Date(),
                    shopid: point.shopid,
                    catalogueid: planItem.cid,
                    userid: point.userid,
                    partnerid: point.partnerid,
                    planid: planItem._id,
                    points: p
                },function(e, d) {
                    if(e)
                        cb({n:3,e:e})
                    else {
                        next()
                    }
                })
            }
            ,function(next) {
                if(!this.charityAmounts)
                    this.charityAmounts = {};
                if(!this.charityAmounts[planItem.cid])
                    this.charityAmounts[planItem.cid] = {};
                if(!this.charityAmounts[planItem.cid][point.partnerid])
                    this.charityAmounts[planItem.cid][point.partnerid] = 0;
                this.charityAmounts[planItem.cid][point.partnerid] += p;
                next();
            }
            ,function() {
                this.src.db.collection('user_points').update({_id: point._id}, {$set:{
                    distributed: 1
                }},function(e, d) {

                    if(e)
                        cb({n:4,e:e})
                    else { 
                        cb(null, result, pointNewRes)
                    }
                })
            }
        ].runEach(this);
    }
    
    ,getSurrender: function(point, surrender, cb) {
        var pointNew = this.clone(point);        
        pointNew.points = surrender;
        point.points -= surrender;       
        pointNew._id = this.src.db.createObjectId();
        
        if(!point.points) {        
            cb({code: 'LMT0001'});
            return;
        }
        
        ;[       
            function(next) {
                this.src.db.collection('user_points').update({
                    _id: point._id
                }, {$set: {points: point.points}}, function(e,d) {
                    if(e)
                        cb(e)
                    else
                        next()
                })
            },
            function() {
                this.src.db.collection('user_points').insert(pointNew, function(e,d) {
                    if(e)
                        cb(e)
                    else {
                        
                        cb(null, pointNew)
                    }
                })
            }
        ].runEach(this);
    }
    
    ,checkCatalogueItemLimit: function(_id, cb) {
        [
            function(next) {
                this.src.db.query('SELECT donation_amount_collected, donation_amount_needed FROM ' + _TABPREFIX + 'catalogue  WHERE _id = $1 and item_type=\'l\'', [_id], function(e, d) {
                    if(e) 
                        cb(e)
                    else
                        if(d && d[0]) 
                            next(d[0])
                        else
                            cb(null, 0)
                })
            }
            ,function(data, next) {
                if(data && data.donation_amount_needed && data.donation_amount_needed <= data.donation_amount_collected) {
                    next(data.donation_amount_collected - data.donation_amount_needed);
                } else {
                    cb(null, 0)
                }
            }
            ,function(surrender, next) {    
                this.src.db.query('UPDATE ' + _TABPREFIX + 'catalogue SET donation_amount_collected = donation_amount_needed, active = 0  WHERE _id = $1', [_id], function(e, d) {
                    if(e) 
                        cb(e)
                    else
                        next(surrender)
                })
            }
            ,function(surrender, next) {             
                this.src.db.query('UPDATE ' + _TABPREFIX + 'catalogue_publ SET donation_amount_collected = donation_amount_needed, active = 0  WHERE _id = $1', [_id], function(e, d) {
                    if(e) 
                        cb(e)
                    else
                        next(surrender)
                })
            }
            ,function(surrender) {
                this.readPartnersPlans(function() {
                    cb(null, surrender)
                })
            }
        ].runEach(this);
    }
    
    // normalization of the plan. if one or more items of the plan is closed, you need to recalculate the interest on the remaining that would be in the amount of 100%
    ,normalizePlan: function(plan) {
        var amount = 0; 

        plan.forEach(function(p) {
            if(p && p.percentage)
                amount += p.percentage;
        })
        if(amount <=0) return false;
        if(amount == 100) return plan;
        var k = 100 / amount;
 
       
        plan.each(function(p) {if(p && p.percentage) p.percentage *= k;else p = {percentage:0};return p;}, true);
        
        return plan;
    }
    
    ,checkCatalogueItem: function(catalogueId, cb) {
        this.src.db.collection('pgd_catalogue').findOne({_id: catalogueId}, {donation_amount_needed:1, donation_amount_collected:1, item_type: 1}, function(e,d) {

            cb(d && (d.item_type =='u' || d.donation_amount_collected < d.donation_amount_needed))
        })
    }
    
    // Donate Immediately main method
    ,donateImmediately: function(userId, pointsAmount, catalogueId, cb) {
        [
            function(next) {
                this.checkCatalogueItem(catalogueId, next)
            }
            ,
            function(res, next) {  
                if(!res) {
                    cb({code: 'DI003', message: 'catalogue limited'})
                } else
                    this.readUserActivePoints(userId, next)
            }
            ,function(availableAmount, availablePoints, next) {
                if(!availableAmount || availableAmount < pointsAmount) {
                    cb({code: 'DI001', message: 'insufficient funds on the account'})
                } else {
                
                    this.freeDonate(pointsAmount, availablePoints, catalogueId, next)
                }
            }
            ,function(err, res) {
                if(res)
                    cb(null, true)
                else
                    cb({code: 'DI002', message: 'Undefined error'})
            }
        ].runEach(this)
    }
    
    ,readUserActivePoints: function(userId, cb) {
        [
            function(next) {
                this.src.db.collection('user_points').find({distributed: 0, userid: userId}, {})
                .sort({ctime: 1})
                .toArray(function(e,d) {
                    if(d && d.length) 
                        next(d)
                    else
                        cb(0, [])
                })
            }
            ,function(points) {
          
                var amount = 0;
                points.forEach(function(p) {
                    amount += p.points
                })
                cb(amount, points)
            }
        ].runEach(this)
    }

    // user's manual donation
    ,freeDonate: function(pointsAmount, availablePoints, catalogueId, cb) {
        var me = this, err = function(err, rollback) {      


            if(rollback) 
                me.src.db.rollback(function() {
                    cb(err)
                })
            else
                cb(err)
        };
        [
            function(next) {
                this.src.db.begin(function(e,d) {
                    if(e)
                        err({n:1,e:e}, false)
                    next()
                })
            }
            ,function(next) {
                var a = pointsAmount, i = 0;
                var f = function() {                 
                    if(a<=0 || i>=availablePoints.length) {
                        next(a);
                        return;
                    }
                    me.applyPoints(a, availablePoints[i], catalogueId, function(e, res) {
                        if(e)
                            err({n:2,e:e}, true)
                        else {                      
                            a -= res;
                            i++;
                            f();
                        }
                    })
                }
                f()
            }
            ,function(a) {
                this.src.db.commit(function(e,d) {
                    if(e)
                        err({n:3,e:e}, true)
                    cb(null, pointsAmount)
                })
            }
        ].runEach(this)
    }
    
    ,applyPoints: function(amount, point, catalogueId, callback) {        
        var cb = function(err, d) {
            callback(err, err? null : d);
        }
        
        if(amount >= point.points) {
        
            amount -= point.points;
            this.savePointsToCatalogueItem({
                cid: catalogueId,
                percentage: 100,
                _id: null
            }, point, function(e,res) {           
                cb(null, res? point.points:0)
            })
        } else {
            var me = this;
            this.separatePoint(point, amount, function(e,p) {
                if(e)
                    cb(e)
                else {
                    amount -= p.points;      
                    me.savePointsToCatalogueItem({
                        cid: catalogueId,
                        percentage: 100,
                        _id: null
                    }, p, function(e,res) {                                           
                        cb(null, res? res.points:0)
                    })
                }
            })
        }        
    }
    
    ,clone: function(o) {
        var r = {}
        for(var i in o) r[i] = o[i];
        return r;
    }
    
    ,separatePoint: function(point, amount, cb) {
        var pointNew = this.clone(point);        
        pointNew.points = point.points - amount;
        point.points = amount;       
        pointNew._id = this.src.db.createObjectId();
        
        ;[
        
        
            function(next) {
                this.src.db.collection('user_points').update({
                    _id: point._id
                }, {$set: {points: amount}}, function(e,d) {
                    if(e)
                        cb(e)
                    else
                        next()
                })
            },
            function() {
                this.src.db.collection('user_points').insert(pointNew, function(e,d) {
                    if(e)
                        cb(e)
                    else {
                        
                        cb(null, point)
                    }
                })
            }
        ].runEach(this);
    }    
    
    ,sendBlogPosts: function(cb) {
        var me = this;
        

        Object.keys(this.charityAmounts).prepEach(
            function(catalogueId, next) {
                me.sendBlogPostForCatalogueItem(catalogueId, me.charityAmounts[catalogueId], next);
            },
            function() {
                cb();
            }
        );       
    }
    
    ,sendBlogPostForCatalogueItem: function(catalogueId, partnersPoints, cb) {
        [
            function(next) {
                this.src.db.collection('pgd_foundations').find({
                    _id: {$in: Object.keys(partnersPoints)}
                }, {name:1, _id:1})
                .toArray(function(e,d) {
                    if(d && d.length)
                        next(d)
                    else
                        cb();
                })                
            }
            ,function(partners) {
                                               
                partners.each(function(p) {
                    p.amount = partnersPoints[p._id];
                    return p;
                }, true)
                               
                var content = this.messTpl.apply({
                    date: new Date(),
                    list:partners 
                });
                
               
                this.blogModel.write({
                    catalogue_id: catalogueId,
                    header: this.config.messages.postInBlogOnDonationsTitle,
                    content:  content,
                    active: true
                }, function() {
                    cb();
                }, {add: true})
            }
        ].runEach(this)
    }
    
    ,sendMessageToUser: function(resultAmounts, userId, cb) {
        [
            function(next) {
                this.src.db.collection(_TABPREFIX + 'catalogue')
                .find({_id: {$in: Object.keys(resultAmounts)}}, {_id: 1, name: 1})
                .toArray(function(e,d) {
                    if(d && d.length)
                        next(d)
                    else
                        cb()
                })
            }
            ,function(items, next) {
                items.each(function(item) { 
                    item.amount = resultAmounts[item._id] || 0;
                    return item;
                }, true)
                next(this.messToUserTpl.apply({items: items}))
            }
            ,function(mess, next) { 

                console.log('to->',userId);
                console.log('message->',mess);
            
                this.messModel.write({
                    touser: [userId],
                    subject: this.config.postToUserOnDonationsTitle,
                    body: mess
                }, cb, {add: true})
            }
        ].runEach(this) 
    }
})
