Ext.define('Crm.Email.mailer', {
    extend: "Core.Controller",
    debug:true,
    sendEmail: function (data, cb) {
        me = this;
        [
            function (next) {
                if (me.debug) {
                    console.log('Sending email', JSON.stringify(data.templateId));
                }
                if (data.templateId) {
                    me.tplApply('Crm.Email.templates.'+data.templateId, {
                        data: data.templateData,
                        cfg: me.config
                    }, function (html) {
                        next(html,undefined);
                    });
                } else {
                    next(undefined, data.text || "");
                }
            },
            function (html, text) {
                var mess = {
                    from: me.config.mail.from,
                    to: data.to,
                    subject: data.subject,
                    text: text,
                    html: html,
                    attachments: data.attachments ? data.attachments : undefined
                };
                me.src.mailTransport.sendMail(mess, function (err) {
                    if (me.debug) {
                        err?console.log('Got error while sending email',err):console.log('',err);
                    }
                    cb(err)
                });
            }
        ].runEach();
    }
});