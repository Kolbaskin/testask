<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <body>
        <p>Hi {data.full_name}</p>
        <p>Click on below link to verify your email address on WPN Banking</p>
        <a href="{data.varificationLink}">{data.varificationLink}</a>
    </body>
</html>
