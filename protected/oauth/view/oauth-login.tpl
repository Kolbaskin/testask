<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>User Auth....</title>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="/css/animate.css">
    <link rel="stylesheet" type="text/css" href="/css/custom.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="com-md-12">
            <div class="notification login-alert">
                Please Enter Your Username And Password!
            </div>
            <div class="notification notification-success logged-out">
                You logged out successfully!
            </div>
            <div class="well welcome-text" align="center">
                Please wait...
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="well login-box">
                <tpl if="!redirectUri">
                    Redirect url is missing
                </tpl>
                <tpl if="redirectUri">
                <form action="" method="post">
                    <legend>Login</legend>
                    <div class="form-group">
                        <label for="username-email">Phone</label>
                        <input value='{username}' id="username-email" placeholder="Phone" name="username" type="text"
                               class="form-control"/>
                        <p if="usernameErr"   style="color:red" class="help-block">{usernameErr}</p>
                    </div>
                    <input type="hidden" name="redirectUri" value="{redirectUri}">
                    <div class="form-group">
                        <label for="password">Pin</label>
                        <input id="password" value='' type="password" placeholder="Pin" type="text" name="password" class="form-control"/>
                        <p if="passwordErr" class="help-block" style="color:red">{passwordErr}</p>
                    </div>
                    <div class="form-group text-center">
                        <input type="submit" name="action" class="btn btn-success  btn-login-submit" value="Login"/>
                        <input type="submit" name="action" class="btn btn-primary  btn-login-submit" value="Get OTP"/>
                        <tpl if="redirectUri">
                            <button type="submit" name="action" class="btn btn-default  btn-login-submit" value="Cancel">
                                Cancel
                            </button>
                        </tpl>
                    </div>
                    <tpl if="otpMessage">
                        <div class="alert alert-success" if="otpMessage">
                            <strong>Success!</strong> {otpMessage}
                        </div>
                    </tpl>
                    <tpl if="otpErrorMessage">
                        <div class="alert alert-danger" if="otpErrorMessage">
                            <strong>Oh!</strong> {otpErrorMessage}
                        </div>
                    </tpl>
                </form>
                </tpl>
            </div>
            <div class="logged-in well" align="">
                <h1>Please wait...</h1>
                <h5>While redirecting to app {redirectUri}</h5>
            </div>
        </div>
    </div>
</div>
<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/custom.js"></script>
</body>
</html>