var crypto = require('crypto')

Ext.define('Crm.oauth.model.UserAuth',{
    extend: "Admin.model.User"

    /**
     * Private login method
     **/
    ,getAutorization: function(params, callback) {

        var collection = this.src.db.collection(params.collection)
            ,mail = this.src.mailTransport
            ,config = this.config
            ,mem = this.src.mem
            ,pass = params.password
            ,lifetime = (params.lifetime || config.token.lifetime)
            ,exp = config.exp || false
            ,me = this
            ,passField = params.passField || 'pass'
            ,idField = this.config.idField || '_id'
        collection.findOne(params.find, function(e, r) {

            if(r) {

                if(r[passField].toString() === pass.toString()) {

                    if(r.activated === false) {
                        callback({status: 'blocked'})
                        return;
                    }

                    // login success
                    // generate token and put it into memcoache
                    crypto.randomBytes(config.token.len, function(ex, buf) {
                        var token = buf.toString('hex')

                        if(exp == 'true')  lifetime = 30*86400 // секунд в месяце

                        mem.set(token, r[idField], function(er, res) {
                            if(res=='STORED') {
                                // if good

                                if(r.email && r.dblauth) {

                                    // if needed dbl authorisation, making session pass and sending it
                                    crypto.randomBytes(config.token.sessPassLen, function(ex, buf) {
                                        var sess = buf.toString('hex');
                                        mem.set(token, sess, function(er, res) {

                                            if(res=='STORED') {
                                                mail.sendMail({
                                                    from: config.mail.from,
                                                    to: r.email,
                                                    subject: config.mail.loginAuthSubject,
                                                    text: config.mail.loginAuthBody.replace('${pass}', sess)
                                                });
                                                callback({id: r[idField], token: token, dblauth: true}, null);
                                            } else {
                                                callback(null, {code: 500}); // if memcache error
                                            }
                                        }, lifetime);
                                    });
                                } else {
                                    callback({id: r[idField], token: token, dblauth: false, user: r}, null);
                                }

                            } else {

                                callback(null, {mess: 500}); // if memcache error
                            }
                        }, lifetime);

                    });

                } else {
                    // login fault
                    callback(null, {code: 401});
                }
            } else callback(null, e);
        })
    }

});