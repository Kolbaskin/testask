Ext.define('Crm.oauth.model.Model', {
    extend: "Core.data.DataModel",
    save:function(collectionName,data,cb){
        data._id=this.src.db.createObjectId();
        var sql='INSERT into '+collectionName+' ';
        var fields=[],vals=[],v=[];
        for(var key in data){
            fields.push(key);
            vals.push(data[key]);
        }
        sql+='('+fields.join()+')';
        for(i=1;i<=fields.length;i++){
            v.push('$'+i);
        }
        sql+='values('+v.join()+')';
        this.src.db.query(sql,vals,cb);
    },
    oauthModel: function (){
        var model = {};
        var me=this;
        model.db=this.src.db;
        model.getAccessToken = function (bearerToken, callback) {
            this.db.collection('oauth_access_tokens').find({'access_token': bearerToken}).toArray(function (err, users){
                if (err || !users || !users.length) return callback(err);
                var token = users[0];
                callback(null, {
                    accessToken: token.access_token,
                    clientId: token.client_id,
                    expires: token.expires,
                    userId: token.user_id,
                    locationId:token.location_id||null,
                    userType:token.user_type|| 'MEMBER'
                });

            });
        };
        model.getClient = function (clientId, clientSecret, callback) {
            this.db.collection('oauth_clients').findOne({"client_id": clientId},{},function (err, user) {
                if (err || !user) return callback(err);
                var client = user;
                if (clientSecret !== null && client.client_secret !== clientSecret) return callback();
                callback(null, {
                    clientId: client.client_id,
                    clientSecret: client.client_secret,
                    redirectUri: client.redirect_uri,
                    userId: client._id
                });
            });
        };
        model.getRefreshToken = function (bearerToken, callback) {
            this.db.collection('oauth_refresh_tokens').findOne({"refresh_token": bearerToken},{},function (err, user) {
                callback(err, user || false);
            });
        };
        var authorizedClientIds = ['1234215215', 'def2'];
        model.grantTypeAllowed = function (clientId, grantType, callback) {
            return callback(false, true);
            if (grantType === 'password') {
                return callback(false, true);
            }
        };
        model.saveAccessToken = function (accessToken, clientId, expires, userId, callback) {
            me.save('oauth_access_tokens',{
                access_token: accessToken,
                client_id: clientId,
                user_id: userId.id || userId,
                expires: expires,
                user_type:userId&&userId.type?userId.type:'MEMBER'
            }, function (err, saved) {
                callback(err);
            });
        };
        model.saveRefreshToken = function (refreshToken, clientId, expires, userId, callback) {
            me.save('oauth_refresh_tokens',{
                refresh_token: refreshToken,
                client_id: clientId,
                user_id: userId.id || userId,
                user_type:userId&&userId.type?userId.type:'MEMBER',
                expires: expires
            }, function (err, saved) {
                callback(err);
            })
        };
        model.getUser = function (username, password, callback) {
            this.db.collection('app_users').findOne({"username": username, password: password},{},function (err, user) {
                callback(err, user ||  false);
            })
        };
        model.saveAuthCode = function (authCode, clientId, expires, user, callback) {
            var code = {
                authCode: authCode,
                clientId: clientId,
                userId: user
            };
            if (expires) code.expires = parseInt(expires / 1000, 10);
            me.save('oauth_codes',code, callback);
        };
        model.getAuthCode = function (bearerCode, callback) {
            this.db.collection('oauth_codes').findOne({authCode: bearerCode},{},function (err, code) {
                if (code && code.expires) {
                    code.expires = new Date(code.expires * 1000);
                }
                callback(err, code);
            })
        };
        return model;
    }
});
