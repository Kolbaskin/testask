/**
 * Created by j2ee on 5/4/16.
 */
Ext.define('Crm.oauth.controller.Pages',{
    extend: "Core.Controller"

    ,login: function() {
        var me = this;

        me.tplApply(".login", {error: false, currentDate: new Date()}).sendHTML()

    }

    ,loginDo: function() {
        var me = this;

        // me.params.gpc -- params GET, POST and COOKIES
        // me.src.db <- this is a link to database:

        me.src.db.collection("admin_users").findOne({
            login: me.params.gpc.login
        }, {}, function(e,d) {
            if(d)
                me.tplApply(".loginOk", d).sendHTML()
            else
                me.tplApply(".login", {error: true, currentDate: new Date()}).sendHTML()
        })
    }

    ,usersList: function() {
        var me = this;

        Ext.create('Crm.modules.users.model.UsersModel', {scope: me}).getData({}, function(data) {
            me.tplApply(".allusers", data).sendHTML()
        })
    }
    ,oneUser: function(id) {
        var me = this;

        Ext.create('Crm.modules.users.model.UsersModel', {scope: me})
            .getData({filters:[{property: '_id', value: id}]}, function(data) {
                if(data && data.list && data.list[0])
                    me.tplApply(".one", data.list[0]).sendHTML()
                else
                    me.error(404)
            })
    }
})