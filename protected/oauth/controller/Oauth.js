Ext.define('Crm.oauth.controller.Oauth', {
    extend: "Crm.REST.RestBaseController",
    oauthserver: null,
    constructor:function () {
        this.callParent(arguments);
        var me=this,res=this.response,req=this.request;
        res.redirect=req.redirect = function (url) {
            res.writeHead(307, {
                'Location': url
            });
            return res.end();
        };
        req.is=function(request){
            return req.headers['content-type'].indexOf(request)>=0;
        };
        res.get=req.get=function (header) {
            return req.headers[header.toLowerCase()];
        };
        res.set=req.set=function (headers){
            for(var i in headers){
                res.setHeader(i,headers[i]);
            }
            return res.headers;
        };
        this.oauthserver = this.oauthserverInit();
        this.MembersModel = Ext.create('Crm.modules.members.model.MembersModel', {scope: this});
    },
   
    me:function () {
        var req = this.request, res = this.response, me = this;
        req.query = req.body = this.params.gpc;
        [
            function (next){
                me.oauthserver.authorise()(req,res,next)
            },
            function (err) {
                if(err){
                    me.error(500, err);
                }else{
                    me.src.db.collection('app_users').findOne({"_id":me.src.db.fieldTypes.ObjectID.getValueToSave(null,req.user.id)}, {},function (err,user){
                        if(user){
                            me.sendJSON(user)
                        }else{
                            me.error(404)
                        }
                    })
                }
            }
        ].runEach();
    },
    grant: function () {
        var req = this.request, res = this.response, me = this;
        this.res = me.response;
        req.query = req.body = this.params.gpc;
        [
            function (next) {
                me.oauthserver.grant()(req,res,next);
            },
            function (err) {
                me.error(500, err);
            }
        ].runEach();

    },
    postAuthorise: function () {
        var req = this.request, res = this.response, me = this;
        req.query = req.body = this.params.gpc;
        if (!me.params.cookies.user || req.body.client_id!=me.params.cookies.clientId ){
            return req.redirect('/oauth/login?redirect=' + encodeURIComponent(req.url) + '&client_id=' + req.query.client_id + '&redirect_uri=' + encodeURIComponent(req.query.redirect_uri));
        }
        req.body.allow = "yes";
        req.body.response_type = req.body.response_type ? req.body.response_type : 'code';
        [
            function (next) {
                me.oauthserver.authCodeGrant(function (req, next) {
                    next(null, req.body.allow === 'yes', me.params.cookies.user, me.params.cookies.user);
                })(req, res, next);
            },
            function (err) {
                me.error(500, err);
            }
        ].runEach();
    },
    authorise: function () {
        var req = this.request, res = this.response, me = this;
        req.query = req.body = this.params.gpc;
        this.res = me.response;
        if (true||!me.params.cookies.user || req.body.client_id!=me.params.cookies.clientId){
            return req.redirect('/oauth/login?redirect=' + encodeURIComponent(req.url) + '&client_id=' + req.query.client_id + '&redirect_uri=' + encodeURIComponent(req.query.redirect_uri));
        }
        req.body.allow = "yes";
        req.body.response_type = req.body.response_type ? req.body.response_type : 'code';
        [
            function (next) {
                me.oauthserver.authCodeGrant(function (req, next) {
                    next(null, req.body.allow === 'yes', me.params.cookies.user, me.params.cookies.user);
                })(req, res, next);
            },
            function (err) {
                me.error(500, err);
            }
        ].runEach();
    },
    oauthLogin: function () {
        var req = this.request, res = this.response, me = this;
        req.query = req.body = this.params.gpc;
        var clientId = req.query.client_id || "";
        var redirectUri = req.query.redirect_uri || "";
        res.setHeader('Set-Cookie', 'clientId=' + clientId + ';redirectUri=' + redirectUri + ';HttpOnly');
        me.tplApply(".oauth-login", {error: false, currentDate: new Date(),redirectUri:redirectUri}).sendHTML();
    },
    oauthPostLogin: function () {
        var req = this.request, res = this.response, me = this;
        req.query = req.body = this.params.gpc;
        var redirect = null;
        res.setHeader('Content-Type', 'application/json');
        if (req.query.redirect) {
            redirect = req.query.redirect;
        }
        switch(req.body.action){
            case 'Cancel' :
                res.writeHead(307, {
                    'Location': redirect + "?response_type=error&client_id=" + me.params.gpc.clientId + "&redirect_uri=" +
                    (me.params.gpc.redirectUri ? me.params.gpc.redirectUri : "")
                });
                break;
            case 'Get OTP' :
                if (req.body.username) {
                    me.src.db.collection('Members').findOne({"tel":req.body.username}, {},function (err,user){
                        if(user){
                            var config=me.config;
                            var crypto = require('crypto');
                            var encPin=crypto.createHash(config.hashtype).update('9119').digest('hex');
                            //user.pin=encPin;
                            me.src.db.collection('Members').update({"_id":user._id}, {$set:{pin:encPin}},function (err,user){
                                me.tplApply(".oauth-login", {username:req.body.username||'',otpMessage: 'OTP, One time password has been sent to '+req.body.username+'.',error: false, currentDate: new Date(),redirectUri:me.params.gpc.redirectUri ? me.params.gpc.redirectUri : ""}).sendHTML();
                            });
                        }else{
                            me.tplApply(".oauth-login", {username:req.body.username||'',otpErrorMessage: 'Your entered number is not exist.',error: false, currentDate: new Date(),redirectUri:me.params.gpc.redirectUri ? me.params.gpc.redirectUri : ""}).sendHTML();
                        }
                    });
                }
                else {
                    me.tplApply(".oauth-login", {username:req.body.username||'',usernameErr: req.body.username?'':'Please enter phone number',error: true, currentDate: new Date(),redirectUri:me.params.gpc.redirectUri ? me.params.gpc.redirectUri : ""}).sendHTML();
                }
                break;
            default :
                if (req.body.username && req.body.password) {
                    me.callModel('Admin.model.User.getAutorization', {
                        collection: 'Members',
                        find: {tel: req.body.username, removed: {$ne: true}},
                        password: req.body.password,
                        passField:'pin'
                    }, function (user,err) {
                        if (user) {
                            if (redirect && me.params.gpc.clientId) {
                                res.setHeader('Set-Cookie', 'user=' + user.id + ';HttpOnly');
                                res.writeHead(307, {
                                    'Location': redirect + "?response_type=code&client_id=" + me.params.gpc.clientId + "&redirect_uri=" +
                                    (me.params.gpc.redirectUri ? me.params.gpc.redirectUri : "")
                                });
                                res.end();
                            }
                            res.end("Logged in");
                        }
                        else {
                            me.tplApply(".oauth-login", {username:req.body.username||'',error: true, passwordErr:'Incorrect PIN',currentDate: new Date(),redirectUri:me.params.gpc.redirectUri ? me.params.gpc.redirectUri : ""}).sendHTML();
                            //me.error(400, err);
                        }
                    });
                }
                else {
                    me.tplApply(".oauth-login", {username:req.body.username||'',usernameErr: req.body.username?'':'Please enter phone number',passwordErr: req.body.password?'':'Please enter pin',error: true, currentDate: new Date(),redirectUri:me.params.gpc.redirectUri ? me.params.gpc.redirectUri : ""}).sendHTML();
                }
        }
    }
});