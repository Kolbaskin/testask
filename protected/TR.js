Ext.define('Crm.TR', {
    extend: "Core.Controller",
    defaultLanguage: 'en',
    LOCALS: {},
    constructor: function () {
        var _this = this;
        _this.callParent(arguments);
        _this.localesDir = _this.config.projectDir + '/locales/';
        _this.init();
        Ext.D = this;
    },
    init: function (lang, next) {
        String.prototype.replaceAll = function (str1, str2, ignore) {
            return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, "\\$&"), (ignore ? "gi" : "g")), (typeof (str2) == "string") ? str2.replace(/\$/g, "$$$$") : str2);
        };
        lang = lang || this.defaultLanguage;
        lang = lang.toLowerCase();
        this.readTranslations(lang, next);
    },
    readTranslations: function (lang, next) {
        var _this = this;
        var file = _this.localesDir + lang + '.json';
        var locals;
        try {
            locals = require(file);
        } catch (e) {
            locals = {};
        }
        for (var i in locals) {
            if (typeof locals[i] == 'object') {
                for (var j in locals[i]) {
                    if (typeof locals[i][j] == "string")
                        _this.LOCALS[i + '.' + j] = locals[i][j];
                }
            } else if (typeof locals[i] == 'string') {
                _this.LOCALS[i] = locals[i][j];
            }
        }
        if (next && typeof next == 'function') {
            next();
        }
    },
    t: function (cat, text, vals) {
        var tr = text;
        if (this.LOCALS[text]) tr = this.LOCALS[text] || text;
        else if (this.LOCALS[cat + '.' + text]) tr = this.LOCALS[cat + '.' + text] || text;
        if (vals) {
            for (var key in vals) {
                if (vals[key]) {
                    tr = tr.replaceAll('{' + key + '}', vals[key]);
                }
            }
        }
        return tr;
    },
    $extract: function (data, cb) {
        this.extractMessages(function () {
            console.log('Done');
        });
    },
    extractMessages: function (next) {
        var _this = this;
        var UglifyJS = require("uglify-js");
        var recursive = require('recursive-readdir');
        var fs = require('fs');
        var ignoreFiles = ['*.tpl', '*.woff2', '*.ttf', '*.svg', '*.woff', '*.eot', '*.txt', '*.gif', '*.png', '*.jpg', '*.jpeg', '*.css', '*.html', '*.json', '*.html', '*.md', '*.otf'];
        [
            function (next) {
                recursive(_this.config.projectDir + '/protected', ignoreFiles, next);
            },
            function (err, protectedList, next) {
                recursive(_this.config.projectDir + '/static', ignoreFiles, function (err, staticList) {
                    next(Array.isArray(staticList) && Array.isArray(protectedList) ? protectedList.concat(staticList) : [])
                });
            },
            function (fileList) {
                var messages = {}
                fileList.each(function (filename) {
                    try {
                        var toplevel = UglifyJS.parse(String(fs.readFileSync(filename)));
                        var walker = new UglifyJS.TreeWalker(function (node) { //walk AST
                            var statement = JSON.parse(JSON.stringify(node));
                            if (statement && statement.expression && statement.expression.property == 't') {
                                if (statement.args && statement.args.length >= 2) {
                                    if (!messages[statement.args[0].value]) {
                                        messages[statement.args[0].value] = {};
                                    }
                                    messages[statement.args[0].value][statement.args[1].value] = statement.args[1].value;
                                } else if (statement.args && statement.args.length == 1) {
                                    messages[statement.args[0].value] = statement.args[0].value;
                                }
                            }
                        });
                        toplevel.walk(walker);
                    } catch (e) {
                        console.log('Error in file', filename, e.message);
                    }
                });
                if (!messages.api) {
                    messages.api = {};
                }
                var errorMessages = require(_this.config.projectDir + '/protected/REST/errorCodes.json');
                for (var code in errorMessages) {
                    messages.api[errorMessages[code]] = errorMessages[code];
                }
                var mergeObjects = function (obj1, obj2) {
                    for (var key in obj2) {
                        if (typeof obj2[key] == 'object' && typeof obj1[key] == 'object') {
                            obj2[key] = Ext.apply(obj1[key], obj2[key]);
                        }
                    }
                    return Ext.apply(obj1, obj2);
                };
                var outputFilename = _this.localesDir + 'pending-list.json';
                var enFile = require(_this.localesDir + 'en.json');
                var ruFile = require(_this.localesDir + 'ru.json');
                messages = mergeObjects(messages, enFile);
                messages = mergeObjects(messages, ruFile);
                fs.writeFile(outputFilename, JSON.stringify(messages, null, 4), function (err) {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log("JSON saved to " + outputFilename);
                    }
                });
            }
        ].runEach();

    }
});