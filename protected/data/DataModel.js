Ext.define('Crm.data.DataModel', {
    extend: 'Core.data.DataModel'
    
    ,getPermissions: function(callback, mn, data) {
        if(!mn) mn = this.getShortName();   

        if(this.user && this.user.profile) {
            if(this.user.profile.superuser)
                callback({read: true, add: true, modify: true, del: true})
            else {
                var modelaccess = this.mergeGroupsAccess();
                if(modelaccess[mn] && modelaccess[mn].read)
                    callback({read: true, add: true, modify: true, del: true})
                else    
                    callback({read: false, add: false, modify: false, del: false})
            }
        }
        else
            callback({read: false, add: false, modify: false, del: false})
    }    
    
    ,mergeGroupsAccess: function() {
        var out = this.user.profile.group && this.user.profile.group.modelaccess? this.user.profile.group.modelaccess:{};        
        if(this.user.profile.xgroups) {
            this.user.profile.xgroups.forEach(function(g) {
                if(g.modelaccess) {
                    for(var i in g.modelaccess) {
                        for(var j in g.modelaccess[i]) if(g.modelaccess[i][j]) {
                            if(!out[i]) out[i] = {}
                            out[i][j] = true;
                        }
                    }
                }                
            })
        }
        return out;        
    }
    
    
    
    
})