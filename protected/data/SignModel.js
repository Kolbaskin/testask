Ext.define('Crm.data.SignModel', {


    signRecord: function(data, cb) {
        var me = this;
        me.isOrgTrusted(function(res) {
            if(res) {
                me.doAutoSign(data, cb)
            } else {
                me.$signRecord(data, cb)
            }
        })
    }

    ,unSignRecord: function(data, cb) {
        var me = this;
        me.isOrgTrusted(function(res) {
            if(res) {
                me.doAutoUnSign(data, cb)
            } else {
                me.$unSignRecord(data, cb)
            }
        })
    }

    ,isOrgTrusted: function(cb) {
        var me = this;
        this.src.db.collection(_TABPREFIX + 'foundations').findOne({_id: this.user.profile.pid}, {trusted: 1}, function(e,d) {           
            cb(d? me.trustedToBoolean(d.trusted) : false);
        })
    } 

    ,doAutoSign: function(data, cb) {
        this.dbCollection.update({_id: data._id},{$set:{active: true}}, function(e,d) {
            cb({success: true})
        })
    }
    /* scope:server */
    ,doAutoUnSign: function(data, cb) {
        this.dbCollection.update({_id: data._id},{$set:{active: false}}, function(e,d) {
            cb({success: true})
        })
    }
    
    
    /* scope:server */
    ,onSignActivate: function(data, cb) {
        
        var me = this;    
        [
            function(next) {
                me.src.db.collection(me.collection + '_publ').remove({_id: data._id}, function() {
                    next()
                })
            },
            function(next) {
                this.src.db.query("INSERT INTO " + me.collection + "_publ SELECT * FROM " + me.collection + " WHERE _id = '"+data._id+"'", [], function(e) {                  
                    cb()
                })
            }
        ].runEach(this)
    }
    
    /* scope:server */
    ,onUnSign: function(data, cb) {
        if(this.localeUnsign) {
            cb();
            return;
        }
        this.src.db.collection(this.collection + '_publ').remove({_id: data._id}, function() {
            cb()
        })
    }
    
    ,afterGetData: function(data, cb) {
        if(!data.length || this.collection == 'pgd_catalogue_publ') {
            cb(data);
            return;
        }
        var ids = [];
        data.forEach(function(d) {ids.push(d._id)});
        this.src.db.collection(this.collection + '_publ').find({_id: {$in: ids}}, {_id:1})
        .toArray(function(e,d) {
            d.forEach(function(d) {
                for(var i = 0;i<data.length;i++) {
                    if(data[i]._id == d._id) {
                        data[i].publ = true;
                        break;
                    }
                }
            })
            cb(data)
        })
    }
    
})
