Ext.define('Crm.Utils.Util',{
    extend: "Core.Controller",
    encrypt:function(str){
        var me=this;
        var crypto = require('crypto');
        return crypto.createHash(me.config.hashtype).update(str).digest('hex');
    },
    generateRandomString:function(len){
        len=len||5
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        return this.generateRandom(len,possible);
    },
    generateRandom:function(len,possible){
        var text = "";
        for( var i=0; i < len; i++ ){
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    },
    generateRandomNumber:function(len){
        len=len||5;
        var possible = "0123456789";
        return this.generateRandom(len,possible);
    },
    buildVerificationLink:function(user,code){
        return this.config.baseUrl+'/verfiy-email?code='+encodeURIComponent(code)+'&email='+encodeURIComponent(user.email);
    },
    getObjectId:function(cb){
        return this.src.db.createObjectId(this.collection, cb);
    }
});