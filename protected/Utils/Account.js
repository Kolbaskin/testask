Ext.define('Crm.Utils.Account', {
    extend: "Core.Controller",
    constructor: function (cfg) {
        var me = this;
        me.callParent(arguments);
        me.Account = Ext.create('Crm.modules.accounts.model.AccountsModel', {
            scope: this
        });
        me.Mailer = Ext.create('Crm.Email.mailer', {
            scope: this
        });
        me.Util = Ext.create('Crm.Utils.Util', {
            scope: this
        });
    },
    ACCOUNT_TYPES: {
        PRINCIPLE: 'PRINCIPLE',
        E_ACCOUNT: 'E_ACCOUNT',
        EEFC: 'EEFC',
    },
    ACCOUNT_STATUS: {
        CREATED: 'CREATED',
        ACTIVE: 'ACTIVE',
        INACTIVE: 'INACTIVE',
        SUSPENDED: 'SUSPENDED',
    },
    ACCOUNT_NUMBER_LENGTH: 13,
    ACCOUNT_NUMBER_PREFIX: 'WPN',
    /*
    @author Datta Bhise
    @date 23 June 2017
    @function getAccountDetails
    @desc function to get account details   
    */
    getAccountDetails: function (accountNumber, cb) {
        var me = this;
        me.Account.findOne({
            account_number: accountNumber
        }, {}, cb);
    },
    /*
    @author Datta Bhise
    @date 23 June 2017
    @function createNewAccount
    @desc function to create new account
    */
    createNewAccount: function (user, accountType, currency, parentAccNum, cb) {
        var me = this;
        var accountDetails = {
            _id: me.Util.getObjectId(),
            type: accountType,
            currency: currency,
            parent_principal_acc: parentAccNum,
            acc_status:me.ACCOUNT_STATUS.CREATED
        };
        [
            function (next) {
                if (parentAccNum) {
                    me.getAccountDetails(parentAccNum, function (err, parentAcc) {
                        if ((parentAcc && parentAcc) || !parentAcc) {
                            return cb({
                                code: 'ERR031'
                            });
                        } else {
                            next();
                        }
                    });
                } else {
                    next();
                }
            },
            function (next) {
                if (accountType === accountType == me.ACCOUNT_TYPES.PRINCIPLE || accountType === me.ACCOUNT_TYPES.E_ACCOUNT) {
                    next()
                } else {
                    return cb({
                        code: 'ERR030'
                    });
                }
            },
            function (next) {
                if (accountType === accountType == me.ACCOUNT_TYPES.PRINCIPLE || accountType === me.ACCOUNT_TYPES.E_ACCOUNT) {
                    next()
                } else {
                    return cb({
                        code: 'ERR030'
                    });
                }
            },
            function (next) {
                me.getNextAccountNumber(function (err, accNum) {
                    if (err) {
                        return cb(err);
                    }
                    next(accNum);
                })
            },
            function (accountNumber) {
                accountDetails.account_number = accountNumber;
                me.Account.error=function(code){
                    return cb(code);
                }
                me.Account.write(accountDetails,function(r){
                    if(r.success){
                        next(r.record);
                    }else{
                        return cb(r.error);
                    }
                },{add:true});
            },
            function(){
                
            }
        ].runEach();


    },
    /*
    @author Datta Bhise
    @date 23 June 2017
    @function getNextAccountNumber
    @desc function to get next account number
    */
    getNextAccountNumber: function (cb) {
        var me = this;
        var formatAccountNumber = function (number) {
                return me.ACCOUNT_NUMBER_PREFIX + (Array(me.ACCOUNT_NUMBER_LENGTH - String(number).length + 1).join('0') + number);
            }
            [
                function (next) {
                    me.src.db.query('SELECT nextval("account_number") as num', [], function (e, d) {
                        if (e) {
                            return cb(e);
                        }
                        var number = d[0].num;
                        next(num);
                    });
                },
                function (acNo) {
                    cb(null, formatAccountNumber(acNo));
                }
            ].runEach();
    }
});