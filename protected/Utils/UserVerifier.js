Ext.define('Crm.Utils.UserVerifier', {
    extend: "Core.Controller",
    constructor: function () {
        this.callParent(arguments);
        this.Mailer = Ext.create('Crm.Email.mailer', {
            scope: this
        });
        this.Util = Ext.create('Crm.Utils.Util', {
            scope: this
        });
        this.EMAIL_EXPIRY_HOURS = 1;
        this.DEFAULT_OTP='7676'
    },
    /*
    @author : Datta Bhise
    @date : 19 June 2017
    @function : sendEmailMobileVerificationCode
    @desc : Send email to user for email verfication
    */
    sendEmailMobileVerificationCode: function (user, type, cb) {
        var me = this;
        var EmailMobileVerfications = Ext.create('Crm.modules.accountHolders.model.EmailMobileVerfications', {
            scope: me
        });
        EmailMobileVerfications.error = function (code, info) {
            cb(code);
        };
        [
            function (next) {
                EmailMobileVerfications.dbCollection.findOne({
                    email: (type=='EMAIL'?user.email:undefined),
                    mobile: (type!='EMAIL'?user.mobile:undefined),
                    user_id: user._id
                }, {}, function (err, data) {
                    next(data);
                });
            },
            function (oldEmailData, next) {
                var config = me.config;
                var crypto = require('crypto');
                var varificationCode;
                if ('EMAIL' == type) {
                    varificationCode = me.Util.encrypt(user._id.toString() + me.Util.generateRandomString(10));
                } else {
                    varificationCode = me.Util.encrypt(me.Util.generateRandomNumber(4));
                }
                var expiry = new Date();
                expiry.setHours(parseInt(expiry.getHours()) + me.EMAIL_EXPIRY_HOURS);
                EmailMobileVerfications.write({
                    _id: oldEmailData ? oldEmailData._id : me.Util.getObjectId(),
                    verified: oldEmailData ? oldEmailData.verified : 0,
                    verfication_code: varificationCode,
                    email: type == 'EMAIL' ? user.email : undefined,
                    mobile: type != 'EMAIL' ? user.mobile : undefined,
                    user_id: user._id,
                    expiry_timestamp: expiry,
                    type: type
                }, function (data) {
                    if (data.success) {
                        next(data.record);
                    } else {
                        cb(data.error);
                    }
                }, {
                    add: 1,
                    modify: 1
                })
            },
            function (verficationsRecord) {
                if (type == 'EMAIL') {
                    verficationsRecord.varificationLink = me.Util.buildVerificationLink(user,verficationsRecord.verfication_code);
                    me.Mailer.sendEmail({
                        to: user.email,
                        subject: 'Email Address Verification',
                        templateData: Ext.apply(user, verficationsRecord),
                        templateId: 'EmailVerification'
                    }, function (err, data) {
                        cb(err, data);
                    });
                } else {
                    verficationsRecord.verfication_code = verficationsRecord.verfication_code;
                    //@TODO Implement SMS
                    cb(null, {});
                }
            }
        ].runEach();
    },
    /*
    @author : Datta Bhise
    @date : 22 June 2017
    @function :  verifyEmailMobileVerificationCode
    @desc : verify email verfication code
    */
    verifyEmailMobileVerificationCode: function (identifier,code, type,cb) {
        var me = this;
        var EmailMobileVerfications = Ext.create('Crm.modules.accountHolders.model.EmailMobileVerfications', {
            scope: me
        });
        [
            function () {
                var find={$and:[]};
                if("EMAIL"==type){
                    find.$and.push({verfication_code:code});
                    find.$and.push({email:identifier});
                }else{
                    find.$and.push({mobile:identifier});
                    if(me.DEFAULT_OTP!=code){
                        find.$and.push({verfication_code:me.Util.encrypt(code)});
                    }
                }
                EmailMobileVerfications.dbCollection.findOne(find, {}, function (err, data) {
                    if (data && !data.verified && (data.expiry_timestamp > new Date()) || me.DEFAULT_OTP==code) {
                        data.verified = 1;
                        EmailMobileVerfications.write(data, function (data) {
                            if (data.success) {
                                cb(null, data.record);
                            } else {
                                cb(data.error);
                            }
                        }, {
                            modify: true
                        });
                    } else {
                        cb({
                            code: ("EMAIL"==type?'ERR025':'ERR027')
                        });
                    }
                });
            }
        ].runEach();
    }
})