_TABPREFIX = '';
_LAST_MESSAGE_TIME_ = new Date();
_LAST_READ_MESSAGE_TIME_ = new Date();

Ext.define("Crm.Core.ProjectServer", {
    extend: "Core.ProjectServer",
    
    dbConnect: function (callback) {
        var me = this;        
        _TABPREFIX = this.config.tablePrefix || '';     
        me.dbx = Ext.create("Database.drivers.Postgresql.Transactions");            
        me.dbx.init(me.config, function() {
            me.sources.db = me.dbx.newConnection(function() {
                me.initCore(callback);
            })
            
        })
    },
    initCore: function (callback) {
        var me = this;
        if (me.config.autorun) {
            me.config.autorun.prepEach(
                function (item, next) {
                    var cls = Ext.create(item.model, {
                        src: me.sources,
                        config: me.config,
                        scope: me
                    });
                    if (!!item.initMethod) {
                        cls[item.initMethod](function () {
                            next();
                        })
                    } else
                        next();
                },
                function () {
                    if (!!callback) callback()
                }
            )
        } else
        if (!!callback) callback();
    }
     ,beforeRouteGo: function(req, res, params, route, cb) {
         var me = this;      
         this.getSources(function(sources) {

            var MobileBaseController = Ext.create("Crm.REST.RestBaseController", {
                request: req, 
                response: res, 
                params: params,
                src: sources,
                config: me.config
            })
            if(MobileBaseController.isSent)
                return;
            me.beforeRouteGo_Do(MobileBaseController, req, res, params, route, cb)
         })
     }
     
     ,beforeRouteGo_Do: function(MobileBaseController, req, res, params, route, callback) {
            var me = this;           
            var cb = function(res) {
                me.freeSources(MobileBaseController.src);               
                callback(res)
            }

            if(req.method=="OPTIONS"){
                return MobileBaseController.sendData({});
            }
            var lang=MobileBaseController.req.body&&MobileBaseController.req.body.header?MobileBaseController.req.body.header.lang:null;
            //Ext.D.init(lang);
            
            MobileBaseController.authorise('',function (err,user,userType) {
                cb({user: user})
            })
    }
    
    
    ,getSources: function(cb, persist) {         
        if(this.dbx.debug)
            var debugData = require('caller-id').getData();
        
        [
            function(next) {
                if(persist) {
                    var db = this.dbx.newConnection(function() {
                        next(db)
                    })
                } else {
                    var me = this;
                    this.dbx.getConnection(function(db) {            
                        if(me.dbx.debug) db.debugData = debugData;
                        next(db)
                    })
                }
            }
            ,function(db) {
                
                cb({
                    db: db,
                    dbx: this.dbx,
                    mem: this.sources.mem,
                    wsConnections: this.sources.wsConnections,
                    mailTransport: this.sources.mailTransport,
                    elasticsearch: this.sources.elasticsearch
                });
            }
        ].runEach(this)

    }
    
    ,freeSources: function(sources) {
        this.dbx.free(sources.db)
    }
})
