Ext.define('Crm.sms.SmsController', {

    serviceUrl:'https://smsc.ru/sys/send.php?login=test&psw=test&phones=<phones>&mes=<message>&charset=utf-8'

    ,sendSMS: function(tel, tpl, data, cb) {
        var mess = Ext.create('Ext.XTemplate', this.config.smsTpl[tpl]).compile().apply(data);
        
        console.log('SMS TO: ', tel,' ', mess)
        
        this.sendSmsDo(tel, mess, function(res) {
            cb(null, true);
        })
        
        
         // error callback
         // cb({code:102, message: 'SMS Gataway is not working!'}, null)
         // cb({code:103, message: 'Timeout limit between two sms on one telephone'}, null)
         
        
    }
    
    
    
    ,sendSmsDo:function(number,message,cb){
        var _this=this;
        var toNumber=number;
        if(toNumber && toNumber.toString().substr(0,1)!='7'){
            toNumber = '+7'+toNumber;
        }
        var request = require('request');
        //79055492917
        
        var xmlData='<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\r\n<data>\r\n\t<authorization>\r\n\t\t<login>TPK_KSL<\/login>\r\n\t\t<password>cNbr2cLCo3<\/password>>\r\n\t<\/authorization>\r\n\t<type>send<\/type>\r\n\t<request>\r\n\t\t<header>KSL<\/header>\r\n\t\t<msisdn>'+toNumber+'<\/msisdn>\r\n\r\n\t\t<message>'+message+'<\/message>\r\n\t<\/request>\r\n<\/data> ';
        
        //if(toNumber.charAt(0) != '+') toNumber = '+7' + toNumber;
        
        var url = this.serviceUrl.replace('<phones>', toNumber).replace('<message>', encodeURIComponent(message))

        
        
        var options = {
            method:'GET',
            url: url
            
        };
        function callback(error, response, body) {
            body = body || '';
            var log = '################################\n';
            log += new Date()+' Sending to ' + toNumber+'\n';
            log += body.toString();
            //require('fs').appendFileSync(__dirname+'/sms-response.log', log+'\n\n');
            
            console.log(log)
            
            cb(body);
        }
        request(options, callback);
    }
    
})
