Ext.define('Crm.REST.RestBaseController', {
    extend: "Core.Controller",
    constructor: function (cfg) {
        var _this = this;
        
        _this.callParent(arguments);
        _this.http_codes = require('http_codes');
        _this.Mailer=Ext.create('Crm.Email.mailer', {scope: this});
        _this.Util=Ext.create('Crm.Utils.Util', {scope: this});
        _this.Members=Ext.create('Crm.modules.accountHolders.model.AccountHoldersModel', {scope: this}).dbCollection;
        _this.req = this.request;
        _this.res = this.response;
        if (!_this.res) return;
        _this.res.errors = [];

        if (!_this.req.body) {
            try{
                _this.req.body = JSON.parse(_this.params.fullData||'');
            }catch(e){
                _this.req.body={header:{},reqData:{}};
            }
            var lang='en';

            if(_this.req.body && _this.req.body.header && _this.req.body.header.lang){
                lang = _this.req.body.header.lang;
            }
        }

        if(
            _this.req.body && 
            _this.req.body.header && 
            _this.req.body.header.version && 
            _this.config.currentApiVersion &&
            _this.req.body.header.version !== _this.config.currentApiVersion
        ) {
            _this.addErrorObj({code: 'VR001', message: 'API version is deprecated. Current API version is ' + _this.config.currentApiVersion});
            _this.isSent = true;
            return;
        }
        
        _this.res.redirect = _this.req.redirect = function (url) {
            _this.res.writeHead(307, {
                'Location': url
            });
            return _this.res.end();
        };
        _this.res.setHeader('Content-Type', 'application/json');
        _this.req.is = function (request) {
            return _this.req.headers['content-type'] == request;
        };
        _this.res.get = _this.req.get = function (header) {
            return _this.req.headers[header.toLowerCase()];
        };
        _this.res.set = _this.req.set = function (headers) {
            for (var i in headers) {
                _this.res.setHeader(i, headers[i]);
            }
            return _this.res.headers;
        };
        _this.oauthserver = _this.oauthserverInit();

    },
    authorise: function (what, next) {
        

        var _this = this;
        if (_this.user) {
            return next(null, _this.user);
        }
        var req = this.request,
            res = this.response,
            _this = this;
        var oAuthToken = req.headers.authorization;
        //console.log("headers", JSON.stringify(req.headers));
        //console.log("==============================");
        //console.log("body",JSON.stringify(req.body));
        if (typeof next == 'function') {

            //if (!req.headers.authorization) {
            if (req.body.reqData && typeof req.body.reqData.Token !== 'undefined') {
                if (req.body.reqData.Token.search('bearer') >= 0) {
                    req.body.reqData.Token = req.body.reqData.Token.replace('bearer', 'Bearer');
                } else if (req.body.reqData.Token.search('Bearer') < 0) {
                    req.body.reqData.Token = 'Bearer ' + req.body.reqData.Token;
                }
                req.headers.authorization = req.body.reqData.Token
            }
            //if(!req["content-type"]){req["content-type"]=req.body.reqData["content-type"]};
            req.query = this.params.gpc;
            _this.oauthserver.authorise()(req, res, function (err) {
                req.headers.authorization = oAuthToken;
                if (err) {
                    next(err, null)
                } else {
                    req.locationId = '';
                    req.userType = '';
                    req.userId = '';
                    if (req.oauth.bearerToken) {
                        req.locationId = req.oauth.bearerToken.locationId ? req.oauth.bearerToken.locationId : undefined;
                        req.userType = req.oauth.bearerToken.userType;
                        req.userId = req.oauth.bearerToken.userId;
                    }
                    if (req.userType == 'MEMBER' || !req.userType) {
                        _this.Members.findOne({
                            "_id": _this.getUserObjectId(true)
                        }, {}, function (err, user) {
                            next(err, user, req.userType);
                        });
                    } else {
                        _this.UserModel.dbCollection.findOne({
                            _id: req.userId
                        }, {}, function (err, user) {
                            if (user) {
                                if (!user.act1) {
                                    next({
                                        code: 'ERR047',
                                        message: 'The user is not active OR is blocked'
                                    }, user, req.userType);
                                } else {

                                    _this.PartnerModel.dbCollection.findOne({
                                        _id: req.locationId
                                    }, {
                                        is_active: 1
                                    }, function (err, partner) {
                                        if (!partner || !partner.is_active) {
                                            next({
                                                code: 'ERR046',
                                                message: 'This POS is not active OR is blocked'
                                            }, user, req.userType);
                                        } else {
                                            user.group = user.groupid;
                                            user.xgroups = user.xgroups || [];
                                            var ids = []
                                            for (var i = 0; user.brands && i < user.brands.length; i++) {
                                                ids.push(user.brands[i].brand_id + '')
                                                if (user.brands[i] && user.brands[i].brand_id && user.brands[i].brand_id.toString() == req.locationId.toString()) {
                                                    user.group = user.brands[i].groupid;
                                                    user.xgroups = user.brands[i].xgroups || [];
                                                    user.xpermissions = user.brands[i].xpermissions;
                                                }
                                            }
                                            if (ids.length) {
                                                user.contract = _this.getContractPartner(ids)
                                                next(err, user, req.userType);
                                            } else {
                                                next(err, user, req.userType);
                                            }
                                        }
                                    });


                                }
                            } else {
                                next(err, user, req.userType);
                            }

                        });
                    }


                }
            });
        } else {
            throw "Second parameter must be callback function"
        }
    },
    basicAuth: function (what, validateIP, next) {
        var me = this;
        if (typeof next == 'function') {
            var req = this.request,
                res = this.response,
                me = this;
            req.query = this.params.gpc;
            var auth = req.headers['authorization']; // auth is in base64(username:password)  so we need to decode the base64
            if (!auth) {
                return next({
                    code: 401,
                    message: Ext.D.t("api", 'Basic authentication required')
                }, null);
            } else if (auth) {
                var tmp = auth.split(' ');
                if (tmp.length == 2) {
                    var buf = new Buffer(tmp[1], 'base64');
                    var plain_auth = buf.toString();
                    var creds = plain_auth.split(':');
                    if (creds.length == 2) {
                        var clientId = creds[0];
                        var clientSecret = creds[1];
                        var ClientModel = me.src.db.collection('oauth_clients');
                        ClientModel.findOne({
                            client_id: clientId,
                            client_secret: clientSecret
                        }, {}, function (err, client) {
                            if (client) {
                                if (client.ip && validateIP) {
                                    client.ip = client.ip || '';
                                    var ipRangeCheck = require("ip-range-check");
                                    var clientIP = req.connection.remoteAddress || '';
                                    var whiteIpList = client.ip.toString().split(';');
                                    if (ipRangeCheck(clientIP, whiteIpList)) {
                                        next(null, client);
                                    } else {
                                        next({
                                            code: 401,
                                            message: Ext.D.t("api", 'Unauthorised, IP Not allowed')
                                        }, null);
                                    }
                                } else {
                                    next(null, client);
                                }
                            } else {
                                next({
                                    code: 401,
                                    message: Ext.D.t("api", 'Unauthorised client id or secret is invalid')
                                }, null);
                            }
                        });
                    } else {
                        next({
                            code: 401,
                            message: Ext.D.t("api", 'Malformed basic authentication')
                        }, null);
                    }
                } else {
                    next({
                        code: 401,
                        message: Ext.D.t("api", 'Malformed basic authentication')
                    }, null);
                }
            } else {
                next({
                    code: 401,
                    message: Ext.D.t("api", 'Requires basic authentication')
                }, null);
            }
        } else {
            throw Ext.D.t("api", "Second parameter must be callback function")
        }
    },
    oauthserverInit: function () {
        var oauthserver = require('oauth2-server');
        var oauthModels = Ext.create('Crm.oauth.model.Model', {
            scope: this
        }).oauthModel();
        this.oauthModels = oauthModels;
        return oauthserver({
            model: oauthModels,
            grants: ['password', 'authorization_code', 'refresh_token'],
            debug: false,
            accessTokenLifetime: (60 * 60 * 24) * 30,
            clientIdRegex: '^[A-Za-z0-9-_\^]{5,30}$'
        });
    },
    charset: 'utf-8',
    http_codes: null,
    formatResponse: function (errs, data) {
        var _this = this;
        return {
            "header": {
                //"nonce": _this.req.body.header?_this.req.body.header.nonce||'1':'',
                "version": _this.req.body.header ? _this.req.body.header.version || '0.1' : '0.1',
                "txName": _this.req.body.header ? _this.req.body.header.txName : 'default',
                "lang": _this.req.body.header ? _this.req.body.header.lang || 'EN' : 'EN',
                "androidVersion": 1,
                "iosVersion": "1.0"
            },
            "result": {
                "status": data ? true : false,
                "message": data ? Ext.D.t("api", "Transaction completed successfully") : Ext.D.t("api", "Transaction Failed")
            },
            "responseData": data,
            "errors": errs
        }
    },
    sendData: function (data) {
        var _this = this;
        _this.res.setHeader('Content-Type', 'application/json');
        _this.res.setHeader('Access-Control-Allow-Origin', '*');
        _this.res.setHeader('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');        
        _this.res.setHeader('Access-Control-Allow-Headers', 'Content-Type,Authorization');
        data = JSON.stringify(_this.formatResponse(null, data));
        _this.res.writeHead(200);
        _this.res.write(data);
        _this.res.end();
        this.src.dbx.free(this.src.db);
    },
    sendError: function (code) {
        var _this = this;
        var data = '';
        _this.res.setHeader('Content-Type', 'application/json');
        _this.res.setHeader('Access-Control-Allow-Origin', '*');
        _this.res.setHeader('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');        
        _this.res.setHeader('Access-Control-Allow-Headers', 'Content-Type,Authorization');
        data = JSON.stringify(_this.formatResponse(_this.res.errors, null));
        _this.res.writeHead(200);
        _this.res.write(data);
        _this.res.end();
        this.src.dbx.free(this.src.db);
    },
    addErrorObj: function (err) {
        var _this = this;
        return _this.addErrorAndSend(err ? err.code : '', null, err ? err.errmsg || err.message : '');
    },
     getErrorObj: function (code, field, customMessage) {
        var error = {
            code: code || 'ERR000',
            message: customMessage || Ext.D.t('api', 'Unknown error'),
            //message:D.t(customMessage||'Unknown error'),
            field: field || 'common'
        };
        if (!customMessage && this.errorList[code]) {
            error.message = (error.code == 'ERR001' ? error.field + ' ' : '') + Ext.D.t('api', this.errorList[code]);
            //error.message=D.t(this.errorList[code]);
        }
        return error;
    },
    addError: function (code, field, customMessage) {        
        this.res.errors.push(this.getErrorObj(code, field, customMessage));
    },
    addErrorAndSend: function (code, field, customMessage, httpCode) {
        var _this = this;
        _this.addError(code, field, customMessage);
        _this.sendError(httpCode || 400);
    },
    errorList: require(__dirname + '/errorCodes.json'),
    validateRequest: function (requiredFields, reqData, alternate) {
        var _this = this;
        var reqData = reqData || _this.req.body.reqData;
        var isValid = true;
        for (var i = 0; i < requiredFields.length; i++) {
            var field = requiredFields[i];
            var requiredType = field.type.toString().trim().toUpperCase();
            var value = null;
            if (reqData[field.field] == null) {
                value = '';
            } else {
                value = reqData[field.field];
            }


            var actualType = (typeof reqData[field.field]).toString().toUpperCase();
            if (field.isRequired && (actualType == 'UNDEFINED' || value.toString().trim() == '')) {
                if (field.alternate) {
                    isValid = _this.validateRequest(Array.isArray(field.alternate) ? field.alternate : [field.alternate], null, field);
                } else {
                    _this.addError('ERR001', alternate ? field.field + ' or ' + alternate.field : field.field);
                    isValid = false;
                }
            }
            if (field.min && value.toString().trim().length < field.min && actualType != 'UNDEFINED') {
                _this.addError('ERR014', field.field, Ext.D.t("api", 'Value is too small, required length is {val}', {
                    val: field.min
                }));
                isValid = false;
            }
            if (field.max && value.toString().trim().length > field.max && actualType != 'UNDEFINED') {
                _this.addError('ERR015', field.field, Ext.D.t("api", 'Value is to large, required maximum length is {val}', {
                    val: field.max
                }));
                isValid = false;
            }
            switch (requiredType) {
                case "NUMBER":
                    if (isNaN(value) && actualType != 'UNDEFINED') {
                        _this.addError('ERR013', field.field, Ext.D.t("api", "Field must be type of {type}", {
                            type: requiredType.toLowerCase()
                        }));
                        isValid = false;
                    }
                    break;
                case "STRING":
                    if (actualType != requiredType && actualType != 'UNDEFINED') {
                        _this.addError('ERR013', field.field, Ext.D.t("api", "Field must be type of  {type}", {
                            type: requiredType.toLowerCase()
                        }));
                        isValid = false;
                    }
                    break;
                case "DATE":
                case "DATETIME":
                    if (value && isNaN(new Date(value).getTime())) {
                        _this.addError('ERR013', field.field, Ext.D.t("api", "Field must be type of  {type}", {
                            type: requiredType.toLowerCase()
                        }));
                        isValid = false;
                    }
                    break;
                case "ENUM":
                    if (field.isRequired && field.enumValues && field.enumValues.indexOf(value) < 0) {
                        _this.addError('ERR013', field.field, Ext.D.t("api", "Value of field must be in {val}", {
                            val: field.enumValues.join(', ')
                        }));
                        isValid = false;
                    }
                    break;
                case "FREEOBJECT":
                    if (actualType != requiredType && actualType != 'UNDEFINED') {
                        _this.addError('ERR013', field.field, Ext.D.t("api", "Field must be type of {val}", {
                            val: requiredType.toLowerCase()
                        }));
                        isValid = false;
                    }
                case "OBJECT":
                    if (actualType != requiredType && actualType != 'UNDEFINED') {
                        _this.addError('ERR013', field.field, Ext.D.t("api", "Field must be type of  {type}", {
                            type: requiredType.toLowerCase()
                        }));
                        isValid = false;
                    } else {
                        if (!_this.validateRequest(field.items, value))
                            isValid = false;
                    }
                    break;
                case "OBJECTARRAY":
                    if (actualType != requiredType && actualType != 'UNDEFINED') {
                        _this.addError('ERR013', field.field, Ext.D.t("api", "Field must be type of {type}", {
                            type: requiredType.toLowerCase()
                        }));
                    } else {
                        if (!_this.validateRequest(field.items, value))
                            isValid = false;
                    }
                    break;
                case "ARRAY":
                    if (!Array.isArray(value) && actualType != 'UNDEFINED') {
                        _this.addError('ERR013', field.field, Ext.D.t("api", "Field must be type of {type}", {
                            type: requiredType.toLowerCase()
                        }));
                        isValid = false;
                    }
                    break;
            }
        }
        return isValid;
    },
    getLoggedOauthUserID: function () {
        var _this = this;
        var userId = _this.req.user ? _this.req.user.id : '';
        if (_this.req.user && typeof _this.req.user.id == 'object') {
            var userId = _this.req.user.id.user._id || _this.req.user.id.user.id;
        }
        return userId;
    },
    getUserObjectId: function (asString) {
        var _this = this;
        var userId = _this.getLoggedOauthUserID();
        if (asString)
            return userId;
        return _this.src.db.fieldTypes.ObjectID.getValueToSave(null, userId);
    },
    objectId: function (id, asString) {
        var _this = this;
        if (asString) return id;
        return _this.src.db.fieldTypes.ObjectID.getValueToSave(null, id);
    },
    basicAuth: function (what, validateIP, next) {
        var _this = this;
        if (typeof next == 'function') {
            var req = this.request,
                res = this.response,
                _this = this;
            req.query = this.params.gpc;
            var auth = req.headers['authorization']; // auth is in base64(username:password)  so we need to decode the base64
            if (!auth) {
                return next({
                    code: 401,
                    message: Ext.D.t("api", 'Basic authentication required')
                }, null);
            } else if (auth) {
                var tmp = auth.split(' ');
                if (tmp.length == 2) {
                    var buf = new Buffer(tmp[1], 'base64');
                    var plain_auth = buf.toString();
                    var creds = plain_auth.split(':');
                    if (creds.length == 2) {
                        var clientId = creds[0];
                        var clientSecret = creds[1];
                        var ClientModel = _this.src.db.collection('oauth_clients');
                        ClientModel.findOne({
                            client_id: clientId,
                            client_secret: clientSecret
                        }, {}, function (err, client) {
                            if (client) {
                                if (client.ip && validateIP) {
                                    client.ip = client.ip || '';
                                    var ipRangeCheck = require("ip-range-check");
                                    var clientIP = req.connection.remoteAddress || '';
                                    var whiteIpList = client.ip.toString().split(';');
                                    if (ipRangeCheck(clientIP, whiteIpList)) {
                                        next(null, client);
                                    } else {
                                        next({
                                            code: 401,
                                            message: Ext.D.t("api", 'Unauthorised, IP Not allowed')
                                        }, null);
                                    }
                                } else {
                                    next(null, client);
                                }
                            } else {
                                next({
                                    code: 401,
                                    message: Ext.D.t("api", 'Unauthorised client id or secret is invalid')
                                }, null);
                            }
                        });
                    } else {
                        next({
                            code: 401,
                            message: Ext.D.t("api", 'Malformed basic authentication')
                        }, null);
                    }
                } else {
                    next({
                        code: 401,
                        message: Ext.D.t("api", 'Malformed basic authentication')
                    }, null);
                }
            } else {
                next({
                    code: 401,
                    message: Ext.D.t("api", 'Requires basic authentication')
                }, null);
            }
        } else {
            throw Ext.D.t("api", "Second parameter must be callback function")
        }
    },
    authoriseBasicClient: function (what, next) {
        this.basicAuth(what, false, next);
    },
    validateToken: function () {
        var _this = this;
        [
            function (next) {
                _this.basicAuth('client', true, next);
            },
            function (err, client) {
                if (client) {
                    var token = _this.req.body.reqData ? _this.req.body.reqData.token : '';
                    _this.oauthModels.getAccessToken(token, function (err, token) {
                        if (token) {
                            var userId = token.userId ? token.userId.id : '';
                            if (token.userId && token.userId.user && typeof token.userId.user == 'object') {
                                var userId = token.userId.user._id || token.userId.user.id;
                            }
                            _this.Members.findOne({
                                _id: userId
                            }, {}, function (e, d) {
                                if (d) {
                                    _this.sendData({
                                        accessToken: token.access_token,
                                        clientId: token.client_id,
                                        expires: token.expires,
                                        user: d
                                    });
                                } else {
                                    _this.addErrorObj({
                                        code: 0,
                                        message: 'Invalid token'
                                    });
                                }
                            })

                        } else {
                            _this.addErrorObj({
                                code: 0,
                                message: 'Invalid token'
                            });
                        }
                    })
                } else {
                    _this.addErrorObj(err);
                }
            }
        ].runEach();
    }
    
    ,callWrite: function(module, params, cb) {
        Ext.create(module, {scope: this}).write(params, cb, {add: true, modify: true})
        
    }
    
    ,getImagesArray: function(data, removeImg) {
        var me = this, k, out = [];
        for(var i = 0;i<5;i++) {
            k = 'img' + (i? i:'');
            if(data[k]) {
                out.push(me.prepImageUrl(data[k]));                
            }
            if(removeImg) delete data[k];
        }
        return out;
    }
    
    ,prepImageUrl: function(url) {
        if(url.charAt(0) == '/') return url.substr(1)
        return url;
    }
});