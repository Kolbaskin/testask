

Ext.define('Crm.REST.Base',{

    tryCount: 1

    ,debug: true  
    
    ,mixins: ["Core.Controller"]
    
    ,remoteAuthorise: function(params, cb) {
        var me = this;
        
        if(!params.login || !params.pass) {
            cb({code: 401, message:'Error login credentials'});
            return;
        }

        this.sendRequest('PGD', 'login', '/rest/user/login', {
            "username":params.login,
            "password":params.pass
        }, {
            'Content-Type': 'application/json',            
            'Authorization': 'Basic ' + new Buffer(me.config.restClient.appId + ':'  + me.config.restClient.apiKey).toString('base64')
        }, function(res) {
            if(res && res.responseData) {
                me.createSession(res.responseData, cb)                
            } else {
                cb({code: 401, message:'Error login credentials'});
            }
        })    
    }   
    
    ,sendRequest: function(apiName, txName, txApi, data, headers, cb) {
        var me = this,
            cb1 = function(res) {
                if(me.debug) {
                    console.log('RESPONSE ----------------');
                    console.log((res));
                }
                cb(res)
            };
        
        [
            function() {               
                var opt = {
                    url: me.config.processing[apiName].url + txApi,
                    method: "POST",                    
                    json: {
                       header:{
                          nonce: me.config.processing[apiName].nonce,
                          version: me.config.processing[apiName].version,
                          txName: txName,
                          lang: me.config.processing[apiName].lang //token //
                       },                          
                       reqData: data
                    }
                }
                if(headers) opt.headers = headers;

                if(me.debug) {
                    console.log('------------------------------------');
                    console.log('REQUEST to ',opt.url);
                    console.log('HEADERS:');
                    console.log(opt.headers);
                    console.log('BODY:');
                    console.log(opt.json);
                }
                
                require('request')(
                    opt
                    ,function(e,res) {
                        if(e) {
                            if(me.debug) console.log('error: ', e)
                            if(me.tryCount) {
                                setTimeout(function() {
                                    me.tryCount--;
                                    me.sendRequest(apiName, txName, txApi, data, headers, cb)
                                }, 1000)
                            }
                        } else
                            cb1(res.body)    
                    }
                )
            }
        ].runEach();
    }
    
    ,sendBinnaryRequest: function(apiName, txName, txApi, data, headers, cb) {
        var me = this,
            cb1 = function(res) {
                if(me.debug) console.log('res (',txName,'|', txApi,'):   ', JSON.stringify(res))
                cb(res)
            };
        
        [
            function() {      

                if(me.debug) console.log('req (',txName,'|',me.config.processing[apiName].url,txApi,'):  ', data)
                
                var opt = {
                    url: me.config.processing[apiName].url + txApi,
                    method: "POST",                    
                    body: data
                }
                if(headers) opt.headers = headers;
            
                require('request')(
                    opt
                    ,function(e,res) {
                        if(e) {
                            if(me.debug) console.log('error: ', e)
                            if(me.tryCount) {
                                setTimeout(function() {
                                    me.tryCount--;
                                    me.sendBinnaryRequest(apiName, txName, txApi, data, headers, cb)
                                }, 1000)
                            }
                        } else
                            cb1(res.body)    
                    }
                )
            }
        ].runEach();
    }
    
    ,createSession: function(params, cb) {
        var me = this
            ,token;       
            
        [
            function(next) {        
                require('crypto').randomBytes(me.config.token.len, function(ex, buf) {                    
                    next(buf.toString('hex'))
                })
            }
            ,function(token, next) {
                me.src.mem.set(
                    token, 
                    'Bearer ' + params.access_token,
                    function(er, res) {
                        cb(er, res=='STORED'? token:null);
                    },
                    params.expires_in
                );
            }
        ].runEach();
    }
    
    ,callPGDApi: function(uri, data, cb, txName, requireBarer) {
        var me = this;       
        if(requireBarer === null) requireBarer = true;
        [
            function(next) {              
                if(me.user && me.user.profile && me.user.profile.bearer) {
                    next(me.user.profile.bearer);
                } else        
                if(me.params && me.params.gpc && me.params.gpc.token) {
                    me.getBearer(me.params.gpc.token, next);
                } else {
                    if(requireBarer) 
                        cb({code: 1, mess: 'access token expired'});
                    else
                        next(null)
                }
            },
            function(bearer) {
                var headers = {}
                if(bearer) {
                    headers.Authorization = bearer;
                } else {
                    if(requireBarer) {
                        cb({code: 1, mess: 'access token expired'});
                        return;
                    }
                }
                if(txName) {
                    txName = txName.split('.')
                    txName = txName[txName.length - 1]
                }
                
                //console.log('bearer:', bearer)
                
                me.sendRequest('PGD', txName || 'apimethod', uri, data, headers, function(res) {
                    if(res && res.responseData) {
                        cb(null, res.responseData)
                    } else {
                        cb(res? res.result:null)
                    }
                })
            }
        ].runEach();
    }
    
    ,sendBinnaryData: function(uri, data, cb) {
        var me = this;        
        [
            function(next) {
                if(me.user && me.user.profile && me.user.profile.bearer) {
                    next(me.user.profile.bearer);
                } else        
                if(me.params && me.params.gpc && me.params.gpc.token) {
                    me.getBearer(me.params.gpc.token, next);
                } else {
                    cb({code: 1, mess: 'access token expired'});
                }
            },
            function(bearer) {
                if(!bearer) {
                    cb({code: 1, mess: 'access token expired'});
                    return;
                }

                me.sendBinnaryRequest('PGD', 'apimethod', uri, data, {
                    Authorization: bearer
                }, function(res) {
                    if(res && res.responseData) {
                        cb(null, res.responseData)
                    } else {
                        cb(res.result)
                    }
                })
            }
        ].runEach();
    }

    ,getBearer: function(token, cb) {       
        this.src.mem.get(token, function(err, bearer) {
            cb(bearer)
        })       
    }
    
    ,sendUploadedFile: function(uri, tmpFileName, cb) {
        var me = this, fs = require('fs'); 

        fs.readFile(tmpFileName, function(e,d) {

            me.sendBinnaryRequest('PGD', 'File', uri, d, null, function(res) {
                try{
                    res = JSON.parse(res);
                }catch(e) {
                    cb(null);
                    return;
                }
            
                cb(res.response || null);
                fs.unlink(tmpFileName, function() {})
            })
        })        
    }
    
    ,$uploadFile: function() {
        var me = this;
        me.headers = {}
        me.headCode =200
        me.headStatus = 'OK'
        me.sendUploadedFile('/Admin.Data.uploadFile/', 'tmp/' + me.params.tmpName, function(res) {
            me.sendJSON(res)
        })        
    }


})
