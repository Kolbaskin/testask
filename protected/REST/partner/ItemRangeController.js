Ext.define('Crm.REST.partner.ItemRangeController', {
    extend: "Crm.REST.RestBaseController",
    constructor: function () {
        this.callParent(arguments);
    },
    /*
    @author : Kaustubh 
    @date : 01 Aug 2017
    @function : apiItemRange
    @desc : Item Range  
    */
    apiSetItemRanges: function (cb) {
        var me = this;
        var reqBody = me.req.body.reqData,
            params = me.req.body.reqData;

        if (!me.user) {
            me.addErrorAndSend(401);
            return;
        }
        if (!me.user.pid) {
            me.addErrorAndSend(ERR015);
            return;
        }

        var ItemRangeModel = Ext.create('Crm.modules.itemRange.model.ItemRangeModel', {
            scope: me
        });
        ItemRangeModel.error = function (code, info) {
            return me.addErrorObj(code);
        };

        [
            function (err, client) {
                if (client) {
                    if (me.validateRequest([

                        {
                            type: 'ObjectID',
                            field: "catalogue_id",
                            isRequired: true,
                        },
                        {
                            type: "ARRAY", field: "item", item: [
                                {
                                    type: 'number',
                                    field: "from_quantity",
                                    isRequired: true
                                },
                                {
                                    type: 'number',
                                    field: "to_quantity",
                                    isRequired: true
                                },
                                {
                                    type: 'number',
                                    field: "donation_fixed_amount",
                                    isRequired: true
                                },
                                {
                                    type: 'number',
                                    field: "donation_percentage_amount",
                                    isRequired: true
                                },
                                {
                                    type: 'number',
                                    field: "follow_fixed_amount",
                                    isRequired: true
                                },
                                {
                                    type: 'number',
                                    field: "follow_percentage_amount",
                                    isRequired: true
                                }

                            ], isRequired: true
                        }
                    ]
                    )) {
                        [
                            function (next) {

                                var count = -1;
                                var floorvalue = 0, ceilValue = 0;
                                var planRangeFlag = 0; //set to zero initially  on any  range violation set to 1
                                //initial checks 
                                var maxQuantityAmount=9999999999;
                                reqBody.item = reqBody.item.sort(function (a, b) {
                                    return parseFloat(a.from_quantity) - parseFloat(b.from_quantity);
                                });
                                var lastLimitIndex=reqBody.item.length-1; // -1 because off by one values
                                if(reqBody.item[lastLimitIndex].to_quantity == 0){
                                    reqBody.item[lastLimitIndex].to_quantity = maxQuantityAmount;
                                }

                                reqBody.item.prepEach(function (r, next) {
                                    if (parseFloat(r.from_quantity) > parseFloat(r.to_quantity)) {
                                        planRangeFlag = 1;
                                        return me.addErrorAndSend('ERR037');
                                    }
                                    if (count == -1) {
                                        floorvalue = r.from_quantity;
                                        ceilValue = r.to_quantity;
                                        count++;
                                        planRangeFlag = 1;
                                    }
                                    else {
                                        if (parseFloat(r.from_quantity) > floorvalue && parseFloat(r.from_quantity) < ceilValue) {
                                            planRangeFlag = 1;
                                            return me.addErrorAndSend('ERR037');
                                        }
                                        if (parseFloat(r.from_quantity) < floorvalue) {
                                            planRangeFlag = 1;
                                            return me.addErrorAndSend('ERR037');
                                        }
                                        if (parseFloat(r.from_quantity) > ceilValue && parseFloat(r.from_quantity) != (parseFloat(ceilValue) + parseFloat(1))) {
                                            planRangeFlag = 1;
                                            return me.addErrorAndSend('ERR037');
                                        }
                                    }
                                    floorvalue = r.from_total;
                                    ceilValue = r.to_total;
                                    next(r);
                                }, function () {
                                    next(planRangeFlag)
                                });
                            },

                            function (flag) {
                                var abc = [];
                                if (flag == 1) {
                                    ItemRangeModel.dbCollection.find({
                                        catalogue_id: reqBody.catalogue_id,
                                        removed: '0'
                                    }, { _id: 1 }, function (err, data) {
                                        if (data && data.length > 0) {
                                            var params = [];
                                            data.prepEach(function (v, next) {
                                                params.push(v._id);
                                                next(v);
                                            }, function () {
                                            });
                                            ItemRangeModel.removeAction = 'delete';
                                            ItemRangeModel.remove(params, function (res) {

                                                if (res.success == true) {
                                                    reqBody.item.prepEach(function (d, next) {
                                                        ItemRangeModel.write({
                                                            "_id": me.Util.getObjectId(),
                                                            "catalogue_id": reqBody.catalogue_id,
                                                            "from_quantity": d.from_quantity,
                                                            "to_quantity": d.to_quantity,
                                                            "donation_fixed_amount": d.donation_fixed_amount,
                                                            "donation_percentage_amount": d.donation_percentage_amount,
                                                            "follow_fixed_amount": d.follow_fixed_amount,
                                                            "follow_percentage_amount": d.follow_percentage_amount
                                                        }, function (data) {
                                                            if (data.success) {
                                                                abc.push(data.record);
                                                                next(d);
                                                            } else {
                                                                return me.addErrorObj(data.error);
                                                            }
                                                        }, {
                                                                add: 1,
                                                                modify: 1
                                                            });
                                                    }, function () {
                                                        me.sendData(abc);
                                                    });
                                                }
                                            });
                                        } else {
                                            reqBody.item.prepEach(function (d, next) {
                                                ItemRangeModel.write({
                                                    "_id": me.Util.getObjectId(),
                                                    "catalogue_id": reqBody.catalogue_id,
                                                    "from_quantity": d.from_quantity,
                                                    "to_quantity": d.to_quantity,
                                                    "donation_fixed_amount": d.donation_fixed_amount,
                                                    "donation_percentage_amount": d.donation_percentage_amount,
                                                    "follow_fixed_amount": d.follow_fixed_amount,
                                                    "follow_percentage_amount": d.follow_percentage_amount
                                                }, function (data) {
                                                    if (data.success) {
                                                        abc.push(data.record);
                                                        next(d);
                                                    } else {
                                                        return me.addErrorObj(data.error);
                                                    }
                                                }, {
                                                        add: 1,
                                                        modify: 1
                                                    });
                                            }, function () {
                                                me.sendData(abc);
                                            });
                                        }
                                    });
                                }
                            }
                        ].runEach();
                    } else {
                        me.sendError();
                    }
                } else {
                    return me.addErrorObj(err);
                }
            }
        ].runEach();
    },

    apiGetItemRanges: function (cb) {

        var me = this;
        var reqBody = me.req.body.reqData,
            params = me.req.body.reqData;

        if (!me.user) {
            me.addErrorAndSend(401);
            return;
        }
        if (!me.user.pid) {
            me.addErrorAndSend(ERR015);
            return;
        }

        params.filters=[];
        params.filters.push({ value: params.catalogue_id, property: 'catalogue_id'});
        Ext.create('Crm.modules.itemRange.model.ItemRangeModel', { scope: this })
        .getData(params, function (res) {
        me.sendData(res);
        })
    }
});



