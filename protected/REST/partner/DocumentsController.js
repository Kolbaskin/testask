Ext.define('Crm.REST.partner.DocumentsController', {
    extend: "Crm.REST.RestBaseController"
    
    
    ,list: function() {       
        var me=this
            ,params = me.req.body.reqData;        
        if(!me.user) {
            me.addErrorAndSend(401);
            return;
        }
        if(!params.filters) params.filters = [];
        params.filters.push({value: me.user.pid, property: 'pid'});
        Ext.create('Crm.modules.docs.model.DocsModel', {scope: this})
        .getData(params, function(res) {
            me.sendData(res);   
        })
    }
    
    ,write: function() {       
        var me=this
            ,params = me.req.body.reqData;        
        if(!me.user) {
            me.addErrorAndSend(401);
            return;
        } 
        params.pid = me.user.pid;
        Ext.create('Crm.modules.docs.model.DocsModel', {scope: this})
        .write(params, function(res) {
            me.sendData(res);   
        },{add: true, modify: true})            

    }
    
    ,remove: function() {       
        var me=this
            ,params = me.req.body.reqData;        
        if(!me.user) {
            me.addErrorAndSend(401);
            return;
        } 
        params.pid = me.user.pid;
        Ext.create('Crm.modules.docs.model.DocsModel', {scope: this})
        .remove(params, function(res) {
            me.sendData(res);   
        })            

    }
    
    
    ,types: function() {       
        var me=this
            ,params = Ext.clone(me.req.body.reqData);        
        if(!me.user) {
            me.addErrorAndSend(401);
            return;
        }     
        Ext.create('Crm.modules.settings.model.DocTypeModel', {scope: this})
        .getData(params, function(res) {
            me.sendData(res);   
        })            

    }
    
    
    
})