Ext.define('Crm.REST.partner.BaseController', {
    extend: "Crm.REST.RestBaseController"
    
    
    ,profile: function(params, cb) {       
        var me=this;        
        if(!me.user) {
            me.addErrorAndSend(401);
            return;
        }        
        Ext.create('Crm.modules.organizations.model.FoundModel', {scope: this})
        .getData({filters:[{property:'_id', value: me.user.pid}]}, function(res) {
            if(res && res.list && res.list[0]) {
                me.user.organization = res.list[0];
            }
            me.sendData(me.user);            
        })
    }
    ,getPartnerProfile: function() {
        var me=this; 
        var params = {};
        if(!me.user) {
            me.addErrorAndSend(401);
            return;
        }
        var model=null;

        model=Ext.create('Crm.modules.organizations.model.FoundModel', {scope: this});
        var catController = Ext.create('Crm.REST.partner.CatalogueController', {scope: me});
        var CatalogueBlogs = Ext.create('Crm.modules.catalogueBlogs.model.CatalogueBlogsModel', {scope: me});
        model.getData({filters: [{property: '_id', value: me.user.pid}]}, function (res) {
            if (res && res.list && res.list[0]) {
                if(res.list[0].media_urls && typeof(res.list[0].media_urls)== "string")
                {
                res.list[0].media_urls=res.list[0].media_urls.split(",");
                }
                params.filters = [];
                params.filters.push({ value: me.user.pid, property: "catalogue_id" });
                CatalogueBlogs.getData(params, function (data) {
                    if (data && data.list && data.list.length>0) {
                        data.list.prepEach(function (d, next) {
                            catController.splitTagsUrls(d);
                            next(d);
                        },function()
                        {
                            res.list[0].blogs=data;
                            me.user.organization = res.list[0];
                            me.sendData(me.user.organization);
                        });
                    }
                    else
                        {
                            var blogs={};
                            blogs.list=[];
                            res.list[0].blogs=blogs;
                            me.user.organization = res.list[0];
                            me.sendData(me.user.organization);
                        
                        }
                });
            }else{
                me.addErrorAndSend('ERR039');
            }

        })
    }
    ,updatePartnerProfile: function() {
        var me=this;
        var reqData=me.req.body.reqData;
        if(!me.user) {
            me.addErrorAndSend(401);
            return;
        }
        if(!me.validateRequest([
                {type: 'ObjectID', field: "_id", isRequired: true}
            ])){
            return me.sendError();
        }
        if(reqData._id!==me.user.pid){
            return me.addErrorAndSend('ERR040');
        }

        if(reqData.media_urls && reqData.media_urls!=null && typeof(reqData.media_urls)=="object" )
        {
            var media="";
            reqData.media_urls.prepEach(function (e, next) {
                media+=e+",";
                next(e);
            }, function () {
                media = media.substring(0, media.length - 1);
                reqData.media_urls=media;
            });
        }
        var model=null;

        model=Ext.create('Crm.modules.organizations.model.FoundModel', {scope: this});

        model.write(reqData, function (res) {
            if (res && res.success) {
                me.sendData({});
            }else{
                me.sendError();
            }
        },{modify:true});
    }
})