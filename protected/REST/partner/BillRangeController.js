Ext.define('Crm.REST.partner.BillRangeController', {
    extend: "Crm.REST.RestBaseController",
    constructor: function () {
        this.callParent(arguments);
    },
    /*
    @author : Kaustubh 
    @date : 01 Aug 2017
    @function : apiBillRange
    @desc : Bill Range
    Edited on  01/08/2017 -- 1)added clauses to check if range is conflicting within other ranges
                             2)added clause to check if next input is missing range    
    */
    apiSetBillRanges: function (cb) {
        var me = this;
        var reqBody = me.req.body.reqData,
            params = me.req.body.reqData;

        if (!me.user) {
            me.addErrorAndSend(401);
            return;
        }
        if (!me.user.pid) {
            me.addErrorAndSend(ERR015);
            return;
        }

        var BillRangeModel = Ext.create('Crm.modules.billRange.model.BillRangeModel', {
            scope: me
        });
        BillRangeModel.error = function (code, info) {
            return me.addErrorObj(code);
        };

        [
            function (err, client) {
                if (client) {
                    if (me.validateRequest([
                        {
                            type: "ARRAY", field: "item", item: [
                                // {
                                //     type: 'ObjectID',
                                //     field: "partner_id",
                                //     isRequired: true
                                // },
                                {
                                    type: 'number',
                                    field: "from_total",
                                    isRequired: true
                                },
                                {
                                    type: 'number',
                                    field: "to_total",
                                    isRequired: true
                                },
                                {
                                    type: 'number',
                                    field: "donation_fixed_amount",
                                    isRequired: true
                                },
                                {
                                    type: 'number',
                                    field: "donation_percentage_amount",
                                    isRequired: true
                                },
                                {
                                    type: 'number',
                                    field: "follow_fixed_amount",
                                    isRequired: true
                                },
                                {
                                    type: 'number',
                                    field: "follow_percentage_amount",
                                    isRequired: true
                                }

                            ], isRequired: true
                        }
                    ]
                    )) {
                        [
                            function (next) {
                                
                                var count = -1;
                                var floorvalue = 0, ceilValue = 0;
                                var planRangeFlag = 0; //set to zero initially  on any  range violation set to 1
                                var maxBillAmount=999999999999999;
                                
                                
                                //initial checks 
                                reqBody.item=reqBody.item.sort(function(a, b) {
                                        return parseFloat(a.from_total) - parseFloat(b.from_total);
                                    });
                                var lastLimitIndex=reqBody.item.length-1; // -1 because off by one values
                                //Start Vaibhav Mali 27-sept-2017
                                if(reqBody.item[lastLimitIndex].to_total == 0 ||  reqBody.item[lastLimitIndex].to_total == null ||  reqBody.item[lastLimitIndex].to_total == ""){
                                reqBody.item[lastLimitIndex].to_total=maxBillAmount;
                                }
                                //End Vaibhav Mali.
                                reqBody.item.prepEach(function (r, next) {
                                    // if to_total is less than from total throw error
                                    if (parseFloat(r.from_total) > parseFloat(r.to_total)) {
                                              planRangeFlag = 1;
                                        return me.addErrorAndSend('ERR037');
                                    }
                                    // very first range ,no need to compare with previous to_total/from_total
                                    if (count == -1) {
                                        
                                        floorvalue = r.from_total;
                                        ceilValue = r.to_total;
                                        count++;
                                        planRangeFlag = 1;
                                    }
                                    else {
                                    // for every range after first range  
                                    // check if from_total is between previous range from_total and to_total
                                    
                                        if (parseFloat(r.from_total) > floorvalue && parseFloat(r.from_total) < ceilValue) {
                                            planRangeFlag = 1;
                                            return me.addErrorAndSend('ERR037');
                                        }
                                    // check if from_total is less than previous from_total i.e floorValue
                                        if (parseFloat(r.from_total) < floorvalue) {
                                            planRangeFlag = 1;
                                            return me.addErrorAndSend('ERR037');
                                        }
                                    // check if from_total is greater than previous to_total 
                                    // and is exaclty equals to summation of previous to_total+1 
                                    // to maintain range    
                                        if (parseFloat(r.from_total) > ceilValue && parseFloat(r.from_total) != (parseFloat(ceilValue) + parseFloat(1))) {
                                            planRangeFlag = 1;
                                            return me.addErrorAndSend('ERR037');
                                        }
                                    }
                                    floorvalue = r.from_total;
                                    ceilValue = r.to_total;
                                    next(r);
                                }, function () {
                                    next(planRangeFlag)
                                });
                            },

                            function (flag) {
                                var abc = [];
                                //flag =1 ---> all ranges are valid so continue
                                if (flag == 1) {
                                    //find if there are already bill ranges
                                    BillRangeModel.dbCollection.find({
                                        partner_id: me.user.pid,
                                        removed: '0'
                                    }, { _id: 1 }, function (err, data) {
                                        if (data && data.length > 0) {
                                            // it means bill ranges are already defined for current partner
                                            // remove all bill ranges first
                                            var params = [];
                                            data.prepEach(function (v, next) {
                                                params.push(v._id);
                                                next(v);
                                            }, function () {
                                            });
                                            // set remove action to delete or anything else than mark before remove action
                                            // pass only array of _ids to remove method 
                                            BillRangeModel.removeAction = 'delete';
                                            BillRangeModel.remove(params, function (res) {
                                                if (res.success == true) {
                                                    // success--> this means all previous ranges are removed now insert all new ranges
                                                    reqBody.item.prepEach(function (d, next) {
                                                        BillRangeModel.write({
                                                            "_id": me.Util.getObjectId(),
                                                            "partner_id": me.user.pid,
                                                            "from_total": d.from_total,
                                                            "to_total": d.to_total,
                                                            "donation_fixed_amount": d.donation_fixed_amount,
                                                            "donation_percentage_amount": d.donation_percentage_amount,
                                                            "follow_fixed_amount": d.follow_fixed_amount,
                                                            "follow_percentage_amount": d.follow_percentage_amount
                                                        }, function (data) {
                                                            if (data.success) {
                                                                abc.push(data.record);
                                                                next(d);
                                                            } else {
                                                                return me.addErrorObj(data.error);
                                                            }
                                                        }, {
                                                                add: 1,
                                                                modify: 1
                                                            });
                                                    }, function () {
                                                        me.sendData(abc);
                                                    });
                                                }
                                            });
                                        } else {
                                            // this means there are no ranges defined for current partner
                                            // insert all new ranges 
                                            reqBody.item.prepEach(function (d, next) {
                                                BillRangeModel.write({
                                                    "_id": me.Util.getObjectId(),
                                                    "partner_id": me.user.pid,
                                                    "from_total": d.from_total,
                                                    "to_total": d.to_total,
                                                    "donation_fixed_amount": d.donation_fixed_amount,
                                                    "donation_percentage_amount": d.donation_percentage_amount,
                                                    "follow_fixed_amount": d.follow_fixed_amount,
                                                    "follow_percentage_amount": d.follow_percentage_amount
                                                }, function (data) {
                                                    if (data.success) {
                                                        abc.push(data.record);
                                                        next(d);
                                                    } else {
                                                        return me.addErrorObj(data.error);
                                                    }
                                                }, {
                                                        add: 1,
                                                        modify: 1
                                                    });
                                            }, function () {
                                                me.sendData(abc);
                                            });
                                        }
                                    });
                                }
                            }
                        ].runEach();
                    } else {
                        me.sendError();
                    }
                } else {
                    return me.addErrorObj(err);
                }
            }
        ].runEach();
    },

    /*
    @author : Kaustubh 
    @date : 01 Aug 2017
    @function : apiGetBillRange
    @desc : get all Bill Ranges for partner

    */
    apiGetBillRanges: function (cb) {
    var me = this;
        var reqBody = me.req.body.reqData,
            params = me.req.body.reqData;

        if (!me.user) {
            me.addErrorAndSend(401);
            return;
        }
        if (!me.user.pid) {
            me.addErrorAndSend(ERR015);
            return;
        }

        // var params={};
        // params.filters = [];

        // params.filters.push({value: me.user.pid, property: 'partner_id'});
        // Ext.create('Crm.modules.billRange.model.BillRangeModel', {scope: this})
        // .getData(params, function(res) {
        //     me.sendData(res);   
        // })
        var response={};
        var dist=[];
        var total=0;
        var BillRangeModel = Ext.create('Crm.modules.billRange.model.BillRangeModel', {
            scope: me
        });
        BillRangeModel.error = function (code, info) {
            return me.addErrorObj(code);
        }
        BillRangeModel.dbCollection.find({
           partner_id:me.user.pid
           }, {}, function (e, res) {
               total=res.length;
               response.total=total;
               response.list=res;
               me.sendData(response);

        });

    }
});