Ext.define('Crm.REST.partner.CatalogueController', {
    extend: "Crm.REST.RestBaseController",
    constructor: function () {
        this.callParent(arguments);
        this.initModels();
    },
    initModels: function () {
        var me = this;
        me.CatalogueModel = Ext.create('Crm.modules.catalogue.model.CatalogueModel', {
            scope: me
        });
        me.CatalogueModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        me.CharityInfoModel = Ext.create('Crm.modules.charityCatalogue.model.CharityCatalogueInfo', {
            scope: me
        });
        me.CharityInfoModel.error = function (code, info) {
            return me.addErrorObj(code);
        };


    },
    tagsHandler: function (data) {
        var me = this;
        var TagsModel = Ext.create('Crm.modules.tags.model.TagsModel', {
            scope: me
        });
        TagsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        data.prepEach(function (r, next) {
            TagsModel.dbCollection.findOne({
                name: r
            }, { _id: 1, hits: 1 }, function (err, res) {
                if (res) {

                    var count;
                    if (parseInt(res.hits) == 0 || res.hits == null) {
                        count = 1;
                    }
                    else {
                        count = parseInt(res.hits) + 1;
                    }
                    TagsModel.write({
                        "_id": res._id,
                        "hits": count
                    }, function (data) {
                        if (data.success) {
                        } else {
                        }
                    }, {
                            add: 1,
                            modify: 1
                        });
                    next(r);
                } else {
                    TagsModel.write({
                        "_id": me.Util.getObjectId(),
                        "hits": 1,
                        "name": r
                    }, function (data) {
                        if (data.success) {

                        } else {

                        }
                    }, {
                            add: 1,
                            modify: 1
                        });
                    next(r);
                }
            });
        }, function () {
        });
    },
    getTags: function () {
        var me = this,
            params = me.req.body.reqData;
        // if (!me.user) {
        //     me.error(401);
        //     return;
        // }
        params.fieldSet = ['name'];
        if (!params.sorters) params.sorters = [];
        params.sorters.push({ direction: 'DSC', property: 'hits' });
        Ext.create('Crm.modules.tags.model.TagsModel', { scope: this })
            .getData(params, function (res) {
                me.sendData(res);
            });

    },
    // commented since we are not using this functionaloty currently
    // searchByTags: function () {
    //     var me = this,
    //          params = me.req.body.reqData;
    //     if (!me.user) {
    //         me.error(401);
    //         return;
    //     }
    //     //params.fieldSet = ['name'];

    //     if (!params.sorters) params.sorters = [];
    //     params.sorters.push({ direction: 'DSC', property: 'hits' });
    //     Ext.create('Crm.modules.tags.model.TagsModel', { scope: this })
    //         .getData(params, function (res) {
    //             me.sendData(res);
    //         });

    // },

    //Provide all charity catalogues with additional information like type of item donation amount needed donation amount collected
    list: function () {
     //Vaibhav Mali 26 Sept 2017 Added placeholders.
     var me = this,
     params = me.req.body.reqData;
     var id;
     var skip = params.startIndex && /^[0-9]{1,}$/.test(params.startIndex + '') ? parseInt(params.startIndex) : 0;
     var limit = params.numOfRecords && /^[0-9]{1,}$/.test(params.numOfRecords + '') ? parseInt(params.numOfRecords) : 50;
     var topCatalogues = {};
     var pholders = [];
     queryParams = [];
     // if (!me.user) {
     //     me.error(401);
     //     return;
     // }
     if (!params.filters) params.filters = [];

     //Kaustubh 12-09-2017
     if (me.user) {
         if (me.user.user_type == me.config.endUser && me.user.follow && (me.user.follow != null || me.user.follow != "" || me.user.follow != "0")) {
             id = me.user._id;
         }
         else {
             id = me.user.pid;
         }
         var total;
         var sql = "select cat._id, cat.pid, cat.category, cat.name, cat.indx, cat.descript, cat.active, cat.sku, cat.document_id, cat.tags, cat.catalogue_urls, cat.item_type, cat.donation_amount_needed, cat.donation_amount_collected, cat.pay_now, cat.priority, fnd._id as fndid, fnd.name as fndname, fnd.logo_url as fndlogourl from pgd_catalogue_publ cat"
         sql += " left join pgd_foundations fnd on (cat.pid=fnd._id)"
         pholders.push(1);
         sql += " where cat.removed !=$"+pholders.length
         pholders.push(0);
         sql += " and cat.active !=$"+pholders.length
         sql += " and cat._id in "
         sql += " (select pgd_catalogue_id from charity_plan_details where charity_plan_id in"
         if (me.user.user_type != me.config.endUser) {
             pholders.push(id);
             sql += " (select _id from charity_plan where partner_user_id=$"+pholders.length
             pholders.push('A');
             sql += "and plan_type in ($"+pholders.length
             pholders.push('E');
             sql += ","+"$"+pholders.length+")));"
         }
         else {
             pholders.push(id);
             sql += " (select _id from charity_plan where partner_user_id=$"+pholders.length
             pholders.push('A');
             sql +=   "and plan_type = $"+pholders.length+"));"
         }
         me.src.db.query(sql, pholders, function (e, det) {

             if (det && det.length > 0) {
                 total = det.length;
                 det.prepEach(function (k, next) {
                     k = me.splitTagsUrls(k);
                     k = me.reFormatOld(k)
                     next(k);
                 }, function () {
                     topCatalogues.total = total;
                     topCatalogues.list = det;
                 });
             }
             else {
                 //return me.addErrorAndSend('ERR052');
                 topCatalogues.total = total;
                 topCatalogues.list = [];
                 //    me.sendData(resp);
             }
         });

     }
     //kaustubh end    
     //if its not End user provide only catalogue that belongs to that partner/charity
     var ids = [], itemInfoNo = -1;
     var CatalogueModel = Ext.create('Crm.modules.catalogue.model.CataloguePublModel', {
         scope: me
     });
     CatalogueModel.error = function (code, info) {
         return me.addErrorObj(code);
     };

     var catpholders = [];

     var placeHolders = [], pidStore = [], total = 0, catalogue = {}, resp = {};
     var catalogueSql = "select cat._id, cat.pid, cat.category, cat.name, cat.indx, cat.descript, cat.active, cat.sku, cat.document_id, cat.tags, cat.catalogue_urls, cat.item_type, cat.donation_amount_needed, cat.donation_amount_collected, cat.pay_now, cat.priority, fnd._id as fndid, fnd.name as fndname, fnd.logo_url as fndlogourl, fnd.address from " + CatalogueModel.collection + " cat";
     catpholders.push(1);
     catalogueSql += " inner join pgd_foundations fnd on (cat.pid=fnd._id and fnd.active = $"+catpholders.length;
     catpholders.push(0);
     catalogueSql += " and fnd.removed = $"+catpholders.length;
     catpholders.push(1);
     catalogueSql += " and fnd.orgtype = $"+catpholders.length+") ";
     catpholders.push(1);
     catalogueSql += " where cat.removed != $"+catpholders.length;
     catpholders.push(0);
     catalogueSql += " and cat.active != $"+catpholders.length;

     //Vaibhav Mali -- 23 Sept.2017 --- Start
     if(params.item_type){
        if(params.item_type.length > 0){
         catpholders.push(params.item_type.join('\',\'') );
          catalogueSql += " and cat.item_type in ($" +catpholders.length + ")" ;
        }
     }
     //Vaibhav Mali--- End

     //kaustubh -- 12/09/2017 skip catalogues here which are already in users plan
     if (me.user && me.user.pid) {
         // if (me.user.user_type == me.config.partnerUser) {
         //     catalogueSql += " and cat._id not in"
         //     catalogueSql += " (select pgd_catalogue_id from charity_plan_details where charity_plan_id in"
         //     catalogueSql += " (select _id from charity_plan where partner_user_id= '" + me.user.pid + "'))"
         // }
         if (me.user.user_type == me.config.endUser) {

             catalogueSql += " and cat._id not in"
             catalogueSql += " (select pgd_catalogue_id from charity_plan_details where charity_plan_id in"
             catpholders.push(me.user._id);
             catalogueSql += " (select _id from charity_plan where partner_user_id= $" +catpholders.length;
             catpholders.push('A');
             catalogueSql += " and plan_type = $"+ catpholders.length +"))";
         }
         else {

         }
     }
     //end 12/09/2017 kaustubh
     catpholders.push(parseInt(skip));
     offsetSql = " offset $" + catpholders.length;
     catpholders.push(parseInt(limit));
     offsetSql += " limit $" + catpholders.length;
     var tempTag = "";
     if (params.tags != undefined && Array.isArray(params.tags) && params.tags.length > 0) {
         tempTag += "'%(";
         for (i = 0; i < params.tags.length; i++) {
             if (i > 0) {
                 tempTag += "|";
             }

            // catpholders.push(params.tags[i].toLowerCase());
          //   tempTag += "($" + catpholders.length + ")";
             tempTag += params.tags[i].toLowerCase();
         }
         tempTag += ")%'";
         placeHolders.push(tempTag);
         tplTags = " and " + "( lower(cat.descript) similar to " + tempTag + " ";
         tplName = " or lower(cat.name) similar to " + tempTag + " ";
         tplAdd = " or lower(fnd.address) similar to " + tempTag + " )";
         catalogueSql += tplTags + tplName + tplAdd;
     }

     // catalogueSql+=" and pid in (\'"+pidStore.join('\',\'')+"')";

     catalogueSql += offsetSql;
     me.src.db.query(catalogueSql, catpholders, function (e, det) {
         if (det && det.length > 0) {
             total = det.length;
             det.prepEach(function (k, next) {
                 k = me.splitTagsUrls(k);
                 k = me.reFormatOld(k)
                 next(k);
             }, function () {
                 resp.topCatalogues = topCatalogues;
                 catalogue.total = total;
                 catalogue.list = det;
                 resp.catalogues = catalogue;
                 me.sendData(resp);
             });
         }
         else {
             //return me.addErrorAndSend('ERR052');
             resp.topCatalogues = topCatalogues;
             catalogue.total = total;
             catalogue.list = det;
             resp.catalogues = catalogue;
             me.sendData(resp);
         }
     });
    },

    write: function () {
        var me = this,
            params = me.req.body.reqData;
        if (!me.user) {
            me.error(401);
            return;
        }
        params.pid = me.user.pid;

        Ext.create('Crm.modules.catalogue.model.CatalogueModel', { scope: this })
            .write(params, function (res) {
                me.sendData(res);
            }, { add: true, modify: true });

    },

    categories: function () {
        var me = this,
            params = Ext.clone(me.req.body.reqData);
        if (!me.user) {
            me.error(401);
            return;
        }
        Ext.create('Crm.modules.catalogue.model.GroupsModel', { scope: this })
            .getData(params, function (res) {
                me.sendData(res);
            });

    },

    images: function () {
        var me = this,
            params = me.req.body.reqData;
        if (!me.user) {
            me.error(401);
            return;
        }
        if (!params.filters) params.filters = [];
        params.filters.push({ value: me.user.pid, property: 'pid' });

        Ext.create('Crm.modules.images.model.ImagesModel', { scope: this })
            .getData(params, function (res) {
                me.sendData(res);
            });
    },
    imageWrite: function () {
        var me = this,
            params = me.req.body.reqData;
        if (!me.user) {
            me.error(401);
            return;
        }

        Ext.create('Crm.modules.images.model.ImagesModel', { scope: this })
            .write(params, function (res) {
                me.sendData(res);
            }, { add: true, modify: true });

    },
    /*
  @author : Mamta 
  @date : 5 July 2017
  @function : apiItemInformation
  @desc : Item Information 
  */
    apiItemInformation: function (cb) {
        var me = this;
        var reqBody = me.req.body.reqData;
        if (me.req.method == "OPTIONS") {
            me.sendData(data);
        }
        var tagArr = [];
        if (!me.user) {
            me.error(401);
            return;
        }
        if (!me.user.pid) {
            me.error(401);
            return;
        }
        var finalString = "";
        var CharitycatinfoModel = Ext.create('Crm.modules.charityCatInfo.model.CharityCatInfoModel', {
            scope: me
        });
        CharitycatinfoModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var CatalogueModel = Ext.create('Crm.modules.catalogue.model.CatalogueModel', {
            scope: me
        });
        CatalogueModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var PgdUtils = Ext.create('Crm.REST.util.PgdUtils', {
            scope: me
        });
        PgdUtils.error = function (code, info) {
            return me.addErrorObj(code);
        };
        [
            function (err, client) {
                if (client) {
                    if (me.validateRequest([{
                        type: "ARRAY", field: "item", item: [
                            // {
                            //     type: 'string',
                            //     field: "category",
                            //     isRequired: true
                            // },
                            {
                                type: 'string',
                                field: "name",
                                isRequired: true
                            },
                            {
                                type: 'int',
                                field: "indx",
                                isRequired: true
                            },
                            {
                                type: 'string',
                                field: "descript",
                                isRequired: true
                            },
                            {
                                type: 'int',
                                field: "active",
                                isRequired: true
                            },
                            {
                                type: 'string',
                                field: "sku",
                                isRequired: true
                            }
                            // ,
                            // {
                            //     type: 'string',
                            //     field: "tags",
                            //     isRequired: false
                            // }
                        ], isRequired: true
                    }
                    ]
                    )) {
                        [
                            function (next) {
                                if (me.req.body.header.nonce) {
                                    var isDuplicateRequest = PgdUtils.checkDuplicateRequest(me.req.body.header.nonce);
                                    if (isDuplicateRequest) {
                                        var checkReqStatus = PgdUtils.getReqStatusFromNonce(me.req.body.header.nonce);
                                        if (checkReqStatus == me.config.statusSuccess) {
                                            var response = PgdUtils.getReqResFromNonce(me.req.body.header.nonce);
                                            me.sendData(response);
                                        }
                                        else if (checkReqStatus == me.config.statusFailed) {
                                            me.sendError();
                                        }
                                        else {
                                            setTimeout(function () {
                                                var checkReqStatus = PgdUtils.getReqStatusFromNonce(me.req.body.header.nonce);
                                                if (checkReqStatus = me.config.statusSuccess) {
                                                    var response = PgdUtils.getReqResFromNonce(me.req.body.header.nonce);
                                                    me.sendData(response);
                                                }
                                                else {
                                                    me.sendError();
                                                }
                                            }, 15000);

                                        }
                                    }
                                    else {
                                        next();
                                    }
                                }
                                else {
                                    return me.addErrorAndSend('ERR050', 'NOUNCE');
                                }
                                //End edit by Vaibhav Vaidya
                            },
                            function (next) {
                                var resp = {};
                                var abc = [];
                                var abc1 = [];

                                var faildUpdate = [];
                                reqBody.item.prepEach(function (r, next) {
                                    var resp = [];
                                    var tagString = "";
                                    //update tags
                                    // if(r.tags){
                                    //     r.tags.prepEach(function (k, next) {
                                    //     tagArr.push(k);
                                    //     if (tagString == "" && tagString != null) {
                                    //         tagString = tagString + k;
                                    //     }
                                    //     else {
                                    //         tagString = tagString + "," + k;
                                    //     }
                                    //     next(k);
                                    // }, function () {
                                    // });}

                                    var DocsModel = Ext.create('Crm.modules.file.model.FilesModel', {
                                        scope: me
                                    });

                                    DocsModel.error = function (code, info) {
                                        return me.addErrorObj(code);
                                    };

                                    //var hostname = me.req.headers.host;
                                    var path = '/api/rest/refdata/download-file?id=';
                                    DocsModel.dbCollection.find({
                                        group_id: r.document_id
                                    }, { _id: 1 }, function (err, d) {
                                        if (d && d.length > 0) {
                                            d.prepEach(function (e, next) {
                                                var url = path + e._id;
                                                if (finalString != "") {
                                                    finalString = finalString + "," + url;
                                                }
                                                else {
                                                    finalString = finalString + url;
                                                }
                                                resp.push(url);
                                                next(e);
                                            }, function () {
                                                CatalogueModel.dbCollection.findOne({
                                                    pid: me.user.pid,
                                                    sku: r.sku
                                                }, {}, function (err, data) {
                                                    if (!data) {
                                                        CatalogueModel.write({
                                                            "_id": me.Util.getObjectId(),
                                                            "category": r.category,
                                                            "pid": me.user.pid,
                                                            "name": r.name,
                                                            "indx": r.indx,
                                                            "descript": r.descript,
                                                            "active": r.active,
                                                            "document_id": r.document_id,
                                                            "tags": tagString,
                                                            "sku": r.sku,
                                                            "catalogue_urls": finalString
                                                        },
                                                            function (data) {
                                                                if (data.success) {
                                                                    abc.push(data.record);
                                                                    next(r);
                                                                } else {
                                                                    return me.addErrorObj(data.error);
                                                                }
                                                            }, {
                                                                add: 1,
                                                                modify: 1
                                                            });
                                                    } else {
                                                        faildUpdate.push(r);
                                                        next(r);
                                                    }
                                                });
                                            });

                                        } else {
                                            CatalogueModel.dbCollection.findOne({
                                                pid: me.user.pid,
                                                sku: r.sku
                                            }, {}, function (err, data) {
                                                if (!data) {
                                                    CatalogueModel.write({
                                                        "_id": me.Util.getObjectId(),
                                                        "category": r.category,
                                                        "pid": me.user.pid,
                                                        "name": r.name,
                                                        "indx": r.indx,
                                                        "descript": r.descript,
                                                        "active": r.active,
                                                        // "document_id": r.document_id,
                                                        "tags": tagString,
                                                        "sku": r.sku,
                                                        //"catalogue_urls": finalString
                                                    },
                                                        function (data) {
                                                            if (data.success) {

                                                                abc.push(data.record);
                                                                next(r);
                                                            } else {
                                                                return me.addErrorObj(data.error);
                                                            }
                                                        }, {
                                                            add: 1,
                                                            modify: 1
                                                        });
                                                } else {
                                                    faildUpdate.push(r);
                                                    next(r);
                                                }
                                            });
                                        }
                                    });
                                }, function () {
                                    resp.success = abc;
                                    resp.failed = faildUpdate;
                                    PgdUtils.maintainReqStatus(me.req.body.header.nonce, me.config.statusSuccess);
                                    PgdUtils.maintainNonceReqAndRes(me.req.body.header.nonce, resp);
                                    next(resp);
                                }
                                );
                            },
                            function (data) {
                                me.sendData(data);
                                //commented since we need count
                                // tagArr = tagArr.reverse().filter(function (e, i, tagArr) {
                                //     return tagArr.indexOf(e, i+1) === -1;
                                // }).reverse();
                                // send call to store tags after whole processing
                                //me.tagsHandler(tagArr);
                            }
                        ].runEach();

                    } else {
                        PgdUtils.maintainReqStatus(nonce, me.config.statusFailed);
                        me.sendError();
                    }
                } else {
                    return me.addErrorObj(err);
                }
            }
        ].runEach();
    },

    /*
    @author : Mamta
    @date : 11th july 2017
    @function : get
    @desc : retrieves List of items
    @Modification: Vaibhav Vaidya, added retrieval by partnerId. Added filtering. 26th July.
                    Kaustubh Thube --- get data through me.user.pid method
    */

    apiItemsDetails: function (cb) {
        var me = this;
        if (!me.user) {
            me.error(401);
            return;
        }
        if (!me.user.pid) {
            me.error(401);
            return;
        }

        var CatalogueModel = Ext.create('Crm.modules.catalogue.model.CatalogueModel', {
            scope: me
        });
        CatalogueModel.error = function (code, info) {
            return me.addErrorObj(code);
        };


        var category = -1;

        for (var i = 0; i < CatalogueModel.fields.length; i++) {
            if (CatalogueModel.fields[i].name == "category") {
                category = i;
            }
        }

        CatalogueModel.fields[category].bindTo = { collection: 'pgd_cataloguecategories', keyField: '_id', fields: { name: 1 } };
        var params = {};
        if (!params.filters) params.filters = [];
        params.filters.push({ value: me.user.pid, property: 'pid' });
        Ext.create('Crm.modules.catalogue.model.CatalogueModel', { scope: this })
            .getData(params, function (res) {
                me.sendData(res);
            });
    },

    /*
        @author Mamta
        @date 6th July 2017
        @function apiUpdateInfoDetails
        @desc Function to update item details ------ for update records ---
        updated --- 3 aug 2017
        */
    apiUpdateInfoDetails: function () {
        var me = this;
        var reqBody = me.req.body.reqData;
        var CatalogueModel = Ext.create('Crm.modules.catalogue.model.CatalogueModel', {
            scope: me
        });
        CatalogueModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var PgdUtils = Ext.create('Crm.REST.util.PgdUtils', {
            scope: me
        });
        PgdUtils.error = function (code, info) {
            return me.addErrorObj(code);
        };
        [
            function (err, client) {
                if (client) {
                    if (me.validateRequest([

                        {
                            type: 'string',
                            field: "category",
                            isRequired: false
                        },
                        {
                            type: "ARRAY", field: "item", item: [


                                {
                                    type: 'string',
                                    field: "name",
                                    isRequired: false
                                },
                                {
                                    type: 'int',
                                    field: "indx",
                                    isRequired: false
                                },
                                {
                                    type: 'string',
                                    field: "descript",
                                    isRequired: false
                                },
                                {
                                    type: 'int',
                                    field: "active",
                                    isRequired: false
                                }

                            ], isRequired: true
                        }

                    ]
                    )) {
                        [
                            function (next) {
                                if (me.req.body.header.nonce) {
                                    var isDuplicateRequest = PgdUtils.checkDuplicateRequest(me.req.body.header.nonce);
                                    if (isDuplicateRequest) {
                                        var checkReqStatus = PgdUtils.getReqStatusFromNonce(me.req.body.header.nonce);
                                        if (checkReqStatus == me.config.statusSuccess) {
                                            var response = PgdUtils.getReqResFromNonce(me.req.body.header.nonce);
                                            me.sendData(response);
                                        }
                                        else if (checkReqStatus == me.config.statusFailed) {
                                            me.sendError();
                                        }
                                        else {
                                            setTimeout(function () {
                                                var checkReqStatus = PgdUtils.getReqStatusFromNonce(me.req.body.header.nonce);
                                                if (checkReqStatus = me.config.statusSuccess) {
                                                    var response = PgdUtils.getReqResFromNonce(me.req.body.header.nonce);
                                                    me.sendData(response);
                                                }
                                                else {
                                                    me.sendError();
                                                }
                                            }, 15000);

                                        }
                                    }
                                    else {
                                        next();
                                    }
                                }
                                else {
                                    return me.addErrorAndSend('ERR050', 'NOUNCE');
                                }
                                //End edit by Vaibhav Vaidya
                            },
                            function (next) {
                                CatalogueModel.dbCollection.find({
                                    _id: reqBody._id
                                }, {
                                    }, function (err, data) {
                                        if (data) {
                                            next(data);
                                        } else { return me.addErrorObj(data.error); }
                                    });
                            },
                            function (user, next) {
                                var pgdutil = Ext.create('Crm.REST.util.PgdUtils', {
                                    scope: me
                                });
                                pgdutil.returnUrlString(user[0].document_id, function (data) {
                                    var urls = data;
                                    user[0].catalogue_urls = data;
                                    next(user)
                                })
                            },
                            function (user, next) {
                                // function (err, data) {
                                var abc = [];
                                reqBody.item.prepEach(function (r, next) {
                                    CatalogueModel.dbCollection.findOne({
                                        pid: me.user.pid,
                                        sku: r.sku
                                    }, {}, function (err, data) {
                                        if (!data) {
                                            CatalogueModel.write({
                                                "_id": reqBody._id,
                                                "pid": me.user.pid || user.pid,
                                                "category": r.category || user.category,
                                                "name": r.name || user.name,
                                                "indx": r.indx || user.indx,
                                                "descript": r.descript || user.descript,
                                                "active": r.active || user.active,
                                                "document_id": r.document_id,
                                                "tags": r.tags,
                                                "sku": r.sku || user.sku,
                                                "catalogue_urls": user[0].catalogue_urls
                                            },
                                                function (data) {
                                                    if (data.success) {
                                                        if (data.record && data.record.catalogue_urls && typeof (data.record.catalogue_urls) == "string") {
                                                            data.record.catalogue_urls = data.record.catalogue_urls.split(',');
                                                        }
                                                        abc.push(data.record);
                                                        PgdUtils.maintainReqStatus(me.req.body.header.nonce, me.config.statusSuccess);
                                                        PgdUtils.maintainNonceReqAndRes(me.req.body.header.nonce, abc);
                                                        me.sendData(abc);
                                                        next(r);
                                                    } else {
                                                        return me.addErrorObj(data.error);

                                                    }
                                                }, {
                                                    add: 1,
                                                    modify: 1
                                                });
                                        } else {
                                            if (reqBody._id == data._id) {
                                                CatalogueModel.write({
                                                    "_id": reqBody._id,
                                                    "pid": me.user.pid || user.pid,
                                                    "category": r.category || user.category,
                                                    "name": r.name || user.name,
                                                    "indx": r.indx || user.indx,
                                                    "descript": r.descript || user.descript,
                                                    "active": r.active || user.active,
                                                    "document_id": r.document_id,
                                                    "sku": r.sku || user.sku,
                                                    "catalogue_urls": user[0].catalogue_urls
                                                },
                                                    function (data) {
                                                        if (data.success) {
                                                            if (data.record && data.record.catalogue_urls && typeof (data.record.catalogue_urls) == "string") {
                                                                data.record.catalogue_urls = data.record.catalogue_urls.split(',');
                                                            }
                                                            abc.push(data.record);
                                                            PgdUtils.maintainReqStatus(me.req.body.header.nonce, me.config.statusSuccess);
                                                            PgdUtils.maintainNonceReqAndRes(me.req.body.header.nonce, abc);
                                                            me.sendData(abc);
                                                            next(r);
                                                        } else {
                                                            PgdUtils.maintainReqStatus(nonce, me.config.statusFailed);                                                            
                                                            return me.addErrorObj(data.error);
                                                        }
                                                    }, {
                                                        add: 1,
                                                        modify: 1
                                                    });
                                            }
                                            else {
                                                PgdUtils.maintainReqStatus(nonce, me.config.statusFailed);                                                
                                                return me.addErrorAndSend('ERR048');
                                            }
                                        }
                                        next(r);
                                    });
                                }, function () {
                                });
                            }
                        ].runEach();

                    } else {
                        me.sendError();
                    }
                } else {
                    return me.addErrorObj(err);
                }
            }
        ].runEach();
    },
    /*
        @author Mamta
        @date 6th July 2017
        @function apiGetCategories
        @desc Function to update item details ------ for update records ---
        updated --- 3 aug 2017
        */
    apiGetCategories: function () {
        var me = this;
        var params = {};
        params.filters = [];
        if (!me.user) {
            me.addErrorAndSend(401);
            return;
        }
        if (!me.user.pid) {
            me.addErrorAndSend(ERR015);
            return;
        }
        var CatalogueCatModel = Ext.create('Crm.modules.catalogueCat.model.CatalogueCatModel', {
            scope: me
        });
        CatalogueCatModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        CatalogueCatModel.dbCollection.find({
            pid: me.user.pid
        }, { _id: 1, name: 1 }, function (e, res) {
            me.sendData(res);
        });

    },
    /*
    @author Kaustubh
    @date 10th Aug 2017
    @function apiUpdateCatalogueImages
    @desc Function to update item images or records related to it ------ for update records ---
    updated --- 3 aug 2017
    */
    apiUpdateCatalogueImages: function () {
        var me = this;
        params = me.req.body.reqData;
        if (!me.user) {
            me.addErrorAndSend(401);
            return;
        }
        if (!me.user.pid) {
            me.addErrorAndSend(ERR015);
            return;
        }
        var CatalogueModel = Ext.create('Crm.modules.catalogue.model.CatalogueModel', {
            scope: me
        });
        CatalogueModel.error = function (code, info) {
            return me.addErrorObj(code);
        };

        var DocsModel = Ext.create('Crm.modules.file.model.FilesModel', {
            scope: me
        });

        DocsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };

        DocsModel.dbCollection.findOne({
            group_id: params.group_id
        }, {}, function (err, data) {
            if (data) {
                CatalogueModel.dbCollection.update({
                    _id: params._id
                }, {
                        $set: {
                            document_id: params.group_id
                        }
                    }, function (e, d) {
                        if (d) {
                            me.sendData({});
                        }
                        else {
                            return me.addErrorAndSend('ERR042');
                        }
                    });
            } else {
                return me.addErrorAndSend('ERR024', 'document_id');
            }
        });
    },
    /*
   @author Kaustubh
   @date 21th Aug 2017
   @function apiSetCategories
   @desc Function to add catalogue categories
   */
    apiSetCategories: function () {
        var me = this;
        var reqBody = me.req.body.reqData;
        var params = {};
        params.filters = [];
        if (!me.user) {
            me.addErrorAndSend(401);
            return;
        }
        if (!me.user.pid) {
            me.addErrorAndSend(ERR015);
            return;
        }
        var CatalogueCatModel = Ext.create('Crm.modules.catalogueCat.model.CatalogueCatModel', {
            scope: me
        });
        CatalogueCatModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        [
            function (err, client) {
                if (client) {
                    if (me.validateRequest([
                        {
                            type: "ARRAY", field: "item", item: [
                                {
                                    type: 'string',
                                    field: "name",
                                    isRequired: true
                                },
                                {
                                    type: 'int',
                                    field: "indx",
                                    isRequired: true
                                },
                                {
                                    type: 'string',
                                    field: "descript",
                                    isRequired: true
                                },
                                {
                                    type: 'string',
                                    field: "leaf",
                                    isRequired: true
                                }
                            ], isRequired: true
                        }
                    ]
                    )) {
                        [
                            function (next) {
                                var resp = {};
                                var abc = [];
                                var faildUpdate = [];
                                reqBody.item.prepEach(function (r, next) {
                                    CatalogueCatModel.write({
                                        "_id": me.Util.getObjectId(),
                                        "pid": me.user.pid || user.pid,
                                        "name": r.name || user.name,
                                        "indx": r.indx || user.indx,
                                        "descript": r.descript || user.descript,
                                        "leaf": r.leaf
                                    },
                                        function (data) {
                                            if (data.success) {
                                                abc.push(data.record);
                                                next(r);
                                            } else {
                                                return me.addErrorObj(data.error);
                                            }
                                        }, {
                                            add: 1,
                                            modify: 1
                                        });

                                }, function () {
                                    resp.success = abc;
                                    resp.failed = faildUpdate;
                                    next(resp);
                                }
                                );
                            },
                            function (data) {
                                me.sendData(data);
                            }
                        ].runEach();

                    } else {
                        me.sendError();
                    }
                } else {
                    return me.addErrorObj(err);
                }
            }
        ].runEach();
    },
    /*
           @author Kaustubh
           @date 21 aug 2017
           @function apiUpdateCategoriesDetails
           @desc Function to update categories details ------ for update records ---
           */
    apiUpdateCategoriesDetails: function () {
        var me = this;
        var reqBody = me.req.body.reqData;
        var CatalogueCatModel = Ext.create('Crm.modules.catalogueCat.model.CatalogueCatModel', {
            scope: me
        });
        CatalogueCatModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        [
            function (err, client) {
                if (client) {
                    if (me.validateRequest([

                        {
                            type: 'string',
                            field: "_id",
                            isRequired: false
                        },
                        {
                            type: "ARRAY", field: "item", item: [
                                {
                                    type: 'string',
                                    field: "name",
                                    isRequired: true
                                },
                                {
                                    type: 'int',
                                    field: "indx",
                                    isRequired: true
                                },
                                {
                                    type: 'string',
                                    field: "descript",
                                    isRequired: true
                                },
                                {
                                    type: 'string',
                                    field: "leaf",
                                    isRequired: true
                                }

                            ], isRequired: true
                        }

                    ]
                    )) {
                        [
                            function (next) {
                                CatalogueCatModel.dbCollection.find({
                                    _id: reqBody._id
                                }, {
                                    }, function (err, data) {
                                        if (data) {
                                            next(data);
                                        } else { return me.addErrorObj(data.error); }
                                    });
                            },
                            function (user, next) {
                                // function (err, data) {
                                var abc = [];
                                reqBody.item.prepEach(function (r, next) {
                                    CatalogueCatModel.dbCollection.findOne({
                                        pid: me.user.pid,
                                        _id: reqBody._id
                                    }, {}, function (err, data) {
                                        if (data) {
                                            CatalogueCatModel.write({
                                                "_id": reqBody._id,
                                                "pid": me.user.pid || user.pid,
                                                "name": r.name || user.name,
                                                "indx": r.indx || user.indx,
                                                "descript": r.descript || user.descript,
                                                "leaf": r.leaf
                                            },
                                                function (data) {
                                                    if (data.success) {
                                                        abc.push(data.record);
                                                        me.sendData(abc);
                                                        next(r);
                                                    } else {
                                                        return me.addErrorObj(data.error);

                                                    }
                                                }, {
                                                    add: 1,
                                                    modify: 1
                                                });
                                        } else {
                                            return me.addErrorAndSend('ERR040');
                                        }
                                        next(r);
                                    });
                                }, function () {
                                });
                            }
                        ].runEach();

                    } else {
                        me.sendError();
                    }
                } else {
                    return me.addErrorObj(err);
                }
            }
        ].runEach();
    },

    splitTagsUrls: function (k) {
        if (k.tags && k.tags != null) {
            k.tags = k.tags.split(",");
        }
        if (k.catalogue_urls && k.catalogue_urls != null) {
            k.catalogue_urls = k.catalogue_urls.split(",");
        }
        if (k.urls && k.urls != null) {
            k.urls = k.urls.split(",");
        }
        if (k.url && k.url != null) {
            k.url = k.url.split(",");
        }
        return (k);
    },

    reFormatOld: function (k) {
        var pid = {}, iteminfo = {};
        pid._id = k.fndid;
        pid.name = k.fndname;
        pid.logo_url = k.fndlogourl
        iteminfo._id = k.iteminfoid;
        iteminfo.donation_amount_needed = k.donation_amount_needed;
        iteminfo.donation_amount_collected = k.donation_amount_collected;
        iteminfo.item_type = k.item_type;
        iteminfo.pay_now = k.pay_now;
        if (k.fndid != undefined) {
            delete k.fndid;
        }
        if (k.fndname != undefined) {
            delete k.fndname;
        }
        if (k.fndlogourl != undefined) {
            delete k.fndlogourl;
        }
        if (k.iteminfoid != undefined) {
            delete k.iteminfoid;
        }
        if (k.donation_amount_needed != undefined || typeof k.donation_amount_needed == "object") {
            delete k.donation_amount_needed;
        }
        if (k.donation_amount_collected != undefined || typeof k.donation_amount_collected == "object") {
            delete k.donation_amount_collected;
        }
        if (k.item_type != undefined) {
            delete k.item_type;
        }
        if (k.pay_now != undefined) {
            delete k.pay_now;
        }
        if (pid) { k.pid = pid }
        if (iteminfo) { k.iteminfo = iteminfo }
        return (k);
    },

    //Vaibhav Vaidya, 11th October, formatCatalogItemInfo, Used to restructure elements inside pgd_catalogue into their own item object
    formatCatalogItemInfo: function(data){
        if(data.iteminfo==undefined) data.iteminfo={};
        data.iteminfo.item_type=data.item_type;
        data.iteminfo.donation_amount_needed=data.donation_amount_needed;
        data.iteminfo.donation_amount_collected=data.donation_amount_collected;
        data.iteminfo.pay_now=data.pay_now;
        data.iteminfo.priority=data.priority;
        if(data.item_type !=undefined){
            delete data.item_type;
        }
        if(data.donation_amount_needed !=undefined){
            delete data.donation_amount_needed;
        }
        if(data.donation_amount_collected !=undefined){
            delete data.donation_amount_collected;
        }
        if(data.pay_now !=undefined){
            delete data.pay_now;
        }
        if(data.priority !=undefined){
            delete data.priority;
        }
        return;
    },
    /*
        Start: Charity APIs  start exclusive for charities only, not allowed for partners or users 
        @author Kaustubh
        @date 09 sep 2017
        @function insertCharityItems
        @desc Function to add items details ------ for update records ---
    
    */

    insertCharityItems: function (cb) {
        var me = this;
        var reqBody = me.req.body.reqData;
        var PgdUtils = Ext.create('Crm.REST.util.PgdUtils', {
            scope: me
        });
        PgdUtils.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var tagArr = [];
        if (!me.user) {
            me.error(401);
            return;
        }
        if (!me.user.pid) {
            me.error(401);
            return;
        }
        if (!me.user.user_type == me.config.charityUser) {
            me.error(401);
            return;
        }
        var finalString = "";
        var DB_TRANSACTION = {
            BEGIN: function (next) {
                opt = Ext.clone(me.config.pgsql);
                opt.callback = function (conn) {
                    me.src.db.conn.query('BEGIN', function (err) {
                        if (err) {
                            me.sendError(err);
                        } else {
                            me.initModels();
                            next();
                        }
                    });
                }
                me.src.db = Ext.create("Database.drivers.Postgresql.Database", opt);
            },
            COMMIT: function (data) {
                me.src.db.conn.query('COMMIT', function () {
                    me.sendData(data)

                });
            },
            ROLLBACK: function (err) {
                me.src.db.conn.query('ROLLBACK', function () {
                    me.sendError(err);
                });
            }
        }
        var handleError = function (err) {
            return cb(err);
        };
        [
            function (err, client) {
                if (client) {
                    if (me.validateRequest([{
                        type: "ARRAY", field: "item", item: [
                            {
                                type: 'string',
                                field: "category",
                                isRequired: true
                            },
                            {
                                type: 'string',
                                field: "name",
                                isRequired: true
                            },
                            {
                                type: 'int',
                                field: "indx",
                                isRequired: true
                            },
                            {
                                type: 'string',
                                field: "descript",
                                isRequired: true
                            },
                            {
                                type: 'int',
                                field: "active",
                                isRequired: true
                            },
                            // {
                            //     type: 'string',
                            //     field: "sku",
                            //     isRequired: true
                            // }
                            // ,
                            // {
                            //     type: 'string',
                            //     field: "tags",
                            //     isRequired: true
                            // }
                        ], isRequired: true
                    }
                    ]
                    )) {
                        [
                            function (next) {
                                if (me.req.body.header.nonce) {
                                    var isDuplicateRequest = PgdUtils.checkDuplicateRequest(me.req.body.header.nonce);
                                    if (isDuplicateRequest) {
                                        var checkReqStatus = PgdUtils.getReqStatusFromNonce(me.req.body.header.nonce);
                                        if (checkReqStatus == me.config.statusSuccess) {
                                            var response = PgdUtils.getReqResFromNonce(me.req.body.header.nonce);
                                            me.sendData(response);
                                        }
                                        else if (checkReqStatus == me.config.statusFailed) {
                                            me.sendError();
                                        }
                                        else {
                                            setTimeout(function () {
                                                var checkReqStatus = PgdUtils.getReqStatusFromNonce(me.req.body.header.nonce);
                                                if (checkReqStatus = me.config.statusSuccess) {
                                                    var response = PgdUtils.getReqResFromNonce(me.req.body.header.nonce);
                                                    me.sendData(response);
                                                }
                                                else {
                                                    me.sendError();
                                                }
                                            }, 15000);

                                        }
                                    }
                                    else {
                                        next();
                                    }
                                }
                                else {
                                    return me.addErrorAndSend('ERR050', 'NOUNCE');
                                }
                            },
                            //begin statement
                            function (next) {
                                DB_TRANSACTION.BEGIN(function () {
                                    next();
                                });
                            },
                            function (next) {
                                var resp = {};
                                var abc = [];
                                var faildUpdate = [];
                                reqBody.item.prepEach(function (r, next) {
                                    var resp = [];
                                    var tagString = "";
                                    //make tag arrays -- all tags (dont check for repetation)
                                    // r.tags.prepEach(function (k, next) {
                                    //     tagArr.push(k);
                                    //     if (tagString == "" && tagString != null) {
                                    //         tagString = tagString + k;
                                    //     }
                                    //     else {
                                    //         tagString = tagString + "," + k;
                                    //     }
                                    //     next(k);
                                    // }, function () {
                                    // });

                                    var DocsModel = Ext.create('Crm.modules.file.model.FilesModel', {
                                        scope: me
                                    });

                                    DocsModel.error = function (code, info) {
                                        return me.addErrorObj(code);
                                    };

                                    //var hostname = me.req.headers.host;
                                    var path = '/api/rest/refdata/download-file?id=';
                                    DocsModel.dbCollection.find({
                                        group_id: r.document_id
                                    }, { _id: 1 }, function (err, d) {
                                        if (d && d.length > 0) {
                                            d.prepEach(function (e, next) {

                                                var url = path + e._id;
                                                if (finalString != "") {
                                                    finalString = finalString + "," + url;
                                                }
                                                else {
                                                    finalString = finalString + url;
                                                }
                                                resp.push(url);
                                                next(e);
                                            }, function () {
                                                me.CatalogueModel.dbCollection.findOne({
                                                    pid: me.user.pid,
                                                   // sku: r.sku
                                                }, {}, function (err, data) {
                                                    //no sku maintained for charity items
                                                    if (r.item_type == "U") {
                                                        r.amount_needed = parseInt("9999999999");
                                                    }
                                                    me.CatalogueModel.write({
                                                        "_id": me.Util.getObjectId(),
                                                        "category": r.category,
                                                        "pid": me.user.pid,
                                                        "name": r.name,
                                                        "indx": r.indx,
                                                        "descript": r.descript,
                                                        "active": 0,
                                                        "document_id": r.document_id,
                                                        "tags": tagString,
                                                        // "sku": r.sku,
                                                        "catalogue_urls": finalString,
                                                        "item_type": r.item_type,
                                                        "donation_amount_needed": r.amount_needed,
                                                        "donation_amount_collected": 0,
                                                        "charity_id": me.user.pid,
                                                        "pay_now": (r.amount_needed>0)?1:0
                                                    },
                                                        function (data) {
                                                            if (data.success) {
                                                                me.formatCatalogItemInfo(data.record);
                                                                abc.push(data.record);
                                                                next(r);
                                                            } else {
                                                                return DB_TRANSACTION.ROLLBACK(data.error);
                                                            }
                                                        }, {
                                                            add: 1
                                                        });
                                                });
                                            });

                                        } else {
                                            if (r.item_type == "U") {
                                                r.amount_needed = parseInt("9999999999");
                                            }
                                            me.CatalogueModel.write({
                                                "_id": me.Util.getObjectId(),
                                                "category": r.category,
                                                "pid": me.user.pid,
                                                "name": r.name,
                                                "indx": r.indx,
                                                "descript": r.descript,
                                                "active": 0,
                                                "document_id": r.document_id,
                                                "tags": tagString,
                                               // "sku": r.sku,
                                                //   "catalogue_urls": finalString,
                                                "item_type": r.item_type,
                                                "donation_amount_needed": r.amount_needed,
                                                "donation_amount_collected": 0,
                                                "charity_id": me.user.pid,
                                                "pay_now": (r.amount_needed>0)?1:0
                                            },
                                                function (data) {
                                                    if (data.success) {
                                                        //kaustubh 9 september 2017
                                                        me.formatCatalogItemInfo(data.record);
                                                        abc.push(data.record);
                                                        next(r);
                                                    } else {
                                                        return DB_TRANSACTION.ROLLBACK(data.error);
                                                    }
                                                }, {
                                                    add: 1
                                                });
                                        }
                                    });
                                }, function () {
                                    resp.success = abc;
                                    resp.failed = faildUpdate;
                                    next(resp);
                                });
                            },
                            function (data) {

                                //commented since we need count
                                // tagArr = tagArr.reverse().filter(function (e, i, tagArr) {
                                //     return tagArr.indexOf(e, i+1) === -1;
                                // }).reverse();
                                // send call to store tags after whole processing
                                // me.tagsHandler(tagArr);
                                PgdUtils.maintainReqStatus(me.req.body.header.nonce, me.config.statusSuccess);
                                PgdUtils.maintainNonceReqAndRes(me.req.body.header.nonce, data);
                                return DB_TRANSACTION.COMMIT(data);
                            }
                        ].runEach();

                    } else {
                        PgdUtils.maintainReqStatus(me.req.body.header.nonce, me.config.statusFailed);
                        me.sendError();
                    }
                } else {
                    return me.addErrorObj(err);
                }
            }
        ].runEach();
    },

    /*
        Start: Charity APIs  start exclusive for charities only, not allowed for partners or users 
        @author Kaustubh
        @date 09 sep 2017
        @function apiUpdateCharityInfoDetails
        @desc Function to update items details ------ for update records ---
    
    */

    apiUpdateCharityInfoDetails: function () {
        var PgdUtils = Ext.create('Crm.REST.util.PgdUtils', {
            scope: me
        });
        PgdUtils.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var me = this;
        var reqBody = me.req.body.reqData;
        [
            function (err, client) {
                if (client) {
                    if (me.validateRequest([

                        {
                            type: 'string',
                            field: "category",
                            isRequired: false
                        },
                        {
                            type: "ARRAY", field: "item", item: [


                                {
                                    type: 'string',
                                    field: "name",
                                    isRequired: false
                                },
                                {
                                    type: 'int',
                                    field: "indx",
                                    isRequired: false
                                },
                                {
                                    type: 'string',
                                    field: "descript",
                                    isRequired: false
                                },
                                {
                                    type: 'int',
                                    field: "active",
                                    isRequired: false
                                }

                            ], isRequired: true
                        }

                    ]
                    )) {
                        [
                            function (next) {
                                if (me.req.body.header.nonce) {
                                    var isDuplicateRequest = PgdUtils.checkDuplicateRequest(me.req.body.header.nonce);
                                    if (isDuplicateRequest) {
                                        var checkReqStatus = PgdUtils.getReqStatusFromNonce(me.req.body.header.nonce);
                                        if (checkReqStatus == me.config.statusSuccess) {
                                            var response = PgdUtils.getReqResFromNonce(me.req.body.header.nonce);
                                            me.sendData(response);
                                        }
                                        else if (checkReqStatus == me.config.statusFailed) {
                                            me.sendError();
                                        }
                                        else {
                                            setTimeout(function () {
                                                var checkReqStatus = PgdUtils.getReqStatusFromNonce(me.req.body.header.nonce);
                                                if (checkReqStatus = me.config.statusSuccess) {
                                                    var response = PgdUtils.getReqResFromNonce(me.req.body.header.nonce);
                                                    me.sendData(response);
                                                }
                                                else {
                                                    me.sendError();
                                                }
                                            }, 15000);

                                        }
                                    }
                                    else {
                                        next();
                                    }
                                }
                                else {
                                    return me.addErrorAndSend('ERR050', 'NOUNCE');
                                }
                            },
                            function (next) {
                                me.CatalogueModel.dbCollection.findOne({
                                    _id: reqBody._id
                                }, {
                                    }, function (err, data) {
                                        if (data) {
                                            //kaustubh
                                            next(data);
                                            //end
                                        } else { return me.addErrorObj(err); }
                                    });
                            },

                            function (user, next) {
                                // function (err, data) {
                                var abc = [];
                                reqBody.item.prepEach(function (r, next) {
                                    // you can make umlimited item to limited but not limited to unlimited
                                    if (r.item_type == "U" && user.item_type == "L") {
                                        me.sendError();
                                    }
                                    // check the new amount is not less than amount that being collected
                                    if (parseInt(r.amount_needed) <= parseInt(user.donation_amount_collected)) {
                                        me.sendError();
                                    }
                                    me.CatalogueModel.dbCollection.findOne({
                                        pid: me.user.pid,
                                        _id: reqBody._id
                                    }, {}, function (err, data) {
                                        if (!data) {
                                            //kaustubh 9 september 2017
                                            // never set pay_now && donation_amount_received from request 
                                            me.CatalogueModel.write({
                                                "_id": reqBody._id,
                                                "pid": me.user.pid || user.pid,
                                                "category": r.category || user.category,
                                                "name": r.name || user.name,
                                                "indx": r.indx || user.indx,
                                                "descript": r.descript || user.descript,
                                                "active": user.active,
                                                "document_id": r.document_id || user.document_id,
                                                "tags": r.tags || user.tags || user.tags,
                                               // "sku": r.sku || user.sku,
                                               "donation_amount_needed": r.amount_needed||user.donation_amount_needed,
                                               "item_type": r.item_type||user.item_type
                                            },
                                                function (data) {
                                                    if (data.success) {
                                                        me.formatCatalogItemInfo(data.record);
                                                        abc.push(data.record);
                                                        PgdUtils.maintainReqStatus(me.req.body.header.nonce, me.config.statusSuccess);
                                                        PgdUtils.maintainNonceReqAndRes(me.req.body.header.nonce, abc);
                                                        me.sendData(abc);
                                                        next(r);
                                                    } else {
                                                        return me.addErrorObj(data.error);
                                                    }
                                                }, {
                                                    add: 1,
                                                    modify: 1
                                                });
                                        } else {
                                            if (reqBody._id == data._id) {
                                                //kaustubh 9 september 2017
                                                // never set pay_now && donation_amount_received from here 
                                                me.CatalogueModel.write({
                                                    "_id": reqBody._id,
                                                    "pid": me.user.pid || user.pid,
                                                    "category": r.category || user.category,
                                                    "name": r.name || user.name,
                                                    "indx": r.indx || user.indx,
                                                    "descript": r.descript || user.descript,
                                                    "active": user.active,
                                                    "document_id": r.document_id,
                                                    "tags": r.tags || user.tags,
                                                   // "sku": r.sku || user.sku
                                                    "donation_amount_needed": r.amount_needed || user.donation_amount_needed,
                                                    "item_type": r.item_type || user.item_type
                                                },
                                                    function (data) {
                                                        if (data.success) {
                                                            me.formatCatalogItemInfo(data.record);
                                                            abc.push(data.record);
                                                            PgdUtils.maintainReqStatus(me.req.body.header.nonce, me.config.statusSuccess);
                                                            PgdUtils.maintainNonceReqAndRes(me.req.body.header.nonce, abc);
                                                            me.sendData(abc);
                                                            next(r);
                                                        } else {
                                                            PgdUtils.maintainReqStatus(me.req.body.header.nonce, me.config.statusFailed);
                                                            return me.addErrorObj(data.error);
                                                        }
                                                    }, {
                                                        add: 1,
                                                        modify: 1
                                                    });
                                            }
                                            else {
                                                PgdUtils.maintainReqStatus(me.req.body.header.nonce, me.config.statusFailed);                                                
                                                return me.addErrorAndSend('ERR048','invalid order id');
                                            }
                                        }
                                    });
                                }, function () {
                                });
                            }
                        ].runEach();

                    } else {
                        me.sendError();
                    }
                } else {
                    return me.addErrorObj(err);
                }
            }
        ].runEach();
    },
    /*
        Start: get all catalogue items 
        @author Kaustubh
        @date 13 sep 2017
        @function getCatalogues
        @desc Function to get all catalogues
    */
    getCatalogues: function () {
/*

        var me = this,
            params = me.req.body.reqData;
        var id;
        var skip = params.startIndex && /^[0-9]{1,}$/.test(params.startIndex + '') ? parseInt(params.startIndex) : 0;
        var limit = params.numOfRecords && /^[0-9]{1,}$/.test(params.numOfRecords + '') ? parseInt(params.numOfRecords) : 50;

        if (me.user && me.user.user_type != me.config.endUser) {
            var response = {};
            var placeHolders = [], pidStore = [], total = 0, catalogue = {}, resp = {};
            var CatalogueModel = Ext.create('Crm.modules.catalogue.model.CatalogueModel', {
                scope: me
            });
            CatalogueModel.error = function (code, info) {
                return me.addErrorObj(code);
            };
            var catalogueSql = "select cat.*, fnd._id as fndid, fnd.name as fndname, fnd.logo_url as fndlogourl, cci._id as iteminfoid, cci.donation_amount_needed, cci.donation_amount_collected, cci.item_type, cci.pay_now, fnd.address from " + CatalogueModel.collection + " cat"
            catalogueSql += " left join charity_catalogue_info cci on (cat.iteminfo=cci._id and cci.removed != 1) "
            catalogueSql += " inner join pgd_foundations fnd on (cat.pid=fnd._id and fnd.active=1 and fnd.removed=0) "
            //    and fnd.orgtype=1 "
            catalogueSql += " where cat.removed!=1 "
            //catalogueSql+=" and cat.active!=0 "
            //catalogueSql+=" and cci.removed!=1"
            catalogueSql += " and cat.pid= '" + me.user.pid + "' ";

            offsetSql = " offset " + parseInt(skip) + " limit " + parseInt(limit);
            var tempTag = "";
            if (params.tags != undefined && Array.isArray(params.tags) && params.tags.length > 0) {
                tempTag += "'%(";
                for (i = 0; i < params.tags.length; i++) {
                    if (i > 0) {
                        tempTag += "|";
                    }

                    tempTag += "(" + params.tags[i].toLowerCase() + ")";

                }
                tempTag += ")%'";
                placeHolders.push(tempTag);
                tplTags = " and " + "( lower(cat.descript) similar to " + tempTag + " ";
                tplName = " or lower(cat.name) similar to " + tempTag + " ";
                tplAdd = " or lower(fnd.address) similar to " + tempTag + " )";
                catalogueSql += tplTags + tplName + tplAdd;
            }

            // catalogueSql+=" and pid in (\'"+pidStore.join('\',\'')+"')";

            catalogueSql += offsetSql;
            me.src.db.query(catalogueSql, null, function (e, det) {


                if (det && det.length > 0) {
                    total = det.length;
                    det.prepEach(function (k, next) {
                        k = me.splitTagsUrls(k);
                        k = me.reFormatOld(k)
                        next(k);
                    }, function () {
                        catalogue.total = total;
                        //Start ...Vaibhav Mali
                        var temp;
                        var i = 0, j = 0;
                        for (i = 0; i < det.length - 1; i++) {
                            for (j = i + 1; j < det.length; j++) {
                                if (det[i].stime < det[j].stime) {
                                    temp = det[i];
                                    det[i] = det[j];
                                    det[j] = temp;
                                }
                            }
                        }
                        //End ...vaibhav Mali 
                        catalogue.list = det;
                        resp.catalogues = catalogue;
                        me.sendData(resp);
                    });
                }
                else {
                    //return me.addErrorAndSend('ERR052');
                    catalogue.total = total;
                    //Start ...Vaibhav Mali
                    var temp;
                    var i = 0, j = 0;
                    for (i = 0; i < det.length - 1; i++) {
                        for (j = i + 1; j < det.length; j++) {
                            if (det[i].stime < det[j].stime) {
                                temp = det[i];
                                det[i] = det[j];
                                det[j] = temp;
                            }
                        }
                    }
                    //End ...vaibhav Mali 
                    catalogue.list = det;
                    resp.catalogues = catalogue;
                    me.sendData(resp);
                }
            });
        }
        else {
            me.sendError();
        }
    }
*/
     //Vaibhav Mali 26 Sept 2017 Added placeholders.
     var me = this,
     params = me.req.body.reqData;
     var id;
     var skip = params.startIndex && /^[0-9]{1,}$/.test(params.startIndex + '') ? parseInt(params.startIndex) : 0;
     var limit = params.numOfRecords && /^[0-9]{1,}$/.test(params.numOfRecords + '') ? parseInt(params.numOfRecords) : 50;
     var catpholders = [];
     if (me.user && me.user.user_type != me.config.endUser) {
     var response = {};
     var placeHolders = [], pidStore = [], total = 0, catalogue = {}, resp = {};
     var CatalogueModel = Ext.create('Crm.modules.catalogue.model.CatalogueModel', {
         scope: me
     });
     CatalogueModel.error = function (code, info) {
         return me.addErrorObj(code);
     };
     var catalogueSql = "select cat._id, cat.pid, cat.category, cat.name, cat.indx, cat.descript, cat.active, cat.sku, cat.document_id, cat.tags, cat.catalogue_urls, cat.item_type, cat.donation_amount_needed, cat.donation_amount_collected, cat.pay_now, cat.priority, fnd._id as fndid, fnd.name as fndname, fnd.logo_url as fndlogourl, fnd.address from " + CatalogueModel.collection + " cat";
     catpholders.push(1);
     catalogueSql += " inner join pgd_foundations fnd on (cat.pid=fnd._id and fnd.active=$"+catpholders.length;
     catpholders.push(0);
     catalogueSql +="  and fnd.removed=$"+catpholders.length+") "
     //    and fnd.orgtype=1 "
     catpholders.push(1);
     catalogueSql += " where cat.removed != $"+catpholders.length;
     //catalogueSql+=" and cat.active!=0 "
     //catalogueSql+=" and cci.removed!=1"
     catpholders.push(me.user.pid);
     catalogueSql += " and cat.pid = $" + catpholders.length;
     catpholders.push(parseInt(skip));
     offsetSql = " offset $" + catpholders.length;
     catpholders.push(parseInt(limit));
     offsetSql += " limit $" + catpholders.length;
     var tempTag = "";
     if (params.tags != undefined && Array.isArray(params.tags) && params.tags.length > 0) {
         tempTag += "'%(";
         for (i = 0; i < params.tags.length; i++) {
             if (i > 0) {
                 tempTag += "|";
             }
            // catpholders.push(params.tags[i].toLowerCase());
            // tempTag += "($" + catpholders.length + ")";
             tempTag += params.tags[i].toLowerCase()

         }
         tempTag += ")%'";
         placeHolders.push(tempTag);
         tplTags = " and " + "( lower(cat.descript) similar to " + tempTag + " ";
         tplName = " or lower(cat.name) similar to " + tempTag + " ";
         tplAdd = " or lower(fnd.address) similar to " + tempTag + " )";
         catalogueSql += tplTags + tplName + tplAdd;
     }

     // catalogueSql+=" and pid in (\'"+pidStore.join('\',\'')+"')";

     catalogueSql += offsetSql;
     me.src.db.query(catalogueSql, catpholders, function (e, det) {
         if (det && det.length > 0) {
             total = det.length;
             det.prepEach(function (k, next) {
                 k = me.splitTagsUrls(k);
                 k = me.reFormatOld(k)
                 next(k);
             }, function () {
                 catalogue.total = total;
                 //Start ...Vaibhav Mali
                 var temp;
                 var i = 0, j = 0;
                 for (i = 0; i < det.length - 1; i++) {
                     for (j = i + 1; j < det.length; j++) {
                         if (det[i].stime < det[j].stime) {
                             temp = det[i];
                             det[i] = det[j];
                             det[j] = temp;
                         }
                     }
                 }
                 //End ...vaibhav Mali 
                 catalogue.list = det;
                 resp.catalogues = catalogue;
                 me.sendData(resp);
             });
         }
         else {
             //return me.addErrorAndSend('ERR052');
             catalogue.total = total;
              //Start ...Vaibhav Mali
                 var temp;
                 var i=0,j=0;
                 for(i=0;i<det.length-1;i++){
                     for (j=i+1;j<det.length;j++){
                         if(det[i].stime < det[j].stime){
                            temp = det[i];
                            det[i] = det[j];
                            det[j] = temp;
                         }
                     }
                 }
                 //End ...vaibhav Mali 
             catalogue.list = det;
             resp.catalogues = catalogue;
             me.sendData(resp);
         }
     });
 }
 else {
     me.sendError();
 }
}

});