Ext.define('Crm.REST.partner.CorporatesController', {
    extend: "Crm.REST.RestBaseController"
    
    
    ,list: function() {       
        var me=this
            ,params = me.req.body.reqData
            ,queryParams=[];
            var myblogs={};
            var myTotalBlogs=[];
            var output=[];

        var skip = params.startIndex && /^[0-9]{1,}$/.test(params.startIndex + '') ? parseInt(params.startIndex) : 0;
        var limit = params.numOfRecords && /^[0-9]{1,}$/.test(params.numOfRecords + '') ? parseInt(params.numOfRecords) : 50;

        var filteredSql='SELECT'
            +' pf._id,'
            +' pf.ctime,'
            +' pf.removed,'
            +' pf.maker,'
            +' pf.orgtype,'
            +' pf. NAME,'
            +' pf.description,'
            +' pf.active,'
            +' pf.province,'
            +' pf.district,'
            +' pf.address,'
            +' pf.tel,'
            +' pf.email,'
            +' pf.donation_type,'
            +' pf.coordinates,'
            +' pf.url,'
            +' pf.logo_url,'
            +' pf.media_urls' ;
        var countSql='SELECT count(*)';
        var whereClause=    ' FROM'
            +' pgd_foundations pf'
            +' where pf.active = 1'
            +' and pf.removed = 0'
            +' and pf.orgtype = 2';


        var limitClause = ' limit '+ limit + ' offset ' + skip;



        me.src.db.query(filteredSql+whereClause+limitClause, queryParams, function (e, data) {
            if (data && data.length>0) {
              
                data.prepEach(function (k, next) {
                    if(k.media_urls && typeof(k.media_urls)=="string")
                    {
                      
                        k.media_urls=k.media_urls.split(",");
                    }
                    var planDetSql="SELECT * FROM ITEM_BLOGS WHERE CATALOGUE_ID='"+ k._id +"'";
                    
                    me.src.db.query(planDetSql, null, function (e, blogs){
                    if(blogs && blogs.length >0){
                        blogs.prepEach(function (m,next) {
                            if(m.urls && typeof(m.urls)=="string")
                            {
                                m.urls=m.urls.split(",");
                                myTotalBlogs.push(m);

                                next(m);
                            }
                            else
                            {
                                myTotalBlogs.push(m);
                                next(m);

                            }
                        }, function () {
                            myblogs.total=myTotalBlogs.length;
                            myblogs.list=myTotalBlogs;
                            k.blogs=myblogs;
                            var partnerIds=[];
                            var qParamHolder='';
                            for(var i=0;i<data.length;i++){
                                partnerIds.push(data[i]._id);
                                if(i==0){
                                    qParamHolder='$'+(i+1);
                                }else {
                                    qParamHolder += ',$' + (i + 1);
                                }
                            }
                            var planSql="select _id,partner_user_id, plan_type, plan_name from charity_plan where plan_type in ('A','E') and partner_user_id in (" + qParamHolder +")";
                            me.src.db.query(planSql, partnerIds, function (e, planData){
                                if(planData && planData.length >0){
                                    var planArr={};
                                    var detParam='';
                                    var planIds=[];
                                    for(var i=0;i<planData.length;i++){
                                        if(!planArr[planData[i].partner_user_id]){
                                            planArr[planData[i].partner_user_id]=[];
                                        }
                                        planArr[planData[i].partner_user_id].push(planData[i]);
                                        planIds.push(planData[i]._id);
                                        if(i==0){
                                            detParam='$'+(i+1);
                                        }else {
                                            detParam += ',$' + (i + 1);
                                        }
                                    }
            
                                    for(var j=0;j<data.length;j++){
                                        data[j].plans=planArr[data[j]._id];
                                    }
            
                                    var planDetSql="select cpd._id,cpd.charity_plan_id, pf.name, cpd.charity_foundation_id,cpd.percentage " +
                                        " from charity_plan_details cpd, pgd_foundations pf " +
                                        " where cpd.charity_foundation_id=pf._id " +
                                        " and charity_plan_id in (" + detParam +")";
                                    me.src.db.query(planDetSql, planIds, function (e, planDetData){
                                        if(planDetData && planDetData.length){
                                            var planDetArr={};
                                            for(var i=0;i<planDetData.length;i++){
                                                if(!planDetArr[planDetData[i].charity_plan_id]){
                                                    planDetArr[planDetData[i].charity_plan_id]=[];
                                                }
                                                planDetArr[planDetData[i].charity_plan_id].push(planDetData[i]);
                                            }
                                            for(var k=0;k<planData.length;k++){
                                                planData[k].plan_details=planDetArr[planData[k]._id];
                                            }
                                        }
                                        me.src.db.query(countSql+whereClause, queryParams, function (e, countData) {
                                            output={total:0};
                                            if(countData && countData.length){
                                                output.total=countData[0].count;
                                            }
            
                                            output.list=data;
                                           
                                        });
                                    });
            
            
                                }
                                else {
                                    output={total:0};
                                    total=data.length;
                                    output.list=data;
                                }
            
                            });
                        });
                        next(k);
                    }
                    else
                    {
                        var partnerIds=[];
                        var qParamHolder='';
                        for(var i=0;i<data.length;i++){
                            partnerIds.push(data[i]._id);
                            if(i==0){
                                qParamHolder='$'+(i+1);
                            }else {
                                qParamHolder += ',$' + (i + 1);
                            }
                        }
                        var planSql="select _id,partner_user_id, plan_type, plan_name from charity_plan where plan_type in ('A','E') and partner_user_id in (" + qParamHolder +")";
                        me.src.db.query(planSql, partnerIds, function (e, planData){
                            if(planData && planData.length >0){
                                var planArr={};
                                var detParam='';
                                var planIds=[];
                                for(var i=0;i<planData.length;i++){
                                    if(!planArr[planData[i].partner_user_id]){
                                        planArr[planData[i].partner_user_id]=[];
                                    }
                                    planArr[planData[i].partner_user_id].push(planData[i]);
                                    planIds.push(planData[i]._id);
                                    if(i==0){
                                        detParam='$'+(i+1);
                                    }else {
                                        detParam += ',$' + (i + 1);
                                    }
                                }
        
                                for(var j=0;j<data.length;j++){
                                    data[j].plans=planArr[data[j]._id];
                                }
        
                                var planDetSql="select cpd._id,cpd.charity_plan_id, pf.name, cpd.charity_foundation_id,cpd.percentage " +
                                    " from charity_plan_details cpd, pgd_foundations pf " +
                                    " where cpd.charity_foundation_id=pf._id " +
                                    " and charity_plan_id in (" + detParam +")";
                                me.src.db.query(planDetSql, planIds, function (e, planDetData){
                                    if(planDetData && planDetData.length){
                                        var planDetArr={};
                                        for(var i=0;i<planDetData.length;i++){
                                            if(!planDetArr[planDetData[i].charity_plan_id]){
                                                planDetArr[planDetData[i].charity_plan_id]=[];
                                            }
                                            planDetArr[planDetData[i].charity_plan_id].push(planDetData[i]);
                                        }
                                        for(var k=0;k<planData.length;k++){
                                            planData[k].plan_details=planDetArr[planData[k]._id];
                                        }
                                    }
                                    me.src.db.query(countSql+whereClause, queryParams, function (e, countData) {
                                        output={total:0};
                                        if(countData && countData.length){
                                            output.total=countData[0].count;
                                        }
        
                                        output.list=data;
                                       
                                    });
                                });
        
        
                            }
                            else {
                                output={total:0};
                                total=data.length;
                                output.list=data;
                            }
        
                        });
                        next(k)
                    }
                    });
                }, function () {
                    me.sendData(output);
                });
            } else if(e) {
                me.sendError();
            }else{
                me.sendData({});
            }
        });
/*
        if(!params.filters) params.filters = [];
        params.filters.push({value: true, property: 'active'});
        params.filters.push({value: false, property: 'removed'});
        params.filters.push({value: 2, property: 'orgtype'});
        var orgModel = Ext.create('Crm.modules.organizations.model.FoundModel', {scope: me});
        var planModel = Ext.create('Crm.modules.charityPlan.model.CharityPlanModel',{scope: me});
        var belongsToField = -1;
        for(var i=0;i<planModel.fields.length;i++){
            if(planModel.fields[i].name=="_id"){
                belongsToField=i;
            }
        }
        planModel.fields[belongsToField].bindTo={collection: 'charity_plan_details', keyFieldType: 'ObjectID', keyField: 'charity_plan_id', fields: {charity_foundation_id:1, percentage:1}};

        planModel.getData(params, function(res) {
            me.sendData(res);
        })*/

    }
    
    ,write: function() {       
        var me=this
            ,params = me.req.body.reqData;        
        if(!me.user) {
            me.addErrorAndSend(401);
            return;
        } 
        params.pid = me.user.pid;
        Ext.create('Crm.modules.organizations.model.FoundModel', {scope: this})
        .write(params, function(res) {
            me.sendData(res);   
        },{add: true, modify: true})            

    }
    
    ,remove: function() {       
        var me=this
            ,params = me.req.body.reqData;        
        if(!me.user) {
            me.addErrorAndSend(401);
            return;
        } 
        params.pid = me.user.pid;
        Ext.create('Crm.modules.docs.model.DocsModel', {scope: this})
        .remove(params, function(res) {
            me.sendData(res);   
        })            

    }
    
    
    ,types: function() {       
        var me=this
            ,params = Ext.clone(me.req.body.reqData);        
        if(!me.user) {
            me.addErrorAndSend(401);
            return;
        }     
        Ext.create('Crm.modules.organizations.model.FoundModel', {scope: this})
        .getData(params, function(res) {
            me.sendData(res);   
        })            

    }
    
    
    
})