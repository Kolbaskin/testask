Ext.define('Crm.REST.partner.CatalogueBlogController', {
    extend: "Crm.REST.RestBaseController",
    constructor: function () {
        this.callParent(arguments);
    },
    /*
@author : Kaustubh 
@date : 09 sep 2017
@function : getBlogComments
@desc : get all comments for that item
*/
    getBlogComments: function () {
        var me = this;
        var inparams = me.req.body.reqData;
        // Vaibhav Mali 26-Sept-2017 Made API public as sandip request.
       /* if (!me.user) {
            me.error(401);
            return;
        }
        if (!me.user.pid) {
            me.error(401);
            return;
        }*/
        var CatalogueBlogs = Ext.create('Crm.modules.catalogueBlogs.model.CatalogueBlogsModel', {
            scope: me
        });
        CatalogueBlogs.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var catController = Ext.create('Crm.REST.partner.CatalogueController', {
            scope: me,
        });
        var params = {};
        params.filters = [];
        params.filters.push({ value: inparams.catalogue_id, property: 'catalogue_id' });
        CatalogueBlogs.getData(params, function (data) {
            if (data && data.list && data.list.length>0) {
                data.list.prepEach(function (d, next) {
                    catController.splitTagsUrls(d);
                    next(d);
                },function()
                {
                    me.sendData(data);
                });
            }
            else
                {
                    var response={};
                    response.list=[];
                    me.sendData(response);
                }
        });
    },
    /*
@author : Kaustubh 
@date : 09 sep 2017
@function : removeBlogComments
@desc : remove blog comment
*/
    removeBlogComments: function () {
        var me = this;
        var inparams = me.req.body.reqData;
        if (!me.user) {
            me.error(401);
            return;
        }
        if (!me.user.pid) {
            me.error(401);
            return;
        }
        var params = {};
        params.filters = [];
        var sql = "delete from item_blogs where _id=$1";
        me.src.db.query(sql, [inparams._id], function (e, data) {
            if (data) {
                me.sendData([]);
            } else {
                me.sendError();
            }
        });

    },
    /*
@author : Kaustubh 
@date : 09 sep 2017
@function : insertBlogComments
@desc : insert blog comment
*/
    insertBlogComments: function () {

        var me = this;
        var finalString = "";
        var params = me.req.body.reqData;
        var CatalogueBlogs = Ext.create('Crm.modules.catalogueBlogs.model.CatalogueBlogsModel', {
            scope: me
        });
        CatalogueBlogs.error = function (code, info) {
            return me.addErrorObj(code);
        };

        var DocsModel = Ext.create('Crm.modules.file.model.FilesModel', {
            scope: me
        });

        DocsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };

        if (!me.user) {
            me.error(401);
            return;
        }
        if (!me.user.pid) {
            me.error(401);
            return;
        }
        if (me.validateRequest([{
                type: 'ObjectID',
                field: "catalogue_id",
                isRequired: true
            }, {
                type: 'string',
                field: "header",
                isRequired: true
            }, /*{
                type: 'ObjectID',
                field: "document_id",
                isRequired: true
            },*/
                {
                    type: 'string',
                    field: "content",
                    isRequired: true
                }
            ])) {
            [
                function (next) {                   
                    var path = '/api/rest/refdata/download-file?id=';
                    DocsModel.dbCollection.find({
                        group_id: params.document_id
                    }, {_id: 1}, function (err, d) {
                     
                        if (d && d.length > 0) {
                            d.prepEach(function (e, nextItem) {
                                var url = path + e._id;
                                if (finalString != "") {
                                    finalString = finalString + "," + url;
                                }
                                else {
                                    finalString = finalString + url;
                                }
                                nextItem(e);
                            }, function () {
                                next()
                            });
                        } else {
                            next()
                        }
                    })
                }

            , function () {                   
                CatalogueBlogs.write({
                        "_id": me.Util.getObjectId(),
                        "catalogue_id": params.catalogue_id,
                        "header": params.header,
                        "document_id": params.document_id,
                        "urls": finalString,
                        "content": params.content
                    },
                    function (res) {
                        if (res.success) {
                            me.sendData(res);
                        } else {
                            me.sendError();
                        }
                    }, {
                        add: 1,
                        modify: 1
                    });
            }

            ].runEach();
        }
    },
    /*
@author : Kaustubh 
@date : 09 sep 2017
@function : updateBlogComments
@desc : update blog comment
*/
    updateBlogComments: function () {
        var me = this;
        var finalString = "";
        var params = me.req.body.reqData;
        var CatalogueBlogs = Ext.create('Crm.modules.catalogueBlogs.model.CatalogueBlogsModel', {
            scope: me
        });
        CatalogueBlogs.error = function (code, info) {
            return me.addErrorObj(code);
        };

        var DocsModel = Ext.create('Crm.modules.file.model.FilesModel', {
            scope: me
        });

        DocsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };

        if (!me.user) {
            me.error(401);
            return;
        }
        if (!me.user.pid) {
            me.error(401);
            return;
        }

        if (me.validateRequest([{
            type: 'ObjectID',
            field: "catalogue_id",
            isRequired: true
        }, {
            type: 'string',
            field: "header",
            isRequired: true
        }, {
            type: 'ObjectID',
            field: "document_id",
            isRequired: true
        },
        {
            type: 'string',
            field: "content",
            isRequired: true
        }
        ])) {
            [
                function (next) {

                    var path = '/api/rest/refdata/download-file?id=';
                    CatalogueBlogs.dbCollection.findOne({
                        _id: params._id
                    }, {}, function (err, originalData) {
                        if (originalData) {
                            DocsModel.dbCollection.find({
                                group_id: params.document_id
                            }, { _id: 1 }, function (err, d) {
                                if (d && d.length > 0) {
                                    d.prepEach(function (e, next) {
                                        var url = path + e._id;
                                        if (finalString != "") {
                                            finalString = finalString + "," + url;
                                        }
                                        else {
                                            finalString = finalString + url;
                                        }
                                        next(e);
                                    }, function () {
                                        CatalogueBlogs.write({
                                            "_id": params._id,
                                            "catalogue_id": params.catalogue_id || originalData.catalogue_id,
                                            "header": params.header || originalData.header,
                                            "document_id": params.document_id || originalData.document_id,
                                            "urls": finalString || originalData.urls
                                        },
                                            function (res) {
                                                if (res.success) {
                                                    me.sendData(res);
                                                } else {
                                                    me.sendError();
                                                }
                                            }, {
                                                add: 1,
                                                modify: 1
                                            });
                                    });
                                }
                            });
                        } else {
                            return me.addErrorAndSend('ERR024', 'email');
                        }
                    });
                }
            ].runEach();
        }
    }
});