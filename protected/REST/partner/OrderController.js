Ext.define('Crm.REST.partner.OrderController', {
    extend: "Crm.REST.RestBaseController",
    constructor: function () {
        this.callParent(arguments);
        this.initModels();
    },
    initModels: function () {
        var me = this;
        me.TransactionModel = Ext.create('Crm.modules.orderTransaction.model.OrderTransactionModel', {
            scope: me
        });
        me.TransactionModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        me.OrderItemsModel = Ext.create('Crm.modules.orderTransactionItems.model.OrderTransactionItemsModel', {
            scope: me
        });
        me.OrderItemsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        me.OrderCharityModel = Ext.create('Crm.modules.orderCharityDetails.model.OrderCharityDetailsModel', {
            scope: me
        });
        me.OrderCharityModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        me.AccountHoldersModel = Ext.create('Crm.modules.accountHolders.model.AccountHoldersModel', {
            scope: me
        });
        me.AccountHoldersModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
    },
    /*
    @author : Vaibhav Vaidya
    @date : 28 June 2017, Revised on 4th July
    @function : apiProcessOrder
    @desc : Processes order 
    */
    apiProcessOrder: function (cb) {
        var me = this;
        var reqBody = me.req.body.reqData;
        var toStoreReq= Ext.clone(me.req.body.reqData);
        var followFlag=false, walletOrderId=me.Util.getObjectId();
        var BillRangeModel = Ext.create('Crm.modules.billRange.model.BillRangeModel', {
            scope: me
        });
        BillRangeModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var ItemRangeModel = Ext.create('Crm.modules.itemRange.model.ItemRangeModel', {
            scope: me
        });
        ItemRangeModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var FoundationModel = Ext.create('Crm.modules.organizations.model.FoundModel', {
            scope: me
        });
        FoundationModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var AccountHoldersModel = Ext.create('Crm.modules.accountHolders.model.AccountHoldersModel', {
            scope: me
        });
        AccountHoldersModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var DetailsModel = Ext.create('Crm.modules.charityPlanDetails.model.CharityPlanDetailsModel', {
            scope: me
        });
        DetailsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
      

        var DB_TRANSACTION = {
            BEGIN: function (next) {
                opt = Ext.clone(me.config.pgsql);
                opt.callback = function (conn) {
                    me.initModels();
                    me.src.db.conn.query('BEGIN', function (err) {
                        if (err) {
                            me.sendError(err);
                        } else {
                            next();
                        }
                    });
                }
                me.src.db = Ext.create("Database.drivers.Postgresql.Database", opt);
            },
            COMMIT: function (data) {
                me.src.db.conn.query('COMMIT', function () {
                        me.sendData(data) 
                });
            },
            ROLLBACK: function (err) {
                me.src.db.conn.query('ROLLBACK', function () {
                    me.sendError(err);
                });
            }
        }
        var handleError = function (err) {
            return DB_TRANSACTION.ROLLBACK(err);
        };
        [
            function (err, client) {
                if (client) {
                    if (me.validateRequest([{
                                type: 'ObjectID',
                                field: "foundationId",
                                isRequired: true
                            },{
                                type: 'string',
                                field: "shopReceiptId",
                                isRequired: true
                            },{
                                type: 'string',
                                field: "username",
                                isRequired: true
                            },
                            {
                                type: 'date',
                                field: "orderCreateTimestamp",
                                isRequired: true
                            },
                            {
                                type: 'number',
                                field: "totalAmount",
                                isRequired: true
                            },
                            {
                                type: 'string',
                                field: "status",
                                isRequired: true
                            },
                            {
                                type: 'ARRAY', field: 'items', items: [
                                    {
                                        type: 'string',
                                        field: "sku",
                                        isRequired: true
                                    },
                                    {
                                        type: 'number',
                                        field: "quantity",
                                        isRequired: true
                                    },
                                    {
                                        type: 'number',
                                        field: "price",
                                        isRequired: true
                                    }
                                ],isRequired:true
                            }
                        ])) {
                        [
                            function (next) {
                                //Delete this function if validateRequest for Array from RestBaseController is fixed
                                for (var i = 0; i < reqBody.items.length; i++) {
                                    if (reqBody.items[i].sku != undefined && reqBody.items[i].quantity != undefined && reqBody.items[i].price != undefined) {
                                        if (typeof reqBody.items[i].sku == "string" && (typeof reqBody.items[i].quantity == "string" || typeof reqBody.items[i].quantity == "number") && (typeof reqBody.items[i].price == "string" || typeof reqBody.items[i].price == "number")) {
                                            if (i == reqBody.items.length - 1) {
                                                next();
                                            }
                                        }
                                        else {
                                            return me.addErrorAndSend('ERR013');
                                            break;
                                        }
                                    }
                                    else {
                                        return me.addErrorAndSend('ERR053');
                                        break;
                                    }
                                }
                            },
                            //Check if Partner exists, Check if user is following Partner
                            function (next) {
                                me.insertRawOrder(toStoreReq, function(storeData){
                                    FoundationModel.dbCollection.findOne({
                                        _id: reqBody.foundationId
                                    }, {_id:1,donation_type:1}, function (err, data) {
                                        if (data) {
                                            next(data,storeData);
                                        } else {
                                            me.updateRawOrder(storeData._id, null, "Failed. Partner not found", function(storeData){
                                                return me.addErrorAndSend('ERR033');                                                
                                            })
                                        }
                                    });
                                })
                            },
                            //Retrieve user's follow status
                            function (foundData, storeData, next) {
                                AccountHoldersModel.dbCollection.findOne({
                                        username: reqBody.username,
                                    }, {_id:1,username:1,follow:1,user_type:1}, function (err, data) {
                                        if (data) {
                                            if(data.user_type && data.user_type!=me.config.endUser)
                                            {
                                                return me.addErrorAndSend('ERR035','Invalid User Please Select other user'); 
                                            }
                                            if (data.follow == reqBody.foundationId) {
                                                followFlag = true;
                                                data.doesExist=true;
                                                next(foundData, storeData, data);
                                            }
                                            else {
                                                followFlag = false;
                                                data.doesExist=true;
                                                next(foundData, storeData, data);
                                            }
                                        } else {
                                            /*me.updateRawOrder(storeData._id, null, "Failed. User not found", function(storeData){
                                                return me.addErrorAndSend('ERR033');                                                
                                            })*/
                                            followFlag=false;
                                            next(foundData, storeData, {"username": reqBody.username, 'doesExist':false});
                                        }
                                    });
                            },
                            //get Names for items based on sku
                            function (foundData, storeData, userData, next) {
                                var skuList = [], itemData;
                                for (var i = 0; i < reqBody.items.length; i++) {
                                    skuList.push(reqBody.items[i].sku);
                                }
                                var sql = "select _id,sku,name from pgd_catalogue where pid=$1 and sku in (\'" + skuList.join('\',\'') + "\')";
                                me.src.db.query(sql, [reqBody.foundationId], function (e, data) {
                                    if (data && data.length > 0) {
                                        for (i = 0; i < reqBody.items.length; i++) {
                                            for (var j = 0; j < data.length; j++) {
                                                if (reqBody.items[i].sku == data[j].sku) {
                                                    reqBody.items[i].dataIndex = j;
                                                    reqBody.items[i].itemName = data[j].name;
                                                }
                                            }
                                        }
                                        next (foundData, storeData, userData, data, skuList, next)
                                    } else {
                                        me.updateRawOrder(storeData._id, null, "Items not available in catalogue", function (storeData) {
                                            return me.addErrorAndSend('ERR036', 'Items not available in catalogue');
                                        });
                                    }
                                });
                            },
                            function (foundData, storeData, userData, itemData, skuList, next) {
                                if(foundData.donation_type.toUpperCase()=="T")
                                    {
                                        //Donation on Total, Donation calculation to be added in main table Order_Transaction
                                        BillRangeModel.dbCollection.findOne({
                                            partner_id: reqBody.foundationId,
                                            from_total: { $lte: reqBody.totalAmount},
                                            to_total: { $gte: reqBody.totalAmount},
                                            removed: {$ne: 1}
                                        }, {}, function (err, data) {
                                            if (data) {
                                                foundData.donation_amount = parseFloat(data.donation_fixed_amount) + (parseFloat(reqBody.totalAmount) * (parseFloat(data.donation_percentage_amount) / 100));
                                                foundData.follow_amount = parseFloat(data.follow_fixed_amount) + (parseFloat(reqBody.totalAmount) * (parseFloat(data.follow_percentage_amount) / 100));
                                                next(foundData, storeData, userData,skuList);
                                            }
                                            else {
                                                var whereClause="where partner_id=$1 and removed != 1";
                                                var sqlMax=" (select Max(from_total) from bill_range "+whereClause+" )";
                                                var sql="select br._id,br.donation_fixed_amount, br.follow_fixed_amount, br.donation_percentage_amount, br.follow_percentage_amount from bill_range br "+whereClause+" and br.from_total= "+sqlMax+" and br.from_total <= $2";
                                                me.src.db.query(sql, [reqBody.foundationId,reqBody.totalAmount], function (e, data) {
                                                    if (data && data[0]) {
                                                        foundData.donation_amount = parseFloat(data[0].donation_fixed_amount) + (parseFloat(reqBody.totalAmount) * (parseFloat(data[0].donation_percentage_amount) / 100));
                                                        foundData.follow_amount = parseFloat(data[0].follow_fixed_amount) + (parseFloat(reqBody.totalAmount) * (parseFloat(data[0].follow_percentage_amount) / 100));
                                                        next(foundData, storeData, userData,skuList);
                                                    } else {
                                                        foundData.donation_amount = 0;
                                                        foundData.donation_amount = 0;
                                                        next(foundData, storeData, userData,skuList);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                //calculate donation/follow per item purchased
                                else if(foundData.donation_type.toUpperCase()=="I"){
                                    var catalogueId;
                                    if (itemData && itemData.length > 0) {
                                        reqBody.items.prepEach(function (r, next) {
                                            catalogueId = itemData[r.dataIndex]._id;
                                            ItemRangeModel.dbCollection.findOne({
                                                catalogue_id: itemData[r.dataIndex]._id,
                                                from_quantity: { $lte: r.quantity },
                                                to_quantity: { $gte: r.quantity },
                                                removed: { $ne: 1 }
                                            }, {}, function (err, data) {
                                                if (data) {
                                                    r.donation_fixed_amount = data.donation_fixed_amount;
                                                    r.follow_fixed_amount = data.follow_fixed_amount;
                                                    r.donation_percentage_amount = data.donation_percentage_amount;
                                                    r.follow_percentage_amount = data.follow_percentage_amount;
                                                    next(r);
                                                } else {
                                                    //if No match found, find nearest
                                                    // var whereClause=" where catalogue_id=$1 and removed != 1 ";
                                                    // var sqlMax=" (select Max(from_quantity) from item_range "+whereClause+" ) ";
                                                    // var sql="select ir._id,ir.donation_fixed_amount, ir.follow_fixed_amount, ir.donation_percentage_amount, ir.follow_percentage_amount from item_range ir "+whereClause+" and ir.from_quantity= "+sqlMax+" and ir.from_quantity <= $2" ;
                                                    // me.src.db.query(sql, [catalogueId,r.quantity], function (e, data) {
                                                    //     if (data && data[0]) {
                                                    //         r.donation_fixed_amount = data[0].donation_fixed_amount;
                                                    //         r.follow_fixed_amount = data[0].follow_fixed_amount;
                                                    //         r.donation_percentage_amount = data[0].donation_percentage_amount;
                                                    //         r.follow_percentage_amount = data[0].follow_percentage_amount;
                                                    //         next(r);
                                                    //     } else {
                                                    //         r.donation_fixed_amount = 0;
                                                    //         r.follow_fixed_amount = 0;
                                                    //         r.donation_percentage_amount = 0;
                                                    //         r.follow_percentage_amount = 0;
                                                    //         next(r);
                                                    //     }
                                                    // });
                                                    r.donation_fixed_amount = 0;
                                                    r.follow_fixed_amount = 0;
                                                    r.donation_percentage_amount = 0;
                                                    r.follow_percentage_amount = 0;
                                                    next(r);

                                                }
                                            });
                                        }, function () {
                                            next(foundData, storeData, userData, skuList);
                                        });
                                    } else {
                                        me.updateRawOrder(storeData._id, null, "Items not available in catalogue", function (storeData) {
                                            return me.addErrorAndSend('ERR036', 'Items not available in catalogue');
                                        })
                                    }
                                }
                                else{
                                    return me.addErrorAndSend('ERR035','Invalid donation type for partner.Contact Admin');
                                }
                            },
                            function (foundData, storeData, userData, skuList, next) {
                                //Calculate Grand Donation/Follow as per Partner's donation setting.
                                var grandDonation = 0, grandFollow = 0;
                                if (foundData.donation_type.toUpperCase() == "I") {
                                    //Calculating Grand Donation and Follow per item
                                    reqBody.items.prepEach(function (r, nextItem) {
                                        r.item_donation_amount = (parseFloat(r.donation_fixed_amount) * parseFloat(r.quantity)) + ((parseFloat(r.price) * parseFloat(r.quantity)) * (parseFloat(r.donation_percentage_amount) / 100.0));
                                        r.item_follow_amount = (parseFloat(r.follow_fixed_amount) * parseFloat(r.quantity)) + ((parseFloat(r.price) * parseFloat(r.quantity)) * (parseFloat(r.follow_percentage_amount) / 100.0));
                                        grandDonation += r.item_donation_amount;
                                        grandFollow += r.item_follow_amount;
                                        nextItem(r);
                                    }, function () {
                                        if (followFlag == false) {
                                            grandFollow = 0;
                                        }
                                        next(foundData, storeData, userData, grandDonation, grandFollow, skuList);
                                    });
                                }
                                else if (foundData.donation_type.toUpperCase() == "T") {
                                    grandDonation = foundData.donation_amount;
                                    if (followFlag == true) {
                                        grandFollow = foundData.follow_amount;
                                    }
                                    next(foundData, storeData, userData, grandDonation, grandFollow, skuList);
                                }
                                else {
                                    me.updateRawOrder(storeData._id, null, "Items not available in catalogue", function (storeData) {
                                        return me.addErrorAndSend('ERR044');
                                    })
                                }
                            },
                            //Brand Partner Start. Calculate Brand partner's donation per item
                            function (foundData, storeData, userData, grandDonation, grandFollow, skuList, next) {
                                var skuFetchData;
                                var sql = "select pgdc._id, pgdc.pid, pgdc.sku from pgd_catalogue pgdc inner join pgd_foundations pgdf on pgdf._id=pgdc.pid where pgdc.sku in (\'" + skuList.join('\',\'')  + "\') and pgdf.orgtype=\'3\' and pgdc.pid not in($1)";
                                me.src.db.query(sql, [reqBody.foundationId], function (e, data) {
                                    if (data && data.length >0) {
                                        reqBody.items.prepEach(function (r, next) {
                                            r.dataIndex=undefined;
                                            for (var i = 0; i < data.length; i++) {
                                                if (r.sku == data[i].sku) {
                                                    r.dataIndex = i;
                                                }
                                            }
                                            skuFetchData=data;
                                            if (r.dataIndex != undefined) {
                                                r.brandPartnerId = data[r.dataIndex].pid;
                                                ItemRangeModel.dbCollection.findOne({
                                                    catalogue_id: data[r.dataIndex]._id,
                                                    from_quantity: { $lte: r.quantity },
                                                    to_quantity: { $gte: r.quantity },
                                                    removed: { $ne: 1 }
                                                }, {}, function (err, data) {
                                                    if (data) {
                                                        r.brand_donation = (parseFloat(data.donation_fixed_amount) * parseFloat(r.quantity)) + ((parseFloat(r.price) * parseFloat(r.quantity)) * (parseFloat(data.donation_percentage_amount) / 100.0));
                                                        next(r);
                                                    } else {
                                                        //if No match found, find nearest
                                                        //start
                                                        var whereClause = " where catalogue_id=$1 and removed != 1 ";
                                                        var sqlMax = " (select Max(from_quantity) from item_range " + whereClause + " ) ";
                                                        var sql = "select ir._id,ir.donation_fixed_amount, ir.follow_fixed_amount, ir.donation_percentage_amount, ir.follow_percentage_amount from item_range ir " + whereClause + " and ir.from_quantity= " + sqlMax + " and ir.from_quantity <= $2";
                                                        me.src.db.query(sql, [skuFetchData[r.dataIndex]._id, r.quantity], function (e, data) {
                                                            if (data && data[0]) {
                                                                r.brand_donation = (parseFloat(data[0].donation_fixed_amount) * parseFloat(r.quantity)) + ((parseFloat(r.price) * parseFloat(r.quantity)) * (parseFloat(data[0].donation_percentage_amount) / 100.0));
                                                                next(r);
                                                            } else {
                                                                r.brand_donation = 0;
                                                                next(r);
                                                            }
                                                        });
                                                        //end
                                                    }
                                                });
                                            }
                                            else {
                                                r.brand_donation = 0;
                                                next(r);
                                            }
                                        }, function () {
                                            next(foundData, storeData, userData, grandDonation, grandFollow);
                                        });
                                    } else {
                                        reqBody.items.prepEach(function (r, next) {
                                            r.brand_donation = 0;
                                            next(r)
                                        }, function () {
                                            next(foundData, storeData, userData, grandDonation, grandFollow);
                                        });
                                    }
                                });

                            },
                            //begin statement
                            function (foundData, storeData, userData, grandDonation, grandFollow, next) {
                                DB_TRANSACTION.BEGIN(function () {
                                    next(foundData, storeData, userData, grandDonation, grandFollow);
                                });
                            },
                            //Brand Partner's Order Transaction
                            function (foundData, storeData, userData, grandDonation, grandFollow, next) {
                                var brandPartnerlist = [], brandPartnerData = [],brandData=[];
                                //select distinct Brand partners
                                for (var i = 0; i < reqBody.items.length; i++) {
                                    if (brandPartnerlist.indexOf(reqBody.items[i].brandPartnerId) == -1) {
                                        brandPartnerlist.push(reqBody.items[i].brandPartnerId);
                                    }
                                }
                                //calculate Grand Donation per Brand partner
                                for (var j = 0; j < brandPartnerlist.length; j++) {
                                    for (var i = 0; i < reqBody.items.length; i++) {
                                        if (reqBody.items[i].brandPartnerId == brandPartnerlist[j]) {
                                            if (!brandPartnerData[j]) {
                                                brandPartnerData[j] = {};
                                                brandPartnerData[j].brandTransactionId = me.Util.getObjectId();
                                                brandPartnerData[j].grandBrandDonation = 0;
                                                brandPartnerData[j].brandTotalAmount = 0;
                                                brandPartnerData[j].brandPartnerId = reqBody.items[i].brandPartnerId;
                                            }
                                            reqBody.items[i].brandTransactionId = brandPartnerData[j].brandTransactionId;
                                            brandPartnerData[j].grandBrandDonation += reqBody.items[i].brand_donation
                                            brandPartnerData[j].brandTotalAmount += (parseFloat(reqBody.items[i].price) * parseFloat(reqBody.items[i].quantity));
                                        }
                                    }
                                }
                                //Brand partner's Order Transaction
                                brandPartnerData.prepEach(function (r, next) {
                                    if (r.brandPartnerId != undefined) {
                                        me.TransactionModel.write({
                                            "_id": r.brandTransactionId,
                                            "partner_id": r.brandPartnerId,
                                            "username": reqBody.username,
                                            "shop_receipt_id": reqBody.shopReceiptId,
                                            "order_create_timestamp": reqBody.orderCreateTimestamp,
                                            "total_amount": r.brandTotalAmount,
                                            "status": "Itemwise Brand Transaction",
                                            "donation_amount": r.grandBrandDonation || 0,
                                            "follow_amount": 0,
                                            "claimed_status":0,
                                            "claimed_amount":0
                                        }, function (data) {
                                            if (data.success) {
                                                brandData.push(data.record);
                                                next(r);
                                            } else {
                                                return DB_TRANSACTION.ROLLBACK(data.error);
                                            }
                                        }, {
                                                add: 1,
                                                modify: 1
                                            });
                                    }
                                    else {
                                        next(r);
                                    }
                                }, function () {
                                    next(foundData, storeData, userData, brandData, grandDonation, grandFollow);
                                });
                            },                            
                            //Brand Partner Order Transaction Items
                            function (foundData, storeData, userData, brandData, grandDonation, grandFollow, next) {
                                reqBody.items.prepEach(function (r, next) {
                                    if (r.brandPartnerId != undefined) {
                                        me.OrderItemsModel.write({
                                            "_id": me.Util.getObjectId(),
                                            "order_transaction_id": r.brandTransactionId,
                                            "item_name": r.itemName,
                                            "price": r.price,
                                            "quantity": r.quantity,
                                            "donation_amount": r.brand_donation || 0,
                                            "follow_amount": 0,
                                            "donation_status": "Brand not Processed"
                                        }, function (data) {
                                            if (data.success) {
                                                next(r);
                                            } else {
                                                return DB_TRANSACTION.ROLLBACK(data.error);
                                            }
                                        }, {
                                                add: 1,
                                                modify: 1
                                            });
                                    }
                                    else {
                                        next(r);
                                    }
                                }, function () {
                                    next(foundData, storeData, userData, brandData, grandDonation, grandFollow)
                                });
                            },
                            //Partner's Order Transaction
                            function (foundData, storeData, userData,brandData,grandDonation,grandFollow,next) {
                                me.TransactionModel.write({
                                    "_id": me.Util.getObjectId(),
                                    "partner_id":reqBody.foundationId,
                                    "username": reqBody.username,
                                    "shop_receipt_id": reqBody.shopReceiptId,
                                    "order_create_timestamp": reqBody.orderCreateTimestamp,
                                    "total_amount": reqBody.totalAmount,
                                    "status": foundData.donation_type.toUpperCase() == "I"?"Itemwise Contributor Donation:"+reqBody.status:foundData.donation_type.toUpperCase() == "T"?"Billwise Contributor Donation: "+reqBody.status:reqBody.status,
                                    "donation_amount":grandDonation||0, 
                                    "follow_amount":grandFollow||0,
                                    "claimed_status":0,
                                    "claimed_amount":0
                                }, function (data) {
                                    if (data.success) {
                                        foundData.orderTransactionId=data.record._id;
                                        foundData.donation_amount=data.record.donation_amount;
                                        foundData.follow_amount=data.record.follow_amount;
                                        foundData.claimed_status=data.record.claimed_status;
                                        foundData.claimed_amount=data.record.claimed_amount;
                                        foundData.username=data.record.username;
                                        next(foundData, storeData, userData,brandData, (parseFloat(grandDonation)+parseFloat(grandFollow)));
                                    } else {
                                        return DB_TRANSACTION.ROLLBACK(data.error);
                                    }
                                }, {
                                    add: 1,
                                    modify: 1
                                });
                            },
                            //Partner's Order Transaction Items
                            function (foundData, storeData, userData,brandData,grandTotal, next) {
                                //if(foundData.donation_type.toUpperCase() == "I")
                                //{
                                    //Donation happens per item
                                    reqBody.items.prepEach(function (r, next) {
                                        if(followFlag==false)
                                            {
                                                r.item_follow_amount=0;
                                            }
                                        me.OrderItemsModel.write({
                                            "_id": me.Util.getObjectId(),
                                            "order_transaction_id": foundData.orderTransactionId,
                                            "item_name": r.itemName,
                                            "price": r.price,
                                            "quantity": r.quantity,
                                            "donation_amount": r.item_donation_amount||0,
                                            "follow_amount": r.item_follow_amount||0,
                                            "donation_status": "Not Processed"
                                        }, function (data) {
                                            if (data.success) {
                                                next(r);
                                            } else {
                                                return DB_TRANSACTION.ROLLBACK(data.error);
                                            }
                                        }, {
                                                add: 1,
                                                modify: 1
                                            });
                                    }, function () {
                                        next(foundData, storeData, userData,brandData,grandTotal);
                                    });
                                /*}
                                else{
                                    next(foundData, storeData, userData,brandData,grandTotal);
                                }*/
                            },

                            //Edit 9 September 2017. Charity plans donation switched to charity items.
                            function (foundData, storeData, userData, brandData, grandTotal, next) {
                                var activeIndex, AddWallet = false;
                                if(userData.doesExist==true)
                                {
                                    me.retrieveCharityPlans(userData._id, function (data) {
                                        if (data) {
                                            for (var i = 0; i < data.length; i++) {
                                                if (data[i].plan_type == 'A') {
                                                    activeIndex = i;
                                                }
                                            }
                                            //Retrieve Details plan by 'ACTIVE' charity plan
                                            if (!isNaN(activeIndex)) {
                                                me.checkActivePlanQualify(data[activeIndex]._id,grandTotal, function (activeData) {
                                                    if (activeData && activeData.walletEmergencyFlag != undefined) {
                                                        AddWallet = activeData.walletEmergencyFlag;
                                                        next(foundData, storeData, userData, brandData, grandTotal, AddWallet);
                                                    }
                                                    else {
                                                        AddWallet = true;
                                                        next(foundData, storeData, userData, brandData, grandTotal, AddWallet);
                                                    }
                                                });
                                            }
                                            else {
                                                AddWallet = true;
                                                next(foundData, storeData, userData, brandData, grandTotal, AddWallet);
                                            }
                                        }
                                        else {
                                            AddWallet = true;
                                            next(foundData, storeData, userData, brandData, grandTotal, AddWallet);
                                        }
                                    });
                                }
                                else{
                                    AddWallet = true;
                                    next(foundData, storeData, userData, brandData, grandTotal, AddWallet);
                                }
                            },
                            function (foundData, storeData, userData, brandData, grandTotal, AddWallet, next) {
                                //User exists, credit donated total amount to wallet
                                if(walletOrderId!=undefined) foundData.walletOrderId=walletOrderId
                                //below condition commented after ronak sirs confirmations
                                // if (AddWallet == true && userData.doesExist == true) {
                                if (userData.doesExist == true) {
                                    //if user exists, add brand partners' donation to his wallet too
                                    for(var i=0; i< brandData.length;i++){
                                        grandTotal+=(parseFloat(brandData[i].donation_amount) + parseFloat(brandData[i].follow_amount))
                                    }
                                     if(foundData.donation_amount<grandTotal) foundData.donation_amount=grandTotal;
                                     me.creditUserWallet(userData, grandTotal, function (err, resp) {
                                        if (resp.success == true) {
                                            
                                            next(foundData, storeData, brandData);
                                        }
                                        else {
                                            return DB_TRANSACTION.ROLLBACK(err);
                                        }
                                    });
                                }
                                //user doesn't exist. Donate directly to Partner's charity plan items
                                else if (AddWallet == true && userData.doesExist==false) {
                                    var activeIndex=-1, partnerEmergencyFlag;
                                    me.retrieveCharityPlans(reqBody.foundationId, function (data) {
                                        if (data) {
                                            for (var i = 0; i < data.length; i++) {
                                                if (data[i].plan_type == 'A') {
                                                    activeIndex = i;
                                                }
                                            }
                                            //Retrieve Details plan by 'ACTIVE' charity plan
                                            if (!isNaN(activeIndex)) {
                                                me.checkActivePlanQualify(data[activeIndex]._id, grandTotal, function (activeData) {
                                                    if (activeData && activeData.walletEmergencyFlag != undefined) {
                                                        partnerEmergencyFlag = activeData.walletEmergencyFlag;
                                                        me.donateCharityPlanNow(reqBody.foundationId, !partnerEmergencyFlag, grandTotal, foundData, function (err, otdata) {
                                                            if (otdata && !err) {
                                                                foundData = otdata;
                                                                next(foundData, storeData, brandData);
                                                            }
                                                            else {
                                                                return DB_TRANSACTION.ROLLBACK(err);
                                                            }
                                                        });
                                                    }
                                                    else {
                                                        partnerEmergencyFlag = true;
                                                        me.donateCharityPlanNow(reqBody.foundationId, !partnerEmergencyFlag, grandTotal, foundData, function (err, otdata) {
                                                            if (otdata && !err) {
                                                                foundData = otdata;
                                                                next(foundData, storeData, brandData);
                                                            }
                                                            else {
                                                                return DB_TRANSACTION.ROLLBACK(err);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                            else {
                                                partnerEmergencyFlag = true;
                                                me.donateCharityPlanNow(reqBody.foundationId, !partnerEmergencyFlag, grandTotal, foundData, function (err, otdata) {
                                                    if (otdata && !err) {
                                                        foundData = otdata;
                                                        next(foundData, storeData, brandData);
                                                    }
                                                    else {
                                                        return DB_TRANSACTION.ROLLBACK(err);
                                                    }
                                                });
                                            }
                                        }
                                        else {
                                            partnerEmergencyFlag = true;
                                            me.donateCharityPlanNow(reqBody.foundationId, !partnerEmergencyFlag, grandTotal, foundData, function (err, otdata) {
                                                if (otdata && !err) {
                                                    foundData = otdata;
                                                    next(foundData, storeData, brandData);
                                                }
                                                else {
                                                    return DB_TRANSACTION.ROLLBACK(err);
                                                }
                                            });
                                        }
                                    });
                                }
                                //user exists. Donate directly to User's charity plan items
                                //below code commented after confirmation by ronak sir
                                // else if (AddWallet == false && userData.doesExist == true) {
                                //     me.donateCharityPlanNow(userData._id, !AddWallet, grandTotal, foundData, function (err, otdata) {
                                //         if (otdata && !err) {
                                //             foundData = otdata;
                                //             next(foundData, storeData, brandData);
                                //         }
                                //         else {
                                //             return DB_TRANSACTION.ROLLBACK(err);
                                //         }
                                //     });
                                // }
                            },
                            //End Edit
                            //Retrieve user's charity plans and make charitywise donation
                            /*function (foundData, storeData, userData, brandData, grandDonation, grandFollow, next) {
                                var activeIndex, partnerPlan = false, charityPlanData;
                                if(userData.doesExist==true)
                                {
                                    me.retrieveCharityPlans(userData._id, function (data) {
                                        if (data) {
                                            charityPlanData = data;
                                            for (var i = 0; i < data.length; i++) {
                                                if (data[i].plan_type == 'A') {
                                                    activeIndex = i;
                                                }
                                            }
                                            //Retrieve Details plan by 'ACTIVE' charity plan
                                            if (!isNaN(activeIndex)) {
                                                DetailsModel.dbCollection.find({
                                                    charity_plan_id: data[activeIndex]._id
                                                }, {}, function (err, data) {
                                                    if (data && data.length > 0) {
                                                        var limit;
                                                        data.prepEach(function (r, next) {
                                                            if (partnerPlan == false) {
                                                                r.charityDonationAmount = (parseFloat(grandDonation) + parseFloat(grandFollow)) * (parseFloat(r.percentage) / 100.0);
                                                                //Checking for charity_type and donation amount limit per charity
                                                                FoundationModel.dbCollection.findOne({
                                                                    _id: r.charity_foundation_id
                                                                }, {}, function (err, data) {
                                                                    if (data) {
                                                                        limit = data.extra_info.amount_limit;
                                                                        if (data.extra_info.charity_type == 'L' && data.active == true) {
                                                                            var sql = "select sum(charity_amount) as start from order_charity_details ocd where charity_id=$1"
                                                                            me.src.db.query(sql, [r.charity_foundation_id], function (e, data) {
                                                                                if (data && data.length > 0) {
                                                                                    if (parseFloat(data[0].start) + parseFloat(r.charityDonationAmount) > parseFloat(limit)) {
                                                                                        partnerPlan = true;
                                                                                        next(r);
                                                                                    }
                                                                                    else {
                                                                                        next(r);
                                                                                    }
                                                                                }
                                                                                else {
                                                                                    //There are no current charity donations for this foundation, check next
                                                                                    next(r);
                                                                                }
                                                                            });
                                                                        }
                                                                        else if (data.extra_info.charity_type == 'L' && data.active == false) {
                                                                            partnerPlan = true;
                                                                            next(r);
                                                                        }
                                                                        else {
                                                                            next(r);
                                                                        }
                                                                    }
                                                                    else {
                                                                        return me.addErrorAndSend('ERR038');
                                                                    }
                                                                });
                                                            }
                                                            else {
                                                                next(r);
                                                            }
                                                        }, function () {
                                                            next(foundData, storeData, userData, brandData, grandDonation, grandFollow, charityPlanData, activeIndex, partnerPlan);
                                                        });
                                                    }
                                                    else {
                                                        partnerPlan = true;
                                                        next(foundData, storeData, userData, brandData, grandDonation, grandFollow, charityPlanData, activeIndex, partnerPlan);
                                                    }
                                                });
                                            }
                                            else {
                                                partnerPlan = true;
                                                next(foundData, storeData, userData, brandData, grandDonation, grandFollow, charityPlanData, activeIndex, partnerPlan);
                                            }
                                        }
                                        else {
                                            partnerPlan = true;
                                            next(foundData, storeData, userData, brandData, grandDonation, grandFollow, charityPlanData, activeIndex, partnerPlan);
                                        }
                                    });
                                }
                                else{
                                    partnerPlan = true;
                                    next(foundData, storeData, userData, brandData, grandDonation, grandFollow, charityPlanData, activeIndex, partnerPlan);
                                }
                            },
                            function (foundData, storeData, userData, brandData, grandDonation, grandFollow, charityPlanData, activeIndex, partnerPlan, next) {
                                //Writing of charitywise percentage distribution by user's ActiveEmergency plan
                                if (partnerPlan == false) {
                                    if (!isNaN(activeIndex)) {
                                        DetailsModel.dbCollection.find({
                                            charity_plan_id: charityPlanData[activeIndex]._id
                                        }, {}, function (err, data) {
                                            if (data) {
                                                data.prepEach(function (r, next) {
                                                    r.charityDonationAmount = (parseFloat(grandDonation) + parseFloat(grandFollow)) * (parseFloat(r.percentage) / 100.0);
                                                    me.OrderCharityModel.write({
                                                        "_id": me.Util.getObjectId(),
                                                        "partner_id": reqBody.foundationId,
                                                        "charity_id": r.charity_foundation_id,
                                                        "username": reqBody.username,
                                                        "order_transaction_id": foundData.orderTransactionId,
                                                        "charity_plan_id": r.charity_plan_id,
                                                        "charity_amount": r.charityDonationAmount,
                                                        "total_amount": (parseFloat(grandDonation) + parseFloat(grandFollow)) || 0
                                                    }, function (data) {
                                                        if (data.success) {
                                                            next(r);
                                                        } else {
                                                            return DB_TRANSACTION.ROLLBACK(data.error);
                                                        }
                                                    }, {
                                                            add: 1,
                                                            modify: 1
                                                        });
                                                }, function () {
                                                    next(foundData, storeData, brandData, grandDonation, grandFollow, partnerPlan)
                                                });
                                            }
                                            else {
                                                return me.addErrorAndSend('ERR038');
                                            }
                                        });
                                    }
                                }
                                else {
                                    next(foundData, storeData, brandData, grandDonation, grandFollow, partnerPlan)
                                }
                            },
                            //Start Partner's donation charity wise if user has no charity plan set
                            function (foundData, storeData, brandData, grandDonation, grandFollow, partnerPlan, next) {
                                var activeIndex, insertionIndex, emergencyFlag = false, charityPlanData;
                                if (partnerPlan == true) {
                                    me.retrieveCharityPlans(foundData._id, function (data) {
                                        if (data) {
                                            charityPlanData = data;
                                            for (var i = 0; i < data.length; i++) {
                                                if (data[i].plan_type == 'A') {
                                                    activeIndex = i;
                                                }
                                                else if (data[i].plan_type == 'E') {
                                                    //Emergency Index stored as backup
                                                    insertionIndex = i;
                                                }
                                            }
                                            //Retrieve Details plan by 'ACTIVE' charity plan
                                            if (!isNaN(activeIndex) && emergencyFlag == false) {
                                                me.retrieveDetailsPlans(data[activeIndex]._id, emergencyFlag, partnerPlan, insertionIndex, grandDonation, grandFollow, function (eFlag, pPlan) {
                                                    emergencyFlag = eFlag;
                                                    next(foundData, storeData, brandData, grandDonation, grandFollow, charityPlanData, activeIndex, insertionIndex, emergencyFlag, partnerPlan);
                                                })
                                            }
                                            else {
                                                if (!isNaN(insertionIndex)) {
                                                    emergencyFlag = true;
                                                    next(foundData, storeData, brandData, grandDonation, grandFollow, charityPlanData, activeIndex, insertionIndex, emergencyFlag, partnerPlan);
                                                }
                                                else {
                                                    partnerPlan = true;
                                                    next(foundData, storeData, brandData, grandDonation, grandFollow, charityPlanData, activeIndex, insertionIndex, emergencyFlag, partnerPlan);
                                                }
                                            }
                                        }
                                        else {
                                            return me.addErrorAndSend('ERR038');
                                        }
                                    });
                                }
                                else {
                                    next(foundData, storeData, brandData, grandDonation, grandFollow, charityPlanData, activeIndex, insertionIndex, emergencyFlag, partnerPlan);
                                }
                            },
                            function (foundData, storeData, brandData, grandDonation, grandFollow, charityPlanData, activeIndex, insertionIndex, emergencyFlag, partnerPlan, next) {
                                //Writing of charitywise percentage distribution For Partner
                                if (partnerPlan == true) {
                                    if (emergencyFlag == false) {
                                        insertionIndex = activeIndex;
                                    }
                                        if (!isNaN(insertionIndex)) {
                                            DetailsModel.dbCollection.find({
                                                charity_plan_id: charityPlanData[insertionIndex]._id
                                            }, {}, function (err, data) {
                                                if (data) {
                                                    data.prepEach(function (r, next) {
                                                        r.charityDonationAmount = (parseFloat(grandDonation) + parseFloat(grandFollow)) * (parseFloat(r.percentage) / 100.0);
                                                        me.OrderCharityModel.write({
                                                            "_id": me.Util.getObjectId(),
                                                            "partner_id": reqBody.foundationId,
                                                            "charity_id": r.charity_foundation_id,
                                                            "username": reqBody.username,
                                                            "order_transaction_id": foundData.orderTransactionId,
                                                            "charity_plan_id": r.charity_plan_id,
                                                            "charity_amount": r.charityDonationAmount,
                                                            "total_amount": (parseFloat(grandDonation) + parseFloat(grandFollow)) || 0
                                                        }, function (data) {
                                                            if (data.success) {
                                                                next(r);
                                                            } else {
                                                                return DB_TRANSACTION.ROLLBACK(data.error);
                                                            }
                                                        }, {
                                                                add: 1,
                                                                modify: 1
                                                            });
                                                    }, function () {
                                                        next(foundData, storeData, brandData, grandDonation, grandFollow)
                                                    });
                                                }
                                                else {
                                                    return me.addErrorAndSend('ERR038');
                                                }
                                            });
                                        }else{
                                            return me.addErrorAndSend('ERR043');
                                        }
                                }
                                else {
                                    next(foundData, storeData, brandData, grandDonation, grandFollow)
                                }
                            },*/
                            //BRAND partner's donation charity wise
                            function (foundData, storeData, brandData, next) {
                                //if user exists, all brand partners' donation go into user wallet else donate as per charity plan per (brand) partner.
                                if (foundData.doesExist == false) {
                                    brandData.prepEach(function (r, next) {
                                        r.partnerEmergencyFlag = false;
                                        //create Partner Object for donateCharityPlanNow
                                        if (r.orderPartnerData == undefined) r.orderPartnerData = {};
                                        r.orderPartnerData._id = r.partner_id;
                                        r.orderPartnerData.username = reqBody.username;
                                        r.orderPartnerData.orderTransactionId = r._id;
                                        r.orderPartnerData.total_amount = r.total_amount;
                                        if (walletOrderId != undefined) r.orderPartnerData.walletOrderId = walletOrderId;
                                        me.retrieveCharityPlans(r.partner_id, function (data) {
                                            if (data) {
                                                r.charityPlanData = data;
                                                for (var i = 0; i < data.length; i++) {
                                                    if (data[i].plan_type == 'A') {
                                                        r.activeIndex = i;
                                                    }
                                                }
                                                //Retrieve Details plan by 'ACTIVE' charity plan
                                                if (!isNaN(r.activeIndex)) {
                                                    me.checkActivePlanQualify(data[r.activeIndex]._id, (r.donation_amount + r.follow_amount), function (activeData) {
                                                        if (activeData && activeData.walletEmergencyFlag != undefined) {
                                                            r.partnerEmergencyFlag = activeData.walletEmergencyFlag;
                                                            me.donateCharityPlanNow(r.partner_id, !r.partnerEmergencyFlag, (r.donation_amount + r.follow_amount), r.orderPartnerData, function (err, otdata) {
                                                                if (otdata && !err) {
                                                                    r.orderPartnerData = otdata;
                                                                    next(r);
                                                                }
                                                                else {
                                                                    return DB_TRANSACTION.ROLLBACK(err);
                                                                }
                                                            });
                                                        }
                                                        else {
                                                            r.partnerEmergencyFlag = true;
                                                            me.donateCharityPlanNow(r.partner_id, !r.partnerEmergencyFlag, (r.donation_amount + r.follow_amount), r.orderPartnerData, function (err, otdata) {
                                                                if (otdata && !err) {
                                                                    r.orderPartnerData = otdata;
                                                                    next(r);
                                                                }
                                                                else {
                                                                    return DB_TRANSACTION.ROLLBACK(err);
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                                else {
                                                    r.partnerEmergencyFlag = true;
                                                    me.donateCharityPlanNow(r.partner_id, !r.partnerEmergencyFlag, (r.donation_amount + r.follow_amount), r.orderPartnerData, function (err, otdata) {
                                                        if (otdata && !err) {
                                                            r.orderPartnerData = otdata;
                                                            next(r);
                                                        }
                                                        else {
                                                            return DB_TRANSACTION.ROLLBACK(err);
                                                        }
                                                    });
                                                }
                                            }
                                            else {
                                                //return me.addErrorAndSend('ERR043',"For Brand partner");
                                                next(r);
                                            }
                                        });
                                    }, function () {
                                        next(foundData, storeData);
                                    });
                                }
                                else {
                                    next(foundData, storeData);
                                }
                            },
                            /*function (foundData, storeData, brandData, next) {
                                //Writing of charitywise percentage distribution For Brand Partner
                                brandData.prepEach(function (r, next) {
                                    if (r.emergencyFlag == false) {
                                        if (r.insertionIndex != undefined)
                                        { r.insertionIndex = r.activeIndex; }
                                    }
                                    if (!isNaN(r.insertionIndex)) {
                                        DetailsModel.dbCollection.find({
                                            charity_plan_id: r.charityPlanData[r.insertionIndex]._id
                                        }, {}, function (err, data) {
                                            if (data) {
                                                data.prepEach(function (d, next) {
                                                    d.charityDonationAmount = (parseFloat(r.donation_amount)+parseFloat(r.follow_amount)) * (parseFloat(d.percentage) / 100.0);
                                                    me.OrderCharityModel.write({
                                                        "_id": me.Util.getObjectId(),
                                                        "partner_id": r.partner_id,
                                                        "charity_id": d.charity_foundation_id,
                                                        "username": reqBody.username,
                                                        "order_transaction_id": r._id,
                                                        "charity_plan_id": d.charity_plan_id,
                                                        "charity_amount": d.charityDonationAmount,
                                                        "total_amount": r.donation_amount || 0
                                                    }, function (data) {
                                                        if (data.success) {
                                                            next(d);
                                                        } else {
                                                            return DB_TRANSACTION.ROLLBACK(data.error);
                                                        }
                                                    }, {
                                                            add: 1,
                                                            modify: 1
                                                        });
                                                }, function () {
                                                    next(r)
                                                });
                                            }
                                            else {
                                                return me.addErrorAndSend('ERR038');
                                            }
                                        });
                                    }
                                    else{
                                        next(r);
                                    }
                                }, function () {
                                    next(foundData, storeData);
                                });
                            },*/
                            //end
                            function (foundData, storeData,next) {
                                me.updateRawOrder(storeData._id, foundData.orderTransactionId, "Transaction completed Successfully", function (storeData) {
                                       // me.sendData(foundData);
                                        next(foundData, storeData);
                                    })
                            },
                            function (foundData, storeData,next) {
                            return DB_TRANSACTION.COMMIT(foundData);
                            }
                        ].runEach();

                    } else {
                        me.sendError();
                    }
                } else {
                    return me.addErrorObj(err);
                }
            }
        ].runEach();
    }
    /*
    @author : Vaibhav Vaidya
    @date : 25 July 2017
    @function : retrieveCharityPlans
    @desc : Returns Active and Emergency plans of a particular Id
    */
    , retrieveCharityPlans: function (toFindId,cb) {
        var me = this;
        var CharityPlanModel = Ext.create('Crm.modules.charityPlan.model.CharityPlanModel', {
            scope: me
        });
        CharityPlanModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        CharityPlanModel.dbCollection.find({
            partner_user_id: toFindId,
            $or: [{ plan_type: 'E' }, { plan_type: 'A' }]
        }, {}, function (err, data) {
            if (data && data.length > 0) {
                cb(data);
            }
            else {
                cb(null);
            }
        });
    }
    /*
    @author : Vaibhav Vaidya
    @date : 9 Sept 2017
    @function : creditUserWallet
    @desc : Credit User's wallet with certain amount
    */
    , creditUserWallet: function (userObj, totalAmt, cb) {
        var me = this, walletbal;
        var AccountHoldersModel = Ext.create('Crm.modules.accountHolders.model.AccountHoldersModel', {
            scope: me
        });
        AccountHoldersModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        AccountHoldersModel.dbCollection.findOne({
            _id: userObj._id
        }, { _id: 1, wallet_balance: 1 }, function (err, userData) {
            if(err==null && userData){
                if(userData.wallet_balance) walletbal=parseFloat(userData.wallet_balance);
                if(walletbal==null || walletbal==undefined) walletbal=0;
                AccountHoldersModel.dbCollection.update({
                _id: userObj._id
            }, {
                    $set: {
                        wallet_balance: walletbal+totalAmt
                    }
                }, function (err, data) {
                    if (err)
                        return handleError(err)
                    else
                        {
                            data.success=true;
                            cb(null, data);
                        }
                });
            }
            else{
                return handleError(err)
            }
        });
    }
    /*
    @author : Vaibhav Vaidya
    @date : 13 Sept 2017
    @function : debitUserWallet
    @desc : Debit User's wallet with certain amount
    */
    , debitUserWallet: function (userObj, totalAmt, cb) {
        var me = this, walletbal;
        var AccountHoldersModel = Ext.create('Crm.modules.accountHolders.model.AccountHoldersModel', {
            scope: me
        });
        var handleError = function (err) {
            return cb(err, null);
        }
        AccountHoldersModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        AccountHoldersModel.dbCollection.findOne({
            _id: userObj._id
        }, { _id: 1, wallet_balance: 1 }, function (err, userData) {
            if (err == null && userData) {
                if (userData.wallet_balance) walletbal = parseFloat(userData.wallet_balance);
                if ((walletbal != null || walletbal != undefined) && userData.wallet_balance>=totalAmt) {
                    AccountHoldersModel.dbCollection.update({
                        _id: userObj._id
                    }, {
                            $set: {
                                wallet_balance: walletbal - totalAmt
                            }
                        }, function (err, data) {
                            if (err)
                                return handleError(err)
                            else {
                                data.success = true;
                                cb(null, data);
                            }
                        });
                }
                else {
                    return handleError({ code: 'ERR054' });
                }
            }
            else {
                return handleError(err)
            }
        });
    }
    /*
    @author : Vaibhav Vaidya
    @date : 12 Sept 2017
    @function : upsertDonateNowInfo
    @desc : Update + Insert Donate now Info
    */
    , upsertDonateNowInfo: function (orderPartnerData, donateTotalAmt, cb) {
        var me = this;
        var DonateModel = Ext.create('Crm.modules.donateNow.model.DonateNowModel', {
            scope: me
        });
        DonateModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var handleError = function (err) {
            return cb(err, null);
        }
        DonateModel.dbCollection.findOne({
            _id: orderPartnerData.walletOrderId
        }, { _id: 1, donated_amount: 1 }, function (err, walletData) {
            if (err == null && walletData == null) {
                DonateModel.write({
                    "_id": orderPartnerData.walletOrderId,
                    "username": orderPartnerData.username,
                    "donated_amount": donateTotalAmt
                }, function (donateData) {
                    if (donateData.success) {
                        cb(null, donateData.record)
                    } else {
                        return handleError(donateData.error);
                    }
                }, {
                        add: 1,
                        modify: 1
                    });
            }
            else {
                DonateModel.write({
                    "_id": orderPartnerData.walletOrderId,
                    "donated_amount": parseFloat(walletData.donated_amount)+donateTotalAmt
                }, function (donateData) {
                    if (donateData.success) {
                        cb(null, donateData.record)
                    } else {
                        return handleError(donateData.error);
                    }
                }, {
                        modify: 1
                    });
            }
        });
    }
    /*
    @author : Vaibhav Vaidya
    @date : 9 Sept 2017
    @function : donateNow
    @desc : Currently for User only. User donates to selected charity items
    */
    /*
    Required params: UserId, catalogue array of objects (Should contain catalogueId and amount to donate)
    */

    , donateNow: function (cb) {
        var me = this;
        var reqBody = me.req.body.reqData;
        var grandTotal=0;
        var DB_TRANSACTION = {
            BEGIN: function (next) {
                opt = Ext.clone(me.config.pgsql);
                opt.callback = function (conn) {
                    me.initModels();
                    me.src.db.conn.query('BEGIN', function (err) {
                        if (err) {
                            me.sendError(err);
                        } else {
                            next();
                        }
                    });
                }
                me.src.db = Ext.create("Database.drivers.Postgresql.Database", opt);
            },
            COMMIT: function (data) {
                me.src.db.conn.query('COMMIT', function () {
                    me.sendData(data)
                });
            },
            ROLLBACK: function (err) {
                me.src.db.conn.query('ROLLBACK', function () {
                    me.sendError(err);
                });
            }
        }
        var handleError = function (err) {
            return DB_TRANSACTION.ROLLBACK(err);
            
        };
        var CatalougeModel = Ext.create('Crm.modules.catalogue.model.CatalogueModel', {
            scope: me
        });
        CatalougeModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        [
            function (err, client) {
                if (client) {
                    if (me.validateRequest([{
                                type: 'string',
                                field: "username",
                                isRequired: true
                            },
                            {
                                type: 'ARRAY', field: 'items', items: [
                                    {
                                        type: 'string',
                                        field: "catalogue_id",
                                        isRequired: true
                                    },
                                    {
                                        type: 'number',
                                        field: "amount",
                                        isRequired: true
                                    }
                                ],isRequired:true
                            }
                        ])) {
                        [
                            function (next) {
                                //Delete this function if validateRequest for Array from RestBaseController is fixed
                                for (var i = 0; i < reqBody.items.length; i++) {
                                    if (reqBody.items[i].catalogue_id != undefined && reqBody.items[i].amount != undefined) {
                                        if (typeof reqBody.items[i].catalogue_id == "string" && typeof reqBody.items[i].amount == "number") {
                                            if (i == reqBody.items.length - 1) {
                                                next();
                                            }
                                        }
                                        else {
                                            return me.addErrorAndSend('ERR013');
                                            break;
                                        }
                                    }
                                    else {
                                        return me.addErrorAndSend('ERR053');
                                        break;
                                    }
                                }
                            },
                            function(next)
                            {
                                me.AccountHoldersModel.dbCollection.findOne({
                                    username: reqBody.username,
                                    user_type: me.config.endUser
                                }, { _id: 1, username: 1, wallet_balance: 1 }, function (err, data) {
                                    if (data && !err) {
                                        next(data);
                                    } else {
                                        return me.addErrorAndSend('ERR033');
                                    }
                                });
                            },
                            function (userData, next) {
                                var calcTotal = 0, otArr = [], penultimateTotal=0;
                                for (var i = 0; i < reqBody.items.length; i++) {
                                    grandTotal += parseFloat(reqBody.items[i].amount);
                                }
                                var OTsql = "select ot.partner_id, ot._id, ot.donation_amount, ot.follow_amount, ot.claimed_status, ot.claimed_amount from order_transaction ot where (ot.claimed_status=0 or ot.claimed_status=1) and username= $1";
                                me.src.db.query(OTsql, [reqBody.username], function (e, data) {
                                    if (!e && data.length>0) {
                                        for (i = 0; i < data.length; i++) {
                                            if (data[i] && data[i].claimed_status == 1) {
                                                calcTotal += (parseFloat(data[i].donation_amount) + parseFloat(data[i].follow_amount)) - data[i].claimed_amount
                                                otArr.push(data[i]._id);
                                                if (calcTotal > grandTotal) {
                                                    break;
                                                }
                                                else{
                                                    penultimateTotal=calcTotal;
                                                }
                                            }
                                        }
                                        if (calcTotal >= grandTotal) {
                                            next(userData, otArr, data, penultimateTotal)
                                        }
                                        else {
                                            for (i = 0; i < data.length; i++) {
                                                if (data[i] && data[i].claimed_status == 0) {
                                                    calcTotal += parseFloat(data[i].donation_amount) + parseFloat(data[i].follow_amount);
                                                    otArr.push(data[i]._id);
                                                    if (calcTotal > grandTotal) {
                                                        break;
                                                    }
                                                    else{
                                                        penultimateTotal=calcTotal;
                                                    }
                                                }
                                            }
                                            if (calcTotal >= grandTotal) {
                                                next(userData, otArr, data, penultimateTotal)
                                            }
                                            else {
                                                return me.addErrorAndSend('ERR054');
                                            }
                                        }
                                    }
                                    else {
                                        return me.addErrorAndSend('ERR051');
                                    }
                                });
                            },
                            //Second check for wallet balance
                            function (userData, otArr, orderData, penultimateTotal, next) {
                                if(parseFloat(userData.wallet_balance) >= parseFloat(grandTotal)){
                                        next(userData, otArr, orderData, penultimateTotal)
                                    }
                                else{
                                    return me.addErrorAndSend('ERR054');
                                }
                            },
                            //begin statement
                            function (userData, otArr, orderData, penultimateTotal, next) {
                                DB_TRANSACTION.BEGIN(function () {
                                    next(userData, otArr, orderData, penultimateTotal);
                                });
                            },
                            function(userData, otArr, orderData, penultimateTotal, next){
                                //All listed OrderTransactionIds are to be fully claimed except for last one
                                var lastOrderId = otArr[otArr.length - 1]
                                otArr.splice(otArr.length - 1, 1);
                                otArr.prepEach(function (r, nextItem) {
                                    var updateOTsql = "update order_transaction set claimed_status= 2, claimed_amount= (donation_amount+follow_amount) where _id =$1";
                                    me.src.db.query(updateOTsql, [r], function (e, data) {
                                        if (!e && data != undefined) {
                                            nextItem(r)
                                        } else {
                                            return DB_TRANSACTION.ROLLBACK(err);
                                        }
                                    });
                                },function(){
                                    var deduct=grandTotal-penultimateTotal;
                                    var claimedStatus=(deduct==0)?2:1
                                    var updateOTsql = "update order_transaction set claimed_status= $1, claimed_amount= $2 where _id =$3";
                                    me.src.db.query(updateOTsql, [claimedStatus,deduct, lastOrderId], function (e, data) {
                                    if(!e && data!=undefined){
                                        next(userData, otArr, orderData,lastOrderId,deduct)
                                    }else{
                                        return DB_TRANSACTION.ROLLBACK(err);
                                    }
                                     });
                                });
                            },
                            function (userData, otArr, orderData,lastOrderId,deduct, next) {
                                //filter OrderData to Selected Ids from OrderTransaction(otArr) only
                                var filteredOrderdata=[]
                                otArr.push(lastOrderId);
                                for(var i=0;i<otArr.length;i++){
                                    for(var j=0;j<orderData.length;j++){
                                        if(orderData[j]._id==otArr[i]){
                                            filteredOrderdata.push(orderData[j]);
                                        }
                                        if(orderData[j]._id==lastOrderId){
                                                totalDonateAmt=deduct;
                                        }
                                    }
                                }
                                //calculate donation amount for each catalogue item from order transaction
                                reqBody.items.prepEach(function (r, nextItem) {
                                    r.wallet_order_id=me.Util.getObjectId();
                                    if (r.collectedAmount == undefined) {r.collectedAmount = 0;}
                                    for (var i = 0; i < filteredOrderdata.length; i++) {
                                            if (filteredOrderdata[i].totalDonateAmt == undefined) {
                                                filteredOrderdata[i].totalDonateAmt = (parseFloat(filteredOrderdata[i].donation_amount) + parseFloat(filteredOrderdata[i].follow_amount)) - filteredOrderdata[i].claimed_amount;
                                                filteredOrderdata[i].initialTotal=filteredOrderdata[i].totalDonateAmt;
                                            }
                                            if(filteredOrderdata[i].catalogue_info==undefined){ filteredOrderdata[i].catalogue_info=[]; }
                                            if (filteredOrderdata[i].totalDonateAmt > 0 && r.collectedAmount !=r.amount) {
                                                if ((r.amount-r.collectedAmount) - filteredOrderdata[i].totalDonateAmt < 0) {
                                                    filteredOrderdata[i].totalDonateAmt-=(r.amount-r.collectedAmount);
                                                    filteredOrderdata[i].catalogue_info.push({"catalogue_id":r.catalogue_id,"wallet_order_id":r.wallet_order_id, "wallet_donate_amt":(r.amount-r.collectedAmount), "catalogue_total":r.amount});
                                                    r.collectedAmount = r.amount;
                                                }
                                                else{
                                                    r.collectedAmount+=filteredOrderdata[i].totalDonateAmt;
                                                    filteredOrderdata[i].catalogue_info.push({"catalogue_id":r.catalogue_id,"wallet_order_id":r.wallet_order_id, "wallet_donate_amt":filteredOrderdata[i].totalDonateAmt, "catalogue_total":r.amount});
                                                    filteredOrderdata[i].totalDonateAmt=0;
                                                }
                                            }
                                        if(i==filteredOrderdata.length-1){
                                            nextItem(r);
                                        }
                                    }
                                }, function () {
                                    next(userData, filteredOrderdata);
                                });
                            },
                            function (userData, filteredOrderdata, next) {
                                reqBody.items.prepEach(function (r, nextItem) {
                                    var tempObj = {};
                                    tempObj.walletOrderId = r.wallet_order_id;
                                    tempObj.username = reqBody.username;
                                    me.upsertDonateNowInfo(tempObj, r.amount, function (donateErr, donateData) {
                                        if (donateErr) {
                                            return handleError(donateErr);
                                        }
                                        else {
                                            nextItem(r);
                                        }
                                    });
                                }, function () {
                                    next(userData, filteredOrderdata);
                                });
                            },
                            function (userData, filteredOrderdata, next) {
                                //fetch charity Id for every catalogue Id
                                filteredOrderdata.prepEach(function (r, nextItem) {
                                    r.catalogue_info.prepEach(function (k, nextCatalogueId){
                                        CatalougeModel.dbCollection.findOne({
                                            _id: k.catalogue_id
                                        }, {}, function (err, data) {
                                            if (data) {
                                                k.charity_id=data.pid;
                                                nextCatalogueId(k);
                                             }
                                            else {
                                                return handleError({code: 'ERR033'});
                                            }
                                        });
                                    }, function () {
                                        nextItem(r);
                                    });
                                }, function () {
                                    next(userData, filteredOrderdata);
                                });
                            },
                            function (userData, filteredOrderdata, next) {
                                //Breakdown for donation distribution
                                filteredOrderdata.prepEach(function (r, nextItem) {
                                    r.catalogue_info.prepEach(function (k, nextCatalogueId) {
                                        me.OrderCharityModel.write({
                                            "_id": me.Util.getObjectId(),
                                            "partner_id": r.partner_id,
                                            "charity_id": k.charity_id,
                                            "username": reqBody.username,
                                            "order_transaction_id": r._id,
                                            "charity_plan_id": 0,
                                            "charity_amount": k.wallet_donate_amt,
                                            "total_amount": k.catalogue_total,
                                            "wallet_order_id": k.wallet_order_id,
                                            "catalogue_id": k.catalogue_id
                                        }, function (OCdata) {
                                            if (OCdata.success) {
                                                nextCatalogueId(k);
                                            } else {
                                                return handleError(OCdata.error);
                                            }
                                        }, {
                                                add: 1,
                                                modify: 1
                                            });
                                    }, function () {
                                        nextItem(r);
                                    });
                                }, function () {
                                    next(userData);
                                });
                            },
                            function (userData, next) {
                                reqBody.items.prepEach(function (r, nextItem) {
                                    var charityPlaceholders = [];
                                    charityPlaceholders.push(parseFloat(parseFloat(r.amount).toFixed(3)));
                                    var charityInfoSql = "update pgd_catalogue cat set donation_amount_collected = "
                                    charityInfoSql += " case when donation_amount_collected is null then ($" + charityPlaceholders.length + ") else (donation_amount_collected + $" + charityPlaceholders.length + ") end";
                                    charityPlaceholders.push(r.catalogue_id);
                                    charityInfoSql += " where cat._id = $" + charityPlaceholders.length;
                                    me.src.db.query(charityInfoSql, charityPlaceholders, function (e, data) {
                                        if (!e && data != undefined) {
                                                    //Update query returns no confirmation/message
                                                    nextItem(r);
                                                }
                                                else {
                                                    return handleError(e);
                                                }
                                    });
                                }, function () {
                                    next(userData);
                                });
                            },
                            function (userData, next) {
                                me.debitUserWallet(userData, grandTotal, function (err, resp) {
                                        if (resp.success == true) {
                                            next(userData);
                                        }
                                        else {
                                            return DB_TRANSACTION.ROLLBACK(err);
                                        }
                                    });
                            },
                            function (userData, next) {
                                me.AccountHoldersModel.dbCollection.findOne({
                                    username: reqBody.username
                                }, { _id: 1, username: 1, wallet_balance: 1 }, function (err, data) {
                                    if (data && !err) {
                                        return DB_TRANSACTION.COMMIT(data);
                                    } else {
                                        return me.addErrorAndSend('ERR033');
                                    }
                                });
                            }
                        ].runEach();

                    } else {
                        me.sendError();
                    }
                } else {
                    return me.addErrorObj(err);
                }
            }
        ].runEach();
    }
    /*
    @author : Vaibhav Vaidya
    @date : 11 Sept 2017
    @function : donateCharityPlanNow
    @desc : Searched for charity plan and donates entire amount as per percentage distribution
    */

    //required Params: First =User/Partner Id. Second= If True, will directly donate to 'ACTIVE' charity plan else 'EMERGENCY', Third= Total Amount to act on.
    , donateCharityPlanNow: function (partnerUserId, doConfirm, donateTotalAmt, orderPartnerData, cb) {
        var me = this, searchPlanType, insertionIndex;
        searchPlanType = doConfirm ? 'A' : 'E';
        var donateStarted=false;
        var DetailsModel = Ext.create('Crm.modules.charityPlanDetails.model.CharityPlanDetailsModel', {
            scope: me
        });
        DetailsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var DonateModel = Ext.create('Crm.modules.donateNow.model.DonateNowModel', {
            scope: me
        });
        DonateModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var handleError = function (err) {
            return cb(err, null);
        }
        me.retrieveCharityPlans(partnerUserId, function (data) {
            if (data) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].plan_type == searchPlanType) {
                        insertionIndex = i;
                        break;
                    }
                }
                if(insertionIndex!=undefined){
                me.upsertDonateNowInfo(orderPartnerData, donateTotalAmt, function (donateErr, donateData) {
                    if (donateErr) {
                        return handleError(donateErr);
                    }
                    else {
                        DetailsModel.dbCollection.find({
                            charity_plan_id: data[insertionIndex]._id
                        }, {}, function (err, data) {
                            if (data && data.length > 0) {
                                donateStarted=true;
                                data.prepEach(function (r, next) {
                                    r.charityDonationAmount = donateTotalAmt * (parseFloat(r.percentage) / 100.0);
                                    me.OrderCharityModel.write({
                                        "_id": me.Util.getObjectId(),
                                        "partner_id": orderPartnerData._id,
                                        "charity_id": r.charity_foundation_id,
                                        "username": orderPartnerData.username,
                                        "order_transaction_id": orderPartnerData.orderTransactionId,
                                        "charity_plan_id": r.charity_plan_id,
                                        "charity_amount": r.charityDonationAmount,
                                        "total_amount": donateTotalAmt || 0,
                                        "wallet_order_id": orderPartnerData.walletOrderId||"",
                                        "catalogue_id": r.pgd_catalogue_id
                                    }, function (OCdata) {
                                        if (OCdata.success) {
                                            //credit donated amount to charity catalogue info's collected amount
                                            var charityPlaceholders = [];
                                            charityPlaceholders.push(parseFloat(parseFloat(r.charityDonationAmount).toFixed(3)));
                                            var charityInfoSql = "update pgd_catalogue cat set donation_amount_collected = "
                                            charityInfoSql += " case when donation_amount_collected is null then ($" + charityPlaceholders.length + ") else (donation_amount_collected + $" + charityPlaceholders.length + ") end";
                                            charityPlaceholders.push(r._id);
                                            charityInfoSql += " from charity_plan_details cpd where cpd.pgd_catalogue_id=cat._id and cpd._id = $" + charityPlaceholders.length;
                                            me.src.db.query(charityInfoSql, charityPlaceholders, function (e, data) {
                                                if (!e && data != undefined) {
                                                    //Update query returns no confirmation/message
                                                    next(r);
                                                }
                                                else {
                                                    return handleError(e);
                                                }
                                            });
                                        } else {
                                            return handleError(OCdata.error);
                                        }
                                    }, {
                                            add: 1,
                                            modify: 1
                                        });
                                }, function () {
                                    if(donateStarted==true){
                                        me.TransactionModel.dbCollection.update({
                                        _id: orderPartnerData.orderTransactionId
                                    }, {
                                            $set: {
                                                "claimed_status": 2,
                                                "claimed_amount": donateTotalAmt
                                            }
                                        }, function (err, data) {
                                            if (err)
                                                return handleError(err)
                                            else {
                                                orderPartnerData.claimed_status = 2;
                                                orderPartnerData.claimed_amount = donateTotalAmt;
                                                cb(null, orderPartnerData);
                                            }
                                        });
                                    }
                                    else{
                                        orderPartnerData.claimed_status = 0;
                                        orderPartnerData.claimed_amount = 0;
                                        cb(null, orderPartnerData);
                                    }
                                });
                            }
                            else {
                                orderPartnerData.claimed_status = 0;
                                orderPartnerData.claimed_amount = 0;
                                cb(null, orderPartnerData);
                            }
                        });
                    }
                })}
                else{
                    return handleError({ code: 'ERR056' });
                }
            }
        });
    }
    /*
    @author : Vaibhav Vaidya
    @date : 25 July 2017
    @function : retrieveDetailsPlans
    @desc : Returns details of a charity Plan Id
    */
    , retrieveDetailsPlans: function (toFindId, emergencyFlag,partnerPlan,insertionIndex,grandDonation,grandFollow, cb) {
        var me = this;
        var DetailsModel = Ext.create('Crm.modules.charityPlanDetails.model.CharityPlanDetailsModel', {
            scope: me
        });
        DetailsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var FoundationModel = Ext.create('Crm.modules.organizations.model.FoundModel', {
            scope: me
        });
        FoundationModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        DetailsModel.dbCollection.find({
            charity_plan_id: toFindId
        }, {}, function (err, data) {
            if (data && data.length > 0) {
                var limit;
                data.prepEach(function (r, next) {
                    if (emergencyFlag == false) {
                        r.charityDonationAmount = (parseFloat(grandDonation) + parseFloat(grandFollow)) * (parseFloat(r.percentage) / 100.0);
                        //Checking for charity_type and donation amount limit per charity
                        FoundationModel.dbCollection.findOne({
                            _id: r.charity_foundation_id
                        }, {}, function (err, data) {
                            if (data) {
                                limit=data.extra_info.amount_limit;
                                if (data.extra_info.charity_type == 'L' && data.active == true) {
                                    var sql= "select sum(charity_amount) as start from order_charity_details ocd where charity_id=$1"
                                    me.src.db.query(sql, [r.charity_foundation_id], function (e, data) {
                                        if (data && data.length > 0) {
                                            if (parseFloat(data[0].start) + parseFloat(r.charityDonationAmount) > parseFloat(limit)) {
                                                emergencyFlag = true;
                                                next(r);
                                            }
                                            else {
                                                next(r);
                                            }
                                        }
                                        else {
                                            //There are no current charity donations for this foundation, check next
                                            next(r);
                                        }
                                    });
                                }
                                else if (data.extra_info.charity_type == 'L' && data.active == false) {
                                    emergencyFlag = true;
                                    next(r);
                                }
                                else {
                                    next(r);
                                }
                            }
                            else {
                                return me.addErrorAndSend('ERR038');
                            }
                        });
                    }
                    else {
                        next(r);
                    }
                }, function () {
                    cb(emergencyFlag, partnerPlan)
                });
            }
            else {
                emergencyFlag = true;
                cb(emergencyFlag, partnerPlan)
            }
        });
    }
    /*
    @author : Vaibhav Vaidya
    @date : 11 Sept 2017
    @function : checkActivePlanQualify
    @desc : Check if Active Plan's catalogue items are within limits to qualify for donation
    */
    , checkActivePlanQualify: function (charityPlanSearchId, grandTotal, cb) {
        var me = this, detailsPlaceHolders=[];
        var walletEmergencyFlag=false;
        var detailsColumns = " cpd.percentage, cat._id, cat.donation_amount_needed, cat.donation_amount_collected, cat.item_type "
        var detailsSql = "select " + detailsColumns + " from charity_plan_details cpd inner join pgd_catalogue cat on (cpd.pgd_catalogue_id = cat._id)  "
        detailsPlaceHolders.push(charityPlanSearchId);
        detailsSql += " where cpd.charity_plan_id = $" + detailsPlaceHolders.length;
        detailsSql += " and cpd.removed !=1 and cat.removed !=1  ";
        me.src.db.query(detailsSql, detailsPlaceHolders, function (err, detailsData) {
            if (detailsData && detailsData.length > 0) {
                for (var i = 0; i < detailsData.length; i++) {
                    if (detailsData[i].item_type == 'L') {
                        //Check if donation_amount_needed is greater than donation_amount_collected
                        if (detailsData[i].percentage != undefined && detailsData[i].donation_amount_needed != undefined) {
                            detailsData[i].charityDonationAmount = grandTotal * (parseFloat(detailsData[i].percentage) / 100.0);
                            if(detailsData[i].donation_amount_collected == undefined) detailsData[i].donation_amount_collected = 0;
                            if ((parseFloat(detailsData[i].donation_amount_collected) + parseFloat(detailsData[i].charityDonationAmount)) > parseFloat(detailsData[i].donation_amount_needed)) {
                                //reject
                                walletEmergencyFlag = true;
                                break;
                            }
                        }
                        else {
                            walletEmergencyFlag = true;
                            break;
                        }
                    }
                }
                //Passed limited check
                cb({walletEmergencyFlag});
            }
            else {
                walletEmergencyFlag = true;
                cb({walletEmergencyFlag});
            }
        });                                            
    }
    /*
    @author : Vaibhav Vaidya
    @date : 26 July 2017
    @function : insertRawOrder
    @desc : Stores REST request as it is
    */
    , insertRawOrder: function (toStoreReq, cb) {
        var me = this;
        var RawOrderModel = Ext.create('Crm.modules.rawOrder.model.RawOrderModel', {
            scope: me
        });
        RawOrderModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        RawOrderModel.write({
            "_id": me.Util.getObjectId(),
            "orderrequest": toStoreReq,
            "order_transaction_id": null,
            "status": "Beginning Processing"
        }, function (data) {
            if (data.success) {
                cb(data.record);
            } else {
                return me.addErrorObj(data.error);
            }
        }, {
                add: 1,
                modify: 1
            });
    }
    /*
    @author : Vaibhav Vaidya
    @date : 26 July 2017
    @function : updateRawOrder
    @desc : Updating table with Current transaction status and Order Transaction Id if successful
    */
    , updateRawOrder: function (rawOrderId, orderTransactionId, status, cb) {
        var me = this;
        var RawOrderModel = Ext.create('Crm.modules.rawOrder.model.RawOrderModel', {
            scope: me
        });
        RawOrderModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        RawOrderModel.dbCollection.findOne({
            _id: rawOrderId
        }, {}, function (err, data) {
            if (data) {
                RawOrderModel.write({
                    "_id": rawOrderId,
                    "orderrequest": data.orderrequest,
                    "order_transaction_id": orderTransactionId||data.order_transaction_id,
                    "status": status
                }, function (data) {
                    if (data.success) {
                        cb(data.record);
                    } else {
                        return me.addErrorObj(data.error);
                    }
                }, {
                        add: 1,
                        modify: 1
                    });
            }
            else{
                return me.addErrorAndSend('ERR036');
            }
        });
    }
     
     /**
     * @author : Max
     * @date : 25th july 2017
     * @function : get
     * @desc : get all orders by partner id
     */
     ,getPartnersOrder: function() {
         if(!this.user) {
            this.error(401);
            return;
        }
        var cls = Ext.create('Crm.modules.orderTransaction.model.OrderTransactionModel', {
            scope: this,
            find: {partner_id: this.user.pid}
        });        
        [
            function(next) {
                cls.getData(this.req.body.reqData, next)
            },
            function(res) {
                this.sendData(res)
                delete cls;
            }
        ].runEach(this)
        
        
     }
});