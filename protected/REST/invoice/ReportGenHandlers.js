Ext.define('Crm.REST.invoice.ReportGenHandlers', {
    extend: "Crm.REST.RestBaseController"
    ,apiGetExcelInvoice: function() {   
       var me = this;  
       var myArr = [];  
       var nodeExcel = require('excel-export');
       var username="default_address";
       var address="default_address";
       const excel = require('node-excel-export');
       
       if(!me.user) {
        me.sendError(401);
        return;
       }
       
       if(me.user && me.user.username!=null && me.user.username!="")
            {
                 username= me.user.username;
            }   
        if(me.user && me.user.address!=null && me.user.address!="")    
            {
                address= me.user.address;
            }
                var sql="select pdgf.name as name,sum(dpd.amount) as donation_amount,bd.bank_name as bank_name,bd.bank_account_number as bank_acc_number"
                        +" from donation_payment_details dpd "
                        +" LEFT OUTER JOIN pgd_foundations pdgf "
                        +" ON pdgf._id = dpd.to_charity_id "
                        +" LEFT OUTER join bank_details bd "
                        +" on pdgf._id = bd.partner_id "
                        +" GROUP BY pdgf.name,bd.bank_name,bd.bank_account_number"
                        //+"wherr "
                        
                        ;
                me.src.db.query(sql, null, function (e, data) {
                    if (data) {
                                const styles = {
                                headerDark: {
                                    fill: {
                                    fgColor: {
                                        rgb: 'FFFFFFFF'
                                    }
                                    },
                                    font: {
                                    color: {
                                        rgb: 'FF000000' 
                                    },
                                    sz: 11,
                                    bold: true
                                  //  underline: true
                                    },
                                    border:{
                                        top:{
                                            style:'medium',
                                            color:'FF000000'
                                        },bottom:{
                                            style:'medium',
                                            color:'FF000000'
                                        },left:{
                                            style:'medium',
                                            color:'FF000000'
                                        },right:{
                                            style:'medium',
                                            color:'FF000000'
                                        }
                                    }
                                },
                                cellPink: {
                                    fill: {
                                    fgColor: {
                                        rgb: 'FFFFCCFF'
                                    }
                                    }
                                },
                                cellGreen: {
                                    fill: {
                                    fgColor: {
                                        rgb: 'FF00FF00'
                                    }
                                    }
                                }
                                };
                                
                                //Array of objects representing heading rows (very top) 
                                const heading = [
                                [{value: 'PARTNER_NAME',style: styles.headerDark},{value: me.user.username,style: styles.headerDark}], 
                                [{value: 'DATE',style: styles.headerDark},{value: new Date().toISOString().slice(0, 10), style: styles.headerDark}],
                                [{value: 'ADDRESS',style: styles.headerDark},{value:me.user.address,style: styles.headerDark}],
                                []// <-- It can be only values 
                                ];
                                
                               
                                const specification = {
                                name: {
                                    displayName: 'CHARITY_NAME',
                                    headerStyle: styles.headerDark, 
                                    width: 220
                                },
                                donation_amount: {
                                    displayName: 'AMOUNT',
                                    headerStyle: styles.headerDark,
                                    width: '10'
                                },
                                bank_name: {
                                    displayName: 'BANK',
                                    headerStyle: styles.headerDark,
                                    width: 220 
                                },
                                bank_acc_number: {
                                    displayName: 'BANK_ACC_NUM',
                                    headerStyle: styles.headerDark,
                                    width: 220 
                                }
                                }
                                const merges = [
                                { start: { row: 1, column: 1 }, end: { row: 1, column: 1 } },
                                { start: { row: 2, column: 1 }, end: { row: 2, column: 1 } }]
                                
                           
                                const report = excel.buildExport(
                                [ 
                                    {
                                    name: 'report.xlsx',
                                    heading: heading,
                                    merges: merges, 
                                    specification: specification, 
                                    data: data 
                                    }
                                ]
                                );
                                 me.res.setHeader('Content-Type', 'application/vnd.openxmlformats');
                                 me.res.setHeader("Content-Disposition", "attachment; filename=" + username+" "+address+".xlsx");
                                 return me.res.end(report,'binary');
                    }
            });
    }
    
    ,apiGetPdfInvoice: function() {       
          var me = this;  
          var fs = require('fs');
          var pdf = require('html-pdf');

        /*  
        PDF GENERATION---

        IMPORTANT FIELDS TO REPLACE WHILE LOOPING THROUGH DATA
         
        #NEWPAGE# --> indicates new page addition if needed replace this with 
        #TABLE# --> replace with table headers 
        #TABLEROW# -->replace with table rows 

        */
          var html = fs.readFileSync('./test.html', 'utf8');
            if(!me.user) {
            me.sendError(401);
            return;
            }
                var options = { format: 'A4', "orientation": "portrait" };
                html = html.replace("#DATE#", new Date().toISOString().slice(0, 10));
                html = html.replace("#NAME#", me.user.username);
                html = html.replace("#FROM DATE#", new Date().toISOString().slice(0, 10));
                html = html.replace("#TO DATE#", new Date().toISOString().slice(0, 10));
                html = html.replace("#PARTNER NAME#", me.user.username);
                html = html.replace("#ADDRESS#", me.user.address);
                html = html.replace("#CONTACT#", me.user.mobile);
                html = html.replace("#SIGNATURE#", me.user.username);
                html = html.replace("#TABLE#", "<tr><th>CHARITY NAME</th><th>AMOUNT</th><th>BANK_NAME</th><th>BANK_ACCOUNT</th></tr>");
                var sql="select pdgf.name as name,sum(dpd.amount) as donation_amount,bd.bank_name as bank_name,bd.bank_account_number as bank_acc_number"
                        +" from donation_payment_details dpd "
                        +" LEFT OUTER JOIN pgd_foundations pdgf "
                        +" ON pdgf._id = dpd.to_charity_id "
                        +" LEFT OUTER join bank_details bd "
                        +" on pdgf._id = bd.partner_id "
                        +" GROUP BY pdgf.name,bd.bank_name,bd.bank_account_number";
                me.src.db.query(sql, null, function (e, data) {
                    if (data) {
                        data.prepEach(function (r, next) {
                            html = html.replace("#TABLEROW#", "<tr><td>" + r.name + "</td><td>" + r.donation_amount + "</td><td>" + r.bank_name + "</td><td>" + r.bank_acc_number + "</td></tr>" + "#TABLEROW#");
                            next(r);
                        }, function () {
                            html = html.replace("#TABLEROW#", "")
                            html = html.replace("#newpage#", "<div class=" + "page" + "><img class=" + "logo" + "src=" + "{{image}}" + "><div class=" + "bottom" + "><div class=" + "line center" + ">8045 zürich</div></div></div>")
                            pdf.create(html, options).toFile('./businesscard.pdf', function (err, res) {
                                if (err) return console.log(err);
                            });
                        });
                    }
                });
                fs.readFile('./businesscard.pdf', function (err,data){
                me.res.setHeader('Content-Type', 'application/vnd.openxmlformats');
                me.res.setHeader("Content-Disposition", "attachment; filename=" + "businesscard.pdf");
                return me.res.end(data,'binary');    
                // res.contentType("application/pdf");
                // res.send(data);
               });

    }
})