Ext.define('Crm.REST.invoice.OrderCharityDetailsController', {
    extend: "Crm.REST.RestBaseController"
    , list: function () {
        var res = {};
        var list = [];
        var me = this
        , inParams = me.req.body.reqData;
        
        if(!me.user) {
        me.sendError(401);
        return;
        }
        var skip = inParams.startIndex && /^[0-9]{1,}$/.test(inParams.startIndex + '') ? parseInt(inParams.startIndex) : 0;
        var limit = inParams.numOfRecords && /^[0-9]{1,}$/.test(inParams.numOfRecords + '') ? parseInt(inParams.numOfRecords) : 50;
       
        var orderTransaction = Ext.create('Crm.modules.orderTransaction.model.OrderTransactionModel', {
            scope: me
        });

        var orderCharityDetails = Ext.create('Crm.modules.orderCharityDetails.model.OrderCharityDetailsModel', {
            scope: me
        });

        orderCharityDetails.error = function (code, info) {
            return me.addErrorObj(code);
        };

        var orderCharityItems = Ext.create('Crm.modules.orderTransactionItems.model.OrderTransactionItemsModel', {
            scope: me
        });

        orderCharityItems.error = function (code, info) {
            return me.addErrorObj(code);
        };

         if (!me.validateRequest([
            {
                type: 'STRING',
                field: "order_transaction_id",
                isRequired: true,
            }
        ])) {
            return me.sendError();
        }


        var userIdField=-1;
        var charityIdField=-1;
        var chariyPlanIdField=-1;
        var orderTransactionIdField=-1;

        for(var i=0;i<orderCharityDetails.fields.length;i++) {

            if(orderCharityDetails.fields[i].name=="charity_id"){
                charityIdField=i;
            }
            if(orderCharityDetails.fields[i].name=="charity_plan_id"){
                chariyPlanIdField=i;
            }
            if(orderCharityDetails.fields[i].name=="order_transaction_id"){
                orderTransactionIdField=i;
            }
        }

        var params={};
        if(!params.filters) params.filters = [];
        var response={};
        var items=[];
        var dist=[];
        var total=0;

        [

        function(next)
        {
        //filter == where clause 
        
            if(inParams && inParams.ispaynowtx && inParams.ispaynowtx=="1")
            {
                //Vaibhav Mali Date:04-10-2017 Done grouping of response list.
               var items={};
               var sql="select "
               +" pdgf.name,ocd.charity_id,sum(ocd.charity_amount) as donation_amount "
               +" from order_CHARITY_DETAILS ocd "
               +" LEFT OUTER JOIN pgd_foundations pdgf ON pdgf._id = ocd.charity_id"
               +" left outer join public.pgd_catalogue pc on pc._id =ocd.catalogue_id"
               +" GROUP BY pdgf.name,ocd.charity_id,ocd._id "
               +" having ocd._id=$1";
                   me.src.db.query(sql,[inParams.order_transaction_id], function (e, res) {
                    if (res && res.length && res.length >0) {
                        res.prepEach(function (j, next) {
                            var sql="select "
                            +" pdgf.name,ocd.charity_id,sum(ocd.charity_amount) as donation_amount "
                            +" from order_CHARITY_DETAILS ocd "
                            +" LEFT OUTER JOIN pgd_foundations pdgf ON pdgf._id = ocd.charity_id"
                            +" left outer join public.pgd_catalogue pc on pc._id =ocd.catalogue_id"
                            +" GROUP BY pdgf.name,ocd.charity_id,ocd._id "
                            +" having ocd._id=$1 AND ocd.charity_id=$2";
                            me.src.db.query(sql,[inParams.order_transaction_id,j.charity_id], function (e, data) {
                                if(data && data.length && data.length >0)
                                {
                                    var intotal=data.length;
                                    var charityItemWiseDistribution={};
                                    charityItemWiseDistribution.total=intotal;
                                    charityItemWiseDistribution.list=data;
                                    j.charityItemWiseDistribution=charityItemWiseDistribution;
                                    next(j);                                            
                                }
                                else
                                {
                                    next(j);
                                }
                            });
                        },function()
                                   {
                                    total=res.length;
                                    response.total=total;
                                    response.items=items;
                                    response.charityWiseDistribution=res;
                                    me.sendData(response);
                                   }); 
                    } else {
                            if(items && items.list.length > 0){
                                response.items=items;
                                me.sendData(response);
                              }
                             else{
                              me.sendError();
                             }
                        }
                });
            }
            else
            {
                //Vaibhav Mali Date:04-10-2017 Done grouping of response list.
                //if its normal transaction
                if(inParams.order_transaction_id!=null && inParams.order_transaction_id!="")
                    {
                        params.filters.push({value: inParams.order_transaction_id, property: 'order_transaction_id'});
                    }
                        params._start=skip;
                        params._limit=limit;
                //select fields to show             
                params.fieldSet=['item_name','price','quantity','donation_amount','follow_amount'];
                orderCharityItems.getData(params,function(res) {
                if(res && res.list.length >0)
                   {
                        items=res;
                        var sql="select "
                        +" pdgf.name, ocd.charity_id,sum(ocd.charity_amount) as donation_amount "
                        +" from order_CHARITY_DETAILS ocd "
                        +" LEFT OUTER JOIN pgd_foundations pdgf ON pdgf._id = ocd.charity_id"
                        +" left outer join public.pgd_catalogue pc on pc._id = ocd.catalogue_id"
                        +" GROUP BY pdgf.name,ocd.charity_id,ocd.order_transaction_id  "
                        +" having order_transaction_id=$1";
                        me.src.db.query(sql,[inParams.order_transaction_id], function (e, res) {
                            if (res && res.length && res.length >0) {
                                res.prepEach(function (j, next) {
                                    var sql="select "
                                    +" pdgf.name, ocd.charity_id,pc.name as item_name,sum(ocd.charity_amount) as donation_amount "
                                    +" from order_CHARITY_DETAILS ocd "
                                    +" LEFT OUTER JOIN pgd_foundations pdgf ON pdgf._id = ocd.charity_id"
                                    +" left outer join public.pgd_catalogue pc on pc._id =ocd.catalogue_id"
                                    +" GROUP BY pdgf.name,ocd.charity_id,pc.name ,ocd.order_transaction_id  "
                                    +" having order_transaction_id=$1 and charity_id=$2";
                                    me.src.db.query(sql,[inParams.order_transaction_id,j.charity_id], function (e, data) {
                                        if(data && data.length && data.length >0)
                                        {
                                            var intotal=data.length;
                                            var charityItemWiseDistribution={};
                                            charityItemWiseDistribution.total=intotal;
                                            charityItemWiseDistribution.list=data;
                                            j.charityItemWiseDistribution=charityItemWiseDistribution;
                                            next(j);                                            
                                        }
                                        else
                                        {
                                            next(j);
                                        }
                                    });
                                },function()
                                           {
                                            total=res.length;
                                            response.total=total;
                                            response.items=items;
                                            response.charityWiseDistribution=res;
                                            me.sendData(response);
                                           }); 
                            }
                            else
                            {
                              if(items && items.list.length > 0){
                                response.items=items;
                                me.sendData(response);
                              }
                              else{
                              me.sendError();
                              }
                            }
                        });

                    }
                else
                    {
                           me.sendError();
                    }
                });
            }
        }
        ].runEach();
        }
    
});