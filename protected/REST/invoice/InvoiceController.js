Ext.define('Crm.REST.invoice.InvoiceController', {
    extend: "Crm.REST.RestBaseController",
    constructor: function () {
        this.callParent(arguments);
    },
    initModels: function () {
        var me = this;
        me.DonationPayment = Ext.create('Crm.modules.donationPayment.model.DonationPaymentModel', {
            scope: me
        });
        me.DonationPayment.error = function (code, info) {
            return me.addErrorObj(code);
        };
        me.DonationPaymentDetails = Ext.create('Crm.modules.donationPaymentDetails.model.DonationPaymentDetailsModel', {
            scope: me
        });
        me.DonationPaymentDetails.error = function (code, info) {
            return me.addErrorObj(code);
        };
        me.OrderCharityModel = Ext.create('Crm.modules.orderCharityDetails.model.OrderCharityDetailsModel', {
            scope: me
        });
        me.OrderCharityModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        me.DocsModel = Ext.create('Crm.modules.file.model.FilesModel', {
            scope: me
        });
        me.DocsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        me.TransactionModel = Ext.create('Crm.modules.orderTransaction.model.OrderTransactionModel', {
            scope: me
        });
        me.TransactionModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        me.OrderController = Ext.create('Crm.REST.partner.OrderController', {
            scope: me
        });
        me.OrderController.error = function (code, info) {
            return me.addErrorObj(code);
        };
    },

    apiGetList: function () {
        var me = this;
        if (!me.user) {
            me.addErrorAndSend(401);
            return;
        }
        if (!me.user.pid) {
            me.addErrorAndSend('ERR033');
            return;
        }
        var params = Ext.clone(me.req.body.reqData);
        var InvoicesModel = Ext.create('Crm.modules.donationPayment.model.DonationPaymentModel', {
            scope: me
        });
        var InvoiceDetailsModel = Ext.create('Crm.modules.donationPaymentDetails.model.DonationPaymentDetailsModel', {
            scope: me
        });

        var fromPartnerField = -1;
        var toCharityField = -1;
        var charityItem= -1;
        for (var i = 0; i < InvoiceDetailsModel.fields.length; i++) {
            if (InvoiceDetailsModel.fields[i].name == "from_partner_id") {
                fromPartnerField = i;
            }
            else if (InvoiceDetailsModel.fields[i].name == "to_charity_id") {
                toCharityField = i;
            }
            else if (InvoiceDetailsModel.fields[i].name == "to_charity_item") {
                charityItem = i;
            }
        }
        InvoiceDetailsModel.fields[fromPartnerField].bindTo = { collection: 'pgd_foundations', keyFieldType: 'ObjectID', keyField: '_id', fields: { name: 1 } };
        InvoiceDetailsModel.fields[toCharityField].bindTo = { collection: 'pgd_foundations', keyFieldType: 'ObjectID', keyField: '_id', fields: { name: 1 } };
        InvoiceDetailsModel.fields[charityItem].bindTo = { collection: 'pgd_catalogue', keyFieldType: 'ObjectID', keyField: '_id', fields: { name: 1 } };
        var reqParams = {};
        reqParams.filters = [];
        reqParams.sorters=[];
        reqParams.sorters.push({ direction: 'DSC', property: 'ctime' });
        reqParams.fieldSet=['_id','amount','ctime','status','brn','pdf_url','xlsx_url'];   
        if (me.user.user_type && me.user.user_type == "P") {
            reqParams.filters.push({ property: 'from_partner_id', value: me.user.pid });
            InvoicesModel.getData(reqParams, function (d) {
                if (d && d.list && d.list.length > 0) {
                    d.list.prepEach(function (f, next) {
                    var sql = "select " 
                        sql +=   "dpd.invoice_number, "
                        sql +=   "pf.name as charity_name, "
                        sql +=   "dpd.to_charity_id, "
                        sql +=   "dpd.partner_payment_status, "
                        sql +=   "dpd.brn, "
                        sql +=   "sum( amount ) as charity_amount "
                        sql += "from "
                        sql +=   "donation_payment_details dpd "
                        sql +=   "left join public.pgd_foundations pf on (dpd.to_charity_id=pf._id) "
                        sql +=   "group by "
                        sql +=   "dpd.invoice_number, "
                        sql +=   "dpd.from_partner_id, "
                        sql +=   "dpd.to_charity_id, "
                        sql +=   "dpd.partner_payment_status, "
                        sql +=   "dpd.brn, "                      
                        sql +=   "pf.name "    
                        sql += "having "
                        sql +=   "invoice_number =$1 ;"
                    me.src.db.query(sql, [f._id], function (e, data) {
                        if (data && data.length > 0) {
                            var invoiceDetails={};
                            var totalInvoice=data.length;
                            data.prepEach(function (j, next) {
                                var sql = "select " 
                                sql +=   "dpd._id, "
                                sql +=   "pc.name as item_name, "
                                sql +=   "dpd.to_charity_item, "
                                sql +=   "sum( amount ) as charity_amount "
                                sql += "from "
                                sql +=   "donation_payment_details dpd "
                                sql +=   "left join public.pgd_foundations pf on (dpd.to_charity_id=pf._id) "
                                sql +=   "left join public.pgd_catalogue pc on (dpd.to_charity_item=pc._id) "                        
                                sql +=   "group by "
                                sql +=   "dpd._id, "
                                sql +=   "dpd.to_charity_item, "  
                                sql +=   "pf.name, "    
                                sql +=   "pc.name "    
                                sql += "having "
                                sql +=   "invoice_number =$1 "
                                sql +=   "and to_charity_id =$2 ;"
                            me.src.db.query(sql, [j.invoice_number,j.to_charity_id], function (e, result) {
                                if(result && result.length >0)
                                {
                                var itemWiseInvoiceDetails={};
                                var total=0;
                                total=result.length;
                                itemWiseInvoiceDetails.total=total;
                                itemWiseInvoiceDetails.list=result;
                                j.itemWiseInvoiceDetails=itemWiseInvoiceDetails;
                                    next(j);
                                }
                                else
                                {
                                   next(j); 
                                }
                            });
                            },function()
                            {
                                invoiceDetails.total=totalInvoice;
                                invoiceDetails.list=data;
                                f.invoiceDetails=invoiceDetails;
                                next(f);
                            });
                        }
                        else {
                            next(f);
                        }
                    });
                },function()
                {
                    me.sendData(d);
                });
                } else {  
                    return me.addErrorAndSend('ERR052');
                }
            });
        }
        else if (me.user.user_type && me.user.user_type == "C") {
            var sql = "select " 
            sql +=   "dpd.invoice_number, "
            sql +=   "pf.name as partner_name, "
            sql +=   "dpd.from_partner_id, "
            sql +=   "dpd.to_charity_id, "
            sql +=   "dpd.partner_payment_status, "
            sql +=   "dpd.charity_payment_status, " 
            sql +=   "dpd.brn, "            
            sql +=   "sum( amount ) as charity_amount "
            sql += "from "
            sql +=   "donation_payment_details dpd "
            sql +=   "left join public.pgd_foundations pf on (dpd.from_partner_id=pf._id) "
            sql +=   "group by "
            sql +=   "dpd.invoice_number, "
            sql +=   "pf.name, "
            sql +=   "dpd.from_partner_id, "
            sql +=   "dpd.to_charity_id, "
            sql +=   "dpd.partner_payment_status, "            
            sql +=   "dpd.charity_payment_status, "
            sql +=   "dpd.brn "                      
           // sql +=   "pf.name "    
            sql += "having "
            sql +=   "to_charity_id =$1 ;"
    me.src.db.query(sql, [me.user.pid], function (e, data) {
        if (data && data.length > 0) {
            var invoiceDetails={};
            var totalInvoice=data.length;
            data.prepEach(function (j, next) {
                var sql = "select " 
                sql +=   "dpd._id, "
                sql +=   "pc.name as item_name, "
                sql +=   "dpd.to_charity_item, "
                sql +=   "sum( amount ) as charity_amount "
                sql += "from "
                sql +=   "donation_payment_details dpd "
                sql +=   "left join public.pgd_foundations pf on (dpd.to_charity_id=pf._id) "
                sql +=   "left join public.pgd_catalogue pc on (dpd.to_charity_item=pc._id) "                        
                sql +=   "group by "
                sql +=   "dpd._id, "
                sql +=   "dpd.to_charity_item, "  
                sql +=   "pf.name, "    
                sql +=   "pc.name "    
                sql += "having "
                sql +=   "invoice_number =$1 "
                sql +=   "and to_charity_id =$2 ;"
            me.src.db.query(sql, [j.invoice_number,j.to_charity_id], function (e, result) {
                if(result && result.length >0)
                {
                var itemWiseInvoiceDetails={};
                var total=0;
                total=result.length;
                itemWiseInvoiceDetails.total=total;
                itemWiseInvoiceDetails.list=result;
                j.itemWiseInvoiceDetails=itemWiseInvoiceDetails;
                    next(j);
                }
                else
                {
                   next(j); 
                }
            });
            },function()
            {
                var response={};
                invoiceDetails.total=totalInvoice;
                invoiceDetails.list=data;
                response.invoiceDetails=invoiceDetails;
                me.sendData(response);
            });
        }
        else {
            me.sendError();
        }
    });
                }
        else
        {
           return me.addErrorAndSend('ERR040');
        }
    }
    /*
     @author : Kaustubh Thube
     @date : 10 July 2017
     @function : generateReport
     @desc : generate Report
     @Modified: Vaibhav Vaidya, 22nd Sept. add PDF pagination, billing cycle on particular day, docs download links, add charity item names.
                Vaibhav Vaidya, 28th Sept. Logic for Settlement.
                Vaibhav Vaidya, 10th October. Code for Rollback/Commit.
     */
    , generateReport: function (cb) {
        var me = this;
        var fs = require('fs');
        //var pdf = require('html-pdf');
        const excel = require('node-excel-export');
        var username = "##001";
        var address = "##001";

        var foundations_ids = [];
        var toDate;
        var fromDate;
        var billing_cycle_date;
        var partnerDocsArr=[];

        var Foundations = Ext.create('Crm.modules.organizations.model.FoundModel', {
            scope: me
        });
        Foundations.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var DonationPayment = Ext.create('Crm.modules.donationPayment.model.DonationPaymentModel', {
            scope: me
        });
        DonationPayment.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var DonationPaymentDetails = Ext.create('Crm.modules.donationPaymentDetails.model.DonationPaymentDetailsModel', {
            scope: me
        });
        DonationPaymentDetails.error = function (code, info) {
            return me.addErrorObj(code);
        };
        TransactionModel = Ext.create('Crm.modules.orderTransaction.model.OrderTransactionModel', {
            scope: me
        });
        TransactionModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var AccountHoldersModel = Ext.create('Crm.modules.accountHolders.model.AccountHoldersModel', {
            scope: me
        });
        AccountHoldersModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
         var DB_TRANSACTION = {
            BEGIN: function (next) {
                opt = Ext.clone(me.config.pgsql);
                opt.callback = function (conn) {
                    me.initModels();
                    me.src.db.conn.query('BEGIN', function (err) {
                        if (err) {
                            me.sendError(err);
                        } else {
                            next();
                        }
                    });
                }
                me.src.db = Ext.create("Database.drivers.Postgresql.Database", opt);
            },
            COMMIT: function (data) {
                me.src.db.conn.query('COMMIT', function () {
                        me.sendData(data) 
                });
            },
            //rollback with error code
            ROLLBACKCODE: function (err) {
                me.src.db.conn.query('ROLLBACK', function () {
                    return me.addErrorAndSend(err);
                });
            },
            //rollback with error object
            ROLLBACKOBJ: function (err) {
                me.src.db.conn.query('ROLLBACK', function () {
                    return me.addErrorObj(err);
                });
            },
            //standard rollback function for orderController's object
            ROLLBACK: function (err) {
                me.src.db.conn.query('ROLLBACK', function () {
                    me.sendError(err);
                });
            }
        }
        var handleError = function (err) {
            return DB_TRANSACTION.ROLLBACKOBJ(err);
        };
        [

            //get dates first to calculate invoice 
            function (next) {
                var today = new Date();
                var dd1 = today.getDate() //- 1;
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                var mm1 = today.getMonth();
                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                if (mm < 10) {
                    mm = '0' + mm;
                }

                if (mm1 < 10) {
                    mm1 = '0' + mm1;
                }

                billing_cycle_date = dd;

                toDate = mm + '/' + dd1 + '/' + yyyy+' 23:59:59';
                fromDate = mm1 + '/' + dd + '/' + yyyy+' 00:00:00';

                //create rawData Object at initial stage to add fields we may need at later stage
                next();
            },

            //get all foundations whose billing cycle date is today
            function (next) {
                Foundations.dbCollection.find({
                    billing_cycle_date: billing_cycle_date,
                    $or:[{orgtype:'2'},{orgtype:'3'}]
                },
                    { _id: 1 }, function (e, data) {
                        if (data) {
                            for (var i = 0; i < data.length; i++) {
                                foundations_ids[i] = data[i]._id;
                            }
                            next(foundations_ids);
                        } else {
                            return me.addErrorObj(e);
                        }
                    });
            },
            //Vaibhav Vaidya 28th Sept 2017, start settlement logic
            //do settlement for all order transactions which are partially claimed or unclaimed for listed foundation ids
            function (foundations_ids, next) {
            var orderData=[];
            var foundationData=[];
                foundations_ids.prepEach(function(r,nextItem){
                    TransactionModel.dbCollection.find({
                        partner_id:r,
                        //Find partially claimed or unclaimed orders
                        $or:[{claimed_status:'1'},{claimed_status:'0'}]
                    }, {}, function (err, orderRecord) {
                        if (orderRecord && orderRecord.length > 0) {
                            for(var i=0;i<orderRecord.length;i++){
                                orderData.push(orderRecord[i])
                            }
                            nextItem(r);
                        }
                        else {
                            nextItem(r);
                        }
                    });
                },function(){
                    for (var i = 0; i < foundations_ids.length; i++) {
                        foundationData.push({ 'partner_id': foundations_ids[i] })
                    }
                    for (var i = 0; i < foundationData.length; i++) {
                        for (var j = 0; j < orderData.length; j++) {
                            if (orderData[j].partner_id == foundationData[i].partner_id) {
                                if (foundationData[i].orderData == undefined) foundationData[i].orderData = [];
                                if (foundationData[i].grandDonation== undefined) foundationData[i].grandDonation = 0;
                                if (orderData[j].claimed_status == 0) {
                                    //full amount
                                    orderData[j].to_donate= parseFloat(orderData[j].donation_amount) + parseFloat(orderData[j].follow_amount)
                                    foundationData[i].grandDonation += orderData[j].to_donate
                                }
                                else if (orderData[j].claimed_status == 1) {
                                    //partial amount
                                    orderData[j].to_donate= (parseFloat(orderData[j].donation_amount) + parseFloat(orderData[j].follow_amount)) - parseFloat(orderData[j].claimed_amount)
                                    foundationData[i].grandDonation += orderData[j].to_donate
                                }
                                foundationData[i].orderData.push(orderData[j]);
                            }
                        }
                    }
                    next(foundations_ids,foundationData)
                });
            },
            //begin statement
            function (foundations_ids, foundationData, next) {
                DB_TRANSACTION.BEGIN(function () {
                    next(foundations_ids, foundationData);
                });
            },
            //Check the user attached to every qualifable order transaction, check if their charity plan qualifies
            function (foundations_ids, foundationData, next) {
                var activeIndex = -1, partnerEmergencyFlag;
                foundationData.prepEach(function (r, nextItem) {
                    if (r.grandDonation > 0) {
                        r.orderData.prepEach(function (k, nextOrder) {
                            if(k.to_donate>0){
                                if (k.partner_plan == undefined) k.partner_plan=false;
                                if (k.orderPartnerData == undefined) k.orderPartnerData = {};
                                k.orderPartnerData._id = r.partner_id;
                                k.orderPartnerData.username = k.username;
                                k.orderPartnerData.orderTransactionId = k._id;
                                k.orderPartnerData.total_amount = k.total_amount;
                                k.orderPartnerData.walletOrderId = "";
                                //check if user of that order exists
                                AccountHoldersModel.dbCollection.findOne({
                                        username: k.username,
                                    }, {_id:1,username:1,follow:1,user_type:1}, function (err, userData) {
                                        if (userData) {
                                            if(k.userData==undefined) k.userData=userData;
                                            //check if User's charity plan exists and qualifies
                                            me.OrderController.retrieveCharityPlans(userData._id, function (data) {
                                                if (data) {
                                                    for (var i = 0; i < data.length; i++) {
                                                        if (data[i].plan_type == 'A') {
                                                            activeIndex = i;
                                                        }
                                                    }
                                                    //Retrieve Details plan by 'ACTIVE' charity plan
                                                    if (!isNaN(activeIndex)) {
                                                        me.OrderController.checkActivePlanQualify(data[activeIndex]._id, k.to_donate, function (activeData) {
                                                            if (activeData && activeData.walletEmergencyFlag != undefined) {
                                                                k.partner_plan = activeData.walletEmergencyFlag;
                                                                nextOrder(k);
                                                            }
                                                            else {
                                                                k.partner_plan=true;
                                                                nextOrder(k);
                                                            }
                                                        });
                                                    }
                                                    else {
                                                        k.partner_plan=true;
                                                        nextOrder(k);
                                                    }
                                                }
                                                else {
                                                    k.partner_plan=true;
                                                    nextOrder(k);
                                                }
                                            });
                                        }
                                        else{
                                            k.partner_plan=true;
                                            nextOrder(k);
                                        } 
                                    });                                
                            }
                            else{
                                nextOrder(k);
                            }
                        }, function () {
                            nextItem(r)
                        });
                    }
                    else {
                        nextItem(r)
                    }
                }, function () {
                    next(foundations_ids,foundationData);
                });
            },
            //call charity plan for every listed foundation/user with their total donation amount per order transaction
            function (foundations_ids, foundationData, next) {
                var activeIndex = -1, partnerEmergencyFlag;
                foundationData.prepEach(function (r, nextItem) {
                    if (r.grandDonation > 0) {
                        r.orderData.prepEach(function (k, nextOrder) {
                            if (k.to_donate > 0) {
                                if (k.partner_plan == true) {
                                    k.donater_id = k.partner_id;
                                } else {
                                    if (k.userData && k.userData._id) {
                                        k.donater_id = k.userData._id;
                                    }
                                    else {
                                        k.donater_id = k.partner_id;
                                    }
                                }
                                //logic for partner charity plan donation
                                me.OrderController.retrieveCharityPlans(k.donater_id, function (data) {
                                    if (data) {
                                        for (var i = 0; i < data.length; i++) {
                                            if (data[i].plan_type == 'A') {
                                                activeIndex = i;
                                            }
                                        }
                                        //Retrieve Details plan by 'ACTIVE' charity plan
                                        if (!isNaN(activeIndex)) {
                                            me.OrderController.checkActivePlanQualify(data[activeIndex]._id, k.to_donate, function (activeData) {
                                                if (activeData && activeData.walletEmergencyFlag != undefined) {
                                                    partnerEmergencyFlag = activeData.walletEmergencyFlag;
                                                    me.donateInvoiceCharityPlanNow(k.donater_id, !partnerEmergencyFlag, k.to_donate, (k.donation_amount+k.follow_amount), k.orderPartnerData, function (err, otdata) {
                                                        if (otdata && !err) {
                                                            r.orderPartnerData = otdata;
                                                            if (k.userData._id!=undefined) {
                                                                me.OrderController.debitUserWallet({"_id":k.userData._id}, k.to_donate, function (err, resp) {
                                                                    if (!err && resp.success == true) {
                                                                        nextOrder(k);
                                                                    }
                                                                    else {
                                                                        return DB_TRANSACTION.ROLLBACKOBJ(err);
                                                                    }
                                                                });
                                                            }
                                                        }
                                                        else {
                                                            nextOrder(k);
                                                        }
                                                    });
                                                }
                                                else {
                                                    partnerEmergencyFlag = true;
                                                    me.donateInvoiceCharityPlanNow(k.donater_id, !partnerEmergencyFlag, k.to_donate, (k.donation_amount + k.follow_amount), k.orderPartnerData, function (err, otdata) {
                                                        if (otdata && !err) {
                                                            r.orderPartnerData = otdata;
                                                            if (k.userData._id != undefined) {
                                                                me.OrderController.debitUserWallet({"_id":k.userData._id}, k.to_donate, function (err, resp) {
                                                                    if (resp.success == true) {
                                                                        nextOrder(k);
                                                                    }
                                                                    else {
                                                                        return DB_TRANSACTION.ROLLBACKOBJ(err);
                                                                    }
                                                                });
                                                            }
                                                        }
                                                        else {
                                                            return DB_TRANSACTION.ROLLBACKOBJ(err);
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                        else {
                                            partnerEmergencyFlag = true;
                                            me.donateInvoiceCharityPlanNow(k.donater_id, !partnerEmergencyFlag, k.to_donate, (k.donation_amount + k.follow_amount), k.orderPartnerData, function (err, otdata) {
                                                if (otdata && !err) {
                                                    r.orderPartnerData = otdata;
                                                    if (k.userData._id != undefined) {
                                                        me.OrderController.debitUserWallet({"_id":k.userData._id}, k.to_donate, function (err, resp) {
                                                            if (resp.success == true) {
                                                                nextOrder(k);
                                                            }
                                                            else {
                                                                return DB_TRANSACTION.ROLLBACKOBJ(err);
                                                            }
                                                        });
                                                    }
                                                }
                                                else {
                                                    return DB_TRANSACTION.ROLLBACKOBJ(err);
                                                }
                                            });
                                        }
                                    }
                                    else {
                                        partnerEmergencyFlag = true;
                                        me.donateInvoiceCharityPlanNow(k.donater_id, !partnerEmergencyFlag, k.to_donate, (k.donation_amount + k.follow_amount), k.orderPartnerData, function (err, otdata) {
                                            if (otdata && !err) {
                                                r.orderPartnerData = otdata;
                                                if (k.userData._id != undefined) {
                                                    me.OrderController.debitUserWallet({"_id":k.userData._id}, k.to_donate, function (err, resp) {
                                                        if (resp.success == true) {
                                                            nextOrder(k);
                                                        }
                                                        else {
                                                            return DB_TRANSACTION.ROLLBACKOBJ(err);
                                                        }
                                                    });
                                                }
                                            }
                                            else {
                                                return DB_TRANSACTION.ROLLBACKOBJ(err);
                                            }
                                        });
                                    }
                                });
                            }
                            else {
                                nextOrder(k);
                            }
                        }, function () {
                            nextItem(r)
                        });
                    }
                    else {
                        nextItem(r)
                    }
                }, function () {
                    next(foundations_ids, foundationData);
                });
            },
            //get all details ab' donation per foundation
            function (foundations_ids,foundationData, next) {
                if (foundations_ids.length > 0) {
                    var sql = "SELECT partner_id,SUM(charity_amount) AS charity_amount "
                        + " from order_charity_details "
                        + " where ctime >= $1 and ctime <=$2 "
                        sql+= " GROUP BY partner_id having partner_id IN (\'" + foundations_ids.join('\',\'') + "\') ";
                    me.src.db.query(sql, [fromDate, toDate], function (e, data) {
                        if (data && data.length > 0) {
                            next(data, foundations_ids);
                        }
                        else if (e == null && data.length == 0) {
                            //return me.addErrorAndSend("ERR057");
                            return DB_TRANSACTION.ROLLBACKCODE("ERR057");
                        }
                        else {
                            return DB_TRANSACTION.ROLLBACKOBJ(e);
                        }
                    });

                }
            },
            //get breakdown of every donation by the Contributors/Partners
            function (donationPayment, foundations_ids, next) {
                var params = [];
                //add ctime to parameters array so its consistent with above query
                foundations_ids.unshift(fromDate, toDate)
                for (var i = 3; i <= foundations_ids.length; i++) {
                    params.push('$' + i);
                }
                var sql = "SELECT partner_id,username,charity_id,catalogue_id, SUM(charity_amount) AS charity_amount from order_charity_details where ctime >= $1 and ctime <=$2 ";
                sql+=" GROUP BY partner_id,username,charity_id,catalogue_id having partner_id IN (" + params.join(",") + ")";
                me.src.db.query(sql, foundations_ids, function (e, data) {
                    if (data && data.length > 0) {
                        foundations_ids.splice(0, 2);
                        next(data, donationPayment, foundations_ids);
                    }
                    else if (e == null && data.length == 0) {
                        return DB_TRANSACTION.ROLLBACKCODE("ERR057");
                    }
                    else {
                        return DB_TRANSACTION.ROLLBACKOBJ(e);
                    }
                });
            },
            function (donationPaymentDetails, donationPayment, foundations_ids, next) {
                var rawData = {};
                rawData.donationPayment = donationPayment;
                rawData.donationPaymentDetails = donationPaymentDetails;
                //   me.sendData(rawData);
                donationPayment.prepEach(function (r, next) {
                    var rowId = me.src.db.createObjectId();
                    var groupId = me.src.db.createObjectId();
                    var path = '/api/rest/refdata/download-file?id=';
                    r.groupDocId=groupId;
                    var pdfFileName= "PDF"+me.src.db.createObjectId();
                    var xlsxFileName= "Excel"+me.src.db.createObjectId();
                    var xlsx_url=xlsxFileName.replace("Excel","");
                    xlsx_url=path + xlsx_url;
                    var pdf_url=pdfFileName.replace("PDF","");
                    pdf_url= path + pdf_url;
                    partnerDocsArr.push({'partner_id':r.partner_id,'document_group_id':groupId, FileName:pdfFileName,'downloadUrl':pdf_url})
                    partnerDocsArr.push({'partner_id':r.partner_id,'document_group_id':groupId, FileName:xlsxFileName,'downloadUrl':xlsx_url})
                    me.DonationPayment.write({
                        "_id": rowId,
                        "from_partner_id": r.partner_id,
                        "amount": r.charity_amount,
                        "pdf_url":pdf_url,
                        "xlsx_url":xlsx_url
                    }, function (data) {
                        if (data.success) {
                            donationPaymentDetails.prepEach(function (d, next) {
                                if (r.partner_id == d.partner_id) {
                                    me.DonationPaymentDetails.write({
                                        "_id": me.Util.getObjectId(),
                                        "invoice_number": rowId,
                                        "from_partner_id": d.partner_id,
                                        "to_charity_id": d.charity_id,
                                        "to_charity_item": d.catalogue_id,
                                        "partner_payment_status": 0,
                                        "charity_payment_status": 0,
                                        "amount": d.charity_amount,
                                        "document_id":groupId,
                                        "username":d.username
                                    }, function (data) {
                                        if (data.success) {
                                            next(d);
                                        }
                                    }, {
                                            add: 1,
                                            modify: 1
                                        });
                                }
                                else {
                                    next(d);
                                }
                            }, function () {
                                next(r);
                            });
                        } else {
                            return DB_TRANSACTION.ROLLBACKOBJ(data.error);
                        }
                    },{
                         add: 1, modify: 1 
                    });
                }, function () {
                    //  me.sendData(rawData);
                    next(foundations_ids,donationPayment);
                });
            },
            function (foundations_ids, donationPayment, next) {
                foundations_ids.prepEach(function (r, next) {
                    var partner_id = r;
                    var docGroupId;
                    Foundations.dbCollection.find({
                        _id: partner_id
                    }, {}, function (err, foundData) {
                        foundData.prepEach(function (f, next) {
                            username = f.name;
                            address = f.address;
                        }, function () {
                            next();
                        });
                    });
                    for(var i=0;i<donationPayment.length;i++){
                        if(r==donationPayment[i].partner_id)
                            {
                                docGroupId=donationPayment[i].groupDocId;
                                break;
                            }
                    }
                    var sqlPlaceHolders=[];
                    sqlPlaceHolders.push(partner_id);
                    var sql = "select pdgf.name as name,sum(dpd.amount) as donation_amount,dpd.document_id as docgroupid, cat.name as catname, bd.bank_name as bank_name,bd.bank_account_number as bank_acc_number"
                        + " from donation_payment_details dpd "
                        + " LEFT OUTER JOIN pgd_foundations pdgf ON pdgf._id = dpd.to_charity_id "
                        + " LEFT OUTER join bank_details bd on pdgf._id = bd.partner_id " 
                        + " LEFT OUTER join pgd_catalogue cat on dpd.to_charity_item = cat._id "

                        + " where dpd.from_partner_id = $1 and dpd.partner_payment_status=0 ";
                    if(docGroupId!=undefined){
                        sql+=" and dpd.document_id=$2 ";
                        sqlPlaceHolders.push(docGroupId);
                    }
                        
                    sql+= " GROUP BY pdgf.name, cat.name,bd.bank_name,bd.bank_account_number,dpd.document_id,pdgf._id ";

                    me.src.db.query(sql, sqlPlaceHolders, function (e, data) {
                        var fileName;
                            for(var z=0;z<partnerDocsArr.length;z++){
                                if(partnerDocsArr[z].partner_id==partner_id && partnerDocsArr[z].FileName!=undefined){
                                    if((partnerDocsArr[z].FileName.search("Excel"))>=0)
                                        {
                                        fileName = partnerDocsArr[z].FileName.replace("Excel","");
                                        break;
                                        }
                                }
                            }
                            if(fileName==undefined){
                                return DB_TRANSACTION.ROLLBACKCODE("ERR058");
                            }
                        data.prepEach(function (d, next)    {
                            const styles = {
                                headerDark: {
                                    fill: {
                                        fgColor: {
                                            rgb: 'FFFFFFFF'
                                        }
                                    },
                                    font: {
                                        color: {
                                            rgb: 'FF000000'
                                        },
                                        sz: 11,
                                        bold: true
                                        //  underline: true
                                    },
                                    border: {
                                        top: {
                                            style: 'medium',
                                            color: 'FF000000'
                                        }, bottom: {
                                            style: 'medium',
                                            color: 'FF000000'
                                        }, left: {
                                            style: 'medium',
                                            color: 'FF000000'
                                        }, right: {
                                            style: 'medium',
                                            color: 'FF000000'
                                        }
                                    }
                                },
                                cellPink: {
                                    fill: {
                                        fgColor: {
                                            rgb: 'FFFFCCFF'
                                        }
                                    }
                                },
                                cellGreen: {
                                    fill: {
                                        fgColor: {
                                            rgb: 'FF00FF00'
                                        }
                                    }
                                }
                            };

                            //Array of objects representing heading rows (very top) 
                            const heading = [
                                [{ value: 'PARTNER_NAME', style: styles.headerDark }, { value: username, style: styles.headerDark }],
                                [{ value: 'DATE', style: styles.headerDark }, { value: new Date().toISOString().slice(0, 10), style: styles.headerDark }],
                                [{ value: 'ADDRESS', style: styles.headerDark }, { value: address, style: styles.headerDark }],
                                []// <-- It can be only values 
                            ];
                            const specification = {
                                name: {
                                    displayName: 'CHARITY_NAME',
                                    headerStyle: styles.headerDark,
                                    width: 220
                                },
                                catname: {
                                    displayName: 'Charity Item Name',
                                    headerStyle: styles.headerDark,
                                    width: 220
                                },
                                donation_amount: {
                                    displayName: 'AMOUNT',
                                    headerStyle: styles.headerDark,
                                    width: '10'
                                },
                                bank_name: {
                                    displayName: 'BANK',
                                    headerStyle: styles.headerDark,
                                    width: 220
                                },
                                bank_acc_number: {
                                    displayName: 'BANK_ACC_NUM',
                                    headerStyle: styles.headerDark,
                                    width: 220
                                }
                            };
                            const merges = [
                                { start: { row: 1, column: 1 }, end: { row: 1, column: 1 } },
                                { start: { row: 2, column: 1 }, end: { row: 2, column: 1 } }];

                            const report = excel.buildExport(
                                [
                                    {
                                        name: fileName,
                                        heading: heading,
                                        merges: merges,
                                        specification: specification,
                                        data: data
                                    }
                                ]
                            );
                            //fs.writeFileSync(__dirname + "/../../../uploads/"  + fileName, report, 'binary');
                            fs.writeFileSync("uploads/"  + fileName, report, 'binary');
                            next(d);
                        }, function () {
                            next(r);
                        });
                    });
                }, function () {
                    next(foundations_ids, donationPayment);
                });
            },

            // create pdf from database 
            function (foundations_ids, donationPayment) {
                var html = fs.readFileSync(__dirname + '/../../../static/report.html', 'utf8');
                var docGroupId;
                foundations_ids.prepEach(function (r, next) {
                /*
                    var partner_id = r;
                    Foundations.dbCollection.find({
                        _id: partner_id
                    }, {}, function (err, foundData) {
                        foundData.prepEach(function (f, next) {
                            username = f.name;
                            address = f.address;
                        }, function () {
                            next();
                        });
                    });
                    var options = { format: 'A4', "orientation": "portrait" };
                    var fileName, continueFlag = false, firstPageLength = 15, nextPageLength = 0, recordDiff = 0;
                    html = html.replace("#DATE#", new Date().toISOString().slice(0, 10));
                    html = html.replace("#NAME#", username);
                    html = html.replace("#CONTACT#", "");
                    html = html.replace("#FROM DATE#", fromDate);
                    html = html.replace("#TO DATE#", toDate);
                    html = html.replace("#PARTNER NAME#", username);
                    html = html.replace("#ADDRESS#", address);
                    html = html.replace("#NEWINSERT#", "");
                    html = html.replace("#PGDReport#", "PGD INVOICE");
                    html = html.replace("#TABLE#", "<tr><th>CHARITY NAME</th><th>Charity Item Name</th><th>AMOUNT</th><th>BANK_NAME</th><th>BANK_ACCOUNT</th></tr>");
                    for(var i=0;i<donationPayment.length;i++){
                        if(r==donationPayment[i].partner_id)
                            {
                                docGroupId=donationPayment[i].groupDocId;
                                break;
                            }
                    }
                    var sqlPlaceHolders=[];
                    sqlPlaceHolders.push(partner_id);
                    var sql = "select pdgf.name as name,sum(dpd.amount) as donation_amount,dpd.document_id as docgroupid, cat.name as catname, bd.bank_name as bank_name,bd.bank_account_number as bank_acc_number"
                        + " from donation_payment_details dpd "
                        + " LEFT OUTER JOIN pgd_foundations pdgf ON pdgf._id = dpd.to_charity_id "
                        + " LEFT OUTER join bank_details bd on pdgf._id = bd.partner_id " 
                        + " LEFT OUTER join pgd_catalogue cat on dpd.to_charity_item = cat._id "

                        + " where dpd.from_partner_id = $1 and dpd.partner_payment_status=0 ";
                    if(docGroupId!=undefined){
                        sql+=" and dpd.document_id=$2 ";
                        sqlPlaceHolders.push(docGroupId);
                    }
                        
                    sql+= " GROUP BY pdgf.name, cat.name,bd.bank_name,bd.bank_account_number,dpd.document_id ";
                    me.src.db.query(sql, sqlPlaceHolders, function (e, data) {
                        //data.prepEach(function (d, next) {
                            nextPageLength = firstPageLength - 9;
                            for (var j = 0; j < data.length; j++) {
                                html = html.replace("#TABLEROW#", "<tr><td>" + data[j].name + "</td><td>" + data[j].catname + "</td><td>" + data[j].donation_amount + "</td><td>" + data[j].bank_name + "</td><td>" + data[j].bank_acc_number + "</td></tr>" + "#TABLEROW#");
                                if (j == nextPageLength) {
                                    nextPageLength += 2;
                                    recordDiff = j + 1;
                                    html = html.replace("#TABLEROW#", "");
                                    continueFlag = true;
                                    break;
                                }
                            }
                            if (continueFlag == true) {
                                for (j = recordDiff; j < data.length; j++) {
                                    if ((j - recordDiff) % nextPageLength == 0) {
                                        html = html.replace("#TABLEROW#", "");
                                        html = html.replace("#NEWPAGE#", "<div class=\"pagebreak\"> </div><div class=\"page\"><div class=\"table\"><table>#TABLE##TABLEROW#</table></div><hr><div class=\"group\"><div class=\"line\">Enovate IT Outsourcing Pvt Ltd</div>" +
                                            "<div class=\"line\">Amanora Chambers, office 429</div><div class=\"line\">020 000000</div>" +
                                            "</div></div>#NEWPAGE#");
                                        html = html.replace("#TABLE#", "<tr><th>CHARITY NAME</th><th>AMOUNT</th><th>BANK_NAME</th><th>BANK_ACCOUNT</th></tr>");
                                    }
                                   html = html.replace("#TABLEROW#", "<tr><td>" + data[j].name + "</td><td>" + data[j].catname + "</td><td>" + data[j].donation_amount + "</td><td>" + data[j].bank_name + "</td><td>" + data[j].bank_acc_number + "</td></tr>" + "#TABLEROW#");
                                }
                            }
                            html = html.replace("#TABLEROW#", "");
                            html = html.replace("#NEWPAGE#", "");
                            var fileName;
                            for(var z=0;z<partnerDocsArr.length;z++){
                                if(partnerDocsArr[z].partner_id==r && partnerDocsArr[z].FileName!=undefined){
                                    if((partnerDocsArr[z].FileName.search("PDF"))>=0)
                                        {
                                        fileName = partnerDocsArr[z].FileName.replace("PDF","");
                                        break;
                                        }
                                }
                            }
                            if(fileName==undefined){
                                return me.addErrorAndSend("ERR058");
                            }
                        //}, function () {
                            html = html.replace("#TABLEROW#", "");
                            //html = html.replace("#newpage#", "<div class=" + "page" + "><img class=" + "logo" + "src=" + "{{image}}" + "><div class=" + "bottom" + "><div class=" + "line center" + ">8045 zürich</div></div></div>");
                            pdf.create(html, options).toFile(__dirname + "/../../../uploads/" + fileName + ".pdf", function (err, res) {
                                if (err) return console.log(err);
                                fs.rename(__dirname + "/../../../uploads/" + fileName + ".pdf", __dirname + "/../../../uploads/" + fileName, function (err) {
                                    if (!err) {
                                        next(r);
                                    }
                                    else {
                                        return me.addErrorAndSend("ERR058");
                                    }

                                });
                            });
                        //});
                    });*/
                    next(r);
                }, function () {
                    partnerDocsArr.prepEach(function(d,nextItem){
                        var fileType=-1;
                        filetype=((d.FileName.search('PDF')>=0)?"application/pdf":"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                        if(!d.fileType) d.fileType=((d.FileName.search('PDF')>=0)?"pdf":"xlsx")
                        d.FileName =d.FileName.replace("Excel","");
                        d.FileName=d.FileName.replace("PDF","");
                        me.DocsModel.write({
                            _id: d.FileName,
                            file_name: d.FileName,
                            doc_type: filetype?filetype:"",
                            group_id: d.document_group_id,
                            active: true,
                            pid: d.partner_id,
                            date_add: new Date(),
                            date_fin: new Date(),
                            doc_name: 'Invoice generated'
                        }, function (data) {
                            if (data.success) {
                                nextItem(d);
                            } else {
                                return DB_TRANSACTION.ROLLBACKOBJ(data.error);
                            }
                        }, { add: true, modify: true })
                    },function(){
                        return DB_TRANSACTION.COMMIT(partnerDocsArr);
                    });
                });
            }
        ].runEach();

    },
    uploadExpenditureProof:function(cb)
    {
        var me = this;
        var inParams= me.req.body.reqData;
        var url="";
        var DocsModel = Ext.create('Crm.modules.file.model.FilesModel', {
            scope: me
        });

        DocsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };

        var InvoiceDetailsModel = Ext.create('Crm.modules.donationPaymentDetails.model.DonationPaymentDetailsModel', {
            scope: me
        });

        InvoiceDetailsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };

        var path = '/api/rest/refdata/download-file?id=';
        DocsModel.dbCollection.find({
            group_id: inParams.document_id
        }, { _id: 1 }, function (err, d) {
            if (d && d.length > 0) {
                d.prepEach(function (e, next) {
                    if(url=="")
                        {
                            url=path + e._id;
                        }
                    else
                        {    
                     url =url+","+ path + e._id;
                        }
                     next(e);
                }, function () {
                    InvoiceDetailsModel.dbCollection.findOne({
                        _id: inParams._id,
                    }, {_id:1}, function (e, data) {
                        if (data && data._id) {
                            InvoiceDetailsModel.dbCollection.update({
                                _id: data._id
                                }, {
                                    $set: {
                                        document_id:inParams.document_id, 
                                        url: url
                                        }
                                    }, function (e, d) {me.sendData([]);}); 
                                 }
                        else {
                            return me.addErrorObj('ERR043');
                        }
                    }); 
                });
            } else {
                    return me.addErrorAndSend('ERRO50', 'document_id');
            }
        });

    }

    /*
    @author : Vaibhav Vaidya
    @date : 29 Sept 2017
    @function : donateInvoiceCharityPlanNow
    @desc : Searched for charity plan and donates entire amount as per percentage distribution
    @Note Modified version of the same function from Order Controller
    */
    , donateInvoiceCharityPlanNow: function (partnerUserId, doConfirm, donateAmt, totalAmt, orderPartnerData, cb) {
        var me = this, searchPlanType, insertionIndex;
        searchPlanType = doConfirm ? 'A' : 'E';
        var donateStarted=false;
        var DetailsModel = Ext.create('Crm.modules.charityPlanDetails.model.CharityPlanDetailsModel', {
            scope: me
        });
        DetailsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var CatalogueInfoModel = Ext.create('Crm.modules.charityCatalogue.model.CharityCatalogueInfo', {
            scope: me
        });
        CatalogueInfoModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var DonateModel = Ext.create('Crm.modules.donateNow.model.DonateNowModel', {
            scope: me
        });
        DonateModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var handleError = function (err) {
            return cb(err, null);
        }
        me.OrderController.retrieveCharityPlans(partnerUserId, function (data) {
            if (data) {
                for (var i = 0; i < data.length; i++) {
                    if (data[i].plan_type == searchPlanType) {
                        insertionIndex = i;
                        break;
                    }
                }
                if(insertionIndex!=undefined){
                        DetailsModel.dbCollection.find({
                            charity_plan_id: data[insertionIndex]._id
                        }, {}, function (err, data) {
                            if (data && data.length > 0) {
                                donateStarted=true;
                                data.prepEach(function (r, next) {
                                    r.charityDonationAmount = donateAmt * (parseFloat(r.percentage) / 100.0);
                                    me.OrderCharityModel.write({
                                        "_id": me.Util.getObjectId(),
                                        "partner_id": orderPartnerData._id,
                                        "charity_id": r.charity_foundation_id,
                                        "username": orderPartnerData.username,
                                        "order_transaction_id": orderPartnerData.orderTransactionId,
                                        "charity_plan_id": r.charity_plan_id,
                                        "charity_amount": r.charityDonationAmount,
                                        "total_amount": totalAmt || 0,
                                        "wallet_order_id": orderPartnerData.walletOrderId||"",
                                        "catalogue_id": r.pgd_catalogue_id
                                    }, function (OCdata) {
                                        if (OCdata.success) {
                                            //credit donated amount to charity catalogue info's collected amount
                                            var charityPlaceholders = [];
                                            charityPlaceholders.push(parseFloat(parseFloat(r.charityDonationAmount).toFixed(3)));
                                            var charityInfoSql = "update pgd_catalogue cat set donation_amount_collected = "
                                            charityInfoSql += " case when donation_amount_collected is null then ($" + charityPlaceholders.length + ") else (donation_amount_collected + $" + charityPlaceholders.length + ") end";
                                            charityPlaceholders.push(r._id);
                                            charityInfoSql += " from charity_plan_details cpd where cpd.pgd_catalogue_id=cat._id and cpd._id = $" + charityPlaceholders.length;
                                            me.src.db.query(charityInfoSql, charityPlaceholders, function (e, data) {
                                                if (!e && data != undefined) {
                                                    //Update query returns no confirmation/message
                                                    next(r);
                                                }
                                                else {
                                                    return handleError(e);
                                                }
                                            });
                                        } else {
                                            return handleError(OCdata.error);
                                        }
                                    }, {
                                            add: 1,
                                            modify: 1
                                        });
                                }, function () {
                                    if(donateStarted==true){
                                        me.TransactionModel.dbCollection.findOne({
                                            _id: orderPartnerData.orderTransactionId
                                        }, { claimed_status: 1, claimed_amount: 1 }, function (err, transactionData) {
                                            if(!err && transactionData){
                                                me.TransactionModel.dbCollection.update({
                                                    _id: orderPartnerData.orderTransactionId
                                                }, {
                                                        $set: {
                                                            "claimed_status": 2,
                                                            "claimed_amount": donateAmt+transactionData.claimed_amount
                                                        }
                                                    }, function (err, data) {
                                                        if (err)
                                                            return handleError(err)
                                                        else {
                                                            orderPartnerData.claimed_status = 2;
                                                            orderPartnerData.claimed_amount = donateAmt+transactionData.claimed_amount;
                                                            cb(null, orderPartnerData);
                                                        }
                                                    });
                                            }
                                        });
                                    }
                                    else{
                                        orderPartnerData.claimed_status = 0;
                                        orderPartnerData.claimed_amount = 0;
                                        cb(null, orderPartnerData);
                                    }
                                });
                            }
                            else {
                                orderPartnerData.claimed_status = 0;
                                orderPartnerData.claimed_amount = 0;
                                cb(null, orderPartnerData);
                            }
                        });
                }
                else{
                    orderPartnerData.claimed_status = 0;
                    orderPartnerData.claimed_amount = 0;
                    cb(null, orderPartnerData);
                }
            }
        });
    }
});