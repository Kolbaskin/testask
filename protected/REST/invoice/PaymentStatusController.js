Ext.define('Crm.REST.invoice.PaymentStatusController', {
    extend: "Crm.REST.RestBaseController",
    constructor: function () {
        this.callParent(arguments);
    },
    /*
     @author : Kaustubh Thube
     @date : 10 July 2017
     @function : api ymentstatus
     @desc : set payment status
     */
    apiSetPaymentStatus: function (cb) {
        var me = this;
        var params = me.req.body.reqData;

        if (!me.user) {
            me.sendError(401);
            return;
        }

        if (me.user.user_type == "U") {
            me.sendError(401);
            return;
        }
      
        var DonationPayment = Ext.create('Crm.modules.donationPayment.model.DonationPaymentModel', {
            scope: me
        });
        DonationPayment.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var DonationPaymentDetails = Ext.create('Crm.modules.donationPaymentDetails.model.DonationPaymentDetailsModel', {
            scope: me
        });
        DonationPaymentDetails.error = function (code, info) {
            return me.addErrorObj(code);
        };

        if (me.user.pid && me.user.user_type == "P") {
            if (!me.validateRequest([
                {
                    type: 'int',
                    field: "status",
                    isRequired: true,
                },
                {
                    type: 'STRING',
                    field: "brn",
                    isRequired: true,
                }
            ])) {
                return me.sendError();
            }
            DonationPaymentDetails.dbCollection.update({
                invoice_number:params.invoice_number,
                from_partner_id:me.user.pid,
                to_charity_id:params.to_charity_id
            }, {
                    $set: {
                        partner_payment_status: params.status,
                        brn:params.brn
                    }   
                }, function (e, d) {
                        if (d) {
                        me.sendData({});
                    }
                    else {
                        return me.addErrorAndSend('ERR040');
                    }
                },{multi:true});
        }
        
        else if (me.user.pid && me.user.user_type == "C") {
            if (!me.validateRequest([
                {
                    type: 'int',
                    field: "status",
                    isRequired: true,
                }
            ])) {
                return me.sendError();
            }
            DonationPaymentDetails.dbCollection.update({
                invoice_number: params.invoice_number,
                from_partner_id:params.from_partner_id,
                to_charity_id:me.user.pid
            }, {
                    $set: {
                        charity_payment_status:params.status,
                    }
                }, function (e, d) {
                    if (d) {
                        me.sendData({});
                    }
                    else {
                        return me.addErrorAndSend('ERR040');
                    }
                },{multi:true});
        }
    }
});