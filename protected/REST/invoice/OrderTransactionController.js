Ext.define('Crm.REST.invoice.OrderTransactionController', {
    extend: "Crm.REST.RestBaseController"
    , list: function () {
        var res = {};
        var list = [];
        var me = this
            , inParams = me.req.body.reqData;

        if (!me.user) {
            me.sendError(401);
            return;
        }
        var skip = inParams.startIndex && /^[0-9]{1,}$/.test(inParams.startIndex + '') ? parseInt(inParams.startIndex) : 0;
        var limit = inParams.numOfRecords && /^[0-9]{1,}$/.test(inParams.numOfRecords + '') ? parseInt(inParams.numOfRecords) : 50;
        var orderTransaction = Ext.create('Crm.modules.orderTransaction.model.OrderTransactionModel', {
            scope: me
        });


        orderTransaction.error = function (code, info) {
            return me.addErrorObj(code);
        };

        // var userIdField = -1;
        // var partnerIdField = -1;

        // for (var i = 0; i < orderTransaction.fields.length; i++) {
        //     if (orderTransaction.fields[i].name == "partner_id") {
        //         partnerIdField = i;
        //     }
        // }

        // var params = {};
        // if (!params.filters) params.filters = [];

        // //filter will act as where clause
        // if (me.user.pid && me.user.pid != null && me.user.pid != "" && me.user.pid != "0") {
        //     params.filters.push({ value: me.user.pid, property: 'partner_id' });
        // }

        // else if (me.user.username != null && me.user.username != "") {
        //     params.filters.push({ value: me.user.username, property: 'username' });
        // }
        // //pagination vars    
        // params._start = skip;
        // params._limit = limit;

        // //fieldset will show only required fields
        // params.fieldSet = ['_id', 'partner_id', 'total_amount', 'donation_amount', 'follow_amount', 'ctime'];

        // //sorters --- will set bydefault on ctime(createTime) desc
        // if (!params.sorters) params.sorters = [];
        // params.sorters.push({ direction: 'DSC', property: 'ctime' });

        // //  orderTransaction.fields[userIdField].bindTo = {collection: 'users', keyField: '_id', fields: {full_name:1}};
        // orderTransaction.fields[partnerIdField].bindTo = { collection: 'pgd_foundations', keyField: '_id', fields: { name: 1 } };
        // orderTransaction.getData(params, function (res) {
        //     if (res.list.length > 0) {
        //         me.sendData(res);
        //     }
        //     else {
        //         return me.addErrorAndSend('ERR051');
        //     }
        // });

        var sql="select "
            sql+="* ,COUNT(*) OVER () as RowCount from ( "
            sql+="select ot._id,pf.name,ot.partner_id,ot.username,ot.total_amount,ot.donation_amount,ot.follow_amount,ot.claimed_amount,ot.ctime,0 as isPayNowTx from public.order_transaction ot "
            sql+="left join public.pgd_foundations pf on (pf._id=ot.partner_id) "
            if(me.user.user_type=="U")
            {
            sql+="where username=$1"
            }
            else
            {
                sql+="where partner_id=$1"   
            }
            sql+="union "
            sql+="select ocd._id,pf.name,ocd.partner_id,ocd.username,ocd.total_amount,ocd.charity_amount,null,null,ocd.ctime,1 as isPayNowTx from public.order_charity_details ocd "
            sql+="left join public.pgd_foundations pf on (pf._id=ocd.partner_id) "
            sql+="where username=$2) "
            sql+="as resutl_table order by ctime desc "
            if(skip!=null && skip!="")
            sql+="OFFSET "+skip+" "
            if(limit!=null && limit!="")
            sql+="limit "+limit+" "; 
            if(me.user.user_type=="U")
            {
                username=me.user.username;
            }
            else
            {
                username=me.user.pid;
            }
            me.src.db.query(sql, [username,username], function (e, data) {
                if(data && data.length >0)
                {
                    var list=[];
                    var responseData={};
                    var total=data[0].rowcount;
                    data.prepEach(function (k, next) {
                        var partner_id={};
                            partner_id._id=k.partner_id;
                            partner_id.name=k.name;
                            k.partner_id=partner_id;
                            delete k.name;  
                            delete k.username;
                            delete k.rowcount;
                            list.push(k);
                       // k.isDonateNowTx=k.?column?;
                        next(k);
                      }, function () {
                        responseData.total=total;  
                        responseData.list=list;                        
                        me.sendData(responseData);
                      });
                }
                else
                {
                    return me.sendError();
                }
            });
    }

});