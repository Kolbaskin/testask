Ext.define('Crm.REST.invoice.FileUploadController', {
    extend: "Crm.REST.RestBaseController",
    constructor: function () {
        this.callParent(arguments);
    },
    /*
   @author : Kaustubh
    @date : 09 Aug 2017
    @function : uploadFile
    @desc : upload multiple files 
    */
    uploadFile: function (cb) {
        var me = this;
        if (!me.user) {
            me.addErrorAndSend(401);
            return;
        }
        if (!me.user.pid) {
            me.addErrorAndSend('ERR033');
            return;
        }
        if (!me.params.files) {
            me.addErrorAndSend('ERR055');
            return;
        }
        var DocsModel = Ext.create('Crm.modules.file.model.FilesModel', { scope: me });
        var pgdUtils = Ext.create('Crm.REST.util.PgdUtils', { scope: me });
        if (!me.params.files) {
            return me.addErrorObj({
                code: "ERR001",
                message: 'File not attached'
            });
        }
        var _ids = [];
        var files = [];
        files = me.params.files;
        var groupId = me.Util.getObjectId();
        if (files.file.length > 1) {
            files.file.prepEach(function (e, next) {
                DocsModel.write({
                    _id: me.Util.getObjectId(),
                    file: e,
                    group_id: groupId,
                    active: true,
                    pid: me.user.pid,
                    date_add: new Date(),
                    date_fin: new Date(),
                    doc_name: 'User Identification'
                }, function (data) {
                    if (data.success) {
                        next(e);
                    } else {
                        next(e);
                        return me.addErrorObj(data.error);
                    }
                }, { add: true, modify: true })
            }, function () {
                pgdUtils.returnUrlArr(groupId, function (result) {
                    if (result) {
                        me.sendData(result);
                    }
                })
            });
        }
        else {
            DocsModel.write({
                _id: me.Util.getObjectId(),
                file: files.file,
                group_id: groupId,
                active: true,
                pid: me.user.pid,
                date_add: new Date(),
                date_fin: new Date(),
                doc_name: 'User Identification'
            }, function (data) {
                if (data.success) {
                    pgdUtils.returnUrlArr(groupId, function (result) {
                        if (result) {
                            me.sendData(result);
                        }
                    })
                } else {

                    return me.addErrorObj(data.error);
                }
            }, { add: true, modify: true })
        }
    }

    , getFilesUrl: function (docid, cb) {

        var me = this;
        var docId;
        if (!docid || docid == "get-url") {
            if (me.validateRequest([
                { type: 'STRING', field: "document_id", isRequired: true }
            ])) {
                docId = me.req.body.reqData.document_id;
            }
            else {
                return me.sendError();
            }
        } else {
            docId = docid;
        }

        var resp = [];

        var DocsModel = Ext.create('Crm.modules.file.model.FilesModel', {
            scope: me
        });

        DocsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };

        //var hostname = me.req.headers.host;
        var path = '/api/rest/refdata/download-file?id=';
        DocsModel.dbCollection.find({
            group_id: docId
        }, { _id: 1 }, function (err, d) {
            if (d && d.length > 0) {
                d.prepEach(function (e, next) {
                    var url = me.config.baseUrl + path + e._id;
                    resp.push(url);
                    next(e);
                }, function () {
                    if (cb) {
                        cb(resp);
                    } else {
                        me.sendData(resp);
                    }
                });

            } else {
                if (cb) {
                    cb();
                }
                else {
                    return me.addErrorAndSend('ERRO50', 'document_id');
                }
            }
        });
    }

    , fetchFileFormLocalStorage: function (cb) {

        var me = this;
        var fs = require('fs');
        var path = require('path');
        var DocsModel = Ext.create('Crm.modules.file.model.FilesModel', {
            scope: me
        });

        DocsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var params = me.req.body.reqData;
        [
            function (next) {
                DocsModel.dbCollection.findOne({ _id: me.params.gpc.id }, {}, function (err, doc) {
                    if (doc) {
                        next(doc)
                    } else {
                        me.response.writeHead(404);
                        me.response.write('Not found');
                        me.response.end();
                    }
                })
            },
            function (doc) {
                try {
                    var filePath = path.join('uploads', doc._id);
                    var stat = fs.statSync(filePath);
                    me.response.writeHead(200, {
                        'Content-Type': doc.doc_type || 'application/octet-stream',
                        'Content-Disposition': 'inline; filename="' + doc.file_name + '"',
                        'Content-Length': stat.size
                    });
                    var readStream = fs.createReadStream(filePath);
                    readStream.pipe(me.response);
                } catch (e) {
                    me.response.writeHead(404);
                    me.response.write('Not found');
                    me.response.end();
                }
            }

        ].runEach();
    },

     fetchuserimgFileFormLocalStorage: function (cb) {
        
                var me = this;
                var fs = require('fs');
                var path = require('path');
                var FoundModel = Ext.create('Crm.modules.organizations.model.FoundModel', {
                    scope: me
                });
        
                FoundModel.error = function (code, info) {
                    return me.addErrorObj(code);
                };
                var params = me.req.body.reqData;
                [
                    function (next) {
                        FoundModel.dbCollection.findOne({ _id: me.params.gpc.id }, {}, function (err, doc) {
                            if (doc) {
                                next(doc)
                            } else {
                                me.response.writeHead(404);
                                me.response.write('Not found');
                                me.response.end();
                            }
                        })
                    },
                    function (doc) {
                        try {
                            var filePath = path.join('uploads', doc._id);
                            var stat = fs.statSync(filePath);
                            me.response.writeHead(200, {
                                'Content-Type': doc.mime || 'application/octet-stream',
                                'Content-Disposition': 'inline; filename="' + doc.file_name + '"',
                                'Content-Length': stat.size
                            });
                            var readStream = fs.createReadStream(filePath);
                            readStream.pipe(me.response);
                        } catch (e) {
                            me.response.writeHead(404);
                            me.response.write('Not found');
                            me.response.end();
                        }
                    }
        
                ].runEach();
            },

    updateFiles: function (cb) {
        var me = this;
        var finalString = "";
        if (!me.user) {
            me.addErrorAndSend(401);
            return;
        }
        if (!me.user.pid) {
            me.addErrorAndSend('ERR033');
            return;
        }
        if (!me.params.files) {
            me.addErrorAndSend('ERR033');
            return;
        }
        var DocsModel = Ext.create('Crm.modules.file.model.FilesModel', { scope: me });
        if (!me.params.files) {
            return me.addErrorObj({
                code: "ERR001",
                message: 'File not attached'
            });
        }
        var _ids = [];
        var files = [];
        var CatalogueModel = Ext.create('Crm.modules.catalogue.model.CatalogueModel', {
            scope: me
        });
        CatalogueModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        files = me.params.files;
        var path = '/api/rest/refdata/download-file?id=';
        var groupId = me.params.gpc.groupId;
        var catalogueId = me.params.gpc.catalogueId;
        if (files && files.file && files.file.length > 1) {
            files.file.prepEach(function (e, next) {
                DocsModel.write({
                    _id: me.Util.getObjectId(),
                    file: e,
                    group_id: groupId,
                    active: true,
                    pid: me.user.pid,
                    date_add: new Date(),
                    date_fin: new Date(),
                    doc_name: 'User Identification'
                }, function (data) {
                    if (data.success) {
                        next(e);
                    } else {
                        next(e);
                        return me.addErrorObj(data.error);
                    }
                }, { add: true, modify: true })
            }, function () {
                DocsModel.dbCollection.find({
                    group_id: groupId
                }, { _id: 1 }, function (err, d) {
                    if (d && d.length > 0) {
                        finalString = "";
                        d.prepEach(function (k, next) {
                            var url = path + k._id;
                            if (finalString != "") {
                                finalString = finalString + "," + url;
                                next(k);
                            }
                            else {
                                finalString = finalString + url;
                                next(k);
                            }

                        }, function () {
                            var sql = "Update pgd_catalogue set catalogue_urls= '" + finalString + "' where _id= '" + catalogueId + "'";
                            me.src.db.query(sql, null, function (e, data) {
                                if (data) {
                                    var response = {};
                                    response.groupId = groupId;
                                    var urls = finalString.split(",");
                                    response.size = urls.length;
                                    response.urls = urls;
                                    me.sendData(response);
                                } else {
                                }
                            });
                        });
                    } else {
                        return me.addErrorAndSend('ERRO50', 'document_id');
                    }
                });

            });
        }
        else if (!files.file) {
            DocsModel.dbCollection.find({
                group_id: groupId
            }, { _id: 1 }, function (err, d) {
                if (d && d.length > 0) {
                    finalString = "";
                    d.prepEach(function (e, next) {
                        var url = path + e._id;
                        if (finalString != "") {
                            finalString = finalString + "," + url;
                        }
                        else {
                            finalString = finalString + url;
                        }
                        next(e);
                    }, function () {
                        var sql = "Update pgd_catalogue set catalogue_urls= '" + finalString + "',document_id= '" + groupId + "' where _id= '" + catalogueId + "'";
                        me.src.db.query(sql, null, function (e, data) {
                            if (data) {
                                var response = {};
                                response.groupId = groupId;
                                var urls = finalString.split(",");
                                response.size = urls.length;
                                response.urls = urls;
                                me.sendData(response);
                            } else {
                                var response = {};
                                response.groupId = groupId;
                                var urls = finalString.split(",");
                                response.size = urls.length;
                                response.urls = urls;
                                me.sendData(response);
                            }

                        });
                    });
                } else {
                    return me.addErrorAndSend('ERRO50', 'document_id');
                }
            });
        }
        else {
            DocsModel.write({
                _id: me.Util.getObjectId(),
                file: files.file,
                group_id: groupId,
                active: true,
                pid: me.user.pid,
                date_add: new Date(),
                date_fin: new Date(),
                doc_name: 'User Identification'
            }, function (data) {
                if (data.success) {
                    DocsModel.dbCollection.find({
                        group_id: data.record.group_id
                    }, { _id: 1 }, function (err, d) {
                        if (d && d.length > 0) {
                            finalString = "";
                            d.prepEach(function (e, next) {
                                var url = path + e._id;
                                if (finalString != "") {
                                    finalString = finalString + "," + url;
                                }
                                else {
                                    finalString = finalString + url;
                                }
                                next(e);
                            }, function () {
                                var sql = "Update pgd_catalogue set catalogue_urls= '" + finalString + "'  where _id= '" + catalogueId + "'";
                                me.src.db.query(sql, null, function (e, data) {
                                    if (data) {
                                        //currently update query returns nothing
                                        var response = {};
                                        response.groupId = groupId;
                                        var urls = finalString.split(",");
                                        response.size = urls.length;
                                        response.urls = urls;
                                        me.sendData(response);
                                    } else {
                                        var response = {};
                                        response.groupId = groupId;
                                        var urls = finalString.split(",");
                                        response.size = urls.length;
                                        response.urls = urls;
                                        me.sendData(response);
                                    }
                                });
                            });
                        } else {
                            return me.addErrorAndSend('ERRO50', 'document_id');
                        }
                    });
                } else {
                    return me.addErrorObj(data.error);
                }
            }, { add: true, modify: true })
        }
    }

    , makeCharityHtmlPage: function (cb) {

        var me = this;
        var fs = require('fs');
        var path = require('path');
        if (!me.validateRequest([
            {
                type: 'STRING',
                field: "html",
                isRequired: true,
            }
        ])) {
            return me.sendError();
        }
        var DocsModel = Ext.create('Crm.modules.file.model.FilesModel', {
            scope: me
        });

        DocsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };

        inParams = me.req.body.reqData;
        var fileName = me.Util.getObjectId();

        var path = '/api/rest/refdata/charity-page?id=';
        var FoundModel = Ext.create('Crm.modules.organizations.model.FoundModel', {
            scope: this
        })
        FoundModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var DocsModel = Ext.create('Crm.modules.file.model.FilesModel', {
            scope: me
        });
        DocsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        //  var url = me.config.baseUrl + path + data._id;
        DocsModel.write({
            "_id": me.Util.getObjectId(),
            "pid": me.user.pid || user.pid,
            "doc_type": "text/html",
            "doc_name": fileName,
            "file_name": fileName,
            "group_id": fileName
        },
            function (data) {
                if (data.success) {
                    FoundModel.dbCollection.update({
                        _id: me.user.pid
                    }, {
                            $set: {
                                "url": path + data.record._id
                            }
                        }, function (e, d) {
                            if (d) {
                                if (inParams.html && inParams.html != null && inParams.html != "") {
                                    fs.writeFile("uploads/" + data.record._id + ".html", inParams.html, function (err) {
                                        if (err) {
                                            return console.log(err);
                                        }
                                        me.sendData("The file was saved!");
                                    });
                                }
                                else {
                                    me.addErrorAndSend('ERR033');
                                }

                                //    me.sendData({});
                            }
                            else {
                                return me.addErrorAndSend('ERR042');
                            }
                        });
                } else {
                    return me.addErrorObj(data.error);
                }
            }, {
                add: 1,
                modify: 1
            })
    },
    updatePartnerImages: function (cb) {
        var me = this;
        var finalString = "";
        if (!me.user) {
            me.addErrorAndSend(401);
            return;
        }
        if (!me.user.pid) {
            me.addErrorAndSend('ERR033');
            return;
        }
        if (!me.params.files) {
            me.addErrorAndSend('ERR033');
            return;
        }
        var DocsModel = Ext.create('Crm.modules.file.model.FilesModel', { scope: me });

        var FoundModel = Ext.create('Crm.modules.organizations.model.FoundModel', {
            scope: this
        });
        FoundModel.error = function (code, info) {
            return me.addErrorObj(code);
        };

        if (!me.params.files) {
            return me.addErrorObj({
                code: "ERR001",
                message: 'File not attached'
            });
        }

        var _ids = [];
        var files = [];

        files = me.params.files;
        var path = '/api/rest/refdata/download-file?id=';
        var groupId = me.params.gpc.groupId;
        var foundId = me.user.pid;
        if (files && files.file && files.file.length > 1) {
            files.file.prepEach(function (e, next) {
                DocsModel.write({
                    _id: me.Util.getObjectId(),
                    file: e,
                    group_id: groupId,
                    active: true,
                    pid: me.user.pid,
                    date_add: new Date(),
                    date_fin: new Date(),
                    doc_name: 'User Identification'
                }, function (data) {
                    if (data.success) {
                        next(e);
                    } else {
                        next(e);
                        return me.addErrorObj(data.error);
                    }
                }, { add: true, modify: true })
            }, function () {
                DocsModel.dbCollection.find({
                    group_id: groupId
                }, { _id: 1 }, function (err, d) {
                    if (d && d.length > 0) {
                        finalString = "";
                        d.prepEach(function (k, next) {
                            var url = path + k._id;
                            if (finalString != "") {
                                finalString = finalString + "," + url;
                                next(k);
                            }
                            else {
                                finalString = finalString + url;
                                next(k);
                            }
                        }, function () {
                            var sql = "Update pgd_foundations set media_urls= '" + finalString + "',document_id= '" + groupId + "' where _id= '" + foundId + "'";
                            me.src.db.query(sql, null, function (e, data) {
                                if (data) {
                                    var response = {};
                                    response.groupId = groupId;
                                    var urls = finalString.split(",");
                                    response.size = urls.length;
                                    response.urls = urls;
                                    me.sendData(response);
                                } else {
                                }
                            });
                        });
                    } else {
                        return me.addErrorAndSend('ERRO50', 'document_id');
                    }
                });
            });
        }
        else if (!files.file) {
            DocsModel.dbCollection.find({
                group_id: groupId
            }, { _id: 1 }, function (err, d) {
                if (d && d.length > 0) {
                    finalString = "";
                    d.prepEach(function (e, next) {
                        var url = path + e._id;
                        if (finalString != "") {
                            finalString = finalString + "," + url;
                        }
                        else {
                            finalString = finalString + url;
                        }
                        next(e);
                    }, function () {
                        var sql = "Update pgd_foundations set media_urls= '" + finalString + "',document_id= '" + groupId + "' where _id= '" + foundId + "'";
                        me.src.db.query(sql, null, function (e, data) {
                            if (data) {
                                var response = {};
                                response.groupId = groupId;
                                var urls = finalString.split(",");
                                response.size = urls.length;
                                response.urls = urls;
                                me.sendData(response);
                            } else {
                                var response = {};
                                response.groupId = groupId;
                                var urls = finalString.split(",");
                                response.size = urls.length;
                                response.urls = urls;
                                me.sendData(response);
                            }

                        });
                    });
                } else {
                    return me.addErrorAndSend('ERRO50', 'document_id');
                }
            });
        }
        else {
            DocsModel.write({
                _id: me.Util.getObjectId(),
                file: files.file,
                group_id: groupId,
                active: true,
                pid: me.user.pid,
                date_add: new Date(),
                date_fin: new Date(),
                doc_name: 'User Identification'
            }, function (data) {
                if (data.success) {
                    DocsModel.dbCollection.find({
                        group_id: data.record.group_id
                    }, { _id: 1 }, function (err, d) {
                        if (d && d.length > 0) {
                            finalString = "";
                            d.prepEach(function (e, next) {
                                var url = path + e._id;
                                if (finalString != "") {
                                    finalString = finalString + "," + url;
                                }
                                else {
                                    finalString = finalString + url;
                                }
                                next(e);
                            }, function () {
                                var sql = "Update pgd_foundations set media_urls= '" + finalString + "'  where _id= '" + foundId + "'";
                                me.src.db.query(sql, null, function (e, data) {
                                    if (data) {
                                        //currently update query returns nothing
                                        var response = {};
                                        response.groupId = groupId;
                                        var urls = finalString.split(",");
                                        response.size = urls.length;
                                        response.urls = urls;
                                        me.sendData(response);
                                    } else {
                                        var response = {};
                                        response.groupId = groupId;
                                        var urls = finalString.split(",");
                                        response.size = urls.length;
                                        response.urls = urls;
                                        me.sendData(response);
                                    }
                                });
                            });
                        } else {
                            return me.addErrorAndSend('ERRO50', 'document_id');
                        }
                    });
                } else {
                    return me.addErrorObj(data.error);
                }
            }, { add: true, modify: true })
        }
    },

    //delete file
    removeFiles: function () {
        var me = this;
        var _ids = [];
        var files = [];
        params = me.req.body.reqData;
        var pgdUtils = Ext.create('Crm.REST.util.PgdUtils', { scope: me });        
        var serverpath = '/api/rest/refdata/download-file?id=';
        var id = params.urls.replace(serverpath, "");
        var groupId;
        var DocsModel = Ext.create('Crm.modules.file.model.FilesModel', { scope: me });
        [
            //delete file first
            function (next) {
                var me = this;
                var fs = require('fs');
                var path = require('path');
                fs.unlink('uploads/' + id, function () { next() })
            },
            function (next) {
                DocsModel.dbCollection.findOne({
                    _id: id
                }, { group_id: 1 }, function (err, d) {
                    if (d && d.group_id) {
                        groupId = d.group_id;
                        next();
                    }
                    else {
                        return me.addErrorAndSend('ERRO50', '_id');
                    }
                });
            },
            function (next) {
                DocsModel.removeAction = 'delete';
                var ids = [];
                ids.push(id);
                DocsModel.remove(ids, function (inres) {
                    if (inres.success == true) {
                        next();
                    }
                    else {
                        return me.addErrorAndSend('ERR##', 'File deletion operation failed,please contact Admin');
                    }
                });
            },
            function (next) {
                if (groupId != undefined && groupId != "") {
                    pgdUtils.returnUrlArr(groupId, function (result) {
                        if (result) {
                            me.sendData(result);
                        }
                    })
                }
            }
        ].runEach();
    }
});