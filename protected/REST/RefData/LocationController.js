Ext.define('Crm.REST.RefData.LocationController', {
    extend: "Crm.REST.RestBaseController",
    constructor: function () {
        this.callParent(arguments);
    },
    /*
    @author : Vaibhav Vaidya
    @date : 23 June 2017
    @function : getCountryDetails
    @desc : retrieves Country List
    */
    apiGetCountryDetails: function (cb) {
        var me = this;
        var sql = "select iso, countrylabel from pgd_countries";
        me.src.db.query(sql,null, function (e, data) {
            if (data) {
                me.sendData(data);
            } else {
                me.sendError();
            }
        });
    },
    /*
    @author : Vaibhav Vaidya
    @date : 23 June 2017
    @function : getStateDetails
    @desc : retrieves State List
    */
    apiGetStateDetails: function (cb) {
        var me = this;
        var reqBody = me.req.body.reqData;
        var sql = "select stateid, countryid,statelabel from pgd_provinces where countryid= $1";
        if (me.validateRequest([{
            type: 'STRING',
            field: "countryid",
            isRequired: true
        }]))
        {
            if (reqBody.countryid) {
                me.src.db.query(sql, [reqBody.countryid], function (e, data) {
                    if (data) {
                        me.sendData(data);
                    } else {
                        me.sendError();
                    }
                });
            }
        }
    },
    /*
    @author : Vaibhav Vaidya
    @date : 23 June 2017
    @function : getCityDetails
    @desc : retrieves City List
    */
    apiGetCityDetails: function (cb) {
        var me = this;
        var reqBody = me.req.body.reqData;
        var sql = "select cityid, stateid, citylabel from pgd_districts where stateid= $1";
        if (me.validateRequest([{
            type: 'STRING',
            field: "stateid",
            isRequired: true
        }]))
        {
            if (reqBody.stateid) {
                me.src.db.query(sql, [reqBody.stateid], function (e, data) {
                    if (data) {
                        me.sendData(data);
                    } else {
                        me.sendError();
                    }
                });
            }
        }
    }
});