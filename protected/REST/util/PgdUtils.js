Ext.define('Crm.REST.util.PgdUtils', {
    extend: "Crm.REST.RestBaseController",
    constructor: function () {
        this.callParent(arguments);
        this.truncateReqArr();        
        this.initreqArr();
    },
    //kt 23/09/2017
    //start avoiding duplicate request
    //dont do any changes
    initreqArr: function () {
        var me = this;
        if (me.config.reqArr == undefined) {
            me.config.reqArr = {};
            return me.config.reqArr;
        }
        else {
            return me.config.reqArr;
        }
    },
    checkDuplicateRequest: function (nonce) {
        var me = this;
        var arr = me.initreqArr();
        if (nonce in arr) {
            return true;
        }
        else {
            me.maintainReqStatus(nonce,me.config.statusProcessing);
            return false;
        }
    },
    maintainReqStatus:function (nonce,data) {
        var me = this;
        if(me.config.reqArr[nonce]==undefined)
        {
            me.config.reqArr[nonce]={};
            var status=data;
            me.config.reqArr[nonce].status = status;
        }
        else
        {
            me.config.reqArr[nonce].status = data;
        }
    },
    getReqStatusFromNonce: function (nonce) {
        var me = this;
        if(me.config.reqArr[nonce])
        {
            var data = me.config.reqArr[nonce].status;
            return data;
        }
        else
        {
            var data={};
            data.error='Invalid nonce';
            return data;
        }
    },
    maintainNonceReqAndRes: function (nonce,data) {
        var me = this;
        resData=data;
        me.config.reqArr[nonce].data=resData;
    },
    getReqResFromNonce: function (nonce) {
        var me = this;
        if(me.config.reqArr[nonce])
        {
            var data = me.config.reqArr[nonce].data;
            return data;
        }
        else
        {
            var data={};
            data.error='Invalid nonce';
            return data;
        }
    },
    truncateReqArr: function () {
        var me = this;
        var ONE_MINUTE = 60 * 1000;
        
        function showTime() {
          //console.log(new Date());
        }
        
        setInterval(showTime, ONE_MINUTE);
        function repeatEvery(func, interval) {
            // Check current time and calculate the delay until next interval
            var now = new Date(),
                delay = interval - now % interval;
        
            function start() {
                // Execute function now...
                me.config.reqArr = {};
                // ... and every interval
                setInterval(func, interval);
            }
        
            // Delay execution until it's an even interval
            setTimeout(start, delay);
        }
        
        repeatEvery(showTime, ONE_MINUTE);
    },

    //end --

    //split string into arrat
    splitStringtoArr: function (k) {
        if (typeof (k) == "string")
            k = k.split(",");
        return k;
    },
    //returns array of urls from docId
    returnUrlArr: function (k, cb) {
        var me = this;
        var response = {};

        var docId = k;
        var resp = [];
        var DocsModel = Ext.create('Crm.modules.file.model.FilesModel', {
            scope: me
        });
        DocsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        //var hostname = me.req.headers.host;
        var path = '/api/rest/refdata/download-file?id=';
        var url = "";
        DocsModel.dbCollection.find({
            group_id: docId
        }, { _id: 1 }, function (err, d) {
            if (d && d.length > 0) {
                response.groupId = docId;
                d.prepEach(function (e, next) {
                    url = path + e._id;
                    resp.push(url);
                    next(e);
                }, function () {
                    response.size=resp.length;
                    response.url = resp;
                    if (cb) {
                        cb(response);
                    }
                    else {
                        return response;
                    }
                });
            } else {
                response.groupId = docId;
                if (cb) {
                    response.url = resp;
                    response.size=resp.length;
                    cb(response);
                }
                else {
                    return response;
                }
            }
        });
    },
    //returns comma seperated string from docId
    returnUrlString: function (k, cb) {
        var me = this;
        var response = {};

        var docId = k;
        var DocsModel = Ext.create('Crm.modules.file.model.FilesModel', {
            scope: me
        });

        DocsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        //var hostname = me.req.headers.host;
        var path = '/api/rest/refdata/download-file?id=';
        var url = "";
        DocsModel.dbCollection.find({
            group_id: docId
        }, { _id: 1 }, function (err, d) {
            if (d && d.length > 0) {
                response.groupId = docId;
                d.prepEach(function (e, next) {
                    if (url == "") {
                        url = path + e._id;
                    }
                    else {
                        url = url + "," + path + e._id
                    }
                    next(e);
                }, function () {
                    if (cb) {
                        cb(url);
                    }
                    else {
                        return url;
                    }
                });
            } else {
                if (cb) {
                    cb(me.addErrorAndSend('ERRO50', 'document_id'));
                }
                else {
                    return url;
                }
            }
        });
    }
}); 