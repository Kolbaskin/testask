Ext.define('Crm.REST.auth.user', {
    extend: "Crm.REST.RestBaseController",
    constructor: function () {
        this.callParent(arguments);
        this.UserVerifier = Ext.create('Crm.Utils.UserVerifier', {
            scope: this
        });
    },


    apiLogin: function () {
        var me = this;
        var reqBody = me.req.body.reqData;
        [
            function (next) {
                me.basicAuth('', false, next);

            },
            function (err, client) {
                if (client) {
                    if (me.validateRequest([{
                        type: 'STRING',
                        field: "username",
                        isRequired: true
                    },
                    {
                        type: 'STRING',
                        field: "password",
                        isRequired: true
                    },
                    ])) {
                        me.callModel('Admin.model.User.getAutorization', {
                            collection: 'users',
                            find: {
                                username: reqBody.username
                            },
                            password: reqBody.password,
                            passField: 'password'
                        }, function (user, err) {                           
                            if (user) {
                                
                                me.req.grantType = 'pin';
                                me.req.user = user.user;
                                me.req.user.id = user.id;
                                me.oauthserver.nativeGrant()(me.req, me.res, function (err, tokens) {
                                    if (tokens) {
                                        tokens.username = user.user.username;
                                        tokens.full_name = user.user.full_name;
                                        me.sendData(tokens);
                                    } else {
                                        return me.addErrorObj(err);
                                    }
                                });
                                            
                                
                            } else {                               
                                return me.addErrorAndSend('ERR017', 'tel');
                            }
                        });

                    } else {
                      
                        me.sendError();
                    }
                } else {                    
                    return me.addErrorObj(err);
                }
            },
        ].runEach();
    },
    

    apiGetProfile: function () {

        this.sendData(this.user);
        
        
    },

});
