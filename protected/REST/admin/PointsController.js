
Ext.define('Crm.REST.admin.PointsController', {
    extend: "Crm.REST.RestBaseController"
    
    ,list: function() {
        var params = this.req.body.reqData;
        
        var me = this, error = function(code) {
            me.sendError(401);
        }
        
        if(!this.user) {
            error(401)
            return;
        }
        
        var model = Ext.create('Crm.modules.points.model.CharityPointsModel', {scope: this});

        var flt = model.getFilterByName(params, 'catalogueid');
         
        if(!flt) {
            error(401);
            return;
        }
        
        [
            function(next) {
                this.src.db.collection('pgd_catalogue').findOne({_id: flt.value}, {pid: 1}, function(e,d) {
                    if(d)
                        next(d)
                    else
                        error(401)
                })
            },
            function(item, next) {
                if(this.user.pid == item.pid)
                    next()
                else
                    error(401)
            },
            function(next) {
                model.getData(params, next)
            },
            function(res) {
                this.sendData(res);
            }
        ].runEach(this)
   
    }
    
});
