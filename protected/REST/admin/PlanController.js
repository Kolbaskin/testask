Ext.define('Crm.REST.admin.PlanController', {
    extend: "Crm.REST.RestBaseController",
    constructor: function () {
        this.callParent(arguments);
        //this.initModels();
        this.params = this.req.body.reqData;
        if(!this.params.filters) {
            this.params.filters = [];
        }
        if(this.user)
            this.user.name = this.user.full_name || this.user.username;
    },
    
        
    list: function () {
        var me = this;
        if (!this.user) {
            this.error(401);
            return;
        }

        this.params.filters.push({
            property: 'partner_user_id',
            value: this.user.pid
        })

        Ext.create('Crm.modules.charityPlan.model.CharityPlanModel', {scope: this}).getDataAndCheckSign(this.params, function(res) {             
            me.sendData(res);          
        }) 
    },

    write: function () {
        var me = this;
        if (!this.user) {
            this.error(401);
            return;
        }
        this.params.partner_user_id = this.user.pid;

        var m = Ext.create('Crm.modules.charityPlan.model.CharityPlanModel', {scope: this});
        m.user = {
            id: this.user._id,
            profile: this.user
        }
        m.write(me.params, function (res) {            
            me.sendData(res);
        }, { add: true, modify: true });

    },
    
    remove: function() {
        var me = this;
        var model = Ext.create('Crm.modules.charityPlan.model.CharityPlanModel', {scope: this});
        if (!this.user) {
            this.error(401);
            return;
        };
        [
            function(next) {              
                model.dbCollection.find({_id: {$in: this.params.records}, active: {$ne: 1}}, {_id: 1, partner_user_id:1}) 
                .toArray(next)
            }
            ,function(e,d, next) {
                if(e)
                    this.error(500)
                else {
                    var ids = [];
                    d.forEach(function(d) {
                        if(d.partner_user_id == me.user.pid) {
                            ids.push(d._id)
                        }
                    })
                    if(ids.length)
                        next(ids)
                    else
                        this.error(401)
                }
            }
            ,function(ids) {
                model.user = {
                    id: this.user._id,
                    profile: this.user
                }
                model.remove(ids, function (res) {                  
                    me.sendData(res);
                });
            }
        ].runEach(this);  
        
    },
    
    listDetails: function () {
        if (!this.user) {
            this.error(401);
            return;
        }
        ;[        
            function(next) {
                var pid = this.getFiltersPid();
                if(!pid) 
                    this.sendData({list: [], total: 0});
                else
                    this.checkPid(pid, next)
            }            
            ,function(ok, next) {  
                if(ok)
                    Ext.create('Crm.modules.charityPlan.model.CharityPlanDetailsModel', {scope: this}).getDataAndCheckSign(this.params, next)
                else
                    next({list: [], total: 0});
            }
            ,function(res) {
                this.sendData(res);
            }
        ].runEach(this);
    }
    
    ,getFiltersPid: function() {
        if(!this.params._filters) {
            return false;
        }
        for(var i=0;i<this.params._filters.length;i++) {
            if(this.params._filters[i]._property == 'charity_plan_id') {
                this.params._filters[i]._operator = 'eq';
                return this.params._filters[i]._value;
            }
        }
        return false;        
    }
    
    ,checkPid: function(pid, cb) {
        var me = this;
        this.src.db.collection('charity_plan').findOne({_id: pid}, {partner_user_id:1},function(e,d) {
            cb(d && d.partner_user_id == me.user.pid)
        })

    }

    ,writeDetails: function () {
        var me = this;
        if (!this.user) {
            this.error(401);
            return;
        }
        //this.params.pid = this.user.pid;
        
       Ext.create('Crm.modules.charityPlan.model.CharityPlanDetailsModel', {scope: this}).write(me.params, function (res) {
            me.sendData(res);
        }, { add: true, modify: true });

    }

})