Ext.define('Crm.REST.admin.ReportsController', {
    extend: "Crm.REST.RestBaseController"
    
    ,error: function(code) {
        this.sendData({})
    }
    
    ,reportCharity: function() {
        var params = this.req.body.reqData;
        [
            function(next) {this.src.db.query([
                    'SELECT c.*, f.name as org_name FROM report_payments_cache c, reports r, pgd_foundations f',
                    'WHERE c.ngo_id = $1 and f._id = r.pid and c.pid = r._id',
                    'ORDER BY c.status'                    
                ].join(' '),[this.user.pid], function(e,r) {
                    next(r)
                })
            },
            function(out, next) {
                if(!out) {
                    this.error(404);
                    return;
                }                
                this.sendData({total: out.length, list: out});
            }            
        ].runEach(this);
    }
    
    ,reportCharityMarkAccept: function() {
        var me = this, params = this.req.body.reqData;
        var error = function(e) {
            me.sendError(e);
        }
        
        if(!this.user)
            return this.sendError(401)
        
        ;[
            function(next) {  
            
                this.src.db.query('select r.pid, c.amount, c.ngo_id, c._id from reports r, report_payments_cache c where c._id = $1 and c.ngo_id = $2 and c.pid = r._id', [params._id, this.user.pid], function(e,d) {
                    next(e,d)                        
                });
            }    
            ,function(e, data, next) { 
                if(e || !data || data.length != 1) {
                    return error(e || 404)
                }
                data = data[0];
                this.src.db.query("UPDATE " + _TABPREFIX + "foundations SET total1 = total1 + " +data.amount + ", total2 = total2 - " + data.amount + " WHERE _id = $1", [data.pid], function(e, d) {
                    next(e,data)
                })                
            }
            ,function(e, data, next) { 
                if(e) {
                    return error(e)
                }
                this.src.db.query("UPDATE " + _TABPREFIX + "foundations SET total1 = total1 + " +data.amount + " WHERE _id = $1", [data.ngo_id], function(e, d) {
                    next(e, data)
                })
            }
            ,function(e, data, next) { 
                if(e) {
                    return error(e)
                }
                this.src.db.query('UPDATE report_payments_cache SET status=2 WHERE _id=$1', [data._id], function(e,d) {           
                    me.sendData({success: !e})
                })
            }
        ].runEach(this)
        
    }
    
    ,reportPaymentMarkPay: function() {
        var me = this, params = this.req.body.reqData;
        
        if(!this.user)
            return this.error(401)
        
        this.src.db.query('UPDATE report_payments_cache SET status=1 WHERE _id=$1 and pid in (SELECT _id FROM reports WHERE pid=$2)', [params._id, this.user.pid], function(e,d) {
            me.sendData({success: !e})
        })
        
    }
    
    ,reportPayment: function() {
        var params = this.req.body.reqData;
        [
            function(next) {
                this.getReportSettings(next)
            },
            function(report, next) {

                this.src.db.query([
                    'SELECT c.*, f.name as ngo_name FROM report_payments_cache c, pgd_foundations f',
                    'WHERE c.pid = $1 and c.ngo_id = f._id',
                    'ORDER BY f.name'                    
                ].join(' '),[report._id], function(e,r) {
                    next(r, report)
                })
            },
            function(out, report, next) {
                if(!out) {
                    this.error(404);
                    return;
                }
                
                this.sendData({total: out.length, list: out});
            }            
        ].runEach(this);     
    }
    
    ,getReportSettings: function(cb) {
        var params = this.req.body.reqData;
        if(params && params._filters && params._filters[0] && params._filters[0]._property == 'report') {
            
            var report = params._filters[0]._value;
        }
        else {
            this.error(404);
            return;
        }
        var me = this
            , m = Ext.create('Crm.modules.reports.model.ReportsModel', {scope: this});        
        
        m.find = {_id: report, pid: me.user.pid};                
        m.getData({},function(res) {
            if(res && res.list && res.list[0])
                cb(res.list[0])
            else
                me.error(404)
        })
    }
    
    
    ,reportPartner: function() {
        var params = this.req.body.reqData;
        [
            function(next) {
                this.getReportSettings(next)
            },
            function(report, next) {
                this.src.db.query([
                    'SELECT f.name as org_name, sum(d.tx) as tx, sum(d.amount) as amount FROM report_partner_data d, pgd_foundations f',
                    'WHERE d.pid = $1 and d.partnerid = f._id',
                    'GROUP BY f.name'                    
                ].join(' '),[report._id], function(e,r) {
                    next(r)
                })
            },
            function(data) {
                this.sendData({total: data.length, list: data});
            }            
        ].runEach(this);     
    }
    
    ,reportPaymentList: function() {  
        
        var me = this, m = Ext.create('Crm.modules.reports.model.ReportsModel', {scope: this});        
        m.find = {report_type: 'brand_donates', pid: this.user.pid}
        m.getData({},function(res) {
            me.sendData(res);
        })

    }
    
    ,reportPartnersList: function() {
        var me = this, m = Ext.create('Crm.modules.reports.model.ReportsModel', {scope: this});        
        m.find = {report_type: 'brand_partners', pid: this.user.pid}
        m.getData({},function(res) {
            me.sendData(res);
        })
    }
    
    
    ,purchases: function() {
        if(!this.user) {
            this.error(404);
            return;
        }
        
        this.formatDate.months = this.config.monthsNames;
        
        var me = this,
            params = this.req.body.reqData,
            date1, // = new Date(Ext.Date.format(new Date(), 'Y-m') + '-01'),
            date2, 
            format = 'MMDD',
            label = 'days',
            and = [],
            vals = [this.user.pid];
        
        
        
        if(params._filters) {
            params._filters.forEach(function(f) {
                if(f._property == 'date' && f._value) {
                    if(f._operator == 'lt') date2 = new Date(f._value)
                    else date1 = new Date(f._value);
                } else
                if(f._property == 'label') {
                    label = f._value;
                }            
            })
        }
        
        if(label == 'days') {
            format = 'MMDD'; 
            if(date1) date1 = new Date(Ext.Date.format(date1, 'Y-m-d'))
            if(date2) date2 = Ext.Date.parse(Ext.Date.format(date2, 'Y-m-d') + ' 23:59', 'Y-m-d H:i')    
        } else 
        if(label == 'day') {
            format = 'HH'; 
            if(!date1) date1 = new Date()
            date1 = Ext.Date.parse(Ext.Date.format(date1, 'Y-m-d') + ' 00:00', 'Y-m-d H:i')
            date2 = Ext.Date.parse(Ext.Date.format(date1, 'Y-m-d') + ' 23:59', 'Y-m-d H:i')
        } else 
        if(label == 'week') {
            format = 'MMDD'; 
            if(!date1) date1 = new Date()
                
            var w = Ext.Date.format(date1, 'w') - 1;
            if(w<0) w = 7;
            
            date1 = new Date(Ext.Date.format(new Date(date1.getTime - w * 86400000), 'Y-m-d'));
            if(date2) date2 = Ext.Date.parse(Ext.Date.format(date2, 'Y-m-d') + ' 23:59', 'Y-m-d H:i')
        } else 
        if(label == 'month') {
            format = 'MMDD'; 
            if(!date1) date1 = new Date();           
            
            date1 = new Date(Ext.Date.format(new Date(), 'Y-m') + '-01');
            if(date2) date2 = Ext.Date.parse(Ext.Date.format(date2, 'Y-m-d') + ' 23:59', 'Y-m-d H:i')
        } 
    
        if(date1) {
            vals.push(date1)
            and.push('ctime >= $' + vals.length)
            
        }
        if(date2) {
            vals.push(date2)
            and.push('ctime <= $' + vals.length)
        }
        
        var sql = "select count(*) as val, to_char(ctime,'" +format+ "') as dt from user_points WHERE partnerid=$1 " +(and.length? " and " + and.join(" and "):"")+ " group by to_char(ctime,'" +format+ "') ";

        this.src.db.query(sql, vals, function(e, res) {            
            if(res) {
                res.sort(function(a,b) {return a.dt>b.dt})
                res.each(function(r) {
                    r.date = me.formatDate[label](r.dt)
                    return r
                },true)
                me.sendData({total: res.length, list: res})
            } else
                me.sendData({total: 0, list: []})
            
        });

    }
    
    ,formatDate: {
        months: ['', 'jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'],
        days: function(dt) {
            return dt.substr(2,2) + ' ' + this.months[parseInt(dt.substr(0,2))]
        },
        day: function(dt) {
            return dt;
        },
        week:function(dt) {
            return dt.substr(2,2) + ' ' + this.months[parseInt(dt.substr(0,2))]
        },        
        month: function(dt) {
            return dt.substr(2,2) + ' ' + this.months[parseInt(dt.substr(0,2))]
        }
    }
    
})