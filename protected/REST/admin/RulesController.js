Ext.define('Crm.REST.admin.RulesController', {
    extend: "Crm.REST.RestBaseController",
    
    constructor: function () {
        this.callParent(arguments);
        //this.initModels();
        this.params = this.req.body.reqData;
        if(!this.params.filters) {
            this.params.filters = [];
        }
        if(this.user)
            this.user.name = this.user.full_name || this.user.username;
    },
    
        
    list: function () {
        var me = this;
        if (!this.user) {
            this.error(401);
            return;
        }

        this.params.filters.push({
            property: 'pid',
            value: this.user.pid
        });
        [
            function(next) {
                Ext.create('Crm.modules.rules.model.RulesModel', {scope: this}).getDataAndCheckSign(this.params, function(res) {           
                    next(res)          
                }) 
            },
            function(res, next) {
                if(res && res.list && res.list.length == 1) {
                    this.src.db.collection(_TABPREFIX + 'rulessku').find({pid: res.list[0]._id},  {})
                    .toArray(function(e,d) {
                        if(d)
                            res.list[0].items = d;
                        next(res)
                    })
                } else 
                    next(res)
            },
            function(res) {
                me.sendData(res);
            }
        ].runEach(this)
    },

    write: function () {
        var me = this;
        if (!this.user) {
            this.error(401);
            return;
        }
        this.params.pid = this.user.pid;
        
        var m = Ext.create('Crm.modules.rules.model.RulesModel', {scope: this});
        
        m.user = {
            id: this.user._id,
            profile: this.user
        }
        
        ;[
            function(next) {
                m.write(me.params, function (res) {
                    next(res)
                }, { add: true, modify: true });
            },
            function(res, next) {
                if(this.params.items) {
                    this.changeSkuItems(res.record._id, this.params.items, function() {
                        next(res)
                    })
                } else
                    next(res)
            },
            function(res, next) {
                me.sendData(res);
            }
        ].runEach(this)

    },
    
    changeSkuItems: function(pid, items, cb) {
        ;[
            function(next) {
                this.src.db.collection(_TABPREFIX + 'rulessku').remove({pid: pid},  {multi: true}, function(e,d) {
                    next()
                })
            },
            function() {
                var me = this;
                items.prepEach(
                    function(item, next) {
                        me.src.db.collection(_TABPREFIX + 'rulessku').insert({
                            pid: pid,
                            ruletype: item.ruletype,
                            sku: item.sku,
                            name: item.name,
                            rangefrom: item.rangefrom,
                            rangeto: item.rangeto,
                            amount: item.amount,
                            pointype: item.pointype
                        
                        }, function(e,d) {
                            next()
                        })
                    },
                    function() {
                        cb()
                    }
                 )                    
            }
        ].runEach(this)
    },
    
    remove: function() {
        var me = this;
        var model = Ext.create('Crm.modules.rules.model.RulesModel', {scope: this});
        if (!this.user) {
            this.error(401);
            return;
        };
        [
            function(next) {              
                model.dbCollection.find({_id: {$in: this.params.records}, active: {$ne:1}}, {_id: 1, pid:1}) 
                .toArray(next)
            }
            ,function(e,d, next) {
                if(e)
                    this.error(500)
                else {
                    var ids = [];
                    d.forEach(function(d) {
                        if(d.pid == me.user.pid) {
                            ids.push(d._id)
                        }
                    })
                    if(ids.length)
                        next(ids)
                    else
                        this.error(401)
                }
            }
            ,function(ids) {
                model.user = {
                    id: this.user._id,
                    profile: this.user
                }
                model.remove(ids, function (res) {                  
                    me.sendData(res);
                });
            }
        ].runEach(this);  
        
    }
    

})