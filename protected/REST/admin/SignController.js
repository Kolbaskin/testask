

Ext.define('Crm.REST.admin.SignController', {
    extend: "Crm.REST.RestBaseController"
    
    ,constructor: function () {
        this.callParent(arguments);
        this.params = this.req.body.reqData;
    }
    
    ,doSign: function(action) {
        if (!this.user) {
            this.error(401);
            return;
        }

        var me = this
            ,model = Ext.create(this.params.model, {scope: this})
        
        this.user.name = this.user.full_name || this.user.username;
        
        model.user = {
            id: this.user._id,
            profile: this.user
        }         
        
        if(!!model[action]) {
            model[action](this.params, function(res) {
                me.sendData(res);
            })
        } else {
            model['$' + action](this.params, function(res) {
                me.sendData(res);
            })
        }
    }
    
    ,sign: function() {        
        this.doSign('signRecord');   
       
    }
    
    ,unsign: function() {

        this.doSign('unSignRecord');
    }
    
    /*
    me.params.active = (trusted == 2);
            //me.CatalogueModel.autoSign = !(trusted == 2);
            me.CatalogueModel.fields = me.setActiveFieldEditable(me.CatalogueModel.fields)
            me.CatalogueModel.isNeedSendSigMessage = !(trusted == 2);
    

    ,setActiveFieldEditable: function(fields) {
        for(var i = 0; i < fields.length; i++) {
            if(fields[i].name = 'active') {
                fields[i].editable = true;
                return fields;
            }
        }
        fields.push({
            name: 'active',
            editable: true
        })
        return fields;
    }
    */
    
})
