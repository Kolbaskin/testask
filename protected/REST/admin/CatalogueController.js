Ext.define('Crm.REST.admin.CatalogueController', {
    extend: "Crm.REST.RestBaseController",
    constructor: function () {
        this.callParent(arguments);
        this.initModels();
        this.params = this.req.body.reqData;
        if(!this.params.filters) {
            this.params.filters = [];
        }        
    },
    
    
    
    
    initModels: function () {
        this.CatalogueModel = Ext.create('Crm.modules.catalogue.model.CatalogueModel', {
            scope: this
        });
        if(this.user) {

            this.user.name = this.user.full_name || this.user.username;
            
            this.CatalogueModel.user = {
                id: this.user._id,
                profile: this.user
            }
        }
    },
    

    list: function () {
        var me = this;
        if (!this.user) {
            this.error(401);
            return;
        }

        this.params.filters.push({
            property: 'pid',
            value: this.user.pid
        })       
        
        
        this.CatalogueModel.getDataAndCheckSign(this.params, function(res) {           
            me.sendData(res);          
        }) 
    },
    
    listAll: function () {
        var me = this;
        if (!this.user) {
            this.error(401);
            return;
        }

        this.CatalogueModel.find = {active:1}
        
        this.CatalogueModel.getData(this.params, function(res) {           
            me.sendData(res);          
        }) 
    },

    write: function () {
        var me = this;
        if (!this.user) {
            this.error(401);
            return;
        }
        this.params.pid = this.user.pid;
        
        me.CatalogueModel.write(me.params, function (res) {
            me.sendData(res);
        }, { add: true, modify: true });

    },
    
    categories: function () {
        var me = this;
        if (!this.user) {
            this.error(401);
            return;
        }

        Ext.create('Crm.modules.catalogue.model.GroupsModel', { scope: this })
        .getData(this.params, function (res) {
            me.sendData(res);
        });

    },
    
    blogList: function() {
        var me = this;
        if (!this.user) {
            this.error(401);
            return;
        }
        if(this.params.fieldSet && Ext.isArray(this.params.fieldSet) && this.params.fieldSet.indexOf('catalogue_id') == -1) {
            this.params.fieldSet.push('catalogue_id')
        }  
        
        
        
        me.src.db.collection('users').find({pid: me.user.pid}, {_id:1})
        .toArray(function(e,users) {
            users.each(function(u) {return u._id}, true)
            var model = Ext.create('Crm.modules.catalogueBlogs.model.CatalogueBlogsModel', { scope: me })
            model.find = {
                maker: {$in: users}
            }
            model.getDataAndCheckSign(me.params, function (res) {
                me.checkBlogPermissions(res.list, function(log) {
                    me.sendData(log? res: {list:[], total:0});
                })            
            });
        })
    },
    
    blogWrite: function () {
        var me = this;
        if (!this.user) {
            this.error(401);
            return;
        }
        if(!this.params.catalogue_id) {
            this.error(500);
            return;
        }
        
        this.checkBlogPermissions([this.params.catalogue_id], function(res) {
            if(res && res[0] && !res[0].ro) {
                var model = Ext.create('Crm.modules.catalogueBlogs.model.CatalogueBlogsModel', {scope: me});
                
                model.user = {
                    id: me.user._id,
                    profile: me.user
                }
                
                model.write(me.params, function(res) {
                    me.sendData(res);
                }, {add:true, modify: true})

            } else {
                me.error(401);
            }
        })
    },
    
    blogRemove: function () {
        var me = this;
        if (!this.user) {
            this.error(401);
            return;
        }
        if(!this.params.catalogue_id) {
            this.error(500);
            return;
        }
        
        this.checkBlogPermissions([this.params._id], function(res) {
            if(res && res[0] && !res[0].ro) {
                me.callRemove('Crm.modules.catalogueBlogs.model.CatalogueBlogsModel', me.params, function(res) {
                    me.sendData(res);
                })
            } else {
                me.error(401);
            }
        })
    },
    
    checkBlogPermissions: function(items, cb) {
        var me = this, ids = [];
        items.forEach(function(i) {ids.push(i._id)});
        if(ids) {
            this.src.db.query('SELECT item_blogs._id, users.pid FROM item_blogs LEFT JOIN users ON item_blogs.maker = users._id WHERE item_blogs._id in (\''+ids.join("','", ids)+'\') and item_blogs.maker = $1', [me.user._id], function(e, res) {
                if(res) {
                    items.each(function(item) {                       
                        for(var i=0;i<res.length;i++) {  
                            if(item._id == res[i]._id) {
                                item.ro = false;
                                return item;
                            }
                        }
                        item.ro = true;
                        return item
                    }, true)
                    cb(items)
                } else
                    cb(false)
            })
        } else
            cb(false)
    },

    images: function () {
        var me = this,
            params = me.req.body.reqData;
        if (!me.user) {
            me.error(401);
            return;
        }
        if (!params.filters) params.filters = [];
        params.filters.push({ value: me.user.pid, property: 'pid' });

        Ext.create('Crm.modules.images.model.ImagesModel', { scope: this })
            .getData(params, function (res) {
                me.sendData(res);
            });
    },
    
    imageWrite: function () {
        var me = this,
            params = me.req.body.reqData;
        if (!me.user) {
            me.error(401);
            return;
        }
        Ext.create('Crm.modules.images.model.ImagesModel', { scope: this })
            .write(params, function (res) {
                me.sendData(res);
            }, { add: true, modify: true });

    }
    


});