Ext.define('Crm.REST.admin.ProfileController', {
    extend: "Crm.REST.RestBaseController"
    
    ,constructor: function() {
        this.callParent(arguments)
        this.model = Ext.create('Crm.modules.accountHolders.model.AccountHoldersModel', {scope: this});
        this.model.user = {
            id: this.user._id,
            profile: this.user
        }
        
        this.params = this.req.body.reqData;
    }
    
    ,list: function() {
        if(!this.user) {
            this.sendError(401)
            return;
        }
        var me = this;
        this.model.find = {_id: this.user._id};     
        this.model.getData({}, function(res) {
            if(res && res.list && res.list[0] && !res.list[0].full_name) 
                res.list[0].full_name = res.list[0].username;
            me.sendData(res)
        })
    }
    
    ,write: function() {
        if(!this.user) {
            this.sendError(401)
            return;
        }
        var me = this;
        this.params.pid = this.user.pid;
        this.params._id = this.user._id;
        this.model.write(this.params, function(res) {
            me.sendData(res)
        }, {modify: true})
    }
    
    ,getTotals: function() {

        if(!this.user) {
            this.sendError(401)
            return;
        };
        [
            function(next) {
                this.src.db.collection(_TABPREFIX + "foundations").findOne({
                    _id: this.user.pid
                },{orgtype: 1, total1:1, total2:1}, function(e,d) {
                    next(d)
                })
            },
            function(d) {
                var res = {amount: '', reportModel: '', tooltip: ''}
                if(d) {
                    var rnd = function(v) {
                        return Math.round(v * 100) / 100;
                    }
                    if(d.orgtype == '1') {
                        res.amount = rnd(d.total1);
                        res.reportModel = 'Crm.modules.reports.view.CharityPaymentsGrid';
                        res.tooltip = 'Received'
                    } else {
                        res.amount = rnd(d.total1) + '/' + rnd(d.total2);
                        res.reportModel = 'Crm.modules.reports.view.PaymentsGrid';
                        res.tooltip = 'Paid/Debt'
                    }
                }
                this.sendData(res);
            }
        ].runEach(this)
    }
    
})