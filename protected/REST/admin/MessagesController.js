

Ext.define('Crm.REST.admin.MessagesController', {
    extend: "Crm.REST.RestBaseController"
    
    ,constructor: function() {
        this.callParent(arguments)
        this.messModel = Ext.create('Crm.modules.messages.model.MessagesModel', {scope: this});
        if(this.user) {
            this.user.name = this.user.full_name || this.user.username;
            this.user.login = this.user.username;
            this.messModel.user = {
                id: this.user._id,
                profile: this.user
            }
        }
    }
    
    
    ,getNewCount: function() {
        var params = this.req.body.reqData;
        if(_LAST_READ_MESSAGE_TIME_ < _LAST_MESSAGE_TIME_) {
            this.sendData({isNewMessages: true});
            _LAST_READ_MESSAGE_TIME_ = _LAST_MESSAGE_TIME_;
 
        } else {
            this.sendData({isNewMessages: false});
        }
    }
    
    ,getNewMess: function() {
        var params = this.req.body.reqData;
        [
            function(next) {
                this.messModel.$getNewMessagesCount(params, next)
            },
            function(data) {
                this.sendData(data)
            }
        ].runEach(this)
    }
    
    ,list: function() {
        var params = this.req.body.reqData;
        [
            function(next) {
                this.messModel.getData(params, next)
            },
            function(data) {
                this.sendData(data)
            }
        ].runEach(this)
    }
    
    ,write: function() {
        var params = this.req.body.reqData;
        [
            function(next) {
                this.messModel.write(params, next, {add: true})
            },
            function(data) {
                this.sendData(data)
            }
        ].runEach(this)
    }
    
    ,markRead: function() {
        var params = this.req.body.reqData;
        [
            function(next) {
                this.messModel.$markAsRead(params, next)
            },
            function(data) {
                this.sendData(data)
            }
        ].runEach(this)
        
    }
    
    ,getAdmins: function() {
        [
            function(next) {
                Ext.create('Crm.modules.users.model.UsersModel', {scope: this})
                .getData(this.req.body.reqData, next)
            },
            function(data) {
                this.sendData(data)
            }
        ].runEach(this)
    }
})