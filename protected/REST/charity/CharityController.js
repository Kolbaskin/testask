Ext.define('Crm.REST.charity.CharityController', {
    extend: "Crm.REST.RestBaseController"


    , list: function () {
        var me = this
            , params = me.req.body.reqData
            , queryParams = [];

        var filteredSql = 'SELECT pf.* '
            /*+ ' pf._id,'
            + ' pf.ctime,'
            + ' pf.removed,'
            + ' pf.maker,'
            + ' pf.orgtype,'
            + ' pf. NAME,'
            + ' pf.description,'
            + ' pf.active,'
            + ' pf.province,'
            + ' pf.district,'
            + ' pf.address,'
            + ' pf.tel,'
            + ' pf.email,'
            + ' pf.donation_type,'
            + ' pf.coordinates,'
            + ' pf.url,'
            + ' pf.logo_url,'
            + ' pf.extra_info'; */
        var countSql = 'SELECT count(*)';
        var whereClause = ' FROM'
            + ' pgd_foundations pf '
            + ' WHERE pf.active = 1'
            + ' and pf.removed = 0'
            + ' and pf.orgtype = 1';


        if (params.type) {
            whereClause = whereClause + " and extra_info->>'charity_type'=$1";
            queryParams.push(params.type);
        }
        var skip = params.startIndex && /^[0-9]{1,}$/.test(params.startIndex + '') ? parseInt(params.startIndex) : 0;
        var limit = params.numOfRecords && /^[0-9]{1,}$/.test(params.numOfRecords + '') ? parseInt(params.numOfRecords) : 50;
        var limitClause = ' limit ' + limit + ' offset ' + skip;

        me.src.db.query(filteredSql + whereClause + limitClause, queryParams, function (e, data) {

console.log('sql:', filteredSql + whereClause + limitClause)
console.log('e:', e) 
console.log('data:', data)        
            
            if (data) {
                me.src.db.query(countSql + whereClause, queryParams, function (e, countData) {
                    var output = { total: 0 };
                    if (countData && countData.length) {
                        output.total = countData[0].count;
                    }
                    if(data.length >0)
                        {
                            data.prepEach(function(r,next){
                             r.url=  me.config.baseUrl + r.url; 
                             next(r);
                            },function()
                            {
                            });
                        }
                    else
                        {
                            data.url = me.config.baseUrl + data.url;
                        }    
                    output.list = data;
                    me.sendData(output);
                });

            } else {
                me.sendError();
            }
        });
        /*if(!params.filters) params.filters = [];

        params.filters.push({value: true, property: 'active'});
        params.filters.push({value: 1, property: 'orgtype'});
        params.filters.push({value:"->>'charity_type'='L'", property:"extra_info"});
        Ext.create('Crm.modules.organizations.model.FoundModel', {scope: this})
        .getData(params, function(res) {
            me.sendData(res);   
        })*/
    }

    , write: function () {
        var me = this
            , params = me.req.body.reqData;
        /*if(!me.user) {
            me.addErrorAndSend(401);
            return;
        }*/
        params.pid = me.user.pid;
        Ext.create('Crm.modules.organizations.model.FoundModel', { scope: this })
            .write(params, function (res) {
                me.sendData(res);
            }, { add: true, modify: true })

    }

    , details: function () {
        var me = this;
        var reqBody = me.req.body.reqData;
        if (!me.validateRequest([
            {
                type: 'STRING',
                field: "charityId",
                isRequired: true,
            }
        ])) {
            return me.sendError();
        }

        [
            function (next) {
                var sql = 'SELECT'
                    + ' pf._id,'
                    + ' pf.ctime,'
                    + ' pf.removed,'
                    + ' pf.maker,'
                    + ' pf.orgtype,'
                    + ' pf. NAME,'
                    + ' pf.description,'
                    + ' pf.active,'
                    + ' pf.province,'
                    + ' pf.district,'
                    + ' pf.address,'
                    + ' pf.tel,'
                    + ' pf.email,'
                    + ' pf.donation_type,'
                    + ' pf.coordinates,'
                    + ' pf.url,'
                    + ' pf.logo_url,'
                    + ' pf.extra_info,'
                    + ' (select sum(total_amount) from order_charity_details  where charity_id=pf._id group by charity_id) as total_charity_amount'
                    + ' FROM'
                    + ' pgd_foundations pf'
                    + ' WHERE pf._id=$1';

                me.src.db.query(sql, [reqBody.charityId], function (e, data) {
                    if (data) {
                        data[0].url = me.config.baseUrl + data[0].url;
                        me.sendData(data);
                        next(data);
                    } else {
                        me.sendError();
                    }
                });
            }
            // *** commented since we are not showing charity items in in charity details 
            // ,function (data, next) {
            //     if (data) {
            //         var CatalogueModel = Ext.create('Crm.modules.catalogue.model.CatalogueModel', {
            //             scope: me
            //         });
            //         CatalogueModel.error = function (code, info) {
            //             return me.addErrorObj(code);
            //         }

            //         CatalogueModel.dbCollection.find({
            //             pid: data[0]._id
            //         }, {}, function (e, res) {
            //             if (res && res.length > 0) {
            //                 data[0].items = res;
            //                 next(data);
            //             }
            //             else {
            //                 me.sendError();
            //             }
            //         });
            //     }
            // }
            // function (data) {
            //     if (data && data[0].items) {
            //         var DocsModel = Ext.create('Crm.modules.file.model.FilesModel', {
            //             scope: me
            //         });

            //         DocsModel.error = function (code, info) {
            //             return me.addErrorObj(code);
            //         };

            //         data[0].items.prepEach(function (item, next) {

            //             DocsModel.dbCollection.find({
            //                 group_id: item.document_id
            //             }, {}, function (e, result) {
            //                 if (result && result.length > 0) {
            //                     var fileController = Ext.create('Crm.REST.invoice.FileUploadController', {
            //                         scope: me,
            //                     });
            //                     result.prepEach(function (cat, nextItem) {
            //                         if (cat.group_id) {
            //                             fileController.getFilesUrl(cat.group_id, function (result) {
            //                                 if (result) {
            //                                     item.docs = result;
            //                                 }
            //                                 nextItem(cat);
            //                             })
            //                         } else {
            //                             nextItem(cat);
            //                         }
            //                     }, function () {
            //                         next(item);
            //                     })
            //                 }
            //                 else {
            //                     me.sendError();
            //                 }
            //             });
            //         }, function () {
            //             me.sendData(data);
            //         });

            //     }

            // }

        ].runEach();

    }

    , remove: function () {
        var me = this
            , params = me.req.body.reqData;
        if (!me.user) {
            me.addErrorAndSend(401);
            return;
        }
        params.pid = me.user.pid;
        Ext.create('Crm.modules.docs.model.DocsModel', { scope: this })
            .remove(params, function (res) {
                me.sendData(res);
            })

    }


    , types: function () {
        var me = this
            , params = Ext.clone(me.req.body.reqData);
        if (!me.user) {
            me.addErrorAndSend(401);
            return;
        }
        Ext.create('Crm.modules.organizations.model.FoundModel', { scope: this })
            .getData(params, function (res) {
                me.sendData(res);
            })

    }

    /**
    * Added by Max 25/07/17
    */
    , getAll: function () {
        var params = this.req.body.reqData
            , cls = Ext.create('Crm.modules.organizations.model.FoundModel', {
                scope: this,
                find: { orgtype: 1 }
            });

        [
            function (next) {
                cls.getData(params, next)
            },
            function (res) {
                this.sendData(res);
                delete cls;
            }
        ].runEach(this)

    }

    /**
    * Added by kt 10/08/17
    */
    , updateCharityUrl: function () {
        var me = this,
            params = me.req.body.reqData;
        if (!me.user) {
            me.addErrorAndSend(401);
            return;
        }
        if (!me.user.pid) {
            me.addErrorAndSend('ERR033');
            return;
        }
        // var hostname = me.req.headers.host;
        var path = '/api/rest/refdata/download-file?id=';
        var FoundModel = Ext.create('Crm.modules.organizations.model.FoundModel', {
            scope: this
        })
        FoundModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var DocsModel = Ext.create('Crm.modules.file.model.FilesModel', {
            scope: me
        });
        DocsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        DocsModel.dbCollection.findOne({
            group_id: params.group_id,
        }, { _id: 1 }, function (e, data) {
            if (data && data._id) {
                var url = me.config.baseUrl + path + data._id;
                FoundModel.dbCollection.update({
                    _id: me.user.pid
                }, {
                        $set: {
                            "extra_info:{info_url}": path + data._id
                        }
                    }, function (e, d) {
                        if (d) {
                            me.sendData({});
                        }
                        else {
                            return me.addErrorAndSend('ERR042');
                        }
                    });
            }
            else {
                return me.addErrorAndSend('ERR039');
            }
        });
    }

    , showCharityPage: function (cb) {

        var me = this;
        var fs = require('fs');
        var path = require('path');
        var DocsModel = Ext.create('Crm.modules.file.model.FilesModel', {
            scope: me
        });

        DocsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var params = me.req.body.reqData;
        [
            function (next) {
                DocsModel.dbCollection.findOne({ _id: me.params.gpc.id }, {}, function (err, doc) {
                    if (doc) {
                        next(doc)
                    } else {
                        me.response.writeHead(404);
                        me.response.write('Not found');
                        me.response.end();
                    }
                })
            },
            function (doc) {
                try {
                    var filePath = path.join('uploads', doc._id+".html");
                    var stat = fs.statSync(filePath);
                    me.response.writeHead(200, {
                        'Content-Type': doc.doc_type || 'application/octet-stream',
                        'Content-Disposition': 'inline; filename="' + doc._id + '"',
                        'Content-Length': stat.size
                    });
                    var readStream = fs.createReadStream(filePath);
                    readStream.pipe(me.response);
                } catch (e) {
                    me.response.writeHead(404);
                    me.response.write('Not found');
                    me.response.end();
                }
            }
        ].runEach();
    },

    getAllCharityItems: function (cb) {
        var me = this
            , params = me.req.body.reqData
            , queryParams = [];
        var reqBody = me.req.body.reqData;
        [
            function (next) {
                var filteredSql = 'SELECT'
                    + ' pf._id,'
                    + ' pf.logo_url,'
                    + ' pf. NAME';
                var countSql = 'SELECT count(*)';
                var whereClause = ' FROM'
                    + ' pgd_foundations pf '
                    + ' WHERE pf.active = 1'
                    + ' and pf.removed = 0'
                    + ' and pf.orgtype = 1';


                if (params.type) {
                    whereClause = whereClause + " and extra_info->>'charity_type'=$1";
                    queryParams.push(params.type);
                }
                var skip = params.startIndex && /^[0-9]{1,}$/.test(params.startIndex + '') ? parseInt(params.startIndex) : 0;
                var limit = params.numOfRecords && /^[0-9]{1,}$/.test(params.numOfRecords + '') ? parseInt(params.numOfRecords) : 50;
                var limitClause = ' limit ' + limit + ' offset ' + skip;

                me.src.db.query(filteredSql + whereClause + limitClause, queryParams, function (e, data) {
                    if (data) {
                        me.src.db.query(countSql + whereClause, queryParams, function (e, countData) {
                            var output = { total: 0 };
                            if (countData && countData.length) {
                                output.total = countData[0].count;
                            }
                            output.list = data;
                            next(output);
                        });

                    } else {
                        me.sendError();
                    }
                });
            },
            function (data, next) {
                if (data) {
                    var CatalogueModel = Ext.create('Crm.modules.catalogue.model.CatalogueModel', {
                        scope: me
                    });
                    CatalogueModel.error = function (code, info) {
                        return me.addErrorObj(code);
                    }

                    data.list.prepEach(function (cat, nextItem) {
                        CatalogueModel.dbCollection.find({
                            pid: cat._id
                        }, {}, function (e, res) {
                            if (res && res.length > 0) {
                                cat.items = res;
                                nextItem(cat);
                            }
                            else {
                                nextItem(cat)
                                //  me.sendError();
                            }
                        });
                    }, function () {
                        next(data);
                    });
                }
            },
            function (data) {
                if (data) {
                    var DocsModel = Ext.create('Crm.modules.file.model.FilesModel', {
                        scope: me
                    });

                    DocsModel.error = function (code, info) {
                        return me.addErrorObj(code);
                    };

                    data.list.prepEach(function (item, next) {
                        if (item.items) {
                            item.items.prepEach(function (charityItem, next) {
                                DocsModel.dbCollection.find({
                                    group_id: charityItem.document_id
                                }, {}, function (e, result) {
                                    if (result && result.length > 0) {
                                        var fileController = Ext.create('Crm.REST.invoice.FileUploadController', {
                                            scope: me,
                                        });
                                        result.prepEach(function (cat, nextItem) {
                                            if (cat.group_id) {
                                                fileController.getFilesUrl(cat.group_id, function (result) {
                                                    if (result) {
                                                        charityItem.docs = result;
                                                    }
                                                    nextItem(cat);
                                                })
                                            } else {
                                                nextItem(cat);
                                            }
                                        }, function () {
                                            next(charityItem);
                                        })
                                    }
                                    else {
                                        next(charityItem);
                                        //me.sendError();
                                    }
                                });
                            }, function () {
                                next(item);
                            });
                        }
                        else {
                            next(item);
                        }
                    }, function () {
                        me.sendData(data);
                    });

                }

            }

        ].runEach();
    }
})