Ext.define('Crm.REST.charity.ChangePlanStatus', {
    extend: "Crm.REST.RestBaseController",
    constructor: function () {
        this.callParent(arguments);
    },
    /*
     @author : Kaustubh Thube
     @date : 10 July 2017
     @function : apiChangePlanStatus
     @desc : change plan status
     */
    apiChangePlanStatus: function (cb) {
        var me = this;
        var params = me.req.body.reqData;
        var itr = 0;
        var charityIds = [];
        if (!me.user) {
            me.addErrorAndSend(401);
            return;
        }
        var charity = Ext.create('Crm.modules.charity.model.CharityModel', {
            scope: me
        });
        charity.error = function (code, info) {
            return me.addErrorObj(code);
        };

        var CharityPlan = Ext.create('Crm.modules.charityPlan.model.CharityPlanModel', {
            scope: me
        });
        CharityPlan.error = function (code, info) {
            return me.addErrorObj(code);
        };

        var CharityPlanDetails = Ext.create('Crm.modules.charityPlanDetails.model.CharityPlanDetailsModel', {
            scope: me
        });
        CharityPlanDetails.error = function (code, info) {
            return me.addErrorObj(code);
        };

        var partners = Ext.create('Crm.modules.organizations.model.FoundModel', {
            scope: me
        });
        partners.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var CharityItem = Ext.create('Crm.modules.catalogue.model.CatalogueModel', {
            scope: me
        });
        CharityItem.error = function (code, info) {
            return me.addErrorObj(code);
        };

        if (!me.validateRequest([
            {
                type: 'STRING',
                field: "plan_id",
                isRequired: true,
            },
            {
                type: 'STRING',
                field: "plan_type",
                isRequired: true,
            }
        ])) {
            return me.sendError();
        }

        //start --> check for given plan_id if there is any plan with DRAFT STATUS ("T")
        CharityPlan.dbCollection.findOne({
            _id: params.plan_id,
            plan_type: "T"
        }, {}, function (e, data) {
            if (data && data._id) {
                if (params.plan_type == me.config.activePlan) {
                    CharityPlanDetails.dbCollection.find({
                        charity_plan_id: data._id,
                    }, {}, function (e, data) {
                        if (data) {
                            data.prepEach(function (r, next) {
                                CharityItem.dbCollection.findOne({
                                    _id: r.pgd_catalogue_id,
                                    pay_now: 1
                                }, {}, function (err, data) {
                                    if (data) {
                                        if (parseInt(data.pay_now) == 1) {
                                            next(r);
                                        }
                                        else {
                                            itemCount = 0;
                                            next(r);
                                        }
                                    }
                                    else {
                                        return me.addErrorAndSend('ERR037', r.itemId);
                                    }
                                });
                            }, function () {
                                    CharityPlan.dbCollection.findOne({
                                        partner_user_id: me.user.pid,
                                        plan_type: me.config.activePlan
                                    }, {}, function (e, data) {
                                        if (data && data._id) {
                                            CharityPlan.dbCollection.update({
                                                _id: data._id,
                                            }, {
                                                    $set: {
                                                        "plan_type": me.config.draftPlan
                                                    }
                                                }, function (e, d) {
                                                    if (d) {
                                                        CharityPlan.dbCollection.update({
                                                            _id: params.plan_id,
                                                        }, {
                                                                $set: {
                                                                    "plan_type":params.plan_type
                                                                }
                                                            }, function (e, d) {
                                                                if (d) {
                                                                   me.sendData([]);
                                                                }
                                                                else {
                                                                    return me.addErrorAndSend('ERR042');
                                                                }
                                                            }, { multi: true });
                                                    }
                                                    else {
                                                        return me.addErrorAndSend('ERR042');
                                                    }
                                                }, { multi: true });
                                        }
                                        else {
                                            CharityPlan.dbCollection.update({
                                                _id: params.plan_id,
                                            }, {
                                                    $set: {
                                                        "plan_type":params.plan_type
                                                    }
                                                }, function (e, d) {
                                                    if (d) {
                                                       me.sendData([]);
                                                    }
                                                    else {
                                                        return me.addErrorAndSend('ERR042');
                                                    }
                                                }, { multi: true });
                                        }
                                    });
                                });
                        }
                    });
                }
                else if (params.plan_type == me.config.emergencyPlan) {
                    CharityPlanDetails.dbCollection.find({
                        charity_plan_id: data._id,
                    }, {}, function (e, data) {
                        if (data) {
                            data.prepEach(function (r, next) {
                                CharityItem.dbCollection.findOne({
                                    _id: r.pgd_catalogue_id,
                                    item_type: me.config.unlimitedCharity,
                                    pay_now: 1
                                }, {}, function (err, data) {
                                    if (data) {
                                        if (parseInt(data.pay_now) == 1) {
                                            next(r);
                                        }
                                        else {
                                            itemCount = 0;
                                            next(r);
                                        }
                                    }
                                    else {
                                        return me.addErrorAndSend('ERR037', r.itemId);
                                    }
                                });
                            }, function () {
                                    CharityPlan.dbCollection.findOne({
                                        partner_user_id: me.user.pid,
                                        plan_type: me.config.emergencyPlan
                                    }, {}, function (e, data) {
                                        if (data && data._id) {
                                            CharityPlan.dbCollection.update({
                                                _id: data._id,
                                            }, {
                                                    $set: {
                                                        "plan_type": me.config.draftPlan
                                                    }
                                                }, function (e, d) {
                                                    if (d) {
                                                        CharityPlan.dbCollection.update({
                                                            _id: params.plan_id,
                                                        }, {
                                                                $set: {
                                                                    "plan_type":params.plan_type
                                                                }
                                                            }, function (e, d) {
                                                                if (d) {
                                                                   me.sendData([]);
                                                                }
                                                                else {
                                                                    return me.addErrorAndSend('ERR042');
                                                                }
                                                            }, { multi: true });
                                                    }
                                                    else {
                                                        return me.addErrorAndSend('ERR042');
                                                    }
                                                }, { multi: true });
                                        }
                                        else {
                                            CharityPlan.dbCollection.update({
                                                _id: params.plan_id,
                                            }, {
                                                    $set: {
                                                        "plan_type":params.plan_type
                                                    }
                                                }, function (e, d) {
                                                    if (d) {
                                                       me.sendData([]);
                                                    }
                                                    else {
                                                        return me.addErrorAndSend('ERR042');
                                                    }
                                                }, { multi: true });
                                        }
                                    });
                                });
                        }
                    });
                }
            } else {
                return me.addErrorAndSend('ERR043');
            }
        });
    }
});