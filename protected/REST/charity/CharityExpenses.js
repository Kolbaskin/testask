Ext.define('Crm.REST.charity.CharityExpenses', {
    extend: "Crm.REST.RestBaseController"

    , setCharityExpenses: function () {

        var me = this;
        var params = me.req.body.reqData;

        var CharityExpenses = Ext.create('Crm.modules.charityExpenses.model.CharityExpenses', {
            scope: me
        });
        CharityExpenses.error = function (code, info) {
            return me.addErrorObj(code);
        };

        CharityExpenses.write({
            "_id": me.Util.getObjectId(),
            "charity_id": me.user.pid,
            "amount": params.amount,
            "description": params.description,
            "document_id": params.document_id,
            "date": new Date()
        }, function (data) {
            if (data.success) {
                me.sendData(data.record)
            } else {
                return me.addErrorObj(data.error);
            }
        }, {
                add: 1,
                modify: 1
            });

    }

    , getCharityExpenses: function () {

        var me = this;
        var params = me.req.body.reqData;

        if (!me.user) {
            me.error(401);
            return;
        }
        var params={};
        if (!params.filters) params.filters = [];
        params.filters.push({ value: me.user.pid, property: 'pid' });
        Ext.create('Crm.modules.charityExpenses.model.CharityExpenses', { scope: this })
            .getData(params, function (res) {
                if (res.list && res.list.length) {
                    var fileController = Ext.create('Crm.REST.invoice.FileUploadController', {
                        scope: me,
                    });
                    res.list.prepEach(function (cat, nextItem) {
                        if (cat.document_id) {
                            fileController.getFilesUrl(cat.document_id, function (result) {
                                if (result && result.length > 0) {
                                    cat.docs = result;
                                }
                                nextItem(cat);
                            })
                        } else {
                            nextItem(cat);
                        }
                    }, function () {
                        me.sendData(res);
                    })
                } else {
                    me.sendData(res);
                }
            })

    }
})