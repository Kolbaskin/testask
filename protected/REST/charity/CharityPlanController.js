Ext.define('Crm.REST.charity.CharityPlanController', {
    extend: "Crm.REST.RestBaseController",
    constructor: function () {
        this.callParent(arguments);
        this.initModels();
    },
    initModels: function () {
        var me = this;
        me.CharityPlanModel = Ext.create('Crm.modules.charityPlan.model.CharityPlanModel', {
            scope: me
        });
        me.CharityPlanModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        me.DetailsModel = Ext.create('Crm.modules.charityPlanDetails.model.CharityPlanDetailsModel', {
            scope: me
        });
        me.DetailsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
    },
    /*
    @author : Vaibhav Vaidya
    @date : 5 July 2017
    @function : apiCreateCharityPlan
    @desc : Create a Charity Plan with Charity Details included
    @modified --kaustubh new changes now plan is for charity items not charities
    */
    apiCreateCharityPlan: function (cb) {
        var me = this;
        if (!me.user) {
            me.sendError(401);
            return;
        }
        var partnerUserId;
        var reqBody = me.req.body.reqData;

        var UsersModel = Ext.create('Crm.modules.accountHolders.model.AccountHoldersModel', {
            scope: me
        });
        UsersModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var PartnersModel = Ext.create('Crm.modules.organizations.model.FoundModel', {
            scope: me
        });
        PartnersModel.error = function (code, info) {
            return me.addErrorObj(code);
        };

        var FoundationModel = Ext.create('Crm.modules.organizations.model.FoundModel', {
            scope: me
        });
        FoundationModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var CharityItem = Ext.create('Crm.modules.catalogue.model.CatalogueModel', {
            scope: me
        });
        CharityItem.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var DB_TRANSACTION = {
            BEGIN: function (next) {
                opt = Ext.clone(me.config.pgsql);
                opt.callback = function (conn) {
                    me.src.db.conn.query('BEGIN', function (err) {
                        if (err) {
                            me.sendError(err);
                        } else {
                            me.initModels();
                            next();
                        }
                    });
                }
                me.src.db = Ext.create("Database.drivers.Postgresql.Database", opt);
            },
            COMMIT: function (data) {
                me.src.db.conn.query('COMMIT', function () {
                    me.sendData(data)

                });
            },
            ROLLBACK: function (err) {
                me.src.db.conn.query('ROLLBACK', function () {
                    me.sendError(err);
                });
            }
        }
        var handleError = function (err) {
            return cb(err);
        };
        [
            function (err, client) {
                if (client) {
                    if (me.validateRequest([{
                        type: 'String',
                        field: "planType",
                        isRequired: true
                    },
                    {
                        type: 'string',
                        field: "planName",
                        isRequired: true
                    },
                    {
                        type: 'ARRAY', field: 'charities', charities: [
                            {
                                type: 'ObjectID',
                                field: "charityId",
                                isRequired: true
                            },
                            {
                                type: 'string',
                                field: "itemId",
                                isRequired: false
                            },
                            {
                                type: 'float',
                                field: "percentage",
                                isRequired: false
                            }
                        ], isRequired: true
                    }
                    ])) {
                        [
                            function (next) {
                                if (me.user.user_type == me.config.endUser) {
                                    UsersModel.dbCollection.findOne({
                                        _id: me.user._id,
                                        removed: '0'
                                    }, { _id: 1 }, function (err, data) {
                                        if (data) {
                                            partnerUserId = me.user._id;
                                            next();
                                        } else {
                                            return me.addErrorAndSend('ERR033', me.user._id);
                                        }
                                    });
                                }
                                else if (me.user.user_type == me.config.partnerUser || me.user.user_type == 'B') {
                                    PartnersModel.dbCollection.findOne({
                                        _id: me.user.pid,
                                        removed: '0'
                                    }, { _id: 1 }, function (err, data) {
                                        if (data) {
                                            partnerUserId = me.user.pid;
                                            next();
                                        } else {
                                            return me.addErrorAndSend('ERR033', me.user.pid);
                                        }
                                    });
                                }
                                else if (me.user.user_type == me.config.charityUser) {
                                    return me.addErrorAndSend('ERR047');
                                }
                                else {
                                    return me.addErrorAndSend('ERR049');
                                }
                            },
                            function (next) {
                                var total = 0, emptyCount = 0;
                                for (var i = 0; i < reqBody.charities.length; i++) {
                                    if (reqBody.charities[i].percentage) {
                                        total += parseFloat(reqBody.charities[i].percentage);
                                    }
                                    else {
                                        emptyCount++;
                                    }
                                }
                                if ((Math.round(total) != 100)) {
                                    return me.addErrorAndSend('ERR034');
                                }
                                else {
                                    next(total, emptyCount);
                                }
                            },
                            function (total, emptyCount, next) {
                                var itemCount = -1;
                                if (reqBody.planType == me.config.activePlan) {
                                    reqBody.charities.prepEach(function (r, next) {
                                        CharityItem.dbCollection.findOne({
                                            _id: r.itemId,
                                            pay_now: 1
                                        }, {}, function (err, data) {
                                            if (data) {
                                                if (parseInt(data.pay_now) == 1) {
                                                    next(r);
                                                }
                                                else {
                                                    itemCount = 0;
                                                    next(r);
                                                }
                                            }
                                            else {
                                                return me.addErrorAndSend('ERR037', r.itemId);
                                            }
                                        });
                                    }, function () {
                                        next(itemCount, total, emptyCount);
                                    });
                                }
                                else if (reqBody.planType == me.config.emergencyPlan) {
                                    reqBody.charities.prepEach(function (r, next) {
                                        CharityItem.dbCollection.findOne({
                                            _id: r.itemId,
                                            item_type: me.config.unlimitedCharity
                                        }, {}, function (err, data) {
                                            if (data) {
                                                if (parseInt(data.pay_now) == 1) {
                                                    next(r);
                                                }
                                                else {
                                                    itemCount = 0;
                                                    next(r);
                                                }
                                            }
                                            else {
                                                return me.addErrorAndSend('ERR037', r.itemId);
                                            }
                                        });
                                    }, function () {
                                        next(itemCount, total, emptyCount);
                                    });
                                }
                                else {
                                    next(itemCount, total, emptyCount);
                                }
                            },
                            //clear unwanted records
                            function (itemCount, total, emptyCount, next) {
                                if (itemCount != 0) {
                                    // active plan and if its end user -- delete all charity plan details here
                                    if (me.user.user_type == me.config.endUser && reqBody.planType == me.config.activePlan) {
                                        me.CharityPlanModel.dbCollection.find({
                                            partner_user_id: me.user._id,
                                            removed: '0'
                                        }, { _id: 1 }, function (err, data) {
                                            if (data && data.length > 0) {
                                                var params = [];
                                                data.prepEach(function (v, next) {
                                                    params.push(v._id);
                                                    next(v);
                                                }, function () {
                                                });
                                                me.DetailsModel.dbCollection.find({
                                                    charity_plan_id: { $in: params },
                                                }, { _id: 1 }, function (err, indata) {
                                                    if (indata && indata.length > 0) {
                                                        params = [];
                                                        indata.prepEach(function (t, next) {
                                                            params.push(t._id);
                                                            next(t);
                                                        }, function () {
                                                            me.DetailsModel.removeAction = 'delete';
                                                            me.DetailsModel.remove(params, function (inres) {
                                                                if (inres.success == true) {
                                                                    next(itemCount, total, emptyCount);
                                                                }
                                                                else {
                                                                }
                                                            });
                                                        });
                                                    }
                                                });
                                            }
                                            else {
                                                next(itemCount, total, emptyCount);
                                            }
                                        });
                                    }
                                    // dont do anything in Case emergenct just change plan status in next method to draft
                                    else if (me.user.user_type == me.config.partnerUser || me.user.user_type == 'B') {
                                        next(itemCount, total, emptyCount);
                                    }
                                    else {
                                        return me.addErrorAndSend('ERR036');
                                    }
                                }
                            },
                            //clear unwanted records
                            function (itemCount, total, emptyCount, next) {
                                if (itemCount != 0) {
                                    // active plan and if its end user -- delete all charity plan details here
                                    if (me.user.user_type == me.config.endUser && reqBody.planType == me.config.activePlan) {
                                        me.CharityPlanModel.dbCollection.find({
                                            partner_user_id: me.user._id,
                                            removed: '0'
                                        }, { _id: 1 }, function (err, data) {
                                            if (data && data.length > 0) {
                                                var params = [];
                                                data.prepEach(function (v, next) {
                                                    params.push(v._id);
                                                    next(v);
                                                }, function () {
                                                    var queryparams = [];
                                                    for (var i = 1; i <= (params.length); i++) {
                                                        queryparams.push('$' + i);
                                                    }
                                                    var sql = "delete from charity_plan where _id IN (\'" + params.join('\',\'') + "\')";
                                                    me.src.db.query(sql, null, function (e, data) {
                                                        if (data) {
                                                            //currently delete query returns nothing
                                                            next(itemCount, total, emptyCount);
                                                        } else {
                                                            next(itemCount, total, emptyCount);
                                                        }
                                                    });
                                                });
                                            }
                                            else {
                                                next(itemCount, total, emptyCount);
                                            }
                                        });
                                    }
                                    else if (me.user.user_type == me.config.partnerUser || me.user.user_type == 'B') {
                                        me.CharityPlanModel.dbCollection.find({
                                            partner_user_id: me.user.pid,
                                            plan_type: reqBody.planType,
                                            removed: '0'
                                        }, { _id: 1 }, function (err, data) {

                                            if (data && data.length > 0) {
                                                var params = [];
                                                data.prepEach(function (v, next) {
                                                    params.push(v._id);
                                                    next(v);
                                                }, function () {
                                                    me.CharityPlanModel.dbCollection.update({
                                                        _id: { $in: params }
                                                    }, {
                                                            $set: {
                                                                "plan_type": me.config.draftPlan
                                                            }
                                                        }, function (e, d) {
                                                            if (d) {
                                                                next(itemCount, total, emptyCount);
                                                            }
                                                            else {
                                                                return me.addErrorAndSend('ERR042');
                                                            }
                                                        }, { multi: true });
                                                });

                                            }
                                            else {
                                                next(itemCount, total, emptyCount);
                                            }
                                        });
                                    }
                                    else {
                                        return me.addErrorAndSend('ERR036');
                                    }
                                }
                            },
                            //begin statement
                            function (itemCount, total, emptyCount, next) {
                                DB_TRANSACTION.BEGIN(function () {
                                    next(itemCount, total, emptyCount);
                                });
                            },
                            //start writing records
                            function (itemCount, total, emptyCount, next) {
                                if (itemCount != 0) {
                                    me.CharityPlanModel.write({
                                        "_id": me.Util.getObjectId(),
                                        "partner_user_id": partnerUserId,
                                        "plan_name": reqBody.planName,
                                        "plan_type": reqBody.planType,
                                        "plan_creator_type": me.user.user_type
                                    }, function (data) {
                                        if (data.success) {
                                            tempObject = {};
                                            tempObject.partner_user_id = data.record.partner_user_id;
                                            tempObject.plan_type = data.record.plan_type;
                                            tempObject.plan_creator_type = data.record.plan_creator_type;
                                            tempObject.plan_name = data.record.plan_name;
                                            tempObject._id = data.record._id;
                                            next(tempObject, total, emptyCount);
                                        } else {
                                            return DB_TRANSACTION.ROLLBACK(data.error);
                                        }
                                    }, {
                                            add: 1,
                                            modify: 1
                                        });
                                }
                                else {
                                    return me.addErrorAndSend('ERR036');
                                }
                            },
                            function (foundData, total, emptyCount, next) {
                                if (foundData) {
                                    var balancePercentage = 0;
                                    //DO NOT DELETE
                                    //Percentage Balance Logic (Not enforced now, directly requesting 100 percent total from request)
                                    /*if (emptyCount != 0) {
                                        balancePercentage = (100 - total) / emptyCount;  //to allocate only for charities with no percentage assigned
                                    }
                                    else if(emptyCount == 0) {
                                        balancePercentage = (100 - total) / reqBody.charities.length; //to allocate to equally among all charities
                                    }*/
                                    //logic start
                                    reqBody.charities.prepEach(function (r, next) {
                                        var tempObject;
                                        //Percentage Balance Logic. DO NOT DELETE
                                        /*if(r.percentage==undefined){
                                            r.percentage=balancePercentage;
                                        }
                                        else{
                                            r.percentage=parseFloat(r.percentage)+parseFloat(balancePercentage);
                                        }*/
                                        me.DetailsModel.write({
                                            "_id": me.Util.getObjectId(),
                                            "charity_plan_id": foundData._id,
                                            "charity_foundation_id": r.charityId,
                                            "pgd_catalogue_id": r.itemId,
                                            "percentage": parseFloat(r.percentage) || 0
                                        }, function (data) {
                                            if (data.success) {
                                                if (!foundData.detailsPlan) {
                                                    foundData.detailsPlan = [];
                                                }
                                                tempObject = {};
                                                tempObject.itemId = data.record.pgd_catalogue_id;
                                                tempObject.charity_foundation_id = data.record.charity_foundation_id;
                                                tempObject.percentage = data.record.percentage;
                                                tempObject._id = data.record._id;
                                                foundData.detailsPlan.push(tempObject);
                                                next(r);
                                            } else {
                                                return DB_TRANSACTION.ROLLBACK(data.error);
                                            }
                                        }, {
                                                add: 1,
                                                modify: 1
                                            });
                                    }, function () {
                                        next(foundData);
                                    });
                                    //logic end
                                }
                            },
                            function (foundData) {
                                return DB_TRANSACTION.COMMIT(foundData);
                            }
                        ].runEach();

                    } else {
                        me.sendError();
                    }
                } else {
                    return me.addErrorObj(err);
                }
            }
        ].runEach();
    },
    /*
    @author : Vaibhav Vaidya
    @date : 6 July 2017
    @function : apiUpsertCharityPlan
    @desc : Upsert Charity Plan with Charity Details Included
    */
    apiUpsertCharityPlan: function (cb) {
        var me = this;
        if (!me.user) {
            me.sendError(401);
            return;
        }
        var reqBody = me.req.body.reqData;
        var CharityPlanModel = Ext.create('Crm.modules.charityPlan.model.CharityPlanModel', {
            scope: me
        });
        CharityPlanModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var UsersModel = Ext.create('Crm.modules.accountHolders.model.AccountHoldersModel', {
            scope: me
        });
        UsersModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var PartnersModel = Ext.create('Crm.modules.organizations.model.FoundModel', {
            scope: me
        });
        PartnersModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var DetailsModel = Ext.create('Crm.modules.charityPlanDetails.model.CharityPlanDetailsModel', {
            scope: me
        });
        DetailsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var FoundationModel = Ext.create('Crm.modules.organizations.model.FoundModel', {
            scope: me
        });
        FoundationModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var CharityItem = Ext.create('Crm.modules.catalogue.model.CatalogueModel', {
            scope: me
        });
        CharityItem.error = function (code, info) {
            return me.addErrorObj(code);
        };
        [
            function (err, client) {
                if (client) {
                    if (me.validateRequest([{
                        type: 'String',
                        field: "planType",
                        isRequired: false
                    },
                    {
                        type: 'ObjectID',
                        field: "charityPlanId",
                        isRequired: true
                    },
                    {
                        type: 'string',
                        field: "planName",
                        isRequired: false
                    },
                    {
                        type: 'ARRAY', field: 'charities', charities: [
                            {
                                type: 'ObjectID',
                                field: "charityId",
                                isRequired: true
                            },
                            {
                                type: 'string',
                                field: "itemId",
                                isRequired: false
                            },
                            {
                                type: 'float',
                                field: "percentage",
                                isRequired: true
                            }
                        ], isRequired: true
                    }
                    ])) {
                        [
                            function (next) {
                                if (me.user.user_type == me.config.endUser && reqBody.planType == me.config.emergencyPlan) {
                                    return me.addErrorAndSend('ERR045');
                                }
                                if (me.user.user_type == me.config.endUser) {
                                    CharityPlanModel.dbCollection.findOne({
                                        _id: reqBody.charityPlanId,
                                        partner_user_id: me.user._id,
                                        removed: '0'
                                    }, { _id: 1, plan_type: 1, partner_user_id: 1, plan_name: 1 }, function (err, data) {
                                        if (data) {
                                            next(data);
                                        } else {
                                            return me.addErrorAndSend('ERR033', me.user._id);
                                        }
                                    });
                                }
                                else if (me.user.user_type == me.config.partnerUser) {
                                    CharityPlanModel.dbCollection.findOne({
                                        _id: reqBody.charityPlanId,
                                        partner_user_id: me.user.pid,
                                        removed: '0'
                                    }, { _id: 1, plan_type: 1, partner_user_id: 1, plan_name: 1 }, function (err, data) {
                                        if (data) {
                                            next(data);
                                        } else {
                                            return me.addErrorAndSend('ERR033', me.user.pid);
                                        }
                                    });
                                }
                                else if (me.user.user_type == me.config.charityUser) {
                                    return me.addErrorAndSend('ERR047');
                                }
                            },
                            function (foundData, next) {
                                var total = 0;
                                if (reqBody.planType == me.config.draftPlan || reqBody.planType == 'D') {
                                    //If shifting to draft or discarded, check Partner should have Emergency+ Active. User should have Active.
                                    CharityPlanModel.dbCollection.find({
                                        partner_user_id: me.user._id,
                                        plan_type: { $in: [foundData.plan_type] },
                                        _id: { $nin: [reqBody.charityPlanId] }
                                    }, {}, function (err, data) {
                                        if (data.length == 1) {
                                            next(foundData);
                                        } else {
                                            return me.addErrorAndSend('ERR046', reqBody.charityPlanId);
                                        }
                                    });
                                }
                                else {
                                    next(foundData);
                                }
                            },
                            function (foundData, next) {
                                var total = 0;
                                if (reqBody.charities.length > 0) {
                                    //logic for adding up total for new charities and updated charities
                                    for (var i = 0; i < reqBody.charities.length; i++) {
                                        total += parseFloat(reqBody.charities[i].percentage);
                                    }
                                    if ((Math.round(total) != 100)) {
                                        return me.addErrorAndSend('ERR034');
                                    }
                                    else {
                                        next(foundData);
                                    }
                                }
                                else {
                                    return me.addErrorAndSend('ERR036');
                                }
                            },
                            //logic for emergency charity plan should have unlimited charities items in it only
                            function (foundData, next) {
                                if (reqBody.planType == me.config.emergencyPlan || reqBody.planType == undefined && foundData.plan_type == me.config.emergencyPlan) {
                                    reqBody.charities.prepEach(function (r, next) {
                                        CharityItem.dbCollection.findOne({
                                            _id: r.itemId,
                                            item_type: me.config.unlimitedCharity,
                                            pay_now: 1
                                        }, {}, function (err, data) {
                                            if (data) {
                                                if (parseInt(data.pay_now) == 1) {
                                                    next(r);
                                                }
                                                else {
                                                    return me.addErrorAndSend('ERR037', r.itemId);
                                                }
                                            }
                                            else {
                                                return me.addErrorAndSend('ERR037', r.itemId);
                                            }
                                        });
                                    }, function () {
                                        next(foundData);
                                    });
                                }
                                else {
                                    next(foundData);
                                }
                            },
                            function (foundData, next) {
                                var userId;
                                //Check for Active Plan
                                if (reqBody.planType == me.config.activePlan) {
                                    if (me.user.user_type == me.config.endUser) {
                                        userId = me.user._id;
                                        CharityPlanModel.dbCollection.find({
                                            partner_user_id: userId,
                                            plan_type: me.config.activePlan,
                                            removed: '0'
                                        }, { _id: 1 }, function (err, data) {
                                            if (data && data.length > 0) {
                                                var params = [];
                                                data.prepEach(function (v, next) {
                                                    params.push(v._id);
                                                    next(v);
                                                }, function () {
                                                });
                                                DetailsModel.dbCollection.find({
                                                    charity_plan_id: { $in: params },
                                                }, { _id: 1 }, function (err, indata) {
                                                    if (indata && indata.length > 0) {
                                                        params = [];
                                                        indata.prepEach(function (t, next) {
                                                            params.push(t._id);
                                                            next(t);
                                                        }, function () {
                                                            DetailsModel.removeAction = 'delete';
                                                            DetailsModel.remove(params, function (inres) {
                                                                if (inres.success == true) {
                                                                    var queryparams = [];
                                                                    for (var i = 1; i <= (params.length); i++) {
                                                                        queryparams.push('$' + i);
                                                                    }
                                                                    var sql = "delete from charity_plan where _id IN (\'" + params.join('\',\'') + "\')";
                                                                    me.src.db.query(sql, null, function (e, data) {
                                                                        if (data) {
                                                                            //currently delete query returns nothing
                                                                            next(foundData);
                                                                        } else {
                                                                            next(foundData);
                                                                        }
                                                                    });
                                                                }
                                                                else {
                                                                }
                                                            });
                                                        });
                                                    }
                                                    else {
                                                        var queryparams = [];
                                                        for (var i = 1; i <= (params.length); i++) {
                                                            queryparams.push('$' + i);
                                                        }
                                                        var sql = "delete from charity_plan where _id IN (\'" + params.join('\',\'') + "\')";
                                                        me.src.db.query(sql, null, function (e, data) {
                                                            if (data) {
                                                                //currently delete query returns nothing
                                                                next(foundData);
                                                            } else {
                                                                next(foundData);
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                            else {
                                                next(foundData);
                                            }
                                        });
                                    }
                                    else {
                                        userId = me.user.pid;
                                        CharityPlanModel.dbCollection.find({
                                            partner_user_id: userId,
                                            plan_type: reqBody.planType,
                                            removed: '0'
                                        }, { _id: 1 }, function (err, data) {
                                            if (data && data.length > 0) {
                                                var params = [];
                                                data.prepEach(function (v, next) {
                                                    params.push(v._id);
                                                    next(v);
                                                }, function () {
                                                    CharityPlanModel.dbCollection.update({
                                                        _id: { $in: params }
                                                    }, {
                                                            $set: {
                                                                "plan_type": me.config.draftPlan
                                                            }
                                                        }, function (e, d) {
                                                            if (d) {
                                                                next(foundData);
                                                            }
                                                            else {
                                                                return me.addErrorAndSend('ERR042');
                                                            }
                                                        }, { multi: true });
                                                });

                                            }
                                            else {
                                                next(itemCount, total, emptyCount);
                                            }
                                        });
                                    }

                                }
                                else {
                                    next(foundData);
                                }
                            },
                            function (foundData, next) {
                                //Check for Emergency Plan
                                if (reqBody.planType == 'E') {
                                    {
                                        var sql = "Update charity_plan set plan_type='T' where partner_user_id=$1 and plan_type='E'";
                                        me.src.db.query(sql, [foundData.partner_user_id], function (e, data) {
                                            if (data) {
                                                //currently update query returns nothing
                                                next(foundData);
                                            } else {
                                                next(foundData);
                                            }
                                        });
                                    }
                                }
                                else {
                                    next(foundData);
                                }
                            },
                            //change follow if requester is a User changing his charity plan
                            function (foundData, next) {
                                if (me.user.user_type == me.user.endUser) {
                                    var sql = "Update users set follow=null where _id=$1";
                                    me.src.db.query(sql, [me.user._id], function (e, data) {
                                        if (data) {
                                            //currently update query returns nothing
                                            next(foundData);
                                        } else {
                                            next(foundData);
                                        }
                                    });
                                }
                                else {
                                    next(foundData);
                                }
                            },
                            function (foundData, next) {
                                var tempObject;
                                CharityPlanModel.write({
                                    "_id": reqBody.charityPlanId,
                                    "partner_user_id": foundData.partner_user_id,
                                    "plan_name": reqBody.planName || foundData.plan_name,
                                    "plan_type": reqBody.planType || foundData.plan_type,
                                    "plan_creator_type": me.user.user_type
                                }, function (data) {
                                    if (data.success) {
                                        tempObject = {};
                                        tempObject.partner_user_id = data.record.partner_user_id;
                                        tempObject.plan_type = data.record.plan_type;
                                        tempObject.plan_name = data.record.plan_name;
                                        tempObject.plan_creator_type = data.record.plan_creator_type;
                                        tempObject._id = data.record._id;
                                        next(tempObject);
                                    } else {
                                        return me.addErrorObj(data.error);
                                    }
                                }, {
                                        add: 1,
                                        modify: 1
                                    });
                            },
                            //Details start
                            function (foundData, next) {
                                var tempObject;
                                var value =reqBody.charityPlanId;
                                var sql = "delete from charity_plan_details where charity_plan_id = $1";
                                me.src.db.query(sql, [value], function (e, data) {
                                    reqBody.charities.prepEach(function (r, next) {
                                        DetailsModel.write({
                                            "_id": me.Util.getObjectId(),
                                            "charity_plan_id": reqBody.charityPlanId,
                                            "charity_foundation_id": r.charityId,
                                            "pgd_catalogue_id": r.itemId,
                                            "percentage": parseFloat(r.percentage) || 0
                                        }, function (data) {
                                            if (data.success) {
                                                if (!foundData.detailsPlan) {
                                                    foundData.detailsPlan = [];
                                                }
                                                tempObject = {};
                                                tempObject.charity_foundation_id = data.record.charity_foundation_id;
                                                tempObject.percentage = data.record.percentage;
                                                tempObject._id = data.record._id;
                                                foundData.detailsPlan.push(tempObject);
                                                next(r);
                                            } else {
                                                return me.addErrorObj(data.error);
                                            }
                                        }, {
                                                add: 1,
                                                modify: 1
                                            });
                                    }, function () {
                                        me.sendData(foundData);
                                    });
                                });
                              
                                //Logic for upsert percentage calculation and balancing. DO NOT DELETE
                                /*var total = 0, charitylist = [], emptyCount = 0, balancePercentage = 0, insertTotal = 0;
                                if (reqBody.charities.length > 0) {
                                    
                                        //logic for adding up total for new charities and updated charities
                                        for (var i = 0; i < reqBody.charities.length; i++) {
                                            if (charitylist.indexOf(reqBody.charities[i].charityId == -1)) {
                                                charitylist.push(reqBody.charities[i].charityId)
                                            }
                                            if (reqBody.charities[i].percentage == undefined) {
                                                emptyCount++;
                                            }
                                            else {
                                                insertTotal += parseFloat(reqBody.charities[i].percentage);
                                            }
                                        }
                                    
                                    DetailsModel.dbCollection.find({
                                        charity_plan_id: reqBody.charityPlanId,
                                    }, {}, function (err, data) {
                                        if (data) {
                                            //logic for finding percentage total of charities that will not be updated
                                            for (var j = 0; j < data.length; j++) {
                                                if (charitylist.indexOf(data[j].charity_foundation_id) == -1) {
                                                    total += parseFloat(data[j].percentage);
                                                }
                                            }
                                            if ((total + insertTotal) > 100) {
                                                return me.addErrorAndSend('ERR034', reqBody.percentage);
                                            }
                                            if (emptyCount != 0) {
                                                balancePercentage = ((100 - total) / emptyCount);
                                            }
                                            reqBody.charities.prepEach(function (r, next) {
                                                r.upserted = false
                                                if (r.percentage == undefined) {
                                                    r.percentage = balancePercentage;
                                                }
                                                data.prepEach(function (d, next) {
                                                    if (d.charity_foundation_id == r.charityId) {
                                                        DetailsModel.write({
                                                            "_id": d._id,
                                                            "charity_plan_id": reqBody.charityPlanId,
                                                            "charity_foundation_id": r.charityId,
                                                            "percentage": parseFloat(r.percentage) || 0
                                                        }, function (data) {
                                                            if (data.success) {
                                                                r.upserted = true;
                                                                next(d);
                                                            } else {
                                                                return me.addErrorObj(data.error);
                                                            }
                                                        }, {
                                                                add: 1,
                                                                modify: 1
                                                            });
                                                    }
                                                    else{
                                                        next(d);
                                                    }
                                                }, function () {
                                                    if (r.upserted == false) {
                                                    DetailsModel.write({
                                                        "_id": me.Util.getObjectId(),
                                                        "charity_plan_id": reqBody.charityPlanId,
                                                        "charity_foundation_id": r.charityId,
                                                        "percentage": parseFloat(r.percentage) || 0
                                                    }, function (data) {
                                                        if (data.success) {
                                                            r.upserted = true;
                                                            next(r);
                                                        } else {
                                                            return me.addErrorObj(data.error);
                                                        }
                                                    }, {
                                                            add: 1,
                                                            modify: 1
                                                        });
                                                }
                                                    else {
                                                        next(r);
                                                    }
                                                });
                                                
                                            }, function () {
                                                next(foundData);
                                            });
                                        } else {
                                            return me.addErrorAndSend('ERR033', reqBody.charityPlanId);
                                        }
                                    });
                                }*/
                            },
                            //Details end
                            function (data) {
                                me.sendData(data);
                            }
                        ].runEach();

                    } else {
                        me.sendError();
                    }
                } else {
                    return me.addErrorObj(err);
                }
            }
        ].runEach();
    },
    apiRetrieveCharityPlan: function (cb) {
        var me = this;
        var params = {};
        if (!me.user) {
            me.sendError(401);
            return;
        }
        var reqBody = me.req.body.reqData;
        var reqHeader = me.req.body.header;
        var followingPartner=null;
        var CharityPlanModel = Ext.create('Crm.modules.charityPlan.model.CharityPlanModel', {
            scope: me
        });
        CharityPlanModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var DetailsModel = Ext.create('Crm.modules.charityPlanDetails.model.CharityPlanDetailsModel', {
            scope: me
        });
        DetailsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        [
            function(next){
                if(me.user && me.user.user_type == me.config.endUser
                    && me.user.follow
                    && (me.user.follow !=null ||me.user.follow !="" || me.user.follow !="0" ))
                    {
                        var sql="select name from pgd_foundations where _id in (select follow from users where _id=$1)";
                        me.src.db.query(sql, [me.user._id], function (e, data) {
                            if (data && data.length >0) {
                                followingPartner=data[0].name;
                                next();
                            } else {
                                next();
                            }              
                        });   
                    }
                else{
                    next();
                }
            },
            // get charity plans 
            function (next) {
                var skip = reqBody.startIndex && /^[0-9]{1,}$/.test(reqBody.startIndex + '') ? parseInt(reqBody.startIndex) : 0;
                var limit = reqBody.numOfRecords && /^[0-9]{1,}$/.test(reqBody.numOfRecords + '') ? parseInt(reqBody.numOfRecords) : 50;
                if (!params.filters) params.filters = [];
                if (me.user.pid && reqHeader.srcInd == "Corp" || me.user.user_type == "P") {
                    params.filters.push({ value: me.user.pid, property: 'partner_user_id' });
                 //   params.filters.push({ value: ["A", "T"], property: 'plan_type', operator: 'in' });
                } else if (me.user && me.user._id) {
                    params.filters.push({ value: me.user._id, property: 'partner_user_id' });
                    params.filters.push({ value: "A", property: 'plan_type' });
                } else {
                    return me.addErrorAndSend(401);
                }
                params._start = skip;
                params._limit = limit;
                CharityPlanModel.getData(params, function (res) {
                    if (res) {
                      
                        next(res);
                    }
                });
            },
            //get charity plans detaisl
            function (data) {
                var params = {}, infoparams={};
                if (data) {
                    data.list.prepEach(function (r, next) {
                        r.followingPartner=followingPartner;
                        params.filters = [];
                        params.filters.push({ value: r._id, property: 'charity_plan_id' });
                        var charityFoundationId = -1;
                        var charityItemId = -1;
                        for (var i = 0; i < DetailsModel.fields.length; i++) {
                            if (DetailsModel.fields[i].name == "charity_foundation_id") {
                                charityFoundationId = i;
                            }
                            if (DetailsModel.fields[i].name == "pgd_catalogue_id") {
                                charityItemId = i;
                            }
                        }
                        DetailsModel.fields[charityFoundationId].bindTo = { collection: 'pgd_foundations', keyField: '_id', fields: { name: 1 ,logo_url:1} };
                        DetailsModel.fields[charityItemId].bindTo = { collection: 'pgd_catalogue', keyField: '_id', fields: { name: 1, tags: 1, catalogue_urls: 1 , donation_amount_needed:1, donation_amount_collected:1, item_type:1, pay_now:1} };

                        DetailsModel.getData(params, function (res) {
                            if (res.list && res.list.length > 0) {
                                res.list.prepEach(function (k, next) {
                                    if (k.pgd_catalogue_id && k.pgd_catalogue_id != null) {
                                            //Vaibhav Vaidya, 8 Sept 2017. Add item obj
                                            //infoparams.filters=[];
                                            //infoparams.fieldSet=['donation_amount_needed','donation_amount_collected','item_type','pay_now','_id'];
                                            //infoparams.filters.push({property:'_id', operator:'eq', value:k.pgd_catalogue_id.iteminfo})
                                            if(typeof k.pgd_catalogue_id=="object") me.formatCatalogItemInfo(k.pgd_catalogue_id);
                                            if (k.pay_now != undefined && typeof k.pay_now == "boolean") {
                                                k.iteminfo.pay_now = k.iteminfo.pay_now ? 1 : 0
                                                delete k.pay_now;
                                            }
                                            k = me.splitTagsUrls(k);
                                            next(k);
                                            //End Edit by Vaibhav Vaidya                                        
                                    }
                                    else {
                                        next(k);
                                    }
                                }, function () {
                                    r.detailsPlan = res;
                                    next(r);
                                });
                            }
                            else {
                                r.detailsPlan = res;
                                next(r);
                            }
                        });
                    }, function () {
                        me.sendData(data);
                    });
                }
            }
        ].runEach();
    },
    apiRetrieveAllCharityPlan: function (cb) {
        var me = this;
        var params = {};
        var myPlans={};
        var followingPartner=null;
        if (!me.user) {
            me.sendError(401);
            return;
        }
        var reqBody = me.req.body.reqData;
        var reqHeader = me.req.body.header;
        var FoundationModel = Ext.create('Crm.modules.organizations.model.FoundModel', {
            scope: me
        });
        FoundationModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var CharityPlanModel = Ext.create('Crm.modules.charityPlan.model.CharityPlanModel', {
            scope: me
        });
        CharityPlanModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        var DetailsModel = Ext.create('Crm.modules.charityPlanDetails.model.CharityPlanDetailsModel', {
            scope: me
        });
        DetailsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        [
            function(next){
                if(me.user && me.user.user_type == me.config.endUser && me.user.follow && (me.user.follow !=null ||me.user.follow !="" || me.user.follow !="0" ))
                    {
                        var sql="select name from pgd_foundations where _id in (select follow from users where _id=$1)";
                        me.src.db.query(sql, [me.user._id], function (e, data) {
                            if (data && data.length >0) {
                                followingPartner=data[0].name;
                                next();
                            } else {
                                next();
                            }              
                        });   
                    }
            },
            function (next) {
                var FoundationId = -1;
                params.filters = [];
                params.fieldSet = null;
                //Kaustubh 12-09-2017
          
                if(me.user && me.user.user_type == me.config.endUser && me.user.follow && (me.user.follow !=null ||me.user.follow !="" || me.user.follow !="0" ))
                    {       
                        params.filters.push({ value: me.user._id, property: 'partner_user_id', operator: 'eq' });
                        params.filters.push({ value: ["A"], property: 'plan_type', operator: 'in' });
                        for (var i = 0; i < CharityPlanModel.fields.length; i++) {
                            if (CharityPlanModel.fields[i].name == "partner_user_id") {
                                FoundationId = i;
                            }
                        }
                        CharityPlanModel.fields[FoundationId].bindTo = { collection: 'pgd_foundations', keyField: '_id', fields: { name: 1,url:1 } };                        
                        CharityPlanModel.getData(params, function (data) {
                            if (data) {
                                var params = {},infoparams={};
                                var charityFoundationId = -1;
                                var charityItemId = -1;
                                for (var i = 0; i < DetailsModel.fields.length; i++) {
                                    if (DetailsModel.fields[i].name == "charity_foundation_id") {
                                        charityFoundationId = i;
                                    }
                                    if (DetailsModel.fields[i].name == "pgd_catalogue_id") {
                                        charityItemId = i;
                                    }
        
                                }
                                DetailsModel.fields[charityFoundationId].bindTo = { collection: 'pgd_foundations', keyField: '_id', fields: { name: 1 ,logo_url:1} };
                                DetailsModel.fields[charityItemId].bindTo = { collection: 'pgd_catalogue', keyField: '_id', fields: { name: 1, tags: 1, catalogue_urls: 1 , donation_amount_needed:1, donation_amount_collected:1, item_type:1, pay_now:1} };
                                data.list.prepEach(function (r, next) {
                                    r.followingPartner=followingPartner;
                                    params.filters = [];
                                    params.filters.push({ value: r._id, property: 'charity_plan_id' });
                                      DetailsModel.getData(params, function (res) {
                                        if (res.list && res.list.length > 0) {
                                            res.list.prepEach(function (k, next) {
                                                if (k.pgd_catalogue_id && k.pgd_catalogue_id != null) {
                                                    //Vaibhav Vaidya, 8 Sept 2017. Add item obj
                                                    //infoparams.filters=[];
                                                    //infoparams.fieldSet=['donation_amount_needed','donation_amount_collected','item_type','pay_now','_id'];
                                                    //infoparams.filters.push({property:'_id', operator:'eq', value:k.pgd_catalogue_id.iteminfo})
                                                    if(typeof k.pgd_catalogue_id == "object") me.formatCatalogItemInfo(k.pgd_catalogue_id);
                                                    if (k.pay_now != undefined && typeof k.pay_now == "boolean") {
                                                        k.iteminfo.pay_now = k.iteminfo.pay_now ? 1 : 0
                                                        delete k.pay_now;
                                                    }
                                                    k = me.splitTagsUrls(k);
                                                    next(k);
                                                    //End Edit by Vaibhav Vaidya
                                                }
                                                else {
                                                    k = me.splitTagsUrls(k);
                                                    next(k);
                                                }
                                            }, function () {
                                                r.detailsPlan = res;
                                                next(r);
                                            });
                                        }
                                        else {
                                            r.detailsPlan = res;
                                            next(r);
                                        }
                                    });
                                }, function () {
                                    myPlans=data;
                                   next();
                                });
                            }
                        });
                }
                else
                {
                next();
                }
            },
            // get charity plans 
            function (next) {
                var skip = reqBody.startIndex && /^[0-9]{1,}$/.test(reqBody.startIndex + '') ? parseInt(reqBody.startIndex) : 0;
                var limit = reqBody.numOfRecords && /^[0-9]{1,}$/.test(reqBody.numOfRecords + '') ? parseInt(reqBody.numOfRecords) : 50;
                if (!params.filters) params.filters = [];
                // if (me.user.pid && reqHeader.srcInd == "Corp" || me.user.user_type == "P") {
                //     params.filters.push({ value: me.user.pid, property: 'partner_user_id' });
                //     params.filters.push({ value: ["A", "T"], property: 'plan_type', operator: 'in' });
                // } else if (me.user && me.user._id) {
                //     params.filters.push({ value: me.user._id, property: 'partner_user_id' });
                //     params.filters.push({ value: "A", property: 'plan_type' });
                // } else {
                //     return me.addErrorAndSend(401);
                // }
                
                params.fieldSet = ['_id'];
                params.filters.push({ value: 2, property: 'orgtype' });
                FoundationModel.getData(params, function (res) {
                    if (res) {
                        var arr = [];
                        res.list.prepEach(function (v, next) {
                            arr.push(v._id);
                            next(v);
                        }, function () {
                        });
                        params.filters = [];
                        params.fieldSet = null;
                        params._start = skip;
                        params._limit = limit;
                        // var following=[];
                        // following.push(me.user.follow);
                        if(me.user && me.user.follow && me.user.follow!=null && me.user.follow!="0")
                        {
                        params.filters.push({ value: me.user.follow, property: 'partner_user_id', operator: 'neq' }); 
                        }
                        params.filters.push({ value: arr, property: 'partner_user_id', operator: 'in' });
                        params.filters.push({ value: ["A"], property: 'plan_type', operator: 'in' });
                        var FoundationId = -1;
                        for (var i = 0; i < CharityPlanModel.fields.length; i++) {
                            if (CharityPlanModel.fields[i].name == "partner_user_id") {
                                FoundationId = i;
                            }
                        }
                        CharityPlanModel.fields[FoundationId].bindTo = { collection: 'pgd_foundations', keyField: '_id', fields: { name: 1,url:1 } };                        
                        CharityPlanModel.getData(params, function (data) {
                            if (data) {
                                next(data);
                            }
                        });
                    }
                });
            },
            //get charity plans detaisl
            function (data) {
                var params = {},infoparams={};
                if (data) {
                    data.list.prepEach(function (r, next) {
                        params.filters = [];
                        params.filters.push({ value: r._id, property: 'charity_plan_id' });
                        var charityFoundationId = -1;
                        var charityItemId = -1;
                        for (var i = 0; i < DetailsModel.fields.length; i++) {
                            if (DetailsModel.fields[i].name == "charity_foundation_id") {
                                charityFoundationId = i;
                            }
                            if (DetailsModel.fields[i].name == "pgd_catalogue_id") {
                                charityItemId = i;
                            }

                        }
                        DetailsModel.fields[charityFoundationId].bindTo = { collection: 'pgd_foundations', keyField: '_id', fields: { name: 1 ,logo_url:1} };
                        DetailsModel.fields[charityItemId].bindTo = { collection: 'pgd_catalogue_publ', keyField: '_id', fields: { name: 1, tags: 1, catalogue_urls: 1 , donation_amount_needed:1, donation_amount_collected:1, item_type:1, pay_now:1} };
                        DetailsModel.getData(params, function (res) {
                            if (res.list && res.list.length > 0) {
                                res.list.prepEach(function (k, next) {
                                    if (k.pgd_catalogue_id && k.pgd_catalogue_id != null) {
                                        //Vaibhav Vaidya, 8 Sept 2017. Add item obj
                                        //infoparams.filters=[];
                                        //infoparams.fieldSet=['donation_amount_needed','donation_amount_collected','item_type','pay_now','_id'];
                                        //infoparams.filters.push({property:'_id', operator:'eq', value:k.pgd_catalogue_id.iteminfo})
                                        if(typeof k.pgd_catalogue_id == "object") me.formatCatalogItemInfo(k.pgd_catalogue_id);
                                        if (k.iteminfo && k.iteminfo.pay_now != undefined && typeof k.iteminfo.pay_now == "boolean") { k.iteminfo.pay_now = k.iteminfo.pay_now ? 1 : 0 }
                                        k = me.splitTagsUrls(k);
                                        next(k);
                                        //End Edit by Vaibhav Vaidya
                                    }
                                    else {
                                        next(k);
                                    }
                                }, function () {
                                    r.detailsPlan = res;
                                    next(r);
                                });
                            }
                            else {
                                r.detailsPlan = res;
                                next(r);
                            }
                        });
                    }, function () {
                        var partnersPlan={};
                        partnersPlan=data;
                        var response={};
                        response.myPlans=myPlans;
                        response.partnersPlan=partnersPlan;
                        me.sendData(response);
                    });
                }
            }
        ].runEach();
    },

    splitTagsUrls: function(k){
        if (k.pgd_catalogue_id.tags && typeof k.pgd_catalogue_id.tags == "string") {
            k.pgd_catalogue_id.tags = k.pgd_catalogue_id.tags.split(",");
        }
        if (k.pgd_catalogue_id.catalogue_urls && typeof k.pgd_catalogue_id.catalogue_urls == "string") {
            k.pgd_catalogue_id.catalogue_urls = k.pgd_catalogue_id.catalogue_urls.split(",");
        }
        if (k.pgd_catalogue_id && k.pgd_catalogue_id != null) {
            return(k);
        }
    },
    
    //Vaibhav Vaidya, 11th October, formatCatalogItemInfo, Used to restructure elements inside pgd_catalogue into their own item object
    formatCatalogItemInfo: function(data){
        if(data.iteminfo==undefined) data.iteminfo={};
        data.iteminfo.item_type=data.item_type;
        data.iteminfo.donation_amount_needed=data.donation_amount_needed;
        data.iteminfo.donation_amount_collected=data.donation_amount_collected;
        data.iteminfo.pay_now=data.pay_now;
        data.iteminfo.priority=data.priority;
        if(data.item_type !=undefined){
            delete data.item_type;
        }
        if(data.donation_amount_needed !=undefined){
            delete data.donation_amount_needed;
        }
        if(data.donation_amount_collected !=undefined){
            delete data.donation_amount_collected;
        }
        if(data.pay_now !=undefined){
            delete data.pay_now;
        }
        if(data.priority !=undefined){
            delete data.priority;
        }
        return;
    }
});



