Ext.define('Crm.REST.charity.FollowCharityPlan', {
    extend: "Crm.REST.RestBaseController",
    constructor: function () {
        this.callParent(arguments);
    },
    /*
     @author : Kaustubh Thube
     @date : 10 July 2017
     @function : apiFollowCharityPlan
     @desc : follow chairty plan API 
     */
    apiFollowCharityPlan: function (cb) {
        var me = this;
        if (!me.user) {
            me.addErrorAndSend(401);
            return;
        }

        var CharityPlan = Ext.create('Crm.modules.charityPlan.model.CharityPlanModel', {
            scope: me
        });
        CharityPlan.error = function (code, info) {
            return me.addErrorObj(code);
        };

        var CharityPlanDetails = Ext.create('Crm.modules.charityPlanDetails.model.CharityPlanDetailsModel', {
            scope: me
        });
        CharityPlanDetails.error = function (code, info) {
            return me.addErrorObj(code);
        };

        var Users = Ext.create('Crm.modules.accountHolders.model.AccountHoldersModel', {
            scope: me
        });
        Users.error = function (code, info) {
            return me.addErrorObj(code);
        };

        var partners = Ext.create('Crm.modules.organizations.model.FoundModel', {
            scope: me
        });
        partners.error = function (code, info) {
            return me.addErrorObj(code);
        };

        var DetailsModel = Ext.create('Crm.modules.charityPlanDetails.model.CharityPlanDetailsModel', {
            scope: me
        });
        DetailsModel.error = function (code, info) {
            return me.addErrorObj(code);
        };
        if (!me.validateRequest([
            {
                type: 'STRING',
                field: "partner_id",
                isRequired: true,
            }
        ])) {
            return me.sendError();
        }
        var charityPlanIds = [];
        //find if user exists with specified user id in request
        Users.dbCollection.findOne({
            _id: me.user._id
        }, { _id: 1 }, function (e, data) {
            if (data && data._id) {
                //find if partner exists with specified plan_id in request 
                partners.dbCollection.findOne({
                    _id: me.req.body.reqData.partner_id,
                }, { _id: 1 }, function (e, data) {
                    if (data && data._id) {
                        //request is valid since both user && plan id exist so proceed
                        //check if there is charityPlan already exist for that user 
                        CharityPlan.dbCollection.find({
                            //_id:params.plan_id,
                            partner_user_id: me.user._id,
                            //plan_type: { $in: ["A", "E"] }
                            plan_type:me.config.activePlan
                        }, {}, function (e, indata) {
                            if (indata && indata.length > 0) {
                                //this means there is already plan for user
                                //set this plan type to D i.e draft
                                params = [];
                                indata.prepEach(function (t, next) {
                                    params.push(t._id);
                                    next(t);
                                }, function () {
                                    DetailsModel.dbCollection.find({
                                        charity_plan_id: { $in: params },
                                    }, { _id: 1 }, function (err, data) {
                                        if (data && data.length > 0) {
                                            charityPlanDetailsIds = [];
                                            data.prepEach(function (t, next) {
                                                charityPlanDetailsIds.push(t._id);
                                                next(t);
                                            }, function () {
                                                DetailsModel.removeAction = 'delete';
                                                DetailsModel.remove(charityPlanDetailsIds, function (inres) {
                                                    if (inres.success == true) {
                                                        var queryparams = [];
                                                        for (var i = 1; i <= (params.length); i++) {
                                                            queryparams.push('$' + i);
                                                        }
                                                        var sql = "delete from charity_plan where _id IN (\'" + params.join('\',\'') + "\')";
                                                        me.src.db.query(sql, null, function (e, data) {
                                                            if (data) {
                                                                CharityPlan.dbCollection.findOne({
                                                                    //_id:params.plan_id,
                                                                    partner_user_id: me.req.body.reqData.partner_id,
                                                                    //  plan_type: {$in:["A","E"]}
                                                                    plan_type: "A"
                                                                }, {}, function (e, data) {
                                                                    if (data && data._id) {
                                                                        var planId = data._id;
                                                                        CharityPlan.write({
                                                                            "_id": me.Util.getObjectId(),
                                                                            "plan_name":data.plan_name,
                                                                            "partner_user_id": me.user._id,
                                                                            //"users_id": params.user_id,
                                                                            "plan_type": data.plan_type,
                                                                            "plan_creator_type": me.config.endUser,
                                                                            "removed": 0
                                                                        }, function (data) {
                                                                            if (data.success) {
                                                                                var userCharityPlanId=data.record._id;
                                                                                CharityPlanDetails.dbCollection.find({
                                                                                   charity_plan_id: planId
                                                                                }, {}, function (e, data) {
                                                                                    if (data && data.length > 0) {
                                                                                        data.prepEach(function (r, next)
                                                                                        {
                                                                                            CharityPlanDetails.write({
                                                                                            "_id": me.Util.getObjectId(),
                                                                                            "charity_plan_id": userCharityPlanId,
                                                                                            //"users_id": params.user_id,
                                                                                            "pgd_catalogue_id":r.pgd_catalogue_id,
                                                                                            "charity_foundation_id": r.charity_foundation_id,
                                                                                            "percentage": parseFloat(r.percentage),
                                                                                            "removed": 0
                                                                                            }, function (data) {
                                                                                            if (data.success) {
                                                                                                next(r);
                                                                                                   } else {
                                                                                                return me.addErrorObj(data.error);
                                                                                            }
                                                                                              }, {
                                                                                                add: 1,
                                                                                                modify: 1
                                                                                            });
                                                                                        },function () {
                                                                                            Users.dbCollection.update({
                                                                                            _id: me.user._id
                                                                                            }, {
                                                                                                    $set: {
                                                                                                        follow: me.req.body.reqData.partner_id,
                                                                                                    }
                                                                                            }, function (e, d) {me.sendData([]);});
                                                                                            
                                                                                        });
                                                                                    }
                                                                                    else {
                                                                                        return me.addErrorObj('ERR043');
                                                                                    }
                                                                                });
                                                                            } else {
                                                                                return me.addErrorObj(data.error);
                                                                            }
                                                                        }, {
                                                                                add: 1,
                                                                                modify: 1
                                                                            });
                    
                                                                    }
                                                                    else {
                                                                        return me.addErrorObj('ERR043');
                                                                    }
                                                                });
                                                            } else {
                                                            }
                                                        });
                                                    }
                                                    else {
                                                    }
                                                });
                                            });
                                        }
                                        else {
                                            
                                            var sql = "delete from charity_plan where _id IN (\'" + params.join('\',\'') + "\')";
                                            me.src.db.query(sql, null, function (e, data) {
                                                if (data) {
                                                    CharityPlan.dbCollection.findOne({
                                                        //_id:params.plan_id,
                                                        partner_user_id: me.req.body.reqData.partner_id,
                                                        //  plan_type: {$in:["A","E"]}
                                                        plan_type: me.config.activePlan
                                                    }, {}, function (e, data) {
                                                        if (data && data._id) {
                                                            var planId = data._id;
                                                            CharityPlan.write({
                                                                "_id": me.Util.getObjectId(),
                                                                "plan_name":data.plan_name,
                                                                "partner_user_id": me.user._id,
                                                                //"users_id": params.user_id,
                                                                "plan_type": data.plan_type,
                                                                "plan_creator_type": me.config.endUser ,
                                                                "removed": 0
                                                            }, function (data) {
                                                                if (data.success) {
                                                                    var userCharityPlanId=data.record._id;
                                                                    CharityPlanDetails.dbCollection.find({
                                                                       charity_plan_id: planId
                                                                    }, {}, function (e, data) {
                                                                        if (data && data.length > 0) {
                                                                            data.prepEach(function (r, next)
                                                                            {
                                                                                CharityPlanDetails.write({
                                                                                "_id": me.Util.getObjectId(),
                                                                                "charity_plan_id": userCharityPlanId,
                                                                                //"users_id": params.user_id,
                                                                                "pgd_catalogue_id":r.pgd_catalogue_id,
                                                                                "charity_foundation_id": r.charity_foundation_id,
                                                                                "percentage": parseFloat(r.percentage),
                                                                                "removed": 0
                                                                                }, function (data) {
                                                                                if (data.success) {
                                                                                    next(r);
                                                                                       } else {
                                                                                    return me.addErrorObj(data.error);
                                                                                }
                                                                                  }, {
                                                                                    add: 1,
                                                                                    modify: 1
                                                                                });
                                                                            },function () {
                                                                                Users.dbCollection.update({
                                                                                _id: me.user._id
                                                                                }, {
                                                                                        $set: {
                                                                                            follow: me.req.body.reqData.partner_id,
                                                                                        }
                                                                                }, function (e, d) {me.sendData([]);});
                                                                                
                                                                            });
                                                                        }
                                                                        else {
                                                                            return me.addErrorObj('ERR043');
                                                                        }
                                                                    });
                                                                } else {
                                                                    return me.addErrorObj(data.error);
                                                                }
                                                            }, {
                                                                    add: 1,
                                                                    modify: 1
                                                                });
        
                                                        }
                                                        else {
                                                            return me.addErrorObj('ERR043');
                                                        }
                                                    });
                                                } else {
                                                }
                                            });
                                        }
                                    });
                                });
                               
                            }
                            //this means we need to add new plan entry for this user
                            else {
                                CharityPlan.dbCollection.findOne({
                                    //_id:params.plan_id,
                                    partner_user_id: me.req.body.reqData.partner_id,
                                   // plan_type: { $in: ["A", "E"] }
                                   plan_type: me.config.activePlan
                                }, {}, function (e, data) {
                                     if (data && data._id) {
                                                    var planId = data._id;
                                                    CharityPlan.write({
                                                        "_id": me.Util.getObjectId(),
                                                        "plan_name":data.plan_name,
                                                        "partner_user_id": me.user._id,
                                                        //"users_id": params.user_id,
                                                        "plan_type": data.plan_type,
                                                        "plan_creator_type": me.config.endUser,
                                                        "removed": 0
                                                    }, function (data) {
                                                        if (data.success) {
                                                             var userCharityPlanId=data.record._id;
                                                            CharityPlanDetails.dbCollection.find({
                                                               charity_plan_id: planId
                                                            }, {}, function (e, data) {
                                                                if (data && data.length > 0) {
                                                                     data.prepEach(function (r, next)
                                                                    {
                                                                        CharityPlanDetails.write({
                                                                        "_id": me.Util.getObjectId(),
                                                                        "charity_plan_id": userCharityPlanId,
                                                                        //"users_id": params.user_id,
                                                                        "pgd_catalogue_id":r.pgd_catalogue_id,                                                                        
                                                                        "charity_foundation_id": r.charity_foundation_id,
                                                                        "percentage": parseFloat(r.percentage),
                                                                        "removed": 0
                                                                        }, function (data) {
                                                                        if (data.success) {
                                                                            next(r);
                                                                               } else {
                                                                            return me.addErrorObj(data.error);
                                                                        }
                                                                          }, {
                                                                            add: 1,
                                                                            modify: 1
                                                                        });
                                                                    },function () {
                                                                        Users.dbCollection.update({
                                                                        _id: me.user._id
                                                                        }, {
                                                                                $set: {
                                                                                    follow: me.req.body.reqData.partner_id,
                                                                                }
                                                                        }, function (e, d) {me.sendData([]);});
                                                                    });
                                                                }
                                                                else {
                                                                    return me.addErrorObj('ERR043');
                                                                }
                                                            });
                                                        } else {
                                                            return me.addErrorObj(data.error);
                                                        }
                                                    }, {
                                                            add: 1,
                                                            modify: 1
                                                        });

                                                }
                                                else {
                                                    return me.addErrorObj('ERR043');
                                                }
                                });
                            }
                        });
                    }
                    else {
                        return me.addErrorAndSend('ERR039');
                    }
                });
            }
            else {
                return me.addErrorAndSend('ERR040');
            }
        });
    }
});