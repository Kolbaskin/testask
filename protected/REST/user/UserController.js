Ext.define('Crm.REST.user.UserController', {
    extend: "Crm.REST.RestBaseController"
         
    ,create: function() {
              
        var me = this, params = me.req.body.reqData;
        if (!me.validateRequest([{
                type: 'STRING',
                field: "tel",
                isRequired: true
            }])){
            return me.sendError();
        }
        Ext.create('Crm.modules.accountHolders.model.AccountHoldersModel', {
            scope: this,
            getPermissions: function(callback, mn, data) {
                callback({read: false, add: true, modify: false, del: false})
            }
        })
        .write({
            username: params.tel,
            sendpassword: true
        }, function(res) {
            if(res) {
                if(res.success)
                    me.sendData(res.record) 
                else
                    me.addErrorObj(res.error)
            } else
                me.error(500)            
        })
    }
    , generateOtp: function(){
        var me = this, params = me.req.body.reqData;
        if (!me.validateRequest([{
                type: 'STRING',
                field: "tel",
                isRequired: true
            }])){
            return me.sendError();
        }
        [
            function (next) {

            me.basicAuth('', false, next);
        },
        function(err,client) {        
            if(err){
                return me.addErrorObj(err);
            }
            Ext.create('Crm.modules.accountHolders.model.AccountHoldersModel', {
                scope: me,
                getPermissions: function (callback, mn, data) {
                    callback({read: false, add: true, modify: false, del: false})
                }
            }).generateOtp({
                username: params.tel,
                sendpassword: true
            }, function (res) {
                if (res) {
                    if (res.success)
                        me.sendData(res)
                    else
                        me.addErrorObj(res)
                } else
                    me.error(500)
            })
        }].runEach();
    }
    
    ,resetPassword: function() {
        var me = this, params = me.req.body.reqData;
        
        
        Ext.create('Crm.modules.accountHolders.model.AccountHoldersModel', {
            scope: me
        })
        .resetPassword({
            username: params.tel || params.username
        }, function(res) {

            if(res) {
                if(res.success)
                    me.sendData(res) 
                else
                    me.addErrorObj(res.error)
            } else
                me.error(500)            
        })
    }

    ,getUserProfile: function(params, cb) {
        var me=this;
        if(!me.user) {
            me.addErrorAndSend(401);
            return;
        }
        var   model=Ext.create('Crm.modules.organizations.model.FoundModel', {scope: this});
        model.getData({filters: [{property: '_id', value: me.user.pid}]}, function (res) {
            if (res && res.list && res.list[0]) {
                me.user.organization = res.list[0];
            }
            me.sendData(me.user);
        })
    }
    
    ,checkIssetPhone: function() {
        var me = this, params = me.req.body.reqData;

        Ext.create('Crm.modules.accountHolders.model.AccountHoldersModel', {scope: this})
        .getData({_filters:[{_property: 'username', _value: params.username}]}, function(res) {
            me.sendData({isset: !!(res && res.list && res.list.length)});           
        })
    }


    
})
