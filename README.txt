КАК ПОЛЬЗОВАТЬСЯ API

Для проверки АПИ лучше всего использовать POSTMAN'ом

LOGIN API

REQUEST

Url: 
http://www.makeshow.ru:8807/rest/user/login

Headers:
Content-Type: application/json
Authorization: Basic SWE3TzFiZDlvMjBKYUpjMTppTXJ6YVRhcktlRkFRczdRTEFsOEJUMTZqTG94NVQ=

Body:

{
    "header":{
        "version":"0.1",
        "txName":"login",
        "lang":"EN"
    },
    "reqData":{        
        "username":"1111111111",
        "password":"111111"        
    }
}

RESPONSE

{
    "header": {
        "version": "0.1",
        "txName": "login",
        "lang": "EN",
        "androidVersion": 1,
        "iosVersion": "1.0"
    },
    "result": {
        "status": true,
        "message": "Transaction completed successfully"
    },
    "responseData": {
        "token_type": "bearer",
        "access_token": "942861d23c73796a5914ca1e3043f15d61737d30",
        "expires_in": 2592000,
        "refresh_token": "2e15d0e9bf260735cb4daaf6bc32f5dda7ec024b",
        "username": "1111111111",
        "full_name": "Tester"
    },
    "errors": null
}

ПРИМЕР ЗАПРОСА ПОСЛЕ АВТОРИЗАЦИИ (получения профиля авторизованного пользователя)

REQUEST

Url: 
http://www.makeshow.ru:8807/rest/user/profile

Headers:
Content-Type: application/json
Authorization: Bearer 942861d23c73796a5914ca1e3043f15d61737d30  (токен берем из access_token)

Body:
{
    "header":{
        "version":"0.1",
        "txName":"profile",
        "lang":"EN"
    },
    "reqData":{}
}

RESPONSE
{
    "header": {
        "version": "0.1",
        "txName": "profile",
        "lang": "EN",
        "androidVersion": 1,
        "iosVersion": "1.0"
    },
    "result": {
        "status": true,
        "message": "Transaction completed successfully"
    },
    "responseData": {
        <PROFILE DATA>
    },
    "errors": null
}


------------------------------------------------------------
АДМИНИСТРИРОВАНИЕ

UI находится тут:
http://www.makeshow.ru:8807/admin/
login: yeti
password: 222222

Сразу после логина откроется список пользователей.

В меню "Клиентские приложения" список аккаунтов для Basic авторизации, Client ID и Client Secret и Token заполняются автоматически (потенциально, для разных приложений можно настроить доступы к разным API).   

-------------------------------------------------------------

ФАЙЛЫ

настройки роутинга:
conf/auth-routes.json

обработка запросов API:
protected/REST/auth/user.json

модуль управления списком пользователей в UI:
static/admin/modules/endUsers
