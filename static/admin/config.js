admin_src = "../admin_src/"
__CONFIG__ = {
    
    FirstRedirect: "Crm.modules.endUsers.view.EndUsersGrid",
    
    LoginUrl: "/Crm.Admin.login/",
    
    MainToolbar: "main.MainToolbar",
    
    defaultRequires: ['locale.ru_ext', 'Ext.form.field.Display'],
    
    //LogoString: "",
    LogoText: D.t('Administration'),
    
    NavigationTree: [{
            text: 'Administration',
            iconCls: 'x-fa fa-wrench',
            children: [{
                text: 'Roles',
                iconCls: 'x-fa fa-users',
                view: 'Crm.modules.users.view.GroupsGrid'
            },{
                text: 'Administrators',
                iconCls: 'x-fa fa-users',
                view: 'Crm.modules.users.view.UsersGrid'
            }]
        },{
            text: 'End Users',
            iconCls: 'x-fa fa-users',
            view: 'Crm.modules.endUsers.view.EndUsersGrid'
        },{
            text: 'Client Apps',
            view: 'Crm.modules.clients.view.ClientsGrid',
            iconCls: 'x-fa fa-cube'
        }

    ],
    
    Paths: {
        "Ext.PagingToolbar": admin_src + "ext/classic/classic/src/toolbar/Paging.js",
        "Ext.grid.RowNumberer": admin_src + "ext/classic/classic/src/grid/column/RowNumberer.js",
        "Ext.chart": admin_src + "ext/packages/charts/src/chart",
        "Ext.chart.LegendBase":admin_src + "ext/packages/charts/classic/src/chart/LegendBase.js",
        "Ext.chart.TipSurface":admin_src + "ext/packages/charts/classic/src/chart/TipSurface.js",
        "Ext.chart.interactions.ItemInfo":admin_src + "ext/packages/charts/classic/src/chart/interactions/ItemInfo.js",
        "Ext.chart.overrides":admin_src + "ext/packages/charts/classic/overrides",  
        "Ext.draw":admin_src + "ext/packages/charts/src/draw",
        "Ext.draw.ContainerBase":admin_src + "ext/packages/charts/classic/src/draw/ContainerBase.js",
        "Ext.draw.SurfaceBase":admin_src + "ext/packages/charts/classic/src/draw/SurfaceBase.js",
        "Ext.draw.engine.SvgContext.Gradient":admin_src + "ext/packages/charts/src/draw/engine/SvgContext.js",    
        "Ext.grid.feature": admin_src + "ext/classic/classic/src/grid/feature",
        "Ext": admin_src + "ext/classic/classic/src",
        "Ext.ux.ImageCrop": "ext/src/ux/ImageCrop.js",
        "Core": "core",
        "Desktop": "",
        "Ext.data": admin_src + "ext/packages/core/src/data"
    }
    
}

