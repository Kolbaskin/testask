Ext.define('Crm.modules.endUsers.view.EndUsersForm', {
    extend: 'Core.form.FormContainer'
    
    , titleTpl: D.t('End User: {username}')
    , iconCls: 'x-fa fa-wheelchair'
    , formMargin: '0'
    
    , requires: [
        'Ext.form.field.Number',
        'Ext.form.field.Tag',
        'Core.form.DependedCombo',
        'Ext.form.FieldSet'
    ]
    
    
//, controllerCls: 'Crm.modules.organizations.view.FoundFormController'
    
    , buildAllItems: function () {
        return {
            xtype: 'tabpanel',
            layout: 'fit',            
            items: [
                this.buildProfilePanel()
            ]
        }
    }

    
    , buildProfilePanel: function () {
        return {
            xtype: 'panel',
            title: D.t('Profile'),
            layout: 'hbox',
            padding: 5,
            items: [{
                flex: 1,
                layout: 'anchor',
                defaults: {
                    xtype: 'textfield',
                    anchor: '100%'
                },
                items: this.buildFormItems()
            }]
        }
    }
        
    ,buildFormItems: function() {
        return [{
                name: '_id',
                hidden: true
            },{
                name: 'username',
                fieldLabel: D.t('User name'),
                allowBlank: false  
            },{
                name: 'full_name',
                fieldLabel: D.t('Full name')
            },{
                name: 'password',
                inputType: 'password',
                fieldLabel: D.t('Password')
            }
        ]
    }
    
    
    
})