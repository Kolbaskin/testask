Ext.define('Crm.modules.endUsers.view.EndUsersGrid', {
    extend: 'Core.grid.GridContainer',

    title: D.t('End Users'),
    iconCls: 'x-fa fa-users',
   // controllerCls: 'Crm.modules.endUsers.view.EndUsersGridController',
    
    buildColumns: function() {
        return [{
            text: D.t("User name"),
            flex: 1,
            sortable: true,
            dataIndex: 'username'
        },{
            text: D.t("Full name"),
            flex: 1,
            sortable: true,
            dataIndex: 'full_name'
        }]        
    }

    
})
