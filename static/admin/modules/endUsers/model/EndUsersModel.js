//@scope Server, Client
Ext.define('Crm.modules.endUsers.model.EndUsersModel', {    
    extend: "Core.data.DataModel"

    ,mixins: ['Crm.sms.SmsController'] // scope:server
    ,collection: 'users' // scope:server

    ,fields: [
    {
        name: '_id',
        type: 'ObjectID',
        visible: true,
        editable:true
    },{
        name: 'username',
        type: 'string',
        filterable: true,
        editable: true,
        visible: true
    },{
        name: 'password',
        type: 'password',
        filterable: true,
        editable: true,
        visible: true
    },{
        name: 'full_name',
        type: 'string',
        filterable: false,
        editable: true,
        visible: true
    },{
        name: 'email',
        type: 'string',
        filterable: false,
        vtype: 'email',
        editable: true,
        visible: true
    },{
        name: 'zip',
        type: 'string',
        filterable: false,
        editable: true,
        visible: true
    }]

    
})