/**
 * @author Max Tushev
 * @scope Server, Client
 * The model for Users module
 * @private
 */
Ext.define('Crm.modules.clients.model.ClientsModel', {
    extend: "Core.data.DataModel"

    , collection: 'oauth_clients'

    , fields: [
        {
            name: '_id',
            type: 'ObjectID',
            visible: true
        },
        {
            name: 'app_name',
            type: 'string',
            filterable: true,
            unique: true,
            editable: true,
            visible: true
        },{
            name: 'client_id',
            type: 'string',
            filterable: true,
            unique: true,
            editable: true,
            visible: true
        }, {
            name: 'client_secret',
            type: 'string',
            filterable: true,
            editable: true,
            visible: true
        }, {
            name: 'redirect_uri',
            type: 'string',
            filterable: true,
            editable: true,
            visible: true
        },
        {
            name: 'email',
            type: 'string',
            filterable: true,
            vtype: 'email',
            editable: true,
            visible: true
        },
        {
            name: 'ip',
            type: 'string',
            filterable: true,
            editable: true,
            visible: true
        }, {
            name: 'act1',
            type: 'boolean',
            filterable: true,
            editable: true,
            visible: true
        }]

    /* scope:server */
    , beforeSave: function (data, cb) {
        this.generateUniqueClientCredentials(data, cb);
    }
    , generateUniqueClientCredentials: function (data, cb, generateNew) {
        var me = this;
        if (!data.client_id || generateNew){
            var letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'x',
                'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'I', 'O', 'L', 'M',
                'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'Y', 'U', 'X'];
            var clientId = "";
            for (var i = 0; i < 16; i++) {
                clientId += letters[Math.ceil(Math.random() * letters.length) - 1];
            }
            var clientSecret = "";
            for (var i = 0; i < 30; i++) {
                clientSecret += letters[Math.ceil(Math.random() * letters.length) - 1];
            }
            data.client_id=clientId;
            data.client_secret=clientSecret
        }
        return me.src.db.collection(me.collection).findOne({$and: [{client_id: data.client_id}]}, {client_id: 1,_id:1}, function (e, d) {
            if (d && d._id.toString() != data._id.toString()) {
                return me.generateUniqueClientCredentials(data, cb, true);
            } else {
                return cb(data);
            }
        });
    }
});