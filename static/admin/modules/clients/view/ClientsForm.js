Ext.define('Crm.modules.clients.view.ClientsForm', {
    extend: 'Core.form.FormWindow'
    , titleTpl: 'Client App: {client_id}'
    , requires: [
      //  'Ext.form.field.Display'
    ]
    
    , controllerCls: 'Crm.modules.clients.view.ClientsFormController'
    
    , buildItems: function () {
        return [{
            name: 'app_name',
            fieldLabel: D.t('App Name')
        },{
            name: 'redirect_uri',
            fieldLabel: D.t('Redirect URL')
        }, {
            name: 'email',
            fieldLabel: D.t('Email')
        }, {
            name: 'ip',
            fieldLabel: D.t('Valid IP')
        },{
            name: 'act1',
            xtype: 'checkbox',
            fieldLabel: D.t('Active')
        },{
            name: 'client_id',
            readOnly: true,
            fieldLabel: D.t('Client ID')
        },{
            name: 'client_secret',
            readOnly: true,
            fieldLabel: D.t('Client Secret')
        },{
            name: 'token',
            readOnly: true,
            fieldLabel: D.t('Token (base64)')
        }]
    }

})