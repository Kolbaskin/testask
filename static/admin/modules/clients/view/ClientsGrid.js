
Ext.define('Crm.modules.clients.view.ClientsGrid', {
    extend: 'Core.grid.GridContainer',

    title: D.t('Client Apps'),
    iconCls: 'x-fa fa-cube',
    
    requires: [],
    
    buildColumns: function() {
        return [{
            text: D.t("App Name"),
            flex: 1,
            sortable: true,
            dataIndex: 'app_name'
        },{
            text: D.t("Email"),
            flex: 1,
            sortable: true,
            dataIndex: 'email'
        },{
            text: D.t("Client ID"),
            flex: 1,
            sortable: true,
            dataIndex: 'client_id'
        },{
            text: D.t("Client Secret"),
            flex: 1,
            sortable: true,
            dataIndex: 'client_secret'
        },{
            text: D.t("Redirect URL"),
            flex: 1,
            sortable: true,
            dataIndex: 'redirect_uri'
        },{
            text: D.t("Status"),
            flex: 1,
            sortable: true,
            dataIndex: 'act1',
            renderer: function(v,m) {
                return v?'Active':'InActive'
            }
        }]
    }
    
})
