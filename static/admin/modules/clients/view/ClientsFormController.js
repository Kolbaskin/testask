Ext.define('Crm.modules.clients.view.ClientsFormController', {
    extend: 'Core.form.FormController'
    
    ,setControls: function() {
        
        var me = this;
        
        me.control({
            '[name=client_id]':{change: function() {me.onSecretChange()}},
            '[name=client_secret]':{change: function() {me.onSecretChange()}}
        })
        
        
        this.callParent(arguments)
    }
    
    ,onSecretChange: function() {
        var client_id = this.view.down('[name=client_id]').getValue(),
            client_secret = this.view.down('[name=client_secret]').getValue();
        if(client_id && client_secret) {
            this.view.down('[name=token]').setValue(btoa(client_id + ':' + client_secret))
        }
    }
})