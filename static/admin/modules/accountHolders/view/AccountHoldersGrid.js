
Ext.define('Crm.modules.accountHolders.view.AccountHoldersGrid', {
    extend: 'Core.grid.GridContainer',

    title: D.t('Account Holders'),
    iconCls: 'x-fa fa-users',

    
    buildColumns: function() {
        return [{
            text: D.t("User name"),
            flex: 1,
            sortable: true,
            dataIndex: 'username'
        },{
            text: D.t("Full name"),
            flex: 1,
            sortable: true,
            dataIndex: 'full_name'
        },{
            text: D.t("Email"),
            flex: 1,
            sortable: true,
            dataIndex: 'email'
        },{
            text: D.t("Primary Contact"),
            flex: 1,
            sortable: true,
            dataIndex: 'primary_contact',
            hidden: true            
        },{
            text: D.t("Pan"),
            flex: 1,
            sortable: true,
            dataIndex: 'pan',
            hidden: true            
        },{
            text: D.t("Address"),
            flex: 1,
            sortable: true,
            dataIndex: 'address'
        },{
            text: D.t("User Type"),
            flex: 1,
            sortable: true,
            dataIndex: 'user_type',
            renderer: function(v,m) {
                if(v == 'C')
                    return 'Charity';
                else if(v == 'P')
                    return 'Contributors'
                else 
                    return 'Brand Partner'
            }
        },{
            text: D.t("Know your customer"),
            flex: 1,
            sortable: true,
            dataIndex: 'kyc',
            hidden: true            
        }]        
    }
    
})
