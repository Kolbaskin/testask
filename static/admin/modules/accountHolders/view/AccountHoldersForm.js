Ext.define('Crm.modules.accountHolders.view.AccountHoldersForm', {
    extend: 'Core.form.FormWindow'
    
    //,titleTpl: 'User: {login}'
    ,titleTpl: D.t('Account Holders') + ': {username}'
    
    ,parentClass: 'Crm.modules.organizations.view.FoundForm'
        
    ,buildItems: function() {
        var orgid='';
        var me = this;
        var orgval = '';
        return [
        {
            name: 'username',
            fieldLabel: D.t('User name')+' *',
            allowBlank: false  
        },{
            name: 'full_name',
            fieldLabel: D.t('Full name')
        },{
            name: 'email',
            fieldLabel: D.t('Email')+' *',
            allowBlank: false,
            vtype: 'email'
            
        },{
            name: 'primary_contact',
            fieldLabel: D.t('Primary Contact'),
            hidden: true            
        },{
            name: 'pan',
            fieldLabel: D.t('Pan'),
            hidden: true            
        },{
            name: 'address',
            fieldLabel: D.t('Address')
        },{
            name: 'kyc',
            fieldLabel: D.t('Know Your Customer'),
            hidden: true            
        },{
            name: 'password',
            inputType: 'password',
            fieldLabel: D.t('Password')
        },{
            name: 'pid',
            hidden: true,
            listeners: {
                change: function(e,v) {
                    //@author: Vaibhav Mali. 20 Aug 2017 Made changes for getting organisation type.
                    if(v)me.closeHash = '#' + me.parentClass + '~' + v;
                    me.controller.model.runOnServer("getparentid",{v},function(data){
                       orgid = data.orgtype;
                       if(orgid == '1'){
                            orgval = 'C';
                       }
                       else if(orgid == '2'){
                           orgval = 'P';
                       }
                       else{
                           orgval = 'B';
                       }
                       var a=Ext.getCmp('user_type');
                       a.setValue(orgval);
                       Ext.getCmp('user_type').getEl().hide();
                    });
                }
            }
        },{
                
            xtype:'textfield',
            fieldLabel: 'User Type',
            name: 'user_type',
            id: 'user_type',
            hidden: true
            
        }]
    }
    
})