Ext.define('Crm.modules.accountHolders.model.EmailMobileVerfications', {    
    extend: "Core.data.DataModel"

    ,collection: 'email_mobile_verification'

    ,fields: [
    {
        name: '_id',
        type: 'ObjectID',
        visible: true
    },
    {
        name: 'mobile',
        type: 'string',
        filterable: true,
        editable: true,
        visible: true
    },
    {
        name: 'type',
        type: 'string',
        filterable: true,
        editable: true,
        visible: true
    },
    {
        name: 'verified',
        type: 'int',
        filterable: true,
        editable: true,
        visible: true
    },
    {
        name: 'verfication_code',
        type: 'string',
        filterable: true,
        editable: true,
        visible: true
    },{
        name: 'email',
        type: 'string',
        filterable: false,
        editable: true,
        visible: true
    },{
        name: 'user_id',
        type: 'ObjectID',
        filterable: false,
        editable: true,
        visible: true
    },{
        name: 'expiry_timestamp',
        type: 'string',
        filterable: false,
        editable: true,
        visible: true
    }]
})