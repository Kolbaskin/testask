//@scope Server, Client
Ext.define('Crm.modules.accountHolders.model.AccountHoldersModel', {    
    extend: "Core.data.DataModel"

    ,mixins: ['Crm.sms.SmsController'] // scope:server
    ,collection: 'users' // scope:server

    ,fields: [
    {
        name: '_id',
        type: 'ObjectID',
        visible: true,
        editable:true
    },{
        name: 'pid',
        type: 'ObjectID',
        visible: true,
        filterable: true,
        editable:true
    },
    {
        name: 'username',
        type: 'string',
        filterable: true,
        editable: true,
        visible: true
    },{
        name: 'user_id',
        type: 'string',
        filterable: true,
        editable: true,
        visible: true
    },
    {
        name: 'password',
        type: 'password',
        filterable: true,
        editable: true,
        visible: true
    },
    {
        name: 'security_question',
        type: 'string',
        filterable: true,
        editable: true,
        visible: true
    },
    {
        name: 'security_answer',
        type: 'pass',
        filterable: true,
        editable: true,
        visible: true
    },
    {
        name: 'full_name',
        type: 'string',
        filterable: false,
        editable: true,
        visible: true
    },{
        name: 'email',
        type: 'string',
        filterable: false,
        vtype: 'email',
        editable: true,
        visible: true
    },{
        name: 'mobile',
        type: 'string',
        filterable: false,
        editable: true,
        visible: true
    },{
        name: 'pan',
        type: 'string',
        filterable: false,
        editable: true,
        visible: true
    },{
        name: 'address',
        type: 'string',
        filterable: false,
        editable: true,
        visible: true
    },{
        name: 'country',
        type: 'string',
        filterable: false,
        editable: true,
        visible: true
    },{
        name: 'city',
        type: 'string',
        filterable: false,
        editable: true,
        visible: true
    },{
        name: 'state',
        type: 'string',
        filterable: false,
        editable: true,
        visible: true
    },{
        name: 'zip',
        type: 'string',
        filterable: false,
        editable: true,
        visible: true
    },{
        //E: End-User, C: Charity User, P: Partner User
        name: 'user_type',
        type: 'string',
        filterable: false,
        editable: true,
        visible: true
    },{
        name: 'kyc',
        type: 'string',
        filterable: false,
        editable: true,
        visible: true
    },{
        name: 'follow',
        type: 'ObjectID',
        visible: true,
        filterable: true,
        editable:true
    },{
        name: 'wallet_balance',
        type: 'float',
        filterable: false,
        editable: true,
        visible: true
    },{
        name: 'full_balance',
        type: 'float',
        filterable: false,
        editable: true,
        visible: true
    },{
        name: 'sms_cnt',
        type: 'int',
        filterable: false,
        editable: true,
        visible: true
    }]

    /* scope:server */
    , $checkUnicLogin: function (data, cb) {
        var me = this, find;
        if (data.username) {
            find = {username: data.username};
            if(data._id) 
                find._id = {$ne: me.src.db.fieldTypes.ObjectID.getValueToSave(me, data._id)};
            
            me.dbCollection.findOne(find, {_id: 1}, function (e, d) {
                cb({isset: (!!d && d._id)})
            })
        }else{
            cb();
        }
    }
 /*
  @author : Vaibhav Mali 
  @date : 20 Aug 2017
  @function : $getparentid
  @desc : To get parent id.
  */

    /*scope:server*/
    , $getparentid: function (data,cb){
        var me=this;
        var value = data.v;
        var sql = "select orgtype from pgd_foundations where _id =$1";
        me.src.db.query(sql,[value],function (e, data) {
            cb(data&&data.length?data[0]:null);
        });
    }  
    
    /*scope:server*/
    ,generateOtp:function(data,cb){
        var me=this;
        var password = me.createNewPassword();

        [
            function(next) {
                me.dbCollection.findOne({
                    username: data.username
                },{_id:1,username:1},function(e,d) {
                    next(d)
                })
            },
            function(user, next) {

                if(!user){user={username:data.username};}
                user.password = password;//me.src.db.fieldTypes.password.getValueToSave(me, password);
                user.user_type="U"; 
                if(!user.sms_cnt) user.sms_cnt = 0;
                user.sms_cnt++;
                if(user.sms_cnt>me.config.smsLimit) {
                    cb({success:false, error: {code: 104, message: 'exceeded the limit on the number of sms'}})
                    return;
                }
                me.write(user,function(res){
                    if(res) {
                        if(res.success)
                            next();
                        else
                            cb({success:false, error: {code: 103, message: 'DB Error'}})
                    } else
                        me.error(500)
                },{
                    add:true,
                    modify:true
                });

            },
            function(next) {
                
                console.log('generated OTP:', password)
                
                me.sendSMS(data.username, 'generateOtp', {
                    login: data.username,
                    password: password
                }, function(err, res) {
                    cb({success: !!res})
                })
            }
        ].runEach(me)
    }
    
    /* scope:server */
    , beforeSave: function(data, cb) {
        var me=this;
        [
            function(next) {             
                me.$checkUnicLogin(data, function(res) {
                    if(!res){
                        cb({success:false, error: {code: 102, message: 'User empty'}})
                    }
                    else if(res && res.isset)
                        cb({success:false, error: {code: 101, message: 'Duplicate user'}})
                    else
                        next()
                })
            },
            function(next) {
                if(data.sendpassword) {
                    data.password = me.createNewPassword();
                    me.generatedPassword = data.password;
                } 
                next()
            },
            function() {            
                cb(data)
            }
        ].runEach(me);
    }
    
    /* scope:server */
    ,afterSave: function(data, cb) {
        if(this.generatedPassword && /^[0-9]{10}$/.test(data.username)) {
            this.sendPassword(data, this.generatedPassword, function() {
                cb(data)
            })
        } else {
            cb(data)
        }
    }
    /* scope:server */
    ,createNewPassword: function() {
        return require('password-generator')(4, false, /\d/);
    }
    /* scope:server */
    ,sendPassword: function(data, password, cb) {
        this.sendSMS(data.username, 'newUserPassword', {
            login: data.username,
            password: password
        }, cb)        
    }
    /* scope:server */
    ,resetPassword: function(data, cb) {  

        var password = this.createNewPassword();
        [
        
            function(next) {        
                this.dbCollection.findOne({
                    $or: [{username: data.username},{mobile: data.username}]
                    
                },{_id:1, sms_cnt:1},function(e,d) {
                    next(d)
                })                
            },
            function(user, next) {
            
            
                if(user){
                    
                    if(user.sms_cnt>this.config.smsLimit) {
                        cb({success:false, error: {code: 104, message: 'exceeded the limit on the number of sms'}})
                        return;
                    }
                    if(!user.sms_cnt) user.sms_cnt = 0;
                    user.sms_cnt++;
                    
                    this.dbCollection.update({
                        _id: user._id
                    },{$set: {
                        sms_cnt: user.sms_cnt,
                        password: this.src.db.fieldTypes.password.getValueToSave(this, password)
                    }}, function(e,d) {
                        if(d)
                            next()
                        else
                            cb({success:false, error: {code: 103, message: 'DB Error'}})
                    })
                } else
                    cb({success:false, error: {code: 102, message: 'User not found'}})
            },
            function(next) {        
                this.sendSMS(data.username, 'resetPassword', {
                    login: data.username,
                    password: password
                }, function(err, res) {
                        cb({success: !!res})
                }) 
            }
        ].runEach(this)            
    }
})
