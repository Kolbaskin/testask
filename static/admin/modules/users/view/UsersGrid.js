
Ext.define('Crm.modules.users.view.UsersGrid', {
    extend: 'Core.grid.GridContainer',

    title: D.t('Admins'),
    iconCls: 'x-fa fa-users',
    
    requires: [
        'Core.grid.ComboColumn'    
    ],
    
    buildColumns: function() {
        return [{
            text: D.t("Login"),
            flex: 1,
            sortable: true,
            dataIndex: 'login'
        },{
            xtype: 'combocolumn',
            text: D.t("Group"),
            flex: 1,
            model: 'Crm.modules.users.model.GroupsModel',                  
            dataIndex: 'groupid'
        },{
            text: D.t("name"),
            flex: 1,
            sortable: true,
            dataIndex: 'name'
        },{
            text: D.t("Email"),
            flex: 1,
            sortable: true,
            dataIndex: 'email'
        },{
            text: D.t("Phone"),
            flex: 1,
            sortable: true,
            dataIndex: 'tel'
        }/*,{
            text: D.t("Session password"),
            flex: 1,
            sortable: true,
            dataIndex: 'dblauth',
            renderer: function(v,m) {
                return D.t(v? 'on':'off')    
            }
        }*/]        
    }
    
})
