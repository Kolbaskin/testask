
Ext.define('Crm.modules.users.view.GroupsGrid', {
    extend: 'Core.grid.GridContainer',

    title: D.t('Groups'),
    iconCls: 'x-fa fa-users',    
    //filterable: true,
    
    buildColumns: function() {
        return [{
            text: D.t("Group name"),
            flex: 1,
            sortable: true,
            dataIndex: 'name'
        },{
            text: D.t("Code"),
            flex: 1,
            sortable: true,
            dataIndex: 'code'
        },{
            text: D.t("Description"),
            flex: 1,
            sortable: true,
            dataIndex: 'description'
        }];
    }
})
