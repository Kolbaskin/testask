Ext.define('Crm.modules.users.view.UsersForm', {
    extend: 'Core.form.FormWindow'
    
    ,titleTpl: 'User: {login}'
    
    ,requires: [
        'Ext.ux.form.ItemSelector',
        'Ext.form.field.Tag'
    ]
        
    ,buildItems: function() {
        return [{
            name: 'login',
            fieldLabel: D.t('Login')
        },{
            name: 'name',
            fieldLabel: D.t('User name')
        },{
            name: 'tel',
            fieldLabel: D.t('Phone')
        },{
            name: 'email',
            fieldLabel: D.t('Email')
        },{
            name: 'ip',
            fieldLabel: D.t('Valid IP')
        },{
            name: 'pass',
            inputType: 'password',
            fieldLabel: D.t('Password')
        },
        this.buildManagerCombo(),    
        this.buildGroupCombo(),
        this.buildXGroups()]    
    }
    ,buildManagerCombo: function() {
        var me = this;
        return {
            xtype: 'combo',
            name: 'manager',
            fieldLabel: D.t('Manager'),
            valueField: '_id',
            displayField: 'name',
            queryMode: 'local',
            
            store: Ext.create('Core.data.ComboStore', {
                dataModel: Ext.create('Crm.modules.users.model.UsersModel'),
                fieldSet: ['_id', 'name'],
                scope: me
            })
        }
    }
    ,buildGroupCombo: function() {
        var me = this;
        return {
            xtype: 'combo',
            name: 'groupid',
            fieldLabel: D.t('Main group'),
            valueField: '_id',
            displayField: 'name',
            //queryMode: 'remote',
            //minChars: 2,
            //triggerAction : 'query',
            //typeAhead: true,
            //queryParam: 'query',            
            store: Ext.create('Core.data.ComboStore', {
                dataModel: Ext.create('Crm.modules.users.model.GroupsModel'),
                fieldSet: '_id,name',                
                scope: me
            })
        }
    }
    
    
    ,buildXGroups: function() {
        return {
            xtype: 'tagfield',
            fieldLabel: D.t('Extended groups'),
            store: Ext.create('Core.data.Store', {
                dataModel: 'Crm.modules.users.model.GroupsModel',
                fieldSet: '_id,name'    
            }),
            displayField: 'name',
            valueField: '_id',
            queryMode: 'local',
            name: 'xgroups',
            filterPickList: true
        }
    }
    
})